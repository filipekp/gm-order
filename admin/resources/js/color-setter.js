(function ($) {
  /**
   * Nastavuje barvu na prvek
   * 
   * @param {selectro} pattern // selektor pro nejbližší nadřazený prvek, ze kterého chci brát barvu
   */
  $.fn.colorSetter = function(pattern) {
    var $elements = this;

    // definice hlavnich barev systému
    var colors = ['blue', 'red', 'yellow', 'green', 'purple', 'grey'];
    
    $elements.each(function() {
      var $element = $(this);
      var $pattern = ((typeof pattern === 'string') ? $element.closest(pattern) : pattern);
      var className = null;
      
      $.each(colors, function(key, value) {
        var regex = new RegExp('(' + value + '.*?)(?:\s+|$)');
        var findClassName = regex.exec($pattern.attr('class'));
        className = ((findClassName) ? findClassName[1] : null);

        return ((findClassName) ? false : true);
      });

      $element.addClass(className);
    });
  };
}(jQuery));