(function ($) {
  /**
   * Umozni radit pomoci vice sloupcu
   * 
   * @param {selectro} pattern // selektor pro nejbližší nadřazený prvek, ze kterého chci brát barvu
   */
  $.fn.shiftSelectable = function(settings) {
    var defaultSettings = {
      name: function($item) {
        return $item.data('name');
      },
      getValue: function($item) {
        return $item.data('value');
      },
      setValue: function($item, value) {
        return $item.attr('data-value', value);
      }
    };
    
    var newSettings = $.extend({}, defaultSettings, settings);
    
    
    var changes = [];
    var $items = $(this);
    var prefix = 'sortable_column_';
    var sendQuery = false;
    var $form = null;
    
    $items.each(function(key, item) {
      var $item = $(this);
      if (!$item.attr('id')) { $item.attr('id', prefix + key); }
      changes[$item.attr('id')] = 0;
      
      // nastavi form do promenne
      if (!$form) {
        if ($item.closest('form').length) {
          $form = $item.closest('form');
        } else {
          var $wrapperForm = $item.closest('table').parent();
          var $newForm = $('<form />').append(
            $('<input />').attr('type', 'hidden')
                          .attr('name', 'module')
                          .val($.url().param('module'))
          );
          if ($.url().param('activity')) {
            $newForm.append(
              $('<input />').attr('type', 'hidden')
                            .attr('name', 'activity')
                            .val($.url().param('activity')));
          }
                  
          $newForm.appendTo($wrapperForm);
          
          $form = $newForm;
        }
      }
      
      $item.click(function(e) {
        if (e.shiftKey) {
          e.preventDefault();
          
          var $link = $(this);
          var $parent = $link.parent();
          var newValue = null;
          sendQuery = true;
          
          if (changes[$link.attr('id')] < 2) {
            if (changes[$link.attr('id')] === 0 && !$parent.hasClass('sorting')) {
              changes[$link.attr('id')] = 1;
            }
            
            if ($parent.hasClass('sorting_asc')) {
              $parent.removeClass('sorting_asc').addClass('sorting_desc');
              newValue = 'desc';
            } else if ($parent.hasClass('sorting_desc')) {
              $parent.removeClass('sorting_desc').addClass('sorting_asc');
              newValue = 'asc';
            } else {
              $parent.removeClass('sorting').addClass('sorting_asc');
              newValue = 'asc';
            }
            
            changes[$link.attr('id')]++;
          } else {
            $parent.removeClass('sorting_asc').removeClass('sorting_desc').addClass('sorting');
            changes[$link.attr('id')] = 0;
          }
          
          // pridani/odebrani hidden inputu do formulare
          var $hiddenInput = $('input[name="' + newSettings.name.call(this, $parent) + '"]');
          if (newValue) {
            if ($hiddenInput.length) {
              $hiddenInput.val(newValue);
            } else {
              $form.closest('form').append(
                $('<input />').attr('type', 'hidden')
                              .attr('name', newSettings.name.call(this, $parent))
                              .val(newValue)
              );
            }
          } else {
            $hiddenInput.remove();
          }

          newSettings.setValue.call(this, $parent, newValue);
        }
      });
    });
    
    $('body').on('keyup', function(e) {
      if (e.which === 16 && sendQuery) {
        sendQuery = false;
        $form.trigger('submit');
      }
    });
  };
}(jQuery));