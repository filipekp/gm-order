var FormComponents = function () {
    var handleWysihtml5 = function () {
        if (!jQuery().wysihtml5) {
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["assets/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }
    }

    var resetWysihtml5 = function () {
        if (!jQuery().wysihtml5) {
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["assets/plugins/bootstrap-wysihtml5/wysiwyg-color.css"]
            });
        }
    }

    var handleToggleButtons = function () {
        if (!jQuery().toggleButtons) {
          return;
        }
        
        $('.basic-toggle-button').toggleButtons({
          label: { enabled: __TRANSLATIONS.text.enabled.short, disabled: __TRANSLATIONS.text.disabled.short }
        });
        
        $('.yesno-toggle-button').toggleButtons({
          label: { enabled: __TRANSLATIONS.text.yes, disabled: __TRANSLATIONS.text.no },
          style: { enabled: "success", disabled: "danger" }
        });
        
        $('.text-toggle-button').toggleButtons({
          width: 200,
          label: { enabled: "Lorem Ipsum", disabled: "Dolor Sit" }
        });
        
        $('.danger-toggle-button').toggleButtons({
          // Accepted values ["primary", "danger", "info", "success", "warning"] or nothing
          style: { enabled: "danger", disabled: "info" }
        });
        
        $('.info-toggle-button').toggleButtons({
          style: { enabled: "info", disabled: "" }
        });
        
        $('.success-toggle-button').toggleButtons({
          style: { enabled: "success", disabled: "info" }
        });
        
        $('.warning-toggle-button').toggleButtons({
          style: { enabled: "warning", disabled: "info" }
        });

        $('.height-toggle-button').toggleButtons({
          height: 100,
          font: { 'line-height': '100px', 'font-size': '20px', 'font-style': 'italic' }
        });
        
        $('.custom-toggle-button').each(function() {
          var $button = $(this);
          var options = {
            label: { enabled: __TRANSLATIONS.text.enabled.short, disabled: __TRANSLATIONS.text.disabled.short },
            style: { enabled: "", disabled: "" }
          };
          
          if (typeof $button.attr('data-text-enabled') !== 'undefined' && $button.data('text-enabled')) { options.label.enabled = $button.data('text-enabled'); }
          if (typeof $button.attr('data-text-disabled') !== 'undefined' && $button.data('text-disabled')) { options.label.disabled = $button.data('text-disabled'); }
          
          if (typeof $button.attr('data-style-enabled') !== 'undefined' && $button.data('style-enabled')) { options.style.enabled = $button.data('style-enabled'); }
          if (typeof $button.attr('data-style-disabled') !== 'undefined' && $button.data('style-disabled')) { options.style.disabled = $button.data('style-disabled'); }
          
          if (typeof $button.attr('data-width') !== 'undefined' && $button.data('width')) { options.width = $button.data('width'); }
          if (typeof $button.attr('data-height') !== 'undefined' && $button.data('height')) { options.height = $button.data('height'); }

          $button.toggleButtons(options);
        });
    };

    var handleBootstrapSwitch = function () {
      if (!jQuery().bootstrapSwitch) {
        return;
      }

      $('.basic-bootstrap-switch').bootstrapSwitch({
        onText: __TRANSLATIONS.text.enabled.short,
        offText: __TRANSLATIONS.text.disabled.short
,        size: 'small'
      });

      $('.mini-bootstrap-switch').bootstrapSwitch({
        onText: __TRANSLATIONS.text.enabled.short,
        offText: __TRANSLATIONS.text.disabled.short,
        size: 'mini'
      });

      $('.custom-bootstrap-switch').bootstrapSwitch({
        onInit: function(event, state) {
          var $target = $(event.target);
          var $wrapper = $target.closest('.bootstrap-switch-wrapper');

          if (typeof $target.attr('data-width') !== 'undefined' && $target.data('width')) {
            $wrapper.css('width', $target.data('width') + 'px');
          }

          if (typeof $target.attr('data-height') !== 'undefined' && $target.data('height')) {
            $wrapper.css('height', $target.data('height') + 'px');
          }
        }
      });
    };

    var handleTagsInput = function () {
        if (!jQuery().tagsInput) {
            return;
        }
        $('#tags_1').tagsInput({
            width: 'auto',
            'onAddTag': function () {
                //alert(1);
            },
        });
        $('#tags_2').tagsInput({
            width: 240
        });
    }

    var handlejQueryUIDatePickers = function () {
      // vychozi nastaveni pro datepicker
      var $defaultSettings = {
        closeText: __TRANSLATIONS.button.close,
        prevText: __TRANSLATIONS.button.prev,
        nextText: __TRANSLATIONS.button.next,
        currentText: __TRANSLATIONS.button.now,
        monthNames: [
          __TRANSLATIONS.datetime.months.januar.long,
          __TRANSLATIONS.datetime.months.februar.long,
          __TRANSLATIONS.datetime.months.march.long,
          __TRANSLATIONS.datetime.months.april.long,
          __TRANSLATIONS.datetime.months.may.long,
          __TRANSLATIONS.datetime.months.june.long,
          __TRANSLATIONS.datetime.months.july.long,
          __TRANSLATIONS.datetime.months.august.long,
          __TRANSLATIONS.datetime.months.september.long,
          __TRANSLATIONS.datetime.months.october.long,
          __TRANSLATIONS.datetime.months.november.long,
          __TRANSLATIONS.datetime.months.december.long
        ],
        monthNamesShort: [
          __TRANSLATIONS.datetime.months.januar.short,
          __TRANSLATIONS.datetime.months.februar.short,
          __TRANSLATIONS.datetime.months.march.short,
          __TRANSLATIONS.datetime.months.april.short,
          __TRANSLATIONS.datetime.months.may.short,
          __TRANSLATIONS.datetime.months.june.short,
          __TRANSLATIONS.datetime.months.july.short,
          __TRANSLATIONS.datetime.months.august.short,
          __TRANSLATIONS.datetime.months.september.short,
          __TRANSLATIONS.datetime.months.october.short,
          __TRANSLATIONS.datetime.months.november.short,
          __TRANSLATIONS.datetime.months.december.short
        ],
        dayNames: [
          __TRANSLATIONS.datetime.days.sunday.long,
          __TRANSLATIONS.datetime.days.monday.long,
          __TRANSLATIONS.datetime.days.tuesday.long,
          __TRANSLATIONS.datetime.days.wednesday.long,
          __TRANSLATIONS.datetime.days.thursday.long,
          __TRANSLATIONS.datetime.days.friday.long,
          __TRANSLATIONS.datetime.days.saturday.long
        ],
        dayNamesShort: [
          __TRANSLATIONS.datetime.days.sunday.short,
          __TRANSLATIONS.datetime.days.monday.short,
          __TRANSLATIONS.datetime.days.tuesday.short,
          __TRANSLATIONS.datetime.days.wednesday.short,
          __TRANSLATIONS.datetime.days.thursday.short,
          __TRANSLATIONS.datetime.days.friday.short,
          __TRANSLATIONS.datetime.days.saturday.short
        ],
        dayNamesMin: [
          __TRANSLATIONS.datetime.days.sunday.min,
          __TRANSLATIONS.datetime.days.monday.min,
          __TRANSLATIONS.datetime.days.tuesday.min,
          __TRANSLATIONS.datetime.days.wednesday.min,
          __TRANSLATIONS.datetime.days.thursday.min,
          __TRANSLATIONS.datetime.days.friday.min,
          __TRANSLATIONS.datetime.days.saturday.min
        ],
        weekHeader: __TRANSLATIONS.datetime.week_title,
        dateFormat: __TRANSLATIONS.datetime.date_format_short_js,
        timeFormat: __TRANSLATIONS.datetime.time_format_js,
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: '',
        showWeek: false,
        showAnim: "show",
        showButtonPanel: true,
        onClose: function() {          
          $(this).trigger("focus").trigger("blur");
        },
        // add button for clear input with date
        beforeShow: function(input) {          
          setTimeout(function() {
            var $input = $(input);
            var $datepicker = $input.datepicker('widget');
            var $buttonPanel = $datepicker.find('.ui-datepicker-buttonpane');

            $('<button>', {
              'class': 'ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all',
              text: __TRANSLATIONS.button.clear,
              click: function() {
                $.datepicker._clearDate(input);
              }
            }).appendTo($buttonPanel);
            
            // nastavi barevne schema kalendare podle inputu
            $datepicker.colorSetter($input);
            
            // zavola uzivatelsky definovanou funkci before-show
            if ($input.attr('data-before-show')) {
              var func = $input.data('before-show');
              if (func in window) {
                window[func]($input);
              }
            }
          }, 1);
        }
      };
      
      // vychozi nastaveni pro datetimepicker
      var $defaultDatetimeSettings = {
        timeOnlyTitle: __TRANSLATIONS.datetime_picker.time_title,
        timeText: __TRANSLATIONS.datetime_picker.time,
        hourText: __TRANSLATIONS.datetime_picker.hour,
        minuteText: __TRANSLATIONS.datetime_picker.min,
        secondText: __TRANSLATIONS.datetime_picker.sec,
        millisecText: __TRANSLATIONS.datetime_picker.msec,
        timezoneText: __TRANSLATIONS.text.timezone,
        currentText: __TRANSLATIONS.button.now,
        closeText: __TRANSLATIONS.button.close,
        timeFormat: __TRANSLATIONS.datetime.time_format_js,
        amNames: ['AM', __TRANSLATIONS.datetime.am.short],
        pmNames: ['PM', __TRANSLATIONS.datetime.pm.short],
        isRTL: false
      };
      
      /**
       * Bere vsechna data z predaneho inputu a vrati zpet v objektu.
       * format pro attr data je napr.: data-dtp-time-format pro paramter timeFormat
       */
      var optionalData = function ($input) {
        var options = {};
        
        var data = $input.data();
        for (var prop in data) {
          if (prop.indexOf('dtp') === 0) {
            var option = prop.charAt(3).toLowerCase() + prop.substr(4);
            options[option] = data[prop];
          }
        }
        
        return options;
      };
      
      var initializeDatepicker = function () {
        // inicializace datepickeru
        var $input = $('.form_date > input');        
        $input.datepicker($.extend({}, $defaultSettings, optionalData($input)));
        
        // inicializace datepickeru pro výběr rozsahu datumů
        var $input = $('.form_date_range > input');        
        $input.datepicker($.extend({}, $defaultSettings, {
          onClose: function() {
            delete $(this).data().datepicker.first;
            $(this).data().datepicker.inline = false;

            $(this).trigger("focus").trigger("blur");
          },
          onSelect: function(selectedDate) {
            if(!$(this).data().datepicker.first){
                $(this).data().datepicker.inline = true;
                $(this).data().datepicker.first = selectedDate;
            }else{
              if(selectedDate > $(this).data().datepicker.first){
                $(this).val($(this).data().datepicker.first+" - "+selectedDate);
              } else {
                $(this).val(selectedDate+" - "+$(this).data().datepicker.first);
              }
              $(this).data().datepicker.inline = false;
            }
          }
        }, optionalData($input)));
      };
      
      var initializeDateTimepicker = function () {
        var $input = $('.form_datetime > input');
        
        $input.datetimepicker($.extend({},$defaultSettings, $defaultDatetimeSettings, optionalData($input)));
      };
      
      return {
        init: function() {
          initializeDatepicker();
          initializeDateTimepicker();
        },
        $defaultSettings: $defaultSettings
      };
    }();

    var handleColorPicker = function () {
        if (!jQuery().colorpicker) {
            return;
        }
        $('.colorpicker-default').colorpicker({
            format: 'hex'
        });
        $('.colorpicker-rgba').colorpicker();
    };
    
    var format = function(item) {
      var $item = $($(item.element).get(0));
      var itemHtml = '';

      switch ($item.prop('tagName')) {
        case 'OPTION':
          if ($item.data('country')) {
            itemHtml += '<img src="resources/images/flags/' + $item.data('country') + '.png" class="select_flag" />&nbsp;';
          }
          
          if ($item.data('img_path')) {
            itemHtml += '<img src="' + $item.data('img_path') + '" class="select_flag" />&nbsp;';
          }

          itemHtml += $item.text();
        break;

        case 'OPTGROUP':
          itemHtml += $item.attr('label');
        break;
      }

      return itemHtml;
    };
      
    var handleSelect2 = function () {
      $('.select2').select2(FormComponents.defaultSelect2);
    };

    var handleMultiSelect = function () {
        $('#my_multi_select1').multiSelect();
        $('#my_multi_select2').multiSelect({
            selectableOptgroup: true
        });        
    };

    var handleInputMasks = function () {
        $.extend($.inputmask.defaults, {
            'autounmask': true
        });

        $("#mask_date").inputmask("d/m/y", {autoUnmask: true});  //direct mask        
        $("#mask_date1").inputmask("d/m/y",{ "placeholder": "*"}); //change the placeholder
        $("#mask_date2").inputmask("d/m/y",{ "placeholder": "dd/mm/yyyy" }); //multi-char placeholder
        $("#mask_phone").inputmask("mask", {"mask": "(999) 999-9999"}); //specifying fn & options
        $("#mask_tin").inputmask({"mask": "99-9999999"}); //specifying options only
        $("#mask_number").inputmask({ "mask": "9", "repeat": 10, "greedy": false });  // ~ mask "9" or mask "99" or ... mask "9999999999"
        $("#mask_decimal").inputmask('decimal', { rightAlignNumerics: false }); //disables the right alignment of the decimal input
        $("#mask_currency").inputmask('€ 999.999.999,99', { numericInput: true });  //123456  =>  € ___.__1.234,56
       
        $("#mask_currency2").inputmask('€ 999,999,999.99', { numericInput: true, rightAlignNumerics: false, greedy: false}); //123456  =>  € ___.__1.234,56
        $("#mask_ssn").inputmask("999-99-9999", {placeholder:" ", clearMaskOnLostFocus: true }); //default
    };

    var handleIPAddressInput = function () {
        $('#input_ipv4').ipAddress();
        $('#input_ipv6').ipAddress({v:6});
    };
    
    var handleFileUpload = function() {
      $('body').on('change', '.fileUpload  input[type="file"]', function() {
        var $input = $(this);
        
        var $label = $input.closest('.fileUpload').find('span');
        if (!$input.attr('data-old-label')) {
          $input.attr('data-old-label', $label.html());
        }
        
        var labels = [];
        $.each(this.files, function(i, file) {
          if (file) {
            labels.push(file.name);
          }
        });
        
        var showLabels = [];
        if (labels.length > 3) {
          $.each(labels, function(i, label) {
            if (i < 3) {
              showLabels.push(label);
            } else {
              return false;
            }
          });
          
          showLabels.push('...');
        } else {
          showLabels = labels;
        }

        $input.closest('.fileUpload').find('span').html(((showLabels.length) ? implode(', ', showLabels) : $input.attr('data-old-label')));
      });
    };

    return {
        // vychozi nastaveni pro select2
        defaultSelect2: {
          formatNoMatches: function () { return __TRANSLATIONS.select2.message.no_items; },
          formatInputTooShort: function (input, min) {
              var n = min - input.length;
              if (n === 1) {
                  return __TRANSLATIONS.select2.message.min_1;
              } else if (n <= 4) {
                  return $.sprintf(__TRANSLATIONS.select2.message.min_2_4);
              } else {
                  return $.sprintf(__TRANSLATIONS.select2.message.min_5);
              }
          },
          formatInputTooLong: function (input, max) {
              var n = input.length - max;
              if (n === 1) {
                  return __TRANSLATIONS.select2.message.max_1;
              } else if (n <= 4) {
                  return __TRANSLATIONS.select2.message.max_2_4;
              } else {
                  return __TRANSLATIONS.select2.message.max_5;
              }
          },
          formatSelectionTooBig: function (limit) {
              if (limit === 1) {
                  return __TRANSLATIONS.select2.message.select_max_1;
              } else if (limit <= 4) {
                  return __TRANSLATIONS.select2.message.select_max_2_4;
              } else {
                  return __TRANSLATIONS.select2.message.select_max_5;
              }
          },
          formatLoadMore: function (pageNumber) { return __TRANSLATIONS.select2.loading; },
          formatSearching: function () { return __TRANSLATIONS.select2.searching; },
          allowClear: true,
          formatResult: format,
          formatSelection: format,
          escapeMarkup: function (m) {
            return m;
          }
        },
        
        handleBootstrapSwitch: handleBootstrapSwitch,
        handlejQueryUIDatePickers: handlejQueryUIDatePickers.init,
        jQueryUIDatePickers: handlejQueryUIDatePickers,
          
        //main function to initiate the module
        init: function () {
          //handleWysihtml5();
          handleToggleButtons();
          handleBootstrapSwitch();
          /*handleTagsInput();*/
          handlejQueryUIDatePickers.init();
          //handleClockfaceTimePickers();
          //handleColorPicker();
          handleSelect2();
          handleFileUpload();
          /*handleInputMasks();
          handleIPAddressInput();
          handleMultiSelect();

          App.addResponsiveHandler(function(){
              resetWysihtml5();
          })*/
        }

    };

}();