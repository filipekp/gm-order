var LockScreenPlugin = function() {
  var $timeOutObj = null;
  
  /**
   * Funkce pro kontrolu aktivity uzivatele
   */
  function sessionCountdown() {
    var $countdown;

    // nastartovani idletimer pluginu
    $timeOutObj = $.idleTimeout('#idle-timeout-dialog', '.modal-content button:last', {
      warningLength: 1 * 60,                        // doba odpočtu hlasky
      idleAfter: __GLOBALS.user.lock.idleTime,         // doba neaktivity
      pollingInterval: 120,                         // 2 minuty - doba jak často probiha dotaz na server (prodlouzeni session)
      keepAliveURL: 'index.php?controller=login&action=All_PollingInterval',
      serverResponseEquals: 'OK',
      onTimeout: function(self){
        self.stopTimer();
        $('#idle-timeout-dialog').modal('hide');
        lock();
      },
      onIdle: function(self){
        if (isLocked()) {
          self.stopTimer();
        } else {
          $('#idle-timeout-dialog').modal('show');
          $countdown = $('#idle-timeout-counter');

          $('#idle-timeout-dialog-keepalive').on('click', function () { 
            $('#idle-timeout-dialog').modal('hide');
          });

          $('#idle-timeout-dialog-logout').on('click', function () { 
            $('#idle-timeout-dialog').modal('hide');
            $.idleTimeout.options.onTimeout.call(null, self);
          });
        }
      },
      onCountdown: function(counter, self){
        if (isLocked()) {
          self.stopTimer();
        } else {
          if ($countdown.length) { $countdown.html(counter); } // aktualizuje odpočítávání
        }
      },
      titleMessage: '✄ uzamknout za %s sec. | '
    });
  }
  
  /**
   * Funkce pro zjištění zda je obrazovka zamčená
   * 
   * @returns {Boolean}
   */
  function isLocked() {
    return $('.page-lock-wrapper').is(':visible');
  }
  
  /**
   * Funkce pro uzamčení
   */
  function lock() {
    if (!isLocked()) {
      var $data = {
        controller: 'login',
        action:     'lockScreen'
      };

      $.post('index.php', $data, function(data) {
        $('.page-lock-wrapper').fadeIn(400);
        $('.all_body').addClass('blur');
      });
    }
  }
  
  /**
   * Funkce pro odemčení
   * 
   * @param {jQuery} $form
   */
  function unlock($form) {
    $form.submit(function(e) {
      e.preventDefault();
      $('.page-lock-wrapper span.help-block').remove();
      
      
      $.post('index.php', $form.serialize(), function(data) {
        if(data.login) {
          $('.page-lock-wrapper').fadeOut(400);
          $('.all_body').removeClass('blur');
          sessionCountdown();
        } else {
          var $formGroup = $('.page-lock-wrapper input#password').closest('.form-group').addClass('has-error');
          var $span = $('<span />').addClass('help-block').text('Špatné heslo!').css({ display: 'block' }).hide();
          $formGroup.append($span);
          $span.slideDown(400);
          setTimeout(function() {
            $span.slideUp(400);
          }, 2000);
        }
      }, 'json').fail(function(error) { debug.log(error); });
    });
  }
  
  
  return {
    init: function() {
      if (!isLocked()) {
        sessionCountdown();
      }
      
      $('.dropdown-user .lock').click(function() {
        lock();
      });
      
      unlock($('.lock-form'));
    }
  };
}();