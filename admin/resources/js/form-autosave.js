(function ($) {
  /**
   * Automaticke ukladani pro formulare
   * 
   * @param {object} settings
   */
  $.fn.formAutosave = function(settings) {
    var forbiddenKeys = [
      16, // SHIFT
      17, // CTRL
      18, // ALT
      20, // CAPS LOCK
      37, // ARROW LEFT
      38, // ARROW UP
      39, // ARROW RIGHT
      40, // ARROW DOWN
      91  // WIN
    ];
    var defaultSettings = {
      storeAfter: 5,   // nastavi dobu nekativity, po ktere se provede autosave [sec.]
      onAutosave: function($form) {
        $form.trigger('submit');
      }
    };
    
    var newSettings = $.extend({}, defaultSettings, settings);
    
    // globalni pocitadla formularu
    var timers = [];
    
    var methods = {
      /**
       * Funkce na zjisteni hodnoty v poli
       * 
       * @param {mixed} needle
       * @param {array} haystack
       * 
       * @returns {Boolean}
       */
      _inArray: function(needle, haystack) {
        var length = haystack.length;
        for(var i = 0; i < length; i++) {
          if(haystack[i] == needle) return true;
        }

        return false;
      },
      
      _save: function($form) {
        if (!$form.data('autosave-save')) {
          $form.attr('data-autosave-save', true);
          newSettings.onAutosave.call(this, $form);
        }
      },
      
      _startFormTimer: function($form) {
        timers[$form.data('autosave-identifier')] = setTimeout(function() {
          methods._save($form);
        }, newSettings.storeAfter * 1000);
      },
      
      _stopTimer: function($form) {
        clearTimeout(timers[$form.data('autosave-identifier')]);
      },
      
      _resetFormTimer: function($form) {
        $form.attr('data-autosave-save', false);
        if ($form.data('autosave-identifier') in timers) {
          methods._stopTimer($form);
        }
        methods._startFormTimer($form);
      },
      
      _catchPress: function($form) {
        $form.on('keydown', function(e) {
          if (!methods._inArray(e.which, forbiddenKeys)) {
            methods._resetFormTimer($form);
          }
        });
      },

      init: function($forms) {
        $forms.each(function(key, value) {
          var $form = $(this);
          $form.attr('data-autosave-identifier', 'form_' + key);

          methods._catchPress($form);
          
          // trigger pro stopnuti timeru formulare
          $form.on('form-autosave-stopTimer', function(){
            methods._stopTimer($form);
          });
        });
      }
    };
    
    methods.init($(this));
  };
}(jQuery));