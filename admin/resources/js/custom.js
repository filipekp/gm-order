/**
 * Logs object into the console with specific label.
 * 
 * @param {object} object
 * @param {string} label
 * @param {object} style default style is green background with white font color → object is css object in the correct form
 */
var debug = {
  log: function(object, label, style) {
    var defaultStyle = { 'background-color': '#007F04', 'color': 'white' };
    style = decodeURIComponent($.param($.extend(defaultStyle, ((typeof style === 'undefined') ? {} : style))))
              .replace(/=/g, ': ').replace(/&/g, '; ');

    var objSpecifier;
    switch (typeof object) {
      case 'string':
      case 'undefined':
        objSpecifier = '%s';
      break;
      
      case 'boolean':
        objSpecifier = '%s';
        object = ((object) ? true : false);
      break;
      
      case 'number':
        objSpecifier = ((parseInt(object) !== parseFloat(object)) ? '%f' : '%d');
      break;

      case 'object':
      case 'function':
      case 'xml':
      default:
        objSpecifier = '%O';
      break;
    }
    
    console.log('%c ' + ((typeof label === 'undefined') ? 'debug' : label) + ': %c  ' + objSpecifier, style, 'background-color: white;', object);
  }
};
console.info('Veškeré skripty jsou chráněny autorským zákonem (c) Proclient s.r.o.');

/**
 * Repeats a string.
 * 
 * @param {Integer} num
 * @returns {String}
 */
String.prototype.repeat = function(num) {
  return new Array(num + 1).join(this);
};

/**
 * Vrati parametr z url (GET)
 * 
 * @param {String} name
 * @returns {String}
 */
function getURLParam(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

/**
 * Get TRUE when key in array exists.
 * 
 * @param {mixed} key
 * @param {array|object} search
 * @returns {Boolean}
 */
function array_key_exists(key, search) {
  if (!search || (search.constructor !== Array && search.constructor !== Object)) {
    return false;
  }

  return key in search;
}

/**
 * Funkce na zjisteni hodnoty v poli
 * 
 * @param {mixed} needle
 * @param {array} haystack
 * 
 * @returns {Boolean}
 */
function inArray(needle, haystack) {
  var length = haystack.length;
  for(var i = 0; i < length; i++) {
    if(haystack[i] == needle) return true;
  }

  return false;
}

/**
 * Round number
 * 
 * @param {float} value
 * @param {int} precision
 * @param {string} mode PHP_ROUND_HALF_DOWN | PHP_ROUND_HALF_EVEN | PHP_ROUND_HALF_ODD
 * 
 * @returns {float}
 */
function round(value, precision, mode) {
  var m, f, isHalf, sgn;
  precision |= 0;
  m = Math.pow(10, precision);
  value *= m;
  sgn = (value > 0) | -(value < 0);
  isHalf = value % 1 === 0.5 * sgn;
  f = Math.floor(value);

  if (isHalf) {
    switch (mode) {
      case 'PHP_ROUND_HALF_DOWN':
        value = f + (sgn < 0); // rounds .5 toward zero
        break;
      case 'PHP_ROUND_HALF_EVEN':
        value = f + (f % 2 * sgn); // rouds .5 towards the next even integer
        break;
      case 'PHP_ROUND_HALF_ODD':
        value = f + !(f % 2); // rounds .5 towards the next odd integer
        break;
      default:
        value = f + (sgn > 0); // rounds .5 away from zero
    }
  }

  return (isHalf ? value : Math.round(value)) / m;
}

/**
 * Spoji prvky pole do retezce.
 * 
 * @param {string} glue
 * @param {array} pieces
 * 
 * @returns {String}
 */
function implode(glue, pieces) {
  var i = '',
    retVal = '',
    tGlue = '';
  if (arguments.length === 1) {
    pieces = glue;
    glue = '';
  }
  if (typeof pieces === 'object') {
    if (Object.prototype.toString.call(pieces) === '[object Array]') {
      return pieces.join(glue);
    }
    for (i in pieces) {
      retVal += tGlue + pieces[i];
      tGlue = glue;
    }
    return retVal;
  }
  return pieces;
}

/**
 * Set the cookie.
 * 
 * @param {string} cname
 * @param {mixed} cvalue
 * @param {int} exdays
 */
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+d.toGMTString();
  document.cookie = cname + '=' + cvalue + '; ' + expires + '; path=/;';
}

/**
 * Get the cookie.
 * @param {string} cname
 */
function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1);
    if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
  }
  return "";
}

/***** STATUS MESSAGES *****/
var TIMER_FOR_STATUS_MESSAGES = {};

/**
 * Shows status message.
 */
function showStatusMsg(code) {
  var $msg = $('div#messages_container div.alert-' + code);
  
  jQuery('html,body').animate({
      scrollTop: 0
  }, 'slow');
  
  $msg.slideDown(500);
}

/**
 * Hides status message.
 * @param {int} time
 */
function hideStatusMsg(time, code) {
  time = ((typeof time === 'undefined') ? false : time);
  
  if (TIMER_FOR_STATUS_MESSAGES[code] !== null) clearTimeout(TIMER_FOR_STATUS_MESSAGES[code]);
  if (time) {
    $('div#messages_container div.alert-' + code).slideUp(time, function() {
      $(this).remove();
    });
  } else {
    $('div#messages_container div.alert-' + code).hide(0, function() {
      $(this).remove();
    });
  }  
}

function hideStatusMsgs(time) {
  for (var code in TIMER_FOR_STATUS_MESSAGES) {
    hideStatusMsg(time, code);
  }
}

/**
 * Toggle status message.
 * 
 * @param {int} status
 * @param {string} messages
 */
function getStatusMessage(messages) {
  hideStatusMsg();
  var getClass = function(status) {
    var classMsg;
    switch (status) {
      case 'msg_201': classMsg = 'info';      break;
      case 'msg_400': classMsg = 'danger';     break;
      case 'msg_417': classMsg = 'warning';   break;
      case 'msg_200': classMsg = 'success';   break;
      case 'msg_500':
      default:        classMsg = 'danger';     break;
    }
    
    return classMsg;
  };
  
  var $messages = $.parseJSON(messages);
  if ($messages) {
    var $messageContainer = $('div#messages_container');
    $messageContainer.html('');
    for (var code in $messages) {
      var htmlMessage = $messages[code];
      var className = getClass(code);
      var $messageDiv = $('<div />').addClass('alert')
                                    .addClass('alert-' + className)
                                    .attr('data-identifier', className)
                                    .mouseenter(function() {
                                      if (code !== 'msg_400' && code !== 'msg_417' && code !== 'msg_500') {
                                        var $self = $(this);
                                        clearTimeout(TIMER_FOR_STATUS_MESSAGES[$self.data('identifier')]);
                                      }
                                    })
                                    .mouseleave(function() {
                                      if (code !== 'msg_400' && code !== 'msg_417' && code !== 'msg_500') {
                                        var $self = $(this);
                                        TIMER_FOR_STATUS_MESSAGES[$(this).data('identifier')] = setTimeout(function() {
                                          hideStatusMsg(200, $self.data('identifier'));
                                        }, 5000);
                                      }
                                    })
                                    .css({display: 'none'});
      $('<button />').addClass('close')
                     .attr('data-dismiss', 'alert')
                     .appendTo($messageDiv);
      $('<span />').html(htmlMessage).appendTo($messageDiv);
      $messageDiv.appendTo($messageContainer);
      
      showStatusMsg(className);
      if (code !== 'msg_400' && code !== 'msg_417' && code !== 'msg_500') {
        TIMER_FOR_STATUS_MESSAGES[className] = setTimeout(function() {
          hideStatusMsgs(200);
        }, 5000);
      }
    }
  }
}

/**
 * Toastr zpravy
 * 
 * @param {json} messages
 * @returns {undefined}
 */
function getToasterStatusMessage(messages) {
  toastr.options = {
    'closeButton': true,
    'newestOnTop': __GLOBALS.user.messages_settings.vertical === 'top',
    'progressBar': true,
    'positionClass': 'toast-' + __GLOBALS.user.messages_settings.vertical + '-' + __GLOBALS.user.messages_settings.horizontal,
    'onclick': null,
    'showDuration': '1000',
    'hideDuration': '1000',
    'timeOut': ((__GLOBALS.user.messages_settings.hideTime) ? __GLOBALS.user.messages_settings.hideTime * 1000 : 5000),
    'extendedTimeOut': '3000',
    'showEasing': 'swing',
    'hideEasing': 'linear',
    'showMethod': 'fadeIn',
    'hideMethod': 'fadeOut'
  };
  
  var setParams = function(status) {
    var settings = {};
    
    switch (status) {
      case 'msg_201':
        $.extend(settings, {
          heading: __TRANSLATIONS.notification.info.heading,
          type: 'info'
        });
      break;
      
      case 'msg_417':
        $.extend(settings, {
          heading: __TRANSLATIONS.notification.warning.heading,
          type: 'warning'
        });
      break;
      
      case 'msg_200':
        $.extend(settings, {
          heading: __TRANSLATIONS.notification.success.heading,
          type: 'success'
        });
      break;
      
      case 'msg_400':
      case 'msg_500':
      default:
        $.extend(settings, {
          heading: __TRANSLATIONS.notification.error.heading,
          type: 'error'
        });
      break;
    }
    
    return settings;
  };
  
  var $messages = $.parseJSON(messages);
  if ($messages) {
    for (var code in $messages) {
      var settings = setParams(code);
      
      toastr[settings.type]($messages[code], settings.heading);
    }
  }
}

/**
 * Zobrazi status hlasku po vzoru Win8
 * 
 * @param {JSON} messages
 */
function getWin8StatusMessage(messages) {
  var defaultSettings = {
    life: ((__GLOBALS.user.messages_settings.hideTime) ? __GLOBALS.user.messages_settings.hideTime * 1000 : 5000),
    theme: 'teal',
    sticky: false,
    horizontalEdge: __GLOBALS.user.messages_settings.vertical,
    verticalEdge: __GLOBALS.user.messages_settings.horizontal
  };
  
  var setParams = function(status) {
    var settings = {};
    
    switch (status) {
      case 'msg_201':
        $.extend(settings, defaultSettings, {
          heading: '<i class="fa fa-info-circle"></i> ' + __TRANSLATIONS.notification.info.heading,
          theme: 'teal'
        });
      break;
      
      case 'msg_417':
        $.extend(settings, defaultSettings, {
          heading: '<i class="fa fa-exclamation-triangle"></i> ' + __TRANSLATIONS.notification.warning.heading,
          theme: 'tangerine'
        });
      break;
      
      case 'msg_200':
        $.extend(settings, defaultSettings, {
          heading: '<i class="fa fa-check-circle"></i> ' + __TRANSLATIONS.notification.success.heading,
          theme: 'lime'
        });
      break;
      
      case 'msg_400':
      case 'msg_500':
      default:
        $.extend(settings, defaultSettings, {
          heading: '<i class="fa fa-exclamation-circle"></i> ' + __TRANSLATIONS.notification.error.heading,
          theme: 'ruby',
          sticky: true
        });
      break;
    }
    
    return settings;
  };
  
  var $messages = $.parseJSON(messages);
  if ($messages) {
    for (var code in $messages) {
      var settings = setParams(code);
      
      $.notific8('zindex', 11500);
      $.notific8($.trim($messages[code]), settings);
    }
  }
}

/**
 * Plugin pro synchronizaci a aktualizaci času
 */
var TimePlugin = function() {
  var correction = null;
  var date = null;
  var timer = null;
  
  function setTimeCorrection() {
    if (correction === null) {
      $.ajax({
        url: 'index.php',
        async: false,
        method: 'post',
        data: {
          controller: 'admin',
          action: 'getCurrentDatetimeJson'
        },
        success: function(response) {
          correction = Date.parse(new Date(Date.parse(response['date_time'])).toUTCString()) - Date.parse(new Date().toUTCString());
        }
      });
    }
  }
  
  /**
   * Funkce pro aktualizaci času
   * 
   * @param {jQuery} $object
   */
  function updateTime($object) {
    if (isNaN(correction)) {
      clearTimeout(timer);
      correction = null;
      setTimeCorrection();
      
      setTimeout(function() { updateTime($object) }, 20 * 1000);
    } else {
      date = new Date();
      date.setTime(date.getTime() + correction);

      var hour = date.getHours();
      var min = date.getMinutes();
      var sec = date.getSeconds();
      hour = ((hour < 10) ? '0' + hour : hour);
      min = ((min < 10) ? '0' + min : min);
      sec = ((sec < 10) ? '0' + sec : sec);

      $object.text(hour + ':' + min + ':' + sec);

      // opakovani nacitani po 0.5 sekunde
      timer = setTimeout(function() { updateTime($object); }, 1000);
    }
  }
  
  return {
    init: function($object) {
      setTimeCorrection();
      updateTime($object);
    }
  };
}();

/**
 * Funkce pro skoloňování slov podle čísla.
 * 
 * @param {float} number
 * @param {string} once
 * @param {string} oneToFive
 * @param {string} float
 * @param {string} allOther
 * 
 * @returns {string} 'cislo a format slova' => '1 hrnek'
 */
function inflection(number, once, oneToFive, float, allOther) {
  number = Math.abs(number);
  
  var string = '';
  if (parseInt(number) != number) {
    string += float;
  } else if (number == 0 || number >= 5) {
    string += allOther;
  } else if (number == 1) {
    string += once;
  } else if (number < 5) {
    string += oneToFive;
  }

  return string;
}

/**
 * V pripade smazani vsech polozek vypise hlasku o nenalezenem zaznamu.
 * 
 * @param {string} dataSelector     // selector pro vyber datovych radku v tabulce
 * @param {object} $target          // jQuery objekt tabulky
 * @param {string} text             // text hlášky
 * @param {string} heading          // nadpis hlášky
 * @param {int} animationTime       // doba animace hlasky
 * 
 * @returns {Boolean} vrati true, jestlize cil ($target) obsahuje nejaka data
 */
function manageNoDataRow(dataSelector, $target, text, heading, animationTime) {
  animationTime = ((typeof animationTime === 'undefined') ? 400 : animationTime);
  var isData = true;
  
  if ($target.find(dataSelector).length) {
    $target.find('.no_object').fadeOut(animationTime, function() {
      $(this).remove();
    });
  } else {
    var $alert = $('<tr class="no_object" />').append(
                    $('<td class="alert alert-error" colspan="' + $target.closest('table').find('thead > tr th').length + '"/>')
                      .append(
                        $('<span class="heading" />')
                          .append($('<i class="fa fa-warning" /> '))
                          .append(heading)
                      )
                      .append($('<div />').text(text))
                 ).css('display', 'none')
                  .appendTo($target);
    $alert.fadeIn(animationTime);
    
    isData = false;
  }
  
  return isData;
}

/**
 * Odstrani diakritiku textu
 * 
 * @param {string} text
 * @returns {string}
 */
function removeDiacritics(text) {
  // remove diacritics
  var conversionTable = [
    [/ä/g, 'a'], [/Ä/g, 'A'], [/á/g, 'a'], [/Á/g, 'A'], [/à/g, 'a'], [/À/g, 'A'], [/ã/g, 'a'], [/Ã/g, 'A'], [/â/g, 'a'], [/Â/g, 'A'], [/ą/g, 'a'], [/Ą/g, 'A'], [/ă/g, 'a'], [/Ă/g, 'A'],
    [/č/g, 'c'], [/Č/g, 'C'], [/ć/g, 'c'], [/Ć/g, 'C'], [/ç/g, 'c'], [/Ç/g, 'C'],
    [/ď/g, 'd'], [/Ď/g, 'D'], [/đ/g, 'd'], [/Đ/g, 'D'],
    [/ě/g, 'e'], [/Ě/g, 'E'], [/é/g, 'e'], [/É/g, 'E'], [/ë/g, 'e'], [/Ë/g, 'E'], [/è/g, 'e'], [/È/g, 'E'], [/ê/g, 'e'], [/Ê/g, 'E'], [/ę/g, 'e'], [/Ę/g, 'E'],
    [/í/g, 'i'], [/Í/g, 'I'], [/ï/g, 'i'], [/Ï/g, 'I'], [/ì/g, 'i'], [/Ì/g, 'I'], [/î/g, 'i'], [/Î/g, 'I'],
    [/ľ/g, 'l'], [/Ľ/g, 'L'], [/ĺ/g, 'l'], [/Ĺ/g, 'L'], [/ł/g, 'l'], [/Ł/g, 'L'],
    [/ń/g, 'n'], [/Ń/g, 'N'], [/ň/g, 'n'], [/Ň/g, 'N'], [/ñ/g, 'n'], [/Ñ/g, 'N'],
    [/ó/g, 'o'], [/Ó/g, 'O'], [/ö/g, 'o'], [/Ö/g, 'O'], [/ô/g, 'o'], [/Ô/g, 'O'], [/ò/g, 'o'], [/Ò/g, 'O'], [/õ/g, 'o'], [/Õ/g, 'O'], [/ő/g, 'o'], [/Ő/g, 'O'],
    [/ř/g, 'r'], [/Ř/g, 'R'], [/ŕ/g, 'r'], [/Ŕ/g, 'R'],
    [/š/g, 's'], [/Š/g, 'S'], [/ś/g, 's'], [/Ś/g, 'S'], [/ş/g, 's'], [/Ş/g, 'S'],
    [/ť/g, 't'], [/Ť/g, 'T'], [/ţ/g, 't'], [/Ţ/g, 'T'],
    [/ú/g, 'u'], [/Ú/g, 'U'], [/ů/g, 'u'], [/Ů/g, 'U'], [/ü/g, 'u'], [/Ü/g, 'U'], [/ù/g, 'u'], [/Ù/g, 'U'], [/ũ/g, 'u'], [/Ũ/g, 'U'], [/û/g, 'u'], [/Û/g, 'U'], [/ű/g, 'u'], [/Ű/g, 'U'],
    [/ý/g, 'y'], [/Ý/g, 'Y'],
    [/ž/g, 'z'], [/Ž/g, 'Z'], [/ź/g, 'z'], [/Ź/g, 'Z'], [/ż/g, 'z'], [/Ż/g, 'Z']
  ];
  
  for (var i in conversionTable) {
    text = text.replace(conversionTable[i][0], conversionTable[i][1]);
  }
  
  return text;
}

/**
 * Prevede text na seo typ
 * 
 * @param {string} text
 * @returns {string}
 */
function generateSeo(text) {
  // convert to lower
  text = removeDiacritics(text).toLowerCase();
  
  // workout dashes
  return text.replace('_', '-').replace(/[^\w-]/gi, '-').replace(/-+/gi, '-').replace(/^-?(.+?)-?$/, '$1');
}

var Menu = {
  setActiveMenuPath: function() {
    $('.page-sidebar-menu li.active').each(function() {
      var current = parseInt($(this).data('parent'));
      while (current > 0) {
        var $parent = $('li[data-id="' + current + '"]');
        $parent.addClass('active').addClass('open');
        $($parent.find('span.arrow').get(0)).addClass('open');
        current = parseInt($parent.data('parent'));
      }
    });
  }
};

var pluginTinymce = {
  commonSettingsTinyMCE: {
    mode: 'textareas',
    fontsize_formats: "8px 10px 12px 14px 16px 20px 36px 48px 92px 110px",
    theme: 'modern',
    valid_elements : "@[id|class|style|title|dir<ltr?rtl|lang|xml::lang|onclick|ondblclick|"
                      + "onmousedown|onmouseup|onmouseover|onmousemove|onmouseout|onkeypress|"
                      + "onkeydown|onkeyup],a[rel|rev|charset|hreflang|tabindex|accesskey|type|"
                      + "name|href|target|title|class|onfocus|onblur],strong/b,em,i,strike,u,"
                      + "#p,-ol[type|compact],-ul[type|compact],-li,br,img[longdesc|usemap|"
                      + "src|border|alt=|title|hspace|vspace|width|height|align],-sub,-sup,"
                      + "-blockquote,-table[border=0|cellspacing|cellpadding|width|frame|rules|"
                      + "height|align|summary|bgcolor|background|bordercolor],-tr[rowspan|width|"
                      + "height|align|valign|bgcolor|background|bordercolor],tbody,thead,tfoot,"
                      + "#td[colspan|rowspan|width|height|align|valign|bgcolor|background|bordercolor"
                      + "|scope],#th[colspan|rowspan|width|height|align|valign|scope],caption,-div,"
                      + "-span,-code,-pre,address,-h1,-h2,-h3,-h4,-h5,-h6,hr[size|noshade],-font[face"
                      + "|size|color],dd,dl,dt,cite,abbr,acronym,del[datetime|cite],ins[datetime|cite],"
                      + "object[classid|width|height|codebase|*],param[name|value|_value],embed[type|width"
                      + "|height|src|*],script[src|type],map[name],area[shape|coords|href|alt|target],bdo,"
                      + "button,col[align|char|charoff|span|valign|width],colgroup[align|char|charoff|span|"
                      + "valign|width],dfn,fieldset,form[action|accept|accept-charset|enctype|method],"
                      + "input[accept|alt|checked|disabled|maxlength|name|readonly|size|src|type|value],"
                      + "kbd,label[for],legend,noscript,optgroup[label|disabled],option[disabled|label|selected|value],"
                      + "q[cite],samp,select[disabled|multiple|name|size],small,"
                      + "textarea[cols|rows|disabled|name|readonly],tt,var,big",
    plugins: [
      'eqneditor advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker tabindex',
      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
      'save table contextmenu directionality emoticons template paste textcolor colorpicker textpattern responsivefilemanager previewhtml'
    ],
    image_advtab: true,
    width: '100%',
    toolbar_items_size: 'small',
    menubar: false,
    style_formats: [
      {title: 'Odstavec', block: 'p'},
      {title: 'Nadpis 1', block: 'h1'},
      {title: 'Nadpis 2', block: 'h2', classes: 'section'},
      {title: 'Nadpis 3', block: 'h3', classes: 'underline'},
      {title: 'Nadpis 4', block: 'h4'},
      {title: 'Tučný text', inline: 'b'}
    ],
    language : __TRANSLATIONS.code,
    entity_encoding : 'raw',
    entities : '160,nbsp',
    /*toolbar1: 'undo redo | previewhtml fullscreen code | bold italic underline strikethrough | cut copy paste pasteword pastetext searchreplace | image media responsivefilemanager',
    toolbar2: 'alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | forecolor backcolor | charmap eqneditor',*/
    toolbar1: "newdocument fullpage | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
    toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | insertdatetime previewhtml | forecolor backcolor",
    toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft | eqneditor",
    insertdatetime_formats: [
      '%d.%m.%Y',
      '%d. %B %Y',
      '%d. %b %Y',
      '%A %d. %B %Y',
      '%d.%m.%Y (%a)',
      '%d.%m.%Y %H:%M:%S',
      '%H:%M:%S'
    ],
    setup: function(editor) {
      editor.on('blur', function(e) {
        debug.log(e.target.id, 'on change tinymce');
        tinyMCE.triggerSave();
        $("#" + e.target.id).valid();
      });
    },
    // external file manager
    external_filemanager_path: '/admin/resources/plugins/filemanager/',
    filemanager_title: 'Správce souborů',
    external_plugins: { "filemanager" : '/admin/resources/plugins/filemanager/plugin.min.js'}
  },
  
  smallEditor: {
    selector: 'textarea.tinymceAnswer',
    toolbar1: 'undo redo | previewhtml fullscreen code | bold italic underline strikethrough | cut copy paste pasteword pastetext searchreplace | image media responsivefilemanager',
    toolbar2: 'alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | forecolor backcolor | charmap eqneditor',
    toolbar3: '',
    setup: function (ed) {
      ed.on('focus', function () {
        $(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp").slideDown();
        $(this.contentAreaContainer.parentElement).find("div.mce-statusbar").slideDown();
      });
      ed.on('blur', function () {
        $(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp").slideUp();
        $(this.contentAreaContainer.parentElement).find("div.mce-statusbar").slideUp();
      });
      ed.on("init", function() {
        $(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp").hide();
        $(this.contentAreaContainer.parentElement).find("div.mce-statusbar").hide();
      });
    }
  },
  
  reInit: function(selector, settings) {
    settings = ((typeof settings === 'undefined') ? {} : settings);
    
    var params;
    switch (settings) {
      case 'small': 
        params = this.smallEditor;
      break;
      default:
        params = $.extend({}, settings);
      break;
    }
    
    tinymce.remove(selector);
    tinymce.triggerSave();    
    tinymce.init($.extend({}, this.commonSettingsTinyMCE, params, {
      selector: selector
    }));
  },
  
  init: function() {
    tinymce.init($.extend({}, this.commonSettingsTinyMCE, {
      selector: 'textarea.tinymce'    
    }));

    tinymce.init($.extend({}, this.commonSettingsTinyMCE, {
      selector: 'textarea.tinymceAnswer',
      toolbar1: 'undo redo | previewhtml fullscreen code | bold italic underline strikethrough | cut copy paste pasteword pastetext searchreplace | image media responsivefilemanager',
      toolbar2: 'alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | forecolor backcolor | charmap eqneditor',
      toolbar3: '',
      setup: function (ed) {
        ed.on('focus', function () {
          $(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp").slideDown();
          $(this.contentAreaContainer.parentElement).find("div.mce-statusbar").slideDown();
        });
        ed.on('blur', function () {
          $(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp").slideUp();
          $(this.contentAreaContainer.parentElement).find("div.mce-statusbar").slideUp();
        });
        ed.on("init", function() {
          $(this.contentAreaContainer.parentElement).find("div.mce-toolbar-grp").hide();
          $(this.contentAreaContainer.parentElement).find("div.mce-statusbar").hide();
        });
      }
    }));
  }
};

var CheckboxGroup = {
  selector: '[data-checkbox-group]',
  data: 'checkbox-group',
  
  checkHandler: function($handler, groupSelector, selectorAll) {
    var $group = $(groupSelector);
    var checked = $group.filter(':checked').length;
    var unchecked = $group.filter(':not(:checked)').length;

    $handler.prop({
      checked: checked && !unchecked,
      indeterminate: checked && unchecked
    });

    $.uniform.update(selectorAll);
  },
  
  handle: function($handler) {
    var groupSelector = $handler.data(CheckboxGroup.data);
    var selectorAll = CheckboxGroup.selector + ', ' + groupSelector;

    // select or deselect all
    $handler.change(function() {
      $(groupSelector).prop('checked', $handler.is(':checked'));
      $.uniform.update(selectorAll);
    });

    // set handler according to group change
    $(groupSelector).change(function() {
      CheckboxGroup.checkHandler($handler, groupSelector, selectorAll);
    });
    
    // set handler on load of page
    this.checkHandler($handler, groupSelector, selectorAll);
  },
  init: function() {
    // ovladani skupiny checkboxu
    $(this.selector).each(function() {
      CheckboxGroup.handle($(this));
    });
  }
};

var CheckableControlsHierarchy = {
  selector: '.checkable-controls-hierarchy',
  handlerClass: 'cch-handler',
  handlerLabelClass: 'cch-handler-label',
  
  toggle: function() {
    $(this.selector + '[data-parent]').each(function() {
      var $children = $(this);
      var $parent = $('#' + $children.data('parent'));
      var $groupHandler = $parent.closest('label').find('.' + CheckableControlsHierarchy.handlerLabelClass);
      
      if ($parent.is(':checked') && $children.is(':hidden')) {
        $children.slideDown();
        $groupHandler.show();
      } else if ($parent.is(':not(:checked)') && $children.is(':visible')) {
        $children.slideUp();
        $groupHandler.hide();
      }
    });
  },
  init: function() {
    this.toggle();
    
    $(this.selector + '[data-parent]').each(function() {
      var $children = $(this);
      var $parent = $('#' + $children.data('parent'));
    
      // ke kazdemu rodici, ktery ma vice nez jeden checkbox prida checkbox pro ovladani skupiny
      var $checkboxes = $children.find('input[type="checkbox"]');
      if ($checkboxes.length > 1) {
        var selectors = new Array();
        $checkboxes.each(function() {
          var $checkbox = $(this);
          selectors.push("input[name='" + $checkbox.attr('name') + "'][value='" + $checkbox.val() + "']");
        });

        var $checkboxGroupHandler = $('<input type="checkbox" />').attr('data-checkbox-group', selectors.join(', '));
        var $handlerLabel = $('<label/>').addClass('checkbox ' + CheckableControlsHierarchy.handlerLabelClass)
                                         .text('O(d)značit vše')
                                         .prepend($checkboxGroupHandler);
        
        $parent.closest('label').append($handlerLabel);
        
        // udela z checkboxu uniform input
        $checkboxGroupHandler.uniform()
                             .closest('.checker').addClass(CheckableControlsHierarchy.handlerClass);
        
        // inicializuje checkbox group
        CheckboxGroup.handle($checkboxGroupHandler);
      }

      // zjisti zmeny pri zmene inputu
      $('input[name="' + $parent.attr('name') + '"]').change(function() {
        CheckableControlsHierarchy.toggle();
      });
    });
  }
};

var TableRowCheckable = {
  init: function(event) {
    $('tr[data-checkable]').click(function(event) {
      var checkable = '#' + $(this).data('checkable');
      if ($(event.target).is(':not(' + checkable + ')')) {
        $(checkable).click();
      }
    });
  }
};

var Lightbox = {
  $tpl: {            
    error    : '<p class="fancybox-error">Cíl nemohl být načten.<br/>Prosím zkuste to později.</p>',
    closeBtn : '<a class="fancybox-item fancybox-close" href="javascript:void(0);" title="Zavřít"></a>',
    next     : '<a class="fancybox-nav next" href="javascript:void(0);" title="Další"><span></span></a>',
    prev     : '<a class="fancybox-nav prev" href="javascript:void(0);" title="Předchozí"><span></span></a>'
  },
  
  init: function() {
    // lightbox  
    $('.fancybox').fancybox({
      maxWidth    : '80%',
      maxHeight   : '90%',
      fitToView   : false,
      width       : '70%',
      height      : '70%',
      autoSize    : true,
      closeClick	: false,
      openEffect	: 'fade',
      closeEffect	: 'fade',
      tpl         : this.$tpl,
      wrapCSS     : 'fancy',
      afterLoad   : function() {
        var callbackOpen = this.element.data('calback-open');
        if (typeof window[callbackOpen] !== 'undefined') {
          window[callbackOpen]();
        }
      },
      afterClose  : function() {
        var callbackClose = this.element.data('calback-close');
        if (typeof window[callbackClose] !== 'undefined') {
          window[callbackClose]();
        }
      }
    });
  }
};

var InputMaskPlugin = function() {
  /**
   * Vrati cestu obrazku s vlajkou podle ISO zkratky.
   * 
   * @param {string} cc
   * @returns {string}
   */
  var getFlag = function (cc) {
    var path = location.current_path + 'resources/images/flags/';
    var flag = '';
    switch (cc) {
      case 'CZ': flag = path + 'cz.png';
      break;
      case 'SK': flag = path + 'sk.png';
      break;
      case 'DE': flag = path + 'de.png';
      break;
      case 'PL': flag = path + 'pl.png';
      break;
      default:
        flag = path + 'uni.png';
    };

    return flag;
  };
  
  /**
   * Inicializuje masku telefonu.
   */
  var InitPhoneInputMask = function() {
    $('input.phonemask').each(function() {
      var $phone = $(this);

      if (!$phone.parent().is('span')) {
        $phone.wrap($('<span />').css({
          position: 'relative',
          display:  'block'
        }));
      }

      // set default value
      if (!$phone.val()) {
        //$phone.val('420');
      }
    });

    $('input.phonemask').inputmasks({
      inputmask: {
        definitions: { '#': { validator: '[0-9]', cardinality: 1 } },
        showMaskOnHover: false,
        autoUnmask: false
      },
      match: /[0-9]/,
      replace: '#',
      list: $.masksSort($.masksLoad(location.current_path + 'resources/plugins/inputmask-multi/phone-codes.json'), ['#'], /[0-9]|#/, 'sort_order'),
      listKey: 'mask',
      onMaskChange: function(maskObj, determined) {
        var $phone = $(this);
        $phone.next('.determined_phone_country').remove();
        $phone.attr('data-country', maskObj.cc);

        if (determined) {
          $phone.after(
            $('<img class="determined_phone_country" src="' + getFlag(maskObj.cc) + '" title="' + maskObj.name_cz + '" />').css({
              height: '20px',
              color: '#999999',
              position: 'absolute',
              right: '10px',
              top: $phone.innerHeight() / 2 - 10 + 'px'
            })
          );
        }
      }
    });
  };
  
  /**
   * Inicializuje masku emailu.
   */
  var InitEmailInputMask = function() {
    $('input.emailmask').inputmask({ alias: "email"});
  };
  
  return {
    init: function () {
      InitPhoneInputMask();
      InitEmailInputMask();
    }
  };
}();

/**
 * Najde prvky s predanym selectorem a prida jim checkbox pro zobrazeni/skryti hesla.
 * 
 * @param {string} selector
 */
function activateToggleShowPassword(selector) {
  $(selector).each(function() {
    var $self = $(this);
    var $id = $self.attr('id');
    var $idChck = $id + '_togglePsw';
    
    $self.wrap($('<div />').css({ position: 'relative' }));
    
    var $div = $('<div />').css({
        'font-size': '75%',
        position: 'absolute',
        right: '8px'
      })
      .addClass('show_toggle_password')
      .insertAfter($self);
    $('<input />').attr('type', 'checkbox')
                  .attr('id', $idChck)
                  .attr('data-input', $id)
                  .attr('tabindex', -1)
                  .addClass('mini-bootstrap-switch')
                  .appendTo($div);

    var $height = ($self.actual('innerHeight') - $div.actual('innerHeight')) / 2;
    $div.css({ top: $height - $height/2  + 'px' });
  });
  
  FormComponents.handleBootstrapSwitch();
  
  $('body').on('switchChange.bootstrapSwitch', '.bootstrap-switch input', function () {
    var $checked = $(this);
    var $input = $('#' + $checked.data('input'));
    
    if ($checked.is(':checked')) {
      $input.attr('type', 'text');
      setTimeout(function() { $input.focus(); }, 100);
    } else {
      $input.attr('type', 'password');
    }
    
    $input.blur(function() {
      if ($input.attr('type') === 'text') {
        setTimeout(function() {
          $input.attr('type', 'password');
          $checked.attr('checked', false).trigger('change');
        }, 2000);
      }
    });
  });
}

/**
 * Funkce pro inicializaci autocomplete.
 * 
 * @param {jQuery} $object
 * @param {object} $params
 */
function autocompleteSetting($object, $params) {
  var identification = function(item, term) {
    if (item) {
      var remDiaTerm = removeDiacritics(term);
      var $highlightSpan = $('<span />').addClass('highlight');
      var identification = '';
      var desc = '';
      
      var remDiaLabel = removeDiacritics(item.label);
      var remDiaDesc = removeDiacritics(item.desc);      
      var reg = new RegExp('(' + remDiaTerm + ')', 'i');
      
      var identificationArr = remDiaLabel.replace(reg, '|$1|').split('|');
      if (identificationArr.length > 2) {
        identification = item.label.substr(0, identificationArr[0].length) + 
                             $highlightSpan.text(item.label.substr(identificationArr[0].length, identificationArr[1].length)).get(0).outerHTML + 
                             item.label.substr((identificationArr[0].length + identificationArr[1].length), identificationArr[2].length);
      } else {
        identification = item.label;
      }
      
      var descArr = remDiaDesc.replace(reg, '|$1|').split('|');
      if (descArr.length > 2) {
        desc = item.desc.substr(0, descArr[0].length) + 
                   $highlightSpan.text(item.desc.substr(descArr[0].length, descArr[1].length)).get(0).outerHTML + 
                   item.desc.substr((descArr[0].length + descArr[1].length), descArr[2].length);
      } else {
        desc = item.desc;
      }
      
      if (desc) {
        identification += '<br /><small><i>' + desc + '</i></small>';
      }
      
      return identification;
    } else {
      return '';
    }
  };
  
  $object.autocomplete($.extend({}, {
    source: [{label: ''}],
    minLength: 1,
    delay: 100,
    html: true
  }, $params)).autocomplete('instance')._renderItem = function(ul, item) {
      var $li = $('<li />').append(identification(item, this.term));      
      return $li.appendTo(ul);
  };
}

function InitDefaultActionButtonInForm() {
  $('form input, form select').live('keypress', function(e) {
    if ($(this).closest('form').find('button[type=submit].default, input[type=submit].default').length <= 0) {
      return true;
    }

    if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
      $(this).closest('form').find('button[type=submit].default, input[type=submit].default').click();
      return false;
    } else {
      return true;
    }
  });
}

/**
 * Includuje všechny potřebné javaScripty zadané v poli
 */
function IncludeJavascripts() {
  var scripts = [
    'lock'
  ];
  
  var $defaultOptions = {
    async: false,
    dataType: 'script'
  };
  
  $.each(scripts, function(key, script) {
    $.ajax($.extend({}, $defaultOptions, { url: 'resources/js/' + script + '.js'})).done(function(data, textStatus, jqxhr) {
      if (jqxhr.status !== 200) {
        debug.log('Script se nepodařilo načíst. Chyba: ' + textStatus, script + '.js');
      }
    });
  });
}

$(document).ready(function() {
  // prevent default behaviour of the blind links
  $('body').on('click', '[href="#"]', function(e) {
    e.preventDefault();
  });
  
  Metronic.init(); // init metronic core componets
  QuickSidebar.init(); // init quick sidebar
  Layout.init(); // init layout
  Index.init();
  
  Menu.setActiveMenuPath();
  pluginTinymce.init();
  
  CheckboxGroup.init();
  CheckableControlsHierarchy.init();
  TableRowCheckable.init();
  Lightbox.init();
  InputMaskPlugin.init();
  FormComponents.init();
  InitDefaultActionButtonInForm();
  activateToggleShowPassword('input[type="password"]:not(.notTogglePassword)');
  if (typeof LockScreenPlugin !== 'undefined') { LockScreenPlugin.init(); }
  bootbox.setLocale('cs');
  
  // udela radky spravovatelne tabulky klikatelnymi
  $('body').on('click', '.manage-table tbody tr', function(event) {
    var $target = $(event.target);

    $target.css({
      '-webkit-touch-callout': 'none',
      '-webkit-user-select': 'none',
      '-khtml-user-select': 'none',
      '-moz-user-select': 'none',
      '-ms-user-select': 'none',
      'user-select': 'none'
    });
    
    setTimeout(function() {
      $target.css({
        '-webkit-touch-callout': 'text',
        '-webkit-user-select': 'text',
        '-khtml-user-select': 'text',
        '-moz-user-select': 'text',
        '-ms-user-select': 'text',
        'user-select': 'text'
      });
    }, 1000);
  });
  
  /**
   * Obsluhuje dvojklik na řádek.
   */
  $('body').on('dblclick', '.manage-table tbody tr:not(.no_manage_row)', function(event) {
    var $row = $(this);
    var $target = $(event.target);
    
    if (!$target.is('a') && $target.closest('a').length === 0) {
      var $link = $row.find('.default_action:first-child').closest('a');
      if ($link.length) {
        $link.trigger('click');
        if ($link.attr('href') !== '#') {
          document.location.href = $link.attr('href');
        }
      }
    }

    event.stopPropagation();
  });
  
  // obsluha vypnute/zapnute ikonky
  $('body').on('click', '[data-confirm-text]:not(.disabled)', function(e) {
    e.preventDefault();
    var $element = $(this);
    var href = $element.attr('href');
    
    bootbox.confirm($(this).data('confirm-text'), function(result) {
      if (result) {
        window.location.href = href;
      }
    });
  });
  
  $('body').on('click', '.icon_link.disabled', function(e) {
    e.preventDefault();
    e.stopPropagation();
  });
  
  // aktualizace času v zhlaví stránky
  var $watches = $('#top_current_time');
  if ($watches.length) {
    TimePlugin.init($watches);
    
    var $calendar = $('#top_current_time_calendar');
    $calendar.datepicker($.extend({}, FormComponents.jQueryUIDatePickers.$defaultSettings, {
      beforeShow: function(input) {          
        debug.log($(input));
      }
    }));
    $calendar.css({
      top: $watches.outerHeight,
      right: '0px'
    });
    $watches.closest('a').click(function() {
      $calendar.slideToggle(500);
    });
  }
  
  // pridavani barev k elementum
  $('.manage-table, .form-control, .btn-group .btn, .filter-actions .btn, .pagination').filter(function(index) {
    return $(this).closest('.no_color_setter').length === 0 && $(this).not('.no_color_setter');
  }).colorSetter('.portlet');
  
  $('.manage-table a.sorting_link').shiftSelectable({
    name: function($item) {
      return $item.data('sortable-name');
    },
    getValue: function($item) {
      return $item.data('sortable-value');
    },
    setValue: function($item, value) {
      return $item.attr('data-sortable-value', value);
    }
  });
});