(function ($) {
  $.fn.customValidator = function(settings) {
    var $form = $(this);
    
    var defaultSettings = {
      errorClass: 'has-error',
      validClass: 'has-success',
      errorElement: 'span',
      onkeyup: false,
      ficusInvalid: true,
      errorPlacement: function($error, $element) {
        var messageId = $element.attr('name').replace('[', '_').replace(']', '_') + '_message';
        if (!$('#' + messageId).length) {
          $error.addClass('help-block')
            .addClass('help-block-error')
            .attr('id', messageId);
    
          if ($element.closest('.checkbox-list').length) {
            $error.insertAfter($element.closest('.checkbox-list'));
          } else {
            $error.insertAfter($element);
          }
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).closest('div.form-group').removeClass(validClass).addClass(errorClass);
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).closest('div.form-group').removeClass(errorClass).addClass(validClass);
      },
      success: function(label) {
        label.hide().remove();
      },
      focusInvalid: function() {
        // put focus on tinymce on submit validation
        if (this.settings.focusInvalid) {
          try {
            var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
            if (toFocus.is('textarea')) {
              tinyMCE.get(toFocus.attr('id')).focus();
            } else {
              toFocus.filter(':visible').focus();
            }
          } catch (e) {
            // ignore IE throwing errors when focusing hidden elements
          }
        }
      },
      submitHandler: function(form) {
        form.submit();
      }
    };
    
    $form.validate($.extend({}, defaultSettings, settings));
    
    $('.select2', $form).change(function () {
      $form.validate().element($(this));
    });
    
    // funkce pro kontrolu emailu
    function chechEmail(value, element) {
      var re = /^(([^<>()[\]\\.,;:$\*\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\"[a-zA-Z\-0-9]+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(value);
    }
    $.validator.addMethod('checkEmail', function(value, element) {
      return chechEmail(value, element);
    }, 'E-mail musí být ve formátu jmeno@domena.cz.');


    // funkce pro kontrolu vice emailu
    $.validator.addMethod('checkEmails', function(value, element) {
      var err = true;
      var emails = value.split(',');
      $.each(emails, function(key, email) {
        err = err && chechEmail(email.trim(), element);
      });

      return err;
    }, 'E-maily musí být ve formátu jmeno@domena.cz.');

    // funkce pro valicaci inputMask elementu
    $.validator.addMethod('inputmaskValid', function(value, element) {    
      var valid = true;
      valid = valid && $(element).data('isvalid') === 1;

      return valid;
    });
    
    /**
     * Kontrola validitiy vstupu pres PHP
     * 
     * @param {string} controller
     * @param {string} action
     * @param {bool} autoAssign
     */
    function enablePHPValidate(controller, action, autoAssign) {
      var CVMessage = '';
      $.validator.addMethod('checkValidate', function(value, element, params) {
        var isValid = false;
        var $element = $(element);
        
        tinyMCE.triggerSave();

        var data = {
          controller: controller,
          action: action,
          element: $element.attr('name')
        };
        
        $.ajax({
          url: 'index.php',
          dataType: 'json',
          method: 'POST',
          data: $.extend({}, $element.closest('form').serializeObject(), data),
          async: false,
          success: function(response) {
            CVMessage = response.message;
            isValid = response.validate;
          }
        }).fail(function(err, errDesc) {
          debug.log(err, errDesc, {'background-color': '#ff0000'});
        });
        
        return isValid;
      }, function(params, element) {
        return CVMessage;
      });
      
      autoAssign = ((typeof autoAssign === 'undefined') ? true : autoAssign);
      if (autoAssign) {
        // pridani role ke vsem prvkum formulare
        $('.form-control:not(div,span,i,b)', $form).each(function(key, $item) {
          $(this).rules('add', {
            checkValidate: true
          });
        });
      }
    }
    
    return {
      enablePHPValidate: enablePHPValidate
    };
  };
}(jQuery));