Highcharts.setOptions({
  lang: {
    contextButtonTitle: 'Nabídka',
    decimalPoint: ',',
    thousandsSep: ' ',
    downloadJPEG: 'Stahnout jako obrázek JPEG',
    downloadPDF: 'Stahnout jako PDF dokument',
    downloadPNG: 'Stahnout jako obrázek PNG',
    downloadSVG: 'Stahnout jako obrázek SVG',
    drillUpText: '◁ O úroveň výše',
    loading: 'Načítám...',
    noData: 'Žádná data k zobrazení',
    printChart: 'Tisk grafu',
    resetZoom: 'Zrušit přiblížení',
    resetZoomTitle: 'Zrušit příblížení',
    numericSymbols: [ "k" , "M" , "G" , "T" , "P" , "E"],
    months: [ "Leden" , "Únor" , "Březen" , "Duben" , "Květen" , "Červen" , "Červenec" , "Srpen" , "Září" , "Říjen" , "Litopad" , "Prosinec"],
    shortMonths: [ "Led" , "Úno" , "Bře" , "Dub" , "Kvě" , "Čer" , "Čvc" , "Srp" , "Zář" , "Říj" , "Lis" , "Pro"],
    weekdays: ["Neděle", "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota"]
  }
});