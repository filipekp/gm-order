<?php
  namespace jschart;

  /**
   * Trida generujici statistiku vyvoje v case.
   *
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class ChartTimeProgress extends Chart {
    private $_data = ['series' => array()];
    
    /**
     * Prida do statistiky novou serii.
     * 
     * @param string $name
     * @return ChartTimeProgress
     */
    public function addSerie($name, array $settings = []) {
      if (!array_key_exists($name, $this->_data['series'])) {
        $this->_data['series'][$name] = array(
          'name' => $name,
          'data' => [],
          'settings' => []
        );
      }
      
      if ($settings) {
        Functions::unsetItem($settings, 'data');
        Functions::unsetItem($settings, 'name');

        $this->_data['series'][$name]['settings'] = $settings;
      }
      
      return $this;
    }
    
    /**
     * Prida bod do serie.
     * 
     * @param string $serie
     * @param \DateTime $dateTime
     * @param int $value
     * 
     * @return \jschart\ChartTimeProgress
     */
    public function addPoint($serie, \DateTime $dateTime, $value) {
      if (!array_key_exists($serie, $this->_data['series'])) {
        throw new \Exception('Pozadovana serie `' . $serie . '` neexistuje.');
      }
      
      $this->_data['series'][$serie]['data'][] = ['date' => $dateTime, 'value' => $value];

      return $this;
    }
    
    /**
     * Setridi vsechny serie v casove posloupnosti vzestupne nebo sestupne.
     * 
     * @param string $direction
     * 
     * @throws \Exception
     */
    public function sortTimeProgress($direction = 'ASC') {
      throw new \Exception('Not implemented yet.');
    }

    /**
     * Vygeneruje data pro JS knihovnu.
     * 
     * @return array
     */
    public function generate() {
      $timeProgress = ['series' => array()];

      foreach ($this->_data['series'] as $serie) {
        $timeProgress['series'][] = array_merge(
          $serie['settings'],
          array(
            'name' => $serie['name'],
            'data' => array_map(function($point) {
              return array(
                Chart::FUNC . 'Date.UTC(' . $point['date']->format('Y') . ', ' . ($point['date']->format('m') - 1) . ', ' . $point['date']->format('d') . ')' . Chart::FUNC,
                $point['value']
              );
            }, $serie['data'])
          )
        );
      }

      return $timeProgress;
    }
  }
