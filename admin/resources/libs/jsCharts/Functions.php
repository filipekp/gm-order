<?php
  namespace jschart;

  /**
   * Description of Functions
   *
   * @author proclient
   */
  class Functions {
    /**
     * Dekoduje cestu na url od rootu webu.
     * 
     * @param string $baseDir cesta k adresari, ze ktereho je metoda volana - typicky __DIR__
     * @return string
     */
    public static function dirUrl($baseDir) {
      $server = filter_input_array(INPUT_SERVER);

      $docRoot  = preg_replace("!{$server['SCRIPT_NAME']}$!", '', $server['SCRIPT_FILENAME']);
      $baseUrl  = preg_replace("!^{$docRoot}!", '', $baseDir);
      
      $protocol = ((empty($server['HTTPS'])) ? 'http' : 'https');
      $port     = $server['SERVER_PORT'];
      $dispPort = (($protocol == 'http' && $port == 80 || $protocol == 'https' && $port == 443) ? '' : ":$port");
      
      $domain   = $server['SERVER_NAME'];

      return "$protocol://{$domain}{$dispPort}{$baseUrl}/";
    }

    /**
     * Oreze retezec po celych slovech.
     * 
     * @param string $string
     * @param int $countChar
     * 
     * @return string
     */
    public static function substringByWord($string, $countChar = 30) {
      $stringArr = explode(' ', $string);

      $finalString = $string;
      $lastItem = '';
      while (mb_strlen($finalString) > $countChar || mb_strlen($lastItem) == 1) {
        $finalString = implode(' ', $stringArr);
        $lastItem = array_pop($stringArr);
      }

      return (($finalString != $string) ? $finalString . ' ...' : $finalString);
    }
    
    /**
     * "Bezpecne" ziska prvek z pole -> zkontroluje existenci.
     * 
     * @param array $array
     * @param mixed $key
     * 
     * @return mixed
     */
    public static function item(array $array, $key) {
      return ((array_key_exists($key, $array)) ? $array[$key] : NULL);
    }
    
    /**
     * "Bezpecne" odebere prvek z pole -> zkontroluje existenci.
     * 
     * @param array $array
     * @param string $key
     * 
     * @return mixed
     */
    public static function unsetItem(array &$array, $key) {
      if (array_key_exists($key, $array)) {
        $value = ((is_object($array[$key])) ? clone $array[$key] : $key);
        unset($array[$key]);
        
        return $value;
      }
      
      return NULL;
    }
  }
