<?php
  spl_autoload_register(function ($class) {
    $namespace = explode('\\', $class);
    $classname = array_pop($namespace);
    
    $path = __DIR__ . '/' . $classname . '.php';
    if ($namespace && $namespace[0] === 'jschart' && file_exists($path)) {
      require_once $path;
    }
  });