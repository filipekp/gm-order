<?php
  namespace jschart;
  
  use jschart\Functions;

  /**
   * Abstraktni trida reprezentujici obecnou statistiku.<br />
   * Jedna se o knihovnu generujici JavaScriptovy kod.<br />
   * Kazdy potomek by mel mit prefix Chart: napr.: ChartBarHorizontal, ChartPie, ...<br />
   * <br />
   * <b>POZOR!</b> JS funkce, ktere se posilaji do jednotlivych vlastnosti objektu musi byt obalene v konstante Chart::FUNC
   *
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   */
  abstract class Chart {
    const PREFIX = 'jschart_graph_';
    const TITLE_LENGTH = 30;                // maximalni delka popisku na ose x
    const FUNC = '%FUNC%';
    
    const JS_ROOT = 'js/';                  // cesta k hlavnim javascriptum
    const JS_LANG = 'js/lang/';             // cesta k jazykovym mutacim
    
    const CREDITS = 'Procilent s.r.o.';
    const CREDITS_URL = 'http://www.proclient.cz/';

    private static $_JS_LIBS_INCLUDED = FALSE;    // udrzuje informaci o tom, zda byly do webu vlozeny potrebne JavaScripty
    private static $_NUMBER_OF_INSTANCES = 1;     // udrzuje informaci o poctu vytvorenych instanci
    
    protected $_id;
    private $_classCanvas;                        // nastavi tridu platna pro vypis grafu


    private $_json;       // vysledny JSON objekt predavany JS knihovne jako parametr

    private $_lang;
    private $_colors = [
      '#FF0000', '#FF8000', '#FFFF00', '#80FF00',
      '#00FF00', '#00FF80', '#00FFFF', '#007FFF',
      '#0000FF', '#7F00FF', '#FF00FF', '#FF0080',
      '#DF2020', '#DF8020', '#DFDF20', '#80DF20',
      '#20DF20', '#20DF80', '#20DFDF', '#207FDF',
      '#2020DF', '#7F20DF', '#DF20DF', '#DF2080', 
      '#BF4040', '#BF8040', '#BFBF40', '#80BF40',
      '#40BF40', '#40BF80', '#40BFBF', '#407FBF',
      '#4040BF', '#7F40BF', '#BF40BF', '#BF4080',
      '#9F6060', '#9F8060', '#9F9F60', '#809F60',
      '#609F60', '#609F80', '#609F9F', '#607F9F', 
      '#60609F', '#7F609F', '#9F609F', '#9F6080'
    ];
    private $_type;
    private $_chartSettings;
    private $_zoomType;
    private $_title;
    private $_subtitle;
    private $_xTitle;
    private $_xSettings;
    private $_yTitle;
    private $_ySettings;
    private $_tooltip;
    private $_enableLegend;
    private $_enableExporting;
    private $_plotOptions;
    
    /**
     * Inicializuje zakladni informace o objektu.
     */
    public function __construct() {
      $this->_id = self::PREFIX . self::$_NUMBER_OF_INSTANCES++;
      $this->_json = array();
      $this->_enableLegend = true;
      
      $this->classCanvas();
    }
    
    
    /**
     * Vygeneruje data grafu ve tvaru, ktery pozaduje JS knihovna.<br />
     * <b>POZOR!</b> data musi byt vygenerovana vc. properties, ktere pozaduje knihovna. Napr.:
     * <ul>
     *   <li>pro drilldown: ['series' => [...], 'drilldown' => [...]]</li>
     *   <li>pro time progress: ['series' => [...]]</li>
     *   <li>...</li>
     * </ul>
     */
    abstract public function generate();
    
    /**
     * Vygeneruje nastaveni grafu ve formatu, ktery odpovida JSON objektu predavaneho JS knihovne jako parametr.
     * @return array
     */
    private function generateSettings() {
      $settings = array(
        'colors'      => $this->_colors,
        'credits'     => ['text'    => self::CREDITS, 'href' => self::CREDITS_URL],
        'exporting'   => ['enabled' => (bool)$this->_enableExporting],
        'legend'      => ['enabled' => (bool)$this->_enableLegend],
        'plotOptions' => (($this->_plotOptions) ? $this->_plotOptions : array(
          'series' => array('borderWidth' => 0, 'dataLabels' => ['enabled' => TRUE])
        ))
      );
      
      if ($this->_type || $this->_zoomType || $this->_chartSettings) {
        $settings['chart'] = [];
        
        if ($this->_type)           { $settings['chart']['type']     = $this->_type;      }
        if ($this->_zoomType)       { $settings['chart']['zoomType'] = $this->_zoomType;  }
        if ($this->_chartSettings)  { $settings['chart'] = array_merge($settings['chart'], $this->_chartSettings);  }
      }
      
      if ($this->_title)    { $settings['title']    = ['text' => $this->_title];    }
      if ($this->_subtitle) { $settings['subtitle'] = ['text' => $this->_subtitle]; }
      
      if ($this->_xTitle || $this->_xSettings) {
        $settings['xAxis'] = [];
        
        if ($this->_xTitle)    { $settings['xAxis']['title'] = ['text' => $this->_xTitle];                         }
        if ($this->_xSettings) { $settings['xAxis']          = array_merge($settings['xAxis'], $this->_xSettings); }
      }
      
      if ($this->_yTitle || $this->_ySettings) {
        $settings['yAxis'] = [];
        
        if ($this->_yTitle)    { $settings['yAxis']['title'] = ['text' => $this->_yTitle];                         }
        if ($this->_ySettings) { $settings['yAxis']          = array_merge($settings['yAxis'], $this->_ySettings); }
      }
      
      if ($this->_tooltip) {
        $settings['tooltip'] = $this->_tooltip;
      }
      
      return $settings;
    }

    /**
     * Hlavni metoda pro vygenerovani JS kodu vykreslujiciho graf statistiky.<br />
     * <b>POZNAMKA:</b>
     * <pre>  metoda funguje jako abstraktni => musi byt pretizena s volanim try { parent::generate(); } catch (Exception $e) { } na zacatku kazde metody.</pre>
     * <pre>  abstraktni neni, protoze na zacatku vsech volani je nutne vlozit JS knihovny pro generovani grafu.</pre>
     * <pre>  -> skripty pro jednotlive potomky jsou v adresari ulozenem v konstante CHARTS_JS_ROOT a musi zacinat tak, jak se jmenuje dany potomek vyjma prefixu Chart</pre>
     * <pre>  -> napr.: barHorizontalScript1.js, barHorizontalDalsiScript.js pro potomka ChartBarHorizontal</pre>
     * 
     * @throws Exception
     */
    public function plot() {
      // vlozi hlavni JS knihovny
      if (!self::$_JS_LIBS_INCLUDED) {
        $selfDirUrl = Functions::dirUrl(__DIR__);
        echo '<script src="' . $selfDirUrl . self::JS_ROOT . 'highcharts.js"></script>' . PHP_EOL;
        echo '<script src="' . $selfDirUrl . self::JS_LANG . (($this->_lang) ? $this->_lang : 'cs_CZ') . '.js"></script>' . PHP_EOL;

        self::$_JS_LIBS_INCLUDED = TRUE;
      }
      
      $this->_json = array_merge($this->generateSettings(), $this->generate());
      
      // odstrani nove radky a nahradi vice mezer na jednu
      array_walk_recursive($this->_json, function(&$item) {
        $item = ((is_string($item)) ? preg_replace('/\s{2,}/', ' ', str_replace(array("\r\n", "\n", "\r"), ' ', $item)) : $item);
      });
      
      // v params prevede funkce z retezce do funkce
      $params = preg_replace('/"' . self::FUNC . ' ?(.*?) ?' . self::FUNC . '"/', '$1', json_encode($this->_json, JSON_PRETTY_PRINT));
      
      echo <<<PLOT
      <div class="{$this->_classCanvas}" id="{$this->_id}"></div>
      <script>
        function zoomDefault(chart){      
          var maxYPoint = 0;
          $.each(chart.series, function() {
            var serie = this;
            $.each(serie.points, function() {
              if (this.y > maxYPoint) {
                maxYPoint = this.y;
              }
            });
          });
      
          chart.yAxis[0].setExtremes(0,maxYPoint);    
          chart.showResetZoom();
        }
      
        $('#{$this->_id}').highcharts({$params}, function() { zoomDefault(this); });
      </script>
PLOT;
    }
    
    /**
     * Setter.
     * @param array $classes
     * @return \jschart\Chart
     */
    public function classCanvas($classes = array('chart')) {
      $this->_classCanvas = implode(' ', $classes);
      return $this;
    }

    /**
     * Setter.
     * @param string $lang
     * @return \jschart\Chart
     */
    public function lang($lang) {
      $this->_lang = $lang;
      return $this;
    }
    
    /**
     * Setter.
     * @param string $type
     * @return \jschart\Chart
     */
    public function type($type) {
      $this->_type = $type;
      return $this;
    }
    
    /**
     * Setter.
     * @param array $settings
     * @return \jschart\Chart
     */
    public function chartSettings($settings) {
      $this->_chartSettings = $settings;
      return $this;
    }
    
    /**
     * Setter.
     * @param string $type
     * @return \jschart\Chart
     */
    public function zoomType($zoomType) {
      $this->_zoomType = $zoomType;
      return $this;
    }

    /**
     * Setter.
     * @param string $title
     * @return \jschart\Chart
     */
    public function title($title) {
      $this->_title = $title;
      return $this;
    }

    /**
     * Setter.
     * @param string $subtitle
     * @return \jschart\Chart
     */
    public function subtitle($subtitle) {
      $this->_subtitle = $subtitle;
      return $this;
    }

    /**
     * Setter.
     * @param string $xTitle
     * @return \jschart\Chart
     */
    public function xTitle($xTitle) {
      $this->_xTitle = $xTitle;
      return $this;
    }

    /**
     * Setter.
     * @param array $xSettings
     * @return \jschart\Chart
     */
    public function xSettings(array $xSettings) {
      $this->_xSettings = $xSettings;
      return $this;
    }

    /**
     * Setter.
     * @param string $yTitle
     * @return \jschart\Chart
     */
    public function yTitle($yTitle) {
      $this->_yTitle = $yTitle;
      return $this;
    }

    /**
     * Setter.
     * @param array $ySettings
     * @return \jschart\Chart
     */
    public function ySettings(array $ySettings) {
      $this->_ySettings = $ySettings;
      return $this;
    }

    /**
     * Setter.
     * @param array $tooltip
     * @return \jschart\Chart
     */
    public function tooltip(array $tooltip) {
      $this->_tooltip = $tooltip;
      return $this;
    }
    
    /**
     * Setter.
     * @param bool $enableLegend
     * @return \jschart\Chart
     */
    public function enableLegend($enableLegend) {
      $this->_enableLegend = $enableLegend;
      return $this;
    }

    /**
     * Setter.
     * @param bool $enableExporting
     * @return \jschart\Chart
     */
    public function enableExporting($enableExporting) {
      $this->_enableExporting = $enableExporting;
      return $this;
    }

    /**
     * Setter.
     * @param array $plotOptions
     * @return \jschart\Chart
     */
    public function plotOptions(array $plotOptions) {
      $this->_plotOptions = $plotOptions;
      return $this;
    }
  }
