<?php
namespace jschart;

use jschart\Functions;

/**
 * Třída generujici statistiku pomoci sloupcovych grafu s proklikem.
 *
 * @author Pavel Filípek
 * @copyright (c) 2014, Proclient s.r.o.
 */
class ChartDrillDown extends Chart {
  private $_data;
  
  private $_serieName;

  /**
   * Inicializuje zakladni vlastnosti grafu.
   * @param array $data hierarchicky strukturovana data:
   * <pre>array(                                                </pre>
   * <pre>  (int)<i>topic_id</i> => array(                      </pre>
   * <pre>    'id'        => (int)<i>topic_id</i>               </pre>
   * <pre>    'subject'   => (int)<i>subject_id</i>             </pre>
   * <pre>    'name'      => (string)<i>topic_name</i>          </pre>
   * <pre>    'parent'    => (int)<i>topic_parent_id</i>        </pre>
   * <pre>    'children'  => [                                  </pre>
   * <pre>      (int)<i>topic_id</i> => array(                  </pre>
   * <pre>        'id'        => (int)<i>topic_id</i>           </pre>
   * <pre>        'subject'   => (int)<i>subject_id</i>         </pre>
   * <pre>        'name'      => (string)<i>topic_name</i>      </pre>
   * <pre>        'parent'    => (int)<i>topic_parent_id</i>    </pre>
   * <pre>        'children'  => (array)<i>[]</i>               </pre>
   * <pre>      )                                               </pre>
   * <pre>    ]                                                 </pre>
   * <pre>  ),                                                  </pre>
   * <pre>  ...                                                 </pre>
   * <pre>)                                                     </pre>
   */
  public function __construct($data) {
    parent::__construct();

    $this->_data = $data;
  }
  
  /**
   * Vygeneruje id jednoho uzlu.
   * 
   * @param Entity $node
   * @return string
   */
  private function drilldownId($node, $graphId) {
    return $graphId . '_id' . $node['id'];
  }

  /**
   * Vygeneruje data jednoho uzlu.
   * 
   * @param Entity $node
   * @return array
   */
  private function nodeData($node, $graphId) {
    $data = [
      'id'    => $node['id'],
      'name'  => Functions::substringByWord($node['name'], self::TITLE_LENGTH),
      'desc'  => $node['name'],
      'y'     => round($node['value'], 0)
    ];

    if ($node['children']) {
      $data['drilldown'] = $this->drilldownId($node, $graphId);
    }

    return $data;
  }
  
  /**
   * Projde strom do sirky a vraci data pro JS drilldown.<br />
   * <b>POZNAMKA:</b> poslat az rodice pro druhou uroven (vynechat z prvni urovne ty uzly, ktere nemaji potomky)
   * 
   * @param array $parents
   * @param array &$result
   */
  private function breadthFirstDrilldown($parents, &$result, $graphId) {
    $children = [];
    foreach ($parents as $parent) {
      if ($parent['children']) {
        $result[] = [
          'id' => $this->drilldownId($parent, $graphId),
          'name' => Functions::substringByWord($parent['name']),
          'desc' => $parent['name'],
          'data' => array_values(array_map(function($node) use ($graphId) {
            return $this->nodeData($node, $graphId);
          }, $parent['children']))
        ];

        $children = array_merge($children, $parent['children']);
      }
    }

    if ($children) {
      $this->breadthFirstDrilldown($children, $result, $graphId);
    }
  }
  
  /**
   * Vygeneruje data grafu generujici statistiku pomoci sloupcovych grafu s proklikem.
   * 
   * @return array asociativni pole pro JSON
   */
  public function generate() {
    // stahne rodice pro druhou uroven pro generovani drilldown dat
    $level2Parents = array_filter($this->_data, function($item) {
      return (bool)$item['children'];
    });

    // vygeneruje drilldown data
    $drilldownData = [];
    $this->breadthFirstDrilldown($level2Parents, $drilldownData, $this->_id);
      
    // vygeneruje vse potrebne pro JS statistiky
    $drilldown = [
      'series' => [[
        'data' => array_values(array_map(function($node) {
          return $this->nodeData($node, $this->_id);
        }, $this->_data)),
        'name' => (($this->_serieName) ? $this->_serieName : '')
      ]],
      'drilldown' => [
        'series' => $drilldownData
      ]
    ];
    
    return $drilldown;
  }
  
  /**
   * Nastavi nazev hlavni serie.
   * 
   * @param string $name
   */
  public function setSerieName($name) {
    $this->_serieName = $name;
  }
}
