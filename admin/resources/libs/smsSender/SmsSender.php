<?php
namespace SmsSender;

use prosys\core\common\Agents;

/*
 * Sms sendro for gate http://www.neogate.cz/
 */

/**
 * Description of smsSender
 *
 * @author Pavel Filípek
 * @copyright (c) 2014, Proclient s.r.o.
 */
class SmsSender {
  const API_URL = 'http://api.smsbrana.cz/smsconnect/http.php';
  
  private $_login;
  private $_password;  
  private $_preparedApiUrl;
  
  
  //?login=<váš_login>&password=<vaše_heslo>&action=send_sms&number=774884136&message=test'
  
  /**
   * @param string $login
   * @param string $password
   */
  public function __construct($login, $password) {
    $this->_login = $login;
    $this->_password = $password;
    
    $this->prepareSender();
  }
  
  /**
   * Nastavi url pro pouziti API.
   */
  private function prepareSender() {
    $this->_preparedApiUrl = self::API_URL . '?login=' . $this->_login . '&password=' . $this->_password . '&';
  }
  
  private function getError($err) {
    switch ($err) {
      case 2: return 'neplatný login';
      case 3: return 'neplatný hash nebo password (podle varianty zabezpečení přihlášení)';
      case 4: return 'neplatný time, větší odchylka času mezi servery než maximální akceptovaná v nastavení služby SMS Connect';
      case 5: return 'nepovolená IP, viz nastavení služby SMS Connect';
      case 6: return 'neplatný název akce';
      case 7: return 'tato sul byla již jednou za daný den použita';
      case 8: return 'nebylo navázáno spojení s databází';
      case 9: return 'nedostatečný kredit';
      case 10: return 'neplatné číslo příjemce SMS';
      case 11: return 'prázdný text zprávy';
      case 12: return 'SMS je delší než povolených 459 znaků';
      case 1: 
      default: return 'neznámá chyba';
    }
  }
  
  /**
   * Upozorni emailem na chybu.
   */
  private function notificationError($err) {
    /* @var $mailer \prosys\core\common\Mailer */
    $mailer = Agents::getAgent('Mailer', Agents::TYPE_COMMON);
    // odesle informacni email pokud nsatane pri odesilani SMS chyba
    if (in_array($err, array(2,3,4,5,8,9,11,12))) {
      $mailer->sendMail(
        Settings::getVariable('SHOWING', 'WEB_NAME') . ' - nedostatečný kredit pro SMS sender',
        'SMS sender vrátil chybu: ' . $this->getError($err),
        array(Settings::MAIL_ORDER => 'Objednávky ' . Settings::getVariable('SHOWING', 'WEB_NAME')),
        array(Settings::MAIL_ADMIN),
        array(),
        array(),
        'text/html'
      );
    }
  }

  /**
   * Pošle SMS zprávu na zadané číslo.
   * 
   * @param string $number
   * @param string $message
   * @param int $senderId
   * 
   * @throws \prosys\core\common\AppException
   * @return int
   */
  public function sendSms($number, $message, $senderId = NULL) {
    $params = array(
      'action'  => 'send_sms',
      'number'  => $number,
      'message' => $message
    );
    
    if (!is_null($senderId)) {
      $params['sender_id'] = $senderId;
    }
    
    $curl = curl_init();  
      curl_setopt($curl, CURLOPT_URL, $this->_preparedApiUrl . http_build_query($params));
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

      $response = curl_exec($curl);
    curl_close($curl);
    
    $xml = new \DOMDocument();
    $xml->loadXML($response);
    
    if (($err = (int)$xml->getElementsByTagName('err')->item(0)->nodeValue)) {
      $this->notificationError($err);
      throw new \prosys\core\common\AppException($this->getError($err), $err);
    }
    
    return (int)$xml->getElementsByTagName('sms_id')->item(0)->nodeValue;
  }
  
  /**
   * Vrati inbox z SMS brany.
   * 
   * @return string
   */
  public function getInboxSms() {
    $params = array(
      'action'  => 'inbox'
    );
    
    $curl = curl_init();  
      curl_setopt($curl, CURLOPT_URL, $this->_preparedApiUrl . http_build_query($params));
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

      $response = curl_exec($curl);
    curl_close($curl);
    
    return $response;
  }
}
