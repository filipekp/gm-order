/**
 * plugin.js
 *
 * Copyright, Alberto Peripolli
 * Released under Creative Commons Attribution-NonCommercial 3.0 Unported License.
 *
 * Contributing: https://github.com/trippo/ResponsiveFilemanager
 */

tinymce.PluginManager.add('previewhtml', function(editor) {
    
	function openhtml() {
		editor.focus(true);
		var title = 'Náhled textu';

		win = editor.windowManager.open({
			title: title,
			file: tinymce.baseURL + '/plugins/previewhtml/preview.php',
			width: 860,
			height: 570,
			inline: 1,
			resizable: true,
			maximizable: true,
      buttons : [
					{
						type:'container',
						html: 'created by: <a href="http://filipek-czech.cz" target="_blank">Bc. Pavel Filípek</a></span>'
					},
					{type: "spacer", flex: 1},
					{
						text: 'Zavřít',
            subtype: 'primary',
						onclick: function() {              
              win.close();
            }
					}
			]
		},
    {
      text: tinyMCE.activeEditor.getContent({format : 'raw'})
    });
	}
    
	editor.addButton('previewhtml', {
		icon: 'preview',
		tooltip: 'Zobrazit náhled textu',
		shortcut: 'Ctrl+Shift+V',
    onclick:openhtml
	});
        
	editor.addShortcut('Ctrl+Shift+V', '', openhtml);

	editor.addMenuItem('previewhtml', {
		icon: 'preview',
		text: 'Zobrazit náhled textu',
		shortcut: 'Ctrl+Shift+V',
		onclick: openhtml,
		context: 'insert'
	});
	
});