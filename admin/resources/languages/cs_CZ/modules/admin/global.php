<?php
  use prosys\core\common\Settings;

  // login formulář
  ${Settings::TRANSLATE_VAR}['labels']['login_form']['title']         = 'Přihlášení';
  ${Settings::TRANSLATE_VAR}['labels']['login_form']['login_btn']     = 'Přihlásit';
  ${Settings::TRANSLATE_VAR}['labels']['login_form']['user_name']     = 'Uživatelské jméno';
  ${Settings::TRANSLATE_VAR}['labels']['login_form']['password']      = 'Heslo';
  
  // hlášky
  ${Settings::TRANSLATE_VAR}['messages']['login']                 = 'Uživatel %s byl úspěšně přihlášen.';
  ${Settings::TRANSLATE_VAR}['messages']['logout']                = 'Uživatel byl ohlášen.';
  ${Settings::TRANSLATE_VAR}['messages']['not_allowed_login']     = 'Pod přihlašovacím jménem %s se nelze přihlásit do systému.';
  ${Settings::TRANSLATE_VAR}['messages']['incorrect_credentials'] = 'Přihlášení se nepodařilo.<br />Pravděpodobně špatně zadané přihlašovací údaje.';