<?php
  use prosys\core\common\Settings;

  ${Settings::TRANSLATE_VAR}['heading']['new']                = 'Nový produkt';
  ${Settings::TRANSLATE_VAR}['heading']['old']                = 'Úprava produktu &bdquo;%s&ldquo;';

  // tlačítka
  ${Settings::TRANSLATE_VAR}['button']['back']                = 'Zpět za seznam produktů';
  ${Settings::TRANSLATE_VAR}['button']['add']                 = 'Přidat';

  // popisky
  ${Settings::TRANSLATE_VAR}['labels']['product']['common_title']         = 'Společná nastavení';
  ${Settings::TRANSLATE_VAR}['labels']['product']['manufacturer']         = 'Výrobce';
  ${Settings::TRANSLATE_VAR}['labels']['product']['manufacturer_select']  = '--- vyberte výrobce ---';
  ${Settings::TRANSLATE_VAR}['labels']['product']['serie']                = 'Série';
  ${Settings::TRANSLATE_VAR}['labels']['product']['serie_select']         = '--- vyberte sérii ---';
  ${Settings::TRANSLATE_VAR}['labels']['product']['category']             = 'Kategorie';
  ${Settings::TRANSLATE_VAR}['labels']['product']['category_select']      = '--- vyberte kategorie ---';
  
  ${Settings::TRANSLATE_VAR}['labels']['product']['common_descriptions']  = 'Společné překlady';

  ${Settings::TRANSLATE_VAR}['labels']['product']['name']                 = 'Hlavní název produktu';
  ${Settings::TRANSLATE_VAR}['labels']['product']['description']          = 'Hlavní popis produktu';

  ${Settings::TRANSLATE_VAR}['labels']['variant']['name']                 = 'Název varianty';
  ${Settings::TRANSLATE_VAR}['labels']['variant']['description']          = 'Popis varianty';
  ${Settings::TRANSLATE_VAR}['labels']['variant']['is_default']           = 'Výchozí';
  ${Settings::TRANSLATE_VAR}['labels']['variant']['catalog_id']           = 'Katalogové číslo';
  ${Settings::TRANSLATE_VAR}['labels']['variant']['ean']                  = 'EAN';
  ${Settings::TRANSLATE_VAR}['labels']['variant']['date_available']       = 'Dostupné od';
  ${Settings::TRANSLATE_VAR}['labels']['variant']['date_added']           = 'Datum přidání';
  ${Settings::TRANSLATE_VAR}['labels']['variant']['date_modified']        = 'Datum editace';
  ${Settings::TRANSLATE_VAR}['labels']['variant']['delivery']             = 'Doba doručení';
  ${Settings::TRANSLATE_VAR}['labels']['variant']['delivery_unit_select'] = '--- vyberte jednotku ---';
  ${Settings::TRANSLATE_VAR}['labels']['variant']['accessibility']        = 'Dostupný';
  ${Settings::TRANSLATE_VAR}['labels']['variant']['accessibility_note']   = 'určuje přístupnost produktu (%s - neobjevuje se ve výpisech shopu ale dá se zobrazit, %s - objevuje se ve výpisech shopu)';
  ${Settings::TRANSLATE_VAR}['labels']['variant']['disabled']             = 'Aktivní';
  ${Settings::TRANSLATE_VAR}['labels']['variant']['disabled_note']        = 'určuje zda bude produkt dostupný na shopu';
  
  // popisky pro attributy
  ${Settings::TRANSLATE_VAR}['labels']['attributes']['add']                               = 'Přidání atributu';
  ${Settings::TRANSLATE_VAR}['labels']['attributes']['no_record']                         = 'Není žádný atribut pro variantu %s.';
  ${Settings::TRANSLATE_VAR}['labels']['attributes']['value']['placeholder']['input']     = 'Zadejte hodnotu atributu: %s';
  ${Settings::TRANSLATE_VAR}['labels']['attributes']['value']['placeholder']['select']    = '-- vyberte hodnotu atributu: %s --';

  // záložky
  ${Settings::TRANSLATE_VAR}['tab']['header']['title']        = 'Hlavička';
  ${Settings::TRANSLATE_VAR}['tab']['variants']['title']      = 'Varianty';
  ${Settings::TRANSLATE_VAR}['tab']['variants']['header']     = ${Settings::TRANSLATE_VAR}['tab']['header']['title'];
  ${Settings::TRANSLATE_VAR}['tab']['variants']['attributes'] = 'Atributy';
  ${Settings::TRANSLATE_VAR}['tab']['variants']['images']     = 'Obrázky';
  ${Settings::TRANSLATE_VAR}['tab']['variants']['add_title']  = 'Přidat variantu';

  // hlášky
  ${Settings::TRANSLATE_VAR}['message']['confirm_delete']           = 'Opravdu chcete smazat produkt %s?';
  ${Settings::TRANSLATE_VAR}['message']['images']['no_data']        = ['heading' => 'Žádné obrázky', 'message' => 'Varianta produktu nemá žádné obrázky.'];
  ${Settings::TRANSLATE_VAR}['message']['attributes']['no_data']    = ['heading' => 'Žádné attributy', 'message' => 'Varianta produktu nemá žádné attributy.'];
  
  // popisky pro obrazky
  ${Settings::TRANSLATE_VAR}['images']['heading']['image'] = 'Obrázek';
  ${Settings::TRANSLATE_VAR}['images']['heading']['name'] = 'Titulek';
  ${Settings::TRANSLATE_VAR}['images']['heading']['description'] = 'Popisek';
  ${Settings::TRANSLATE_VAR}['images']['heading']['mime'] = 'MIME';
  ${Settings::TRANSLATE_VAR}['images']['heading']['size'] = 'Velikost';
  ${Settings::TRANSLATE_VAR}['images']['heading']['original'] = 'Původní název';
  