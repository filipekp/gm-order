<?php
  use prosys\core\common\Settings;

  ${Settings::TRANSLATE_VAR}['heading']['new']                = 'Nový uživatel';
  ${Settings::TRANSLATE_VAR}['heading']['old']                = 'Úprava uživatele &bdquo;%s&ldquo;';
