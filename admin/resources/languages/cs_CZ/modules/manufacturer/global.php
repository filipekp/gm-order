<?php
  use prosys\core\common\Settings;

  ${Settings::TRANSLATE_VAR}['messages']['not_found'] = 'Nepodařilo se najít výrobce.';
  ${Settings::TRANSLATE_VAR}['messages']['found']     = 'Byl nalezen výrobce: %s.';