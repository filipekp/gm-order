<?php
use prosys\core\common\Settings;

${Settings::TRANSLATE_VAR}['code']                          = 'sk';
${Settings::TRANSLATE_VAR}['direction']                     = 'ltr';
${Settings::TRANSLATE_VAR}['decimal_point']                 = ',';
${Settings::TRANSLATE_VAR}['thousands_separator']           = ' ';

// datumové a časové nastaveni + popisy
${Settings::TRANSLATE_VAR}['datetime']['date_format_short']   = 'd.m.Y';
${Settings::TRANSLATE_VAR}['datetime']['date_format_short_js']= 'dd.mm.yy';
${Settings::TRANSLATE_VAR}['datetime']['date_format_long']    = 'l dS F Y';
${Settings::TRANSLATE_VAR}['datetime']['time_format']         = 'H:i:s';
${Settings::TRANSLATE_VAR}['datetime']['time_format_js']      = 'HH:mm:ss';
${Settings::TRANSLATE_VAR}['datetime']['datetime_format']     = 'd.m.Y H:i:s';

${Settings::TRANSLATE_VAR}['datetime']['week_title']          = ['short' => 'Týd.', 'long' => 'Týden'];
${Settings::TRANSLATE_VAR}['datetime']['month_title']         = ['short' => 'Měs.', 'long' => 'Měsíc'];
${Settings::TRANSLATE_VAR}['datetime']['year_title']          = ['short' => 'R', 'long' => 'Rok'];

${Settings::TRANSLATE_VAR}['datetime']['months']['januar']    = ['short' => 'Led', 'long' => 'Leden'];
${Settings::TRANSLATE_VAR}['datetime']['months']['februar']   = ['short' => 'Úno', 'long' => 'Únor'];
${Settings::TRANSLATE_VAR}['datetime']['months']['march']     = ['short' => 'Bře', 'long' => 'Březen'];
${Settings::TRANSLATE_VAR}['datetime']['months']['april']     = ['short' => 'Dub', 'long' => 'Duben'];
${Settings::TRANSLATE_VAR}['datetime']['months']['may']       = ['short' => 'Kvě', 'long' => 'Květen'];
${Settings::TRANSLATE_VAR}['datetime']['months']['june']      = ['short' => 'Čer', 'long' => 'Červen'];
${Settings::TRANSLATE_VAR}['datetime']['months']['july']      = ['short' => 'Čvc', 'long' => 'Červenec'];
${Settings::TRANSLATE_VAR}['datetime']['months']['august']    = ['short' => 'Srp', 'long' => 'Srpen'];
${Settings::TRANSLATE_VAR}['datetime']['months']['september'] = ['short' => 'Zář', 'long' => 'Září'];
${Settings::TRANSLATE_VAR}['datetime']['months']['october']   = ['short' => 'Říj', 'long' => 'Říjen'];
${Settings::TRANSLATE_VAR}['datetime']['months']['november']  = ['short' => 'Lis', 'long' => 'Listopad'];
${Settings::TRANSLATE_VAR}['datetime']['months']['december']  = ['short' => 'Pro', 'long' => 'Prosinec'];

${Settings::TRANSLATE_VAR}['datetime']['days']['monday']    = ['short' => 'Po', 'long' => 'Pondělí', 'min' => 'P'];
${Settings::TRANSLATE_VAR}['datetime']['days']['tuesday']   = ['short' => 'Út', 'long' => 'Úterý', 'min' => 'Ú'];
${Settings::TRANSLATE_VAR}['datetime']['days']['wednesday'] = ['short' => 'St', 'long' => 'Středa', 'min' => 'S'];
${Settings::TRANSLATE_VAR}['datetime']['days']['thursday']  = ['short' => 'Čt', 'long' => 'Čtvrtek', 'min' => 'Č'];
${Settings::TRANSLATE_VAR}['datetime']['days']['friday']    = ['short' => 'Pá', 'long' => 'Pátek', 'min' => 'P'];
${Settings::TRANSLATE_VAR}['datetime']['days']['saturday']  = ['short' => 'So', 'long' => 'Sobota', 'min' => 'S'];
${Settings::TRANSLATE_VAR}['datetime']['days']['sunday']    = ['short' => 'Ne', 'long' => 'Neděle', 'min' => 'N'];

${Settings::TRANSLATE_VAR}['datetime']['am']                = ['short' => 'dop.', 'long' => 'dopoledne'];
${Settings::TRANSLATE_VAR}['datetime']['pm']                = ['short' => 'odp.', 'long' => 'odpoledne'];

// texty
${Settings::TRANSLATE_VAR}['text']['yes']                   = 'Áno';
${Settings::TRANSLATE_VAR}['text']['no']                    = 'Nie';
${Settings::TRANSLATE_VAR}['text']['enabled']               = ['short' => 'Zap', 'long' => 'Zapnuto'];
${Settings::TRANSLATE_VAR}['text']['disabled']              = ['short' => 'Vyp', 'long' => 'Vypnuto'];
${Settings::TRANSLATE_VAR}['text']['none']                  = ' --- Žádný --- ';
${Settings::TRANSLATE_VAR}['text']['select']                = ' --- Prosím vyberte%s --- ';
${Settings::TRANSLATE_VAR}['text']['select_all']            = 'Označit vše';
${Settings::TRANSLATE_VAR}['text']['unselect_all']          = 'Odznačit vše';
${Settings::TRANSLATE_VAR}['text']['default']               = '<b>(Výchozí)</b>';
${Settings::TRANSLATE_VAR}['text']['close']                 = 'Zavřít';
${Settings::TRANSLATE_VAR}['text']['loading']               = 'Načítám ...';
${Settings::TRANSLATE_VAR}['text']['no_results']            = 'Žádné výsledky';
${Settings::TRANSLATE_VAR}['text']['confirm']               = 'Souhlasíte ?';
${Settings::TRANSLATE_VAR}['text']['home']                  = 'Domů';
${Settings::TRANSLATE_VAR}['text']['mandatory']             = 'Povinné položky';

${Settings::TRANSLATE_VAR}['text']['timezone']              = 'Časová zóna';

${Settings::TRANSLATE_VAR}['text']['save_title']            = 'Uložit a vrátit se na přehled';
${Settings::TRANSLATE_VAR}['text']['use_title']             = 'Uložit změny';
${Settings::TRANSLATE_VAR}['text']['cancel_title']          = 'Zrušit a vrátit se na přehled';
${Settings::TRANSLATE_VAR}['text']['back_title']            = 'Zpět';

// tlačítka
${Settings::TRANSLATE_VAR}['button']['insert']              = 'Přidat nový';
${Settings::TRANSLATE_VAR}['button']['add']                 = 'Přidat nový';
${Settings::TRANSLATE_VAR}['button']['delete']              = 'Smazat';
${Settings::TRANSLATE_VAR}['button']['save']                = 'Uložiť';
${Settings::TRANSLATE_VAR}['button']['use']                 = 'Použít';
${Settings::TRANSLATE_VAR}['button']['cancel']              = 'Zrušit';
${Settings::TRANSLATE_VAR}['button']['continue']            = 'Pokračovat';
${Settings::TRANSLATE_VAR}['button']['next']                = 'Další';
${Settings::TRANSLATE_VAR}['button']['prev']                = 'Předchozí';
${Settings::TRANSLATE_VAR}['button']['clear']               = 'Vymazat';
${Settings::TRANSLATE_VAR}['button']['close']               = 'Zavřít';
${Settings::TRANSLATE_VAR}['button']['enable']              = ['short' => 'Zap', 'long' => 'Zapnout'];
${Settings::TRANSLATE_VAR}['button']['disable']             = ['short' => 'Vyp', 'long' => 'Vypnout'];
${Settings::TRANSLATE_VAR}['button']['filter']              = 'Filtr';
${Settings::TRANSLATE_VAR}['button']['send']                = 'Odeslat';
${Settings::TRANSLATE_VAR}['button']['edit']                = 'Upravit';
${Settings::TRANSLATE_VAR}['button']['copy']                = 'Kopírovat';
${Settings::TRANSLATE_VAR}['button']['back']                = 'Zpět';
${Settings::TRANSLATE_VAR}['button']['remove']              = 'Smazat';
${Settings::TRANSLATE_VAR}['button']['refresh']             = 'Obnovit';
${Settings::TRANSLATE_VAR}['button']['backup']              = 'Backup';
${Settings::TRANSLATE_VAR}['button']['restore']             = 'Restore';
${Settings::TRANSLATE_VAR}['button']['download']            = 'Stáhnout';
${Settings::TRANSLATE_VAR}['button']['rebuild']             = 'Obnovit';
${Settings::TRANSLATE_VAR}['button']['upload']              = 'Nahrát';
${Settings::TRANSLATE_VAR}['button']['submit']              = 'Odeslat';
${Settings::TRANSLATE_VAR}['button']['image_add']           = 'Přidat obrázek';
${Settings::TRANSLATE_VAR}['button']['reset']               = 'Reset';
${Settings::TRANSLATE_VAR}['button']['generate']            = 'Generovat';
${Settings::TRANSLATE_VAR}['button']['folder']              = 'Nová složka';
${Settings::TRANSLATE_VAR}['button']['search']              = 'Hledat';
${Settings::TRANSLATE_VAR}['button']['view']                = 'Zobrazit';
${Settings::TRANSLATE_VAR}['button']['now']                 = 'Nyní';

// datetime picker
${Settings::TRANSLATE_VAR}['datetime_picker']['time']       = 'Čas';
${Settings::TRANSLATE_VAR}['datetime_picker']['time_title'] = 'Čas';
${Settings::TRANSLATE_VAR}['datetime_picker']['hour']       = 'Hod';
${Settings::TRANSLATE_VAR}['datetime_picker']['min']        = 'Min';
${Settings::TRANSLATE_VAR}['datetime_picker']['sec']        = 'Sec';
${Settings::TRANSLATE_VAR}['datetime_picker']['msec']       = 'ms';

// select2
${Settings::TRANSLATE_VAR}['select2']['message']['no_items']            = 'Nenalezeny žádné položky';
${Settings::TRANSLATE_VAR}['select2']['message']['min_1']               = 'Prosím zadejte ještě jeden znak';
${Settings::TRANSLATE_VAR}['select2']['message']['min_2_4']             = 'Prosím zadejte ještě další %s znaky';
${Settings::TRANSLATE_VAR}['select2']['message']['min_5']               = 'Prosím zadejte ještě dalších %s znaků';
${Settings::TRANSLATE_VAR}['select2']['message']['max_1']               = 'Prosím zadejte o jeden znak méně';
${Settings::TRANSLATE_VAR}['select2']['message']['max_2_4']             = 'Prosím zadejte o %s znaky méně';
${Settings::TRANSLATE_VAR}['select2']['message']['max_5']               = 'Prosím zadejte o %s znaků méně';
${Settings::TRANSLATE_VAR}['select2']['message']['select_max_1']        = 'Můžete zvolit jen jednu položku';
${Settings::TRANSLATE_VAR}['select2']['message']['select_max_2_4']      = 'Můžete zvolit maximálně %s položky';
${Settings::TRANSLATE_VAR}['select2']['message']['select_max_5']        = 'Můžete zvolit maximálně %s položek';
${Settings::TRANSLATE_VAR}['select2']['message']['loading']             = 'Načítají se další výsledky...';
${Settings::TRANSLATE_VAR}['select2']['message']['searching']           = 'Vyhledávání...';