<?php
  /* STARTS SESSION */
  session_start();

  /* SET MB_ENCODING  */
  mb_internal_encoding('UTF-8');
  
  /* AUTOLOAD */
  require_once '../../__load__.php';

  use prosys\core\common\Agents,
      prosys\core\common\Functions,
      prosys\core\Translator;

  /* @var $languageDao \prosys\model\LanguageDao */
  $languageDao = Agents::getAgent('LanguageDao', Agents::TYPE_MODEL);

  $get = (array)filter_input_array(INPUT_GET);
  
  $module = Functions::item($get, 'module');
  $activity = Functions::item($get, 'activity');
  $language = Functions::item($get, 'language');
  
  $languageEntity = (($language) ? $languageDao->load($language) : $languageDao->loadDefaultLanguage());

  header('Content-Type: application/javascript; charset=utf-8');
  echo 'var __TRANSLATIONS = ' . json_encode(Translator::getTranslation($module, $activity, $languageEntity) , JSON_PRETTY_PRINT);
  exit();
