<?php
namespace prosys\admin\security;

use prosys\core\common\Agents,
    prosys\core\mapper\SqlFilter,
    prosys\model\ModuleActionEntity,
    prosys\core\common\Settings,
    prosys\core\common\Functions,
    prosys\model\LoginEntity;

/**
 * Description of SecurityEngine
 *
 * @author Pavel Filípek
 */
class SecurityEngine extends \prosys\core\ProSYSLayer {
  /** @var \prosys\model\GroupRightDao */
  private $_rightDao;
  /** @var \prosys\model\UserEntity*/
  private $_user;
  /** @var prosys\model\GroupRightEntity[]*/
  private $_rightsAction;
  /** @var prosys\model\GroupRightEntity[]*/
  private $_rightsActivity;
  /** @var prosys\model\GroupRightEntity[]*/
  private $_rightsOther;
  
  // regularni vyraz, ktery vylouci aktivitu z kontroly a vrati TRUE
  private $_regExp = '~(e\d+)|(.*Screen)|(^All_.*)~';


  public function __construct() {
    parent::__construct();
    $this->_user = self::ProSYS()->loggedUser();
    
    // initialize GroupRightDao
    $this->_rightDao = Agents::getAgent('GroupRightDao', Agents::TYPE_MODEL);
    
    // load all rights for user
    $rights = $this->_rightDao->loadRecords(
      SqlFilter::create()
        ->inFilter('id_group', 
          SqlFilter::create()
            ->filter('id_group', 'user_group',
              SqlFilter::create()
                ->compare('user_id', '=', $this->_user->id)))
    );
    
    // load action rights
    $this->_rightsAction = array_filter($rights, function($right) {
      return $right->action->type === ModuleActionEntity::TYPE_ACTION;
    });
    
    // load activity rights
    $this->_rightsActivity = array_filter($rights, function($right) {
      return $right->action->type === ModuleActionEntity::TYPE_ACTIVITY;
    });
    
    // load other rights
    $this->_rightsOther = array_filter($rights, function($right) {
      return !in_array($right->action->type, [ModuleActionEntity::TYPE_ACTION, ModuleActionEntity::TYPE_ACTIVITY]);
    });
  }
  
  /**
   * Return TRUE when user has rights.
   * 
   * @param \prosys\model\GroupRightEntity[] $rights
   * @param string $module
   * @param string $action
   * @param array $query
   * 
   * @return boolean
   */
  private function hasAuthorization($rights, $module, $action, $query, &$pageName) {
    /* @var $moduleActionDao \prosys\model\ModuleActionDao */
    $moduleActionDao = Agents::getAgent('ModuleActionDao', Agents::TYPE_MODEL);
    $moduleAction = $moduleActionDao->loadByModuleAndAction($module, $action);
    
    if (!$moduleAction) { return TRUE; } 
    $pageName = $moduleAction->title;
    
    return (bool)array_filter($rights, function($right) use ($module, $action, &$query) {
      $allowedQueries = json_decode($right->allowedQueries, true);
      
      $allowed = TRUE;
      if ($allowedQueries) {
        foreach ($allowedQueries as $param => $regexArr) {
          if (array_key_exists($param, $query)) {
            if (is_array($query[$param])) {
              foreach ($query[$param] as $idx => $value) {
                $allowedCurrent = (bool)array_filter($regexArr, function($regex) use ($value) {
                  return preg_match(Settings::REGEX_START_END_CHAR . $regex . Settings::REGEX_START_END_CHAR, $value);
                });
                
                if (!$allowedCurrent) {
                  unset($query[$param][$idx]);
                }
              }
              
              $allowed = $allowed && (bool)$query[$param];
            } else {
              $allowed = $allowed && (bool)array_filter($regexArr, function($regex) use ($query, $param) {
                return preg_match('@' . $regex . '@', $query[$param]);
              });
            }
          } else {
            $allowed = FALSE;
            break;
          }
        }
      }
      
      return $right->action->module->module === $module && $right->action->name === $action && $allowed;
    });
  }
  
  /**
   * Authorize controller and action.
   * 
   * @param string $controller
   * @param string $action
   * @param string $query
   * @param string &$pageName
   * 
   * @return boolean
   */
  public function authorizeAction($controller, $action, $query, &$pageName) {
    // exception for error
    if (preg_match($this->_regExp, $action)) { return TRUE; }
    
    $authorize = $this->hasAuthorization($this->_rightsAction, $controller, $action, $query, $pageName);
    
    // overi pokud ma uzivatel uzamceny ucet tak vraci FALSE
    if (Functions::item($_SESSION, LoginEntity::LOCK_SCREEN)) {
      $pageName = 'z důvodu uzamčení účtu';
      return FALSE;
    }
    
    return $authorize;
  }
  
  /**
   * Authorize module and activity.
   * 
   * @param string $module
   * @param string $activity
   * @param string $query
   * @param string &$pageName
   * 
   * @return boolean
   */
  public function authorizeActivity($module, $activity, $query, &$pageName) {
    // exception for error
    if (preg_match($this->_regExp, $activity)) { return TRUE; }
    
    return $this->hasAuthorization($this->_rightsActivity, $module, $activity, $query, $pageName);
  }
  
  /**
   * Authorize other rights.
   * 
   * @param string $module
   * @param string $activity
   * @param string $query
   * @param string &$pageName
   * 
   * @return boolean
   */
  public function authorizeOtherRights($module, $activity, $query, &$pageName) {
    // exception for error
    if (preg_match($this->_regExp, $activity)) { return TRUE; }
    
    return $this->hasAuthorization($this->_rightsOther, $module, $activity, $query, $pageName);
  }
}
