<?php
  namespace prosys\admin\view;

  use prosys\core\common\Settings,
      prosys\core\common\Functions,
      prosys\core\common\Agents;
  
  /* INCLUDES SMARTY TEMPLATE LIBRARY */
  require_once __DIR__ . '/../resources/libs/Smarty/Smarty.class.php';

  /**
   * Abstract class which should be the parent of every "viewable" classes.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  abstract class View extends \prosys\core\ProSYSLayer
  {
    /** @var \Smarty $_smarty */
    protected static $_SMARTY = NULL;
    public $_moduleName;

    /**
     * Initializes view with prop labels
     */
    public function __construct() {
      parent::__construct();
      
      // nastavi nazev modulu
      $calledModuleArr = explode('\\', get_called_class());
      $calledModule = array_pop($calledModuleArr);
      $this->_moduleName = lcfirst(str_replace('View', '', $calledModule));
      
      // ulozi Smarty
      self::initSmarty();
    }
    
    /**
     * Inicializuje smarty.
     */
    protected static function initSmarty() {
      if (is_null(self::$_SMARTY)) {
        self::$_SMARTY = Agents::getAgent('Smarty', Agents::TYPE_STD, NULL, 'Smarty');
        self::$_SMARTY->debugging = TRUE;
        self::$_SMARTY->caching = TRUE;
        self::$_SMARTY->cache_lifetime = 120;

        $smartyRoot = Settings::SYSTEM_STORAGE_PATH . Settings::SMARTY_DIR;
        self::$_SMARTY->setCacheDir($smartyRoot . Settings::SMARTY_CACHE);
        self::$_SMARTY->setCompileDir($smartyRoot . Settings::SMARTY_COMPILE);
        self::$_SMARTY->addPluginsDir($smartyRoot . Settings::SMARTY_PLUGINS);

        $prosysData = array(
          'CURRENT_LOGIN' => self::ProSYS()->login(),
          'GLOBAL'        => self::ProSYS(),
          
          'SETTINGS'      => self::ProSYS()->settings(),
          'VARIABLES'     => self::ProSYS()->variables(self::$_SMARTY)
        );

        self::$_SMARTY->assign('ProSYS', $prosysData);
      }
    }
    
    /**
     * Vrati instanci smarty
     * 
     * @return \Smarty
     */
    public static function getSmarty() {
      self::initSmarty();
      
      return self::$_SMARTY;
    }
    
    /**
     * Vrati cas v retezci
     */
    public static function diffTimesToString(\DateTime $datetime, \DateTime $default = NULL) {
      $default = ((is_null($default)) ? new \DateTime() : $default);
      
      /* @var $difference \DateTime */
      $difference = $default->diff($datetime);

      $string = '';
      if ($difference->y > 0) {
        $string = Functions::inflection($difference->y, 'let', 'rok', 'roky', 'roku');
      } else if ($difference->m > 0) {
        $string = Functions::inflection($difference->m, 'měsíců', 'měsíc', 'měsíce', 'měsíce');
      } else if ($difference->d > 0) {
        $string = Functions::inflection($difference->d, 'dní', 'den', 'dny', 'dne');
      } else if ($difference->h > 0) {
        $string = Functions::inflection($difference->h, 'hodin', 'hodina', 'hodiny', 'hodiny');
      } else if ($difference->i > 0) {
        $string = Functions::inflection((int)$difference->i, 'minut', 'minuta', 'minuty', 'minuty');
      } else {
        $string = Functions::inflection((int)$difference->s, 'sekund', 'sekunda', 'sekundy', 'sekundy');
      }
      
      return $string;
    }
  }
