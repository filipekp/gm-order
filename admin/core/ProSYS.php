<?php
  namespace prosys\core;
  
  use prosys\core\common\Settings,
      prosys\core\common\ConfigParser,
      prosys\core\common\Agents;

  /**
   * Reprezentuje obecny systemovy objekt.
   * 
   * @property-read array $config
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class ProSYS {
    const DEFAULT_MODULE = 'admin';
    const DEFAULT_ACTIVITY = 'initial';
    
    private $_global = array(
      'CONFIG'        => [],
      'LOGIN'         => NULL,
      'LOGGED_USER'   => NULL,
      'CONTROLLER'    => NULL,
      'MODULE'        => NULL,
      'ACTIVITY'      => NULL,
      'SETTINGS'      => NULL,
      'VARIABLES'     => NULL
    );
    
    public function __construct() {
      $this->_global['CONTROLLER'] = (($controllerP = filter_input(INPUT_POST, 'controller', FILTER_SANITIZE_STRING)) ? $controllerP : filter_input(INPUT_GET, 'controller', FILTER_SANITIZE_STRING));
      $this->_global['MODULE'] = filter_input(INPUT_GET, 'module', FILTER_SANITIZE_STRING, array('options' => array('default' => self::DEFAULT_MODULE)));
      $this->_global['ACTIVITY'] = filter_input(INPUT_GET, 'activity', FILTER_SANITIZE_STRING, array('options' => array('default' => self::DEFAULT_ACTIVITY)));
      
      $parser = new ConfigParser(Settings::ROOT_PATH . '/config.ini');
      $this->_global['CONFIG'] = $parser->config();
    }
    
    /**
     * Vrati vsechna globalni data systemu.
     * @return array
     */
    public function data() {
      return $this->_global;
    }
    
    /**
     * Ulozi aktualne prihlaseneho uzivatele do systemu.
     * 
     * @param \prosys\model\LoginEntity $login
     * @return \prosys\core\ProSYS
     */
    public function storeLogin(\prosys\model\LoginEntity $login) {
      $this->_global['LOGIN'] = $login;
      $this->_global['LOGGED_USER'] = $login->user;
      
      return $this;
    }
    
    /**
     * Vrati informaci, pod kterym loginem je aktualni uzivatel prihlasen.
     * @return \prosys\model\LoginEntity
     */
    public function login() {
      return $this->_global['LOGIN'];
    }
    
    /**
     * Vrati aktualne prihlaseneho uzivatele.
     * @return \prosys\model\UserEntity
     */
    public function loggedUser() {
      return $this->_global['LOGGED_USER'];
    }
    
    /**
     * Vrati jazyk prekladu uzivatele.
     * @return string
     */
    public function language() {
      /* @var $dao \prosys\model\LanguageDao */
      $dao = Agents::getAgent('LanguageDao', Agents::TYPE_MODEL);

      return (($this->loggedUser()) ? $this->loggedUser()->language : $dao->loadDefaultLanguage());
    }
    
    /**
     * Vrati aktualni preklad jazyka.
     * 
     * @param string $module
     * @param string $activity
     * @return array
     */
    public function languageTranslations($module = NULL, $activity = NULL) {
      $moduleTrans = (($this->_global['CONTROLLER']) ? $this->_global['CONTROLLER'] : $this->_global['MODULE']);
      
      return Translator::getTranslation($moduleTrans, $activity, $this->language());
    }
    
    /**
     * Vrati aktualne nacteny modul.
     * @return string
     */
    public function module() {
      return $this->_global['MODULE'];
    }
    
    /**
     * Vrati aktualne nactenou aktivitu.
     * @return string
     */
    public function activity() {
      return $this->_global['ACTIVITY'];
    }
    
    /**
     * Vrati pole konstant tridy Settings.
     * @return array
     */
    public function settings() {
      if (is_null($this->_global['SETTINGS'])) {
        $this->_global['SETTINGS'] = Settings::getConstants();
      }
      
      return $this->_global['SETTINGS'];
    }
    
    /**
     * Vrati pole promennych systemu.
     * @return array
     */
    public function variables(\Smarty &$smarty = NULL) {
      if (is_null($this->_global['VARIABLES'])) {
        $this->_global['VARIABLES'] = Settings::getVariables($smarty);
      }
      
      return $this->_global['VARIABLES'];
    }
    
    /**
     * "Magicky" getter pro ziskani konfigurace systemu.<br />
     * Kvuli primemu pristupu pres hranate zavorky.
     * 
     * @param string $prop
     * @return array
     */
    public function __get($prop) {
      if ($prop === 'config') {
        return $this->_global['CONFIG'];
      }
    }
  }
  