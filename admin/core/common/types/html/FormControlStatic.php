<?php
  namespace prosys\core\common\types\html;
  
  use prosys\core\common\Functions,
      prosys\core\common\AppException;

  /**
   * Reprezentuje statickou hodnotu ve formulari.
   * 
   * @author Pavel Filípek <filipek@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class FormControlStatic extends Element {
    /** @var Element */
    private $_bold = NULL;
    
    /**
     * Inicializuje zakladni data odkazu.
     */
    public function __construct($content) {
      parent::__construct('div', TRUE);
      
      $this->_bold = new Bold($content);
    }
    
    /**
     * Inicializuje url odkazu.
     * @return string
     */
    public function init() {
      $this->addClass('form-control-static');
      
      $this->_content = (string)$this->_bold;
    }
  }
