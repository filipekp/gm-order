<?php
  namespace prosys\core\common\types\html;

  /**
   * Reprezentuje formularove vstupni pole typu password.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class InputPassword extends Input {
    /**
     * Nastavi vstupni pole typu password.
     * @param string $name
     */
    public function __construct($name) {
      parent::__construct($name);
    }
    
    /**
     * Inicializuje vstupni pole typu password. 
     */
    protected function init() {
      parent::init();

      $this->_attributes['type'] = 'password';
    }
  }
  