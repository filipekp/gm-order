<?php
  namespace prosys\core\common\types\html;
  
  use prosys\core\common\Functions,
      prosys\core\common\AppException;

  /**
   * Abstraktni trida. Reprezentuje obecny HTML odkaz.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  abstract class Link extends Element {
    const TARGET_BLANK = '_blank';
    const TARGET_PARENT = '_parent';
    const TARGET_TOP = '_top';
    
    /**
     * Inicializuje zakladni data odkazu.
     */
    public function __construct($content) {
      parent::__construct('a', TRUE);
      
      $this->_content = (string)$content;
      $this->_attributes += [
        'href'     => '',
        'target'   => ''
      ];
    }

    abstract protected function url();
    
    /**
     * Inicializuje url odkazu.
     * @return string
     */
    public function init() {
      $this->_attributes['href'] = $this->url();
    }

    /**
     * Setter / Getter.
     * 
     * @param string $target nepovinny: neni-li uveden, jedna se o getter, jinak o setter - akceptuje jednu z konstant Link::TARGET_*
     * @return \prosys\core\common\types\html\Link | string
     */
    public function target($target = '') {
      if (func_num_args()) {
        if (!Functions::isConstantByPrefix($target, __CLASS__, 'TARGET_')) {
          throw new AppException('"' . $target . '" neni validním targetem odkazu.');
        }
        
        $this->_attributes['target'] = $target;
        
        return $this;
      } else {
        return $this->_attributes['target'];
      }
    }
  }
