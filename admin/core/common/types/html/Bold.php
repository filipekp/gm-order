<?php
  namespace prosys\core\common\types\html;

  /**
   * Reprezentuje HTML b.
   * 
   * @author Pavel Filípek <filipek@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class Bold extends Element {
    public function __construct($content) {
      parent::__construct('b', TRUE);
      $this->_content = (string)$content;
    }

    protected function init() { }
  }
  