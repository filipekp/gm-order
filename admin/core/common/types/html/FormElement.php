<?php
  namespace prosys\core\common\types\html;

  /**
   * Reprezentuje obecny HTML element formulare.
   * 
   * @property type $name query
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  abstract class FormElement extends Element {
    protected $_name;
    protected $_value;
    protected $_placeholder;
    
    /**
     * Nastavi hlavni prvky elementu formulare.
     * 
     * @param string $tag
     * @param bool $paired
     * @param string $name
     * @param array $defaultClasses
     */
    public function __construct($tag, $paired, $name, $defaultClasses = []) {
      parent::__construct($tag, $paired, array_unique(array_merge(['form-control'], $defaultClasses)));
      
      $this->_name = $name;
      $this->_value = NULL;
      $this->_placeholder = NULL;
    }
    
    /**
     * Inicializuje formularovy prvek.
     */
    protected function init() {
      $this->_attributes['name'] = $this->_name;
    }
    
    /**
     * Getter/Setter.
     * 
     * @param string|NULL $name neni-li predana zadna hodnota, metoda funguje jako getter, jinak jako setter
     * @return \prosys\core\common\types\html\FormElement
     */
    public function name($name = NULL) {
      if (func_num_args()) {
        $this->_name = $name;
        return $this;
      } else {
        return $this->_name;
      }
    }
    
    /**
     * Getter/Setter.
     * 
     * @param string|NULL $value neni-li predana zadna hodnota, metoda funguje jako getter, jinak jako setter
     * @return \prosys\core\common\types\html\FormElement
     */
    public function value($value = NULL) {
      if (func_num_args()) {
        $this->_value = $value;
        return $this;
      } else {
        return $this->_value;
      }
    }
    
    /**
     * Getter/Setter.
     * 
     * @param string|NULL $placeholder neni-li predana zadna hodnota, metoda funguje jako getter, jinak jako setter
     * @return \prosys\core\common\types\html\FormElement
     */
    public function placeholder($placeholder = NULL) {
      if (func_num_args()) {
        $this->_placeholder = $placeholder;
        return $this;
      } else {
        return $this->_placeholder;
      }
    }
  }
  