<?php
  namespace prosys\core\common\types\html;
  
  use prosys\core\common\Functions,
      prosys\core\common\AppException;

  /**
   * Reprezentuje html element ikony.<br />
   * Dve varianty ikon: obrazkova a font (glyph) ikona
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class Icon extends Element {
    const TYPE_ICON  = 1;
    const TYPE_FONTAWESOME = 2;
    const TYPE_GLYPH = 3;
    const TYPE_IMAGE = 4;
    
    private $_icon;
    private $_type;
    
    /**
     * Nastavi zakladni vlastnosti ikony.
     * 
     * @param string $icon
     * @param int $type
     * 
     * @throws AppException
     */
    public function __construct($icon, $type = self::TYPE_FONTAWESOME) {
      if (!Functions::isConstantByPrefix($type, __CLASS__, 'TYPE_')) {
        throw new AppException('"' . $type . '" neni validním typem ikony.');
      }
        
      $this->_icon = $icon;
      $this->_type = $type;

      switch ($type) {
        case self::TYPE_ICON:
          $tag = 'i';
          $defaultClasses = ['icon-' . $this->_icon];
          $paired = TRUE;
        break;
      
        case self::TYPE_FONTAWESOME:
          $tag = 'i';
          $defaultClasses = ['fa', 'fa-' . $this->_icon];
          $paired = TRUE;
        break;
      
        case self::TYPE_FONTAWESOME:
          $tag = 'span';
          $defaultClasses = ['glyphicon', 'glyphicon-' . $this->_icon];
          $paired = TRUE;
        break;
      
        case self::TYPE_IMAGE:
          $tag = 'img';
          $defaultClasses = [];
          $this->attribute('src', $this->_icon);
          $paired = FALSE;
        break;
      }

      parent::__construct($tag, $paired, $defaultClasses);
    }

    /**
     * Inicializuje ikonu.
     */
    protected function init() { }
  }
  