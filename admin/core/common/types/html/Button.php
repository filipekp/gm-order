<?php
  namespace prosys\core\common\types\html;
  
  use prosys\core\common\AppException,
      prosys\core\common\Functions;

  /**
   * Reprezentuje html element ikony.<br />
   * Dve varianty ikon: obrazkova a font (glyph) ikona
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class Button extends Element {
    private static $STRIPE_SUFFIX = '-stripe';
    
    const TYPE_BUTTON = 'button';
    const TYPE_LINK = 'a';
    const TYPE_TEXT = 'span';
    
    const PURPOSE_BUTTON = 'button';
    const PURPOSE_SUBMIT = 'submit';
    const PURPOSE_RESET = 'reset';
    
    const SHAPE_SQUARE = '';
    const SHAPE_CIRCLE = 'btn-circle';
    const SHAPE_CIRCLE_RIGHT = 'btn-circle-right';
    
    private $_type;
    private $_shape;
    private $_theme;
    
    private $_stripe;
    private $_stripeTheme;
    
    private $_purpose = self::PURPOSE_BUTTON;
    private $_name;
    private $_value;
    
    /** @var Link */
    private $_link = NULL;
    
    /**
     * Nastavi zakladni vlastnosti ikony.
     * 
     * @param string $content
     * @param Theme $theme
     * @param string $type
     * @param string $shape
     * 
     * @throws AppException
     */
    public function __construct($content, Theme $theme = NULL, $type = self::TYPE_BUTTON, $shape = self::SHAPE_CIRCLE) {
      if (!Functions::isConstantByPrefix($type, __CLASS__, 'TYPE_')) {
        throw new AppException('"' . $type . '" neni validním typem tlacitka.');
      }
      
      if (!Functions::isConstantByPrefix($shape, __CLASS__, 'SHAPE_')) {
        throw new AppException('"' . $shape . '" neni validním tvarem tlacitka.');
      }
      
      $this->_content = (string)$content;
      $this->_type = $type;
      $this->_shape = $shape;
      $this->_theme = $theme;
      
      $this->_stripe = FALSE;
      $this->_stripe = Theme::get(Theme::GREY);

      parent::__construct($this->_type, TRUE, ['btn']);
    }
    
    /**
     * Prida k tlacitku vertikalni prouzek.
     * 
     * @param Theme $theme
     * @return \prosys\core\common\types\html\Button
     */
    public function addStripe(Theme $theme) {
      $this->removeStripe();
      
      $this->_stripe = TRUE;
      $this->_stripeTheme = $theme;
      
      $this->addClass($this->_stripeTheme . self::$STRIPE_SUFFIX);
      
      return $this;
    }
    
    /**
     * Odebere z tlacitka vertikalni prouzek.
     * @return \prosys\core\common\types\html\Button
     */
    public function removeStripe() {
      $this->_stripe = FALSE;
      $this->removeClass($this->_stripeTheme . self::$STRIPE_SUFFIX);
      
      return $this;
    }
    
    /**
     * Nastavi ucel tlacitka typu button.
     * 
     * @param string $purpose
     * @return \prosys\core\common\types\html\Button
     */
    public function setPurpose($purpose) {
      if (!Functions::isConstantByPrefix($purpose, __CLASS__, 'PURPOSE_')) {
        throw new AppException('"' . $purpose . '" neni validním ucelem (html atribut "type") tlacitka.');
      }
      
      $this->_purpose = $purpose;
      return $this;
    }
    
    /**
     * Setter.
     * 
     * @param string $name
     * @return \prosys\core\common\types\html\Button
     */
    public function setName($name) {
      $this->_name = $name;
      $this->attribute('name', $this->_name);
      
      return $this;
    }
    
    /**
     * Setter.
     * 
     * @param mixed $value
     * @return \prosys\core\common\types\html\Button
     */
    public function setValue($value) {
      $this->_value = $value;
      $this->attribute('value', $this->_value);
      
      return $this;
    }
    
    /**
     * Setter.
     * 
     * @param \prosys\core\common\types\html\Link $link
     * @return \prosys\core\common\types\html\Button
     */
    public function setLink(Link $link) {
      $this->_link = $link;
      return $this;
    }
    
    /**
     * Nastavi title tlacitku
     * 
     * @param string $title
     * @return Button
     */
    public function setTitle($title) {
      $this->_attributes['title'] = $title;
      return $this;
    }

    /**
     * Inicializuje ikonu.
     */
    protected function init() {
      switch ($this->_type) {
        case self::TYPE_LINK:
          if (is_null($this->_link)) {
            throw new AppException('Je pozadovano tlacitko typu link, ale neni predan zadny odkaz.');
          }

          // spravne vyplni href
          $this->_link->init();

          // stahnu href do tlacitka
          $this->attribute('href', $this->_link->attribute('href'));
          $this->attribute('target', $this->_link->attribute('target'));
        break;
        
        case self::TYPE_BUTTON:
          $this->attribute('type', $this->_purpose);
        break;
      }
      
      $this->addClass([$this->_shape]);
      
      if ($this->_theme) {
        $this->addClass($this->_theme);
      }
    }
  }
  