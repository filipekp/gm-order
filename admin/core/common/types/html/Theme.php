<?php
  namespace prosys\core\common\types\html;
  
  use prosys\core\common\Functions,
      prosys\core\common\AppException;

  /**
   * Reprezentuje barvu systemu v ruznych odstinech.
   * 
   * @property type $name query
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class Theme {
    const BLUE = 'blue';
    const GREEN = 'green';
    const RED = 'red';
    const YELLOW = 'yellow';
    const PURPLE = 'purple';
    const GREY = 'grey';
    
    private static $THEMES = [
      self::BLUE => array(
        'color' => self::BLUE,
        'hue'   => ['hoki', 'steel', 'madison', 'chambray', 'ebonyclay']
      ),
      self::GREEN => array(
        'color' => self::GREEN,
        'hue'   => ['meadow', 'seagreen', 'turquoise', 'haze', 'jungle']
      ),
      self::RED => array(
        'color' => self::RED,
        'hue'   => ['pink', 'sunglo', 'intense', 'thunderbird', 'flamingo']
      ),
      self::YELLOW => array(
        'color' => self::YELLOW,
        'hue'   => ['gold', 'casablanca', 'crusta', 'lemon', 'saffron']
      ),
      self::PURPLE => array(
        'color' => self::PURPLE,
        'hue'   => ['plum', 'medium', 'studio', 'wisteria', 'seance']
      ),
      self::GREY => array(
        'color' => self::GREY,
        'hue'   => ['cascade', 'silver', 'steel', 'cararra', 'gallery']
      ),
    ];
    
    private $_color;
    private $_hue;
    
    /**
     * Inicializuje barvu a odstin.
     * 
     * @param string $color
     * @param string $hue
     * 
     * @throws AppException
     */
    public function __construct($color, $hue = NULL) {
      if (!Functions::isConstantByPrefix($color, __CLASS__, '')) {
        throw new AppException('"' . $color . '" neni validní barva systému.');
      }
      
      if (!is_null($hue) && $hue >= count(self::$THEMES[$color]['hue'])) {
        throw new AppException('"' . $hue . '" neni validní odstín požadované barvy systému.');
      }
      
      $this->_color = $color;
      $this->_hue = $hue;
    }
    
    /**
     * Vrati tridu popisujici pozadovanou barvu.
     * 
     * @param string $color
     * @param string $hue
     * 
     * @return Color
     */
    public static function get($color, $hue = NULL) {
      return new Theme($color, $hue);
    }
    
    /**
     * Vypise HTML tridu reprezentujici pozadovanou barvu.
     * @return string
     */
    public function __toString() {
      return self::$THEMES[$this->_color]['color'] . ((!is_null($this->_hue)) ? '-' . self::$THEMES[$this->_color]['hue'][$this->_hue] : '');
    }
  }
