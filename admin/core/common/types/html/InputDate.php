<?php
  namespace prosys\core\common\types\html;
  
  use prosys\core\common\AppException;

  /**
   * Reprezentuje formularove vstupni pole typu datum.
   * 
   * @author Pavel Filípek <filipek@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class InputDate extends FormElement {
    /** @var \DateTime */
    protected $_date = NULL;
    protected $_formatDate;
        
    /** @var InputText */
    private $_dateInput;
    
    /**
     * Nastavi element.
     * @param type $name
     */
    public function __construct($name, $formatDate = 'd.m.Y') {
      parent::__construct('div', TRUE, $name, ['input-group', 'date', 'form_date']);

      $this->_dateInput = new InputText($name);
      
      $this->_formatDate = $formatDate;
    }
    
    /**
     * Inicializuje vstupni pole typu text. 
     */
    protected function init() {
      // odebere obalovacimu divu tridu form-control
      $this->removeClass('form-control');
      
      // inicializace ikonky kalendariku
      $icon = (new Span(new Icon('calendar')))->addClass('input-group-addon');
      
      // inicializace inputu rozsahu dat
      // "pretizeni" jmena inputu
      if ($this->_name) {
        $this->_dateInput->name($this->_name);
      }
       
      // placeholder
      if ($this->_placeholder) {
        $this->_dateInput->placeholder($this->_placeholder);
      }
      
      // hodnota
      if ($this->_date && $this->_to) {
        $this->_dateInput->value($this->_date->format($this->_formatDate));
      }
      
      // readonly
      $this->_dateInput->attribute('readonly', 'readonly');
      
      // inicializace celeho pole
      $this->_content = $icon . $this->_dateInput;
    }
    
    /**
     * Getter/Setter.
     * 
     * @param string|NULL $value neni-li predana zadna hodnota, metoda funguje jako getter, jinak jako setter
     * @return \prosys\core\common\types\html\FormElement
     * 
     * @throws AppException
     */
    public function value($value = NULL) {
      if (is_null($value)) {
        return $this->_dateInput->value();
      } else {
        $this->_dateInput->value($value);
        return $this;
      }
    }
  }
  