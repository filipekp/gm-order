<?php
  namespace prosys\core\common\types\html;

  /**
   * Reprezentuje vnitrni odkaz systemu ProSYS.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  abstract class LinkProSYS extends Link {
    protected $_query = [];
    
    /**
     * Inicializuje controller a akci.
     * 
     * @param mixed $content
     * @param string $controller
     * @param string $action
     */
    public function __construct($content) {
      parent::__construct($content);
    }
    
    /**
     * Vygeneruje URL odkazu - prida k systemovym odkazum query.
     * @return string
     */
    protected function urlQuery($url) {
      return $url . (($this->_query) ? '&' . http_build_query($this->_query) : '');
    }
    
    /**
     * Setter.
     * 
     * @param array $query
     * @return \prosys\core\common\types\html\LinkProSYS
     */
    public function setQuery(array $query) {
      $this->_query = $query;
      return $this;
    }
  }
  