<?php
  namespace prosys\core\common\types\html;
  
  use prosys\core\common\AppException;

  /**
   * Reprezentuje formularove vstupni pole typu rozsah dat.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class InputDateRange extends FormElement {
    protected $_separator;

    /** @var \DateTime */
    protected $_from = NULL;
    protected $_formatFrom;
    
    /** @var \DateTime */
    protected $_to = NULL;
    protected $_formatTo;
    
    /** @var InputText */
    private $_dateInput;
    
    /**
     * Nastavi element.
     * @param type $name
     */
    public function __construct($name, $separator = ' - ', $formatFrom = 'd.m.Y', $formatTo = 'd.m.Y') {
      parent::__construct('div', TRUE, $name, ['input-group', 'date', 'form_date_range']);

      $this->_dateInput = new InputText($name);
      
      $this->_separator = $separator;
      $this->_formatFrom = $formatFrom;
      $this->_formatTo = $formatTo;
    }
    
    /**
     * Inicializuje vstupni pole typu text. 
     */
    protected function init() {
      // odebere obalovacimu divu tridu form-control
      $this->removeClass('form-control');
      
      // inicializace ikonky kalendariku
      $icon = (new Span(new Icon('calendar')))->addClass('input-group-addon');
      
      // inicializace inputu rozsahu dat
      // "pretizeni" jmena inputu
      if ($this->_name) {
        $this->_dateInput->name($this->_name);
      }
       
      // placeholder
      if ($this->_placeholder) {
        $this->_dateInput->placeholder($this->_placeholder);
      }
      
      // hodnota
      if ($this->_from && $this->_to) {
        $this->_dateInput->value($this->_from->format($this->_formatFrom) . $this->_separator . $this->_to->format($this->_formatTo));
      }
      
      // readonly
      $this->_dateInput->attribute('readonly', 'readonly');
      
      // inicializace celeho pole
      $this->_content = $icon . $this->_dateInput;
    }
    
    /**
     * Rozparsuje data z predane hodnoty do slotu $_from a $_to a zajisti validitu.
     * 
     * @param type $value
     * @return boolean
     */
    protected function parse($value) {
      list($from, $to) = explode($this->_separator, $value);
      if ($from && $to) {
        $from = strtotime($from);
        $to = strtotime($to);

        if ($from && $to) {
          $this->_from = new \DateTime(date($this->_formatFrom, $from));
          $this->_to = new \DateTime(date($this->_formatTo, $to));

          return TRUE;
        }
      }
      
      return FALSE;
    }
    
    /**
     * Getter/Setter.
     * 
     * @param string|NULL $value neni-li predana zadna hodnota, metoda funguje jako getter, jinak jako setter
     * @return \prosys\core\common\types\html\InputDateRange
     * 
     * @throws AppException
     */
    public function value($value = NULL) {
      if ($this->parse($value)) {
        call_user_func_array(array($this->_dateInput, 'value'), func_get_args());
        return $this;
      } else {
        throw new AppException('Data musí být validní, v takovém formátu, aby je rozpoznala PHP funkce strtotime, a musí být oddělena oddělovačem "' . $this->_separator . '"');
      }
    }
    
    /**
     * Nastavi data atributy inputu s rozsahem dat.
     * 
     * @param string $data
     * @param string $value
     */
    public function data($data, $value = '') {
      call_user_func_array(array($this->_dateInput, 'data'), func_get_args());
      return $this;
    }
  }
  