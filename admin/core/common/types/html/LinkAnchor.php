<?php
  namespace prosys\core\common\types\html;

  /**
   * Reprezentuje odkaz typu "kotva".
   * 
   * @property type $name query
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class LinkAnchor extends Link {
    private $_anchor;
    
    /**
     * Inicializuje kotvu.
     * 
     * @param mixed $content
     * @param string $anchor
     */
    public function __construct($content, $anchor) {
      parent::__construct($content);

      $this->_anchor = $anchor;
    }
    
    /**
     * Vygeneruje kotvu.
     * @return string
     */
    public function releaseAnchor() {
      return '<a name="' . $this->_anchor . '"></a>';
    }
    
    /**
     * Vygeneruje URL odkazu.
     * 
     * @return string
     */
    protected function url() {
      return '#' . $this->_anchor;
    }
  }
  