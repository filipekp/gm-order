<?php
  namespace prosys\core\common\types\html;

  /**
   * Reprezentuje formularovy rozbalovaci seznam.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class Select extends FormElement {
    /** @var Option[] */
    protected $_options;
    protected $_isResetable = TRUE;
    
    /**
     * Nastavi element.
     * 
     * @param string $name
     * @param array $options
     */
    public function __construct($name, array $options = []) {
      parent::__construct('select', TRUE, $name, ['select2']);

      $this->_options = $options;
    }
    
    /**
     * Inicializuje vstupni pole typu text. 
     */
    protected function init() {
      parent::init();
      
      if ($this->_placeholder) {
        $this->_attributes['placeholder'] = $this->_placeholder;
      }
      
      if ($this->_isResetable) {
        array_unshift($this->_options, new Option('', ''));
      }

      $this->_content = implode('', $this->_options);
    }
    
    /**
     * Prida polozku rozbalovaciho seznamu.
     * 
     * @param \prosys\core\common\types\html\Option $option
     * @return \prosys\core\common\types\html\Select
     */
    public function addOption(Option $option) {
      $this->_options[] = $option;
      return $this;
    }
  }
  