{nocache}
  <div class="table-responsive">
    {if $is_filterable || $is_sortable}
      {if $is_filterable}
        <form class="table-filter" action="index.php">
          {"\n"|implode:$filters.hidden}

          <div class="btn-group btn-group-circle filter-actions">
            {button text='Filtrovat' icon='filter'|icon purpose='submit' classes='btn-xs'}
            {button icon='angle-down'|icon shape='btn-circle-right' classes=['btn-xs', 'dropdown-toggle'] data=['toggle' => 'dropdown']}

            <ul class="dropdown-menu pull-right" role="menu">
              <li>{link_action controller=$ProSYS.GLOBAL->module() action='clearFilter' query=['activity' => $ProSYS.GLOBAL->activity()] icon='ban'|icon:2 text=' Zrušit filtr'}</li>
              {if $is_sortable}<li>{link_action controller=$ProSYS.GLOBAL->module() action='clearSorting' query=['activity' => $ProSYS.GLOBAL->activity()] icon='unsorted'|icon:2 text=' Výchozí řazení'}</li>{/if}
            </ul>
          </div>
      {else}
        <div class="filter-actions">
          {link_action assign='link' controller=$ProSYS.GLOBAL->module() action='clearSorting' query=['activity' => $ProSYS.GLOBAL->activity()] text='Výchozí řazení'}
          {button text=' Výchozí řazení' icon='unsorted'|icon type='a' link=$link classes=['btn-xs']}
        </div>
      {/if}
    {/if}

        <table class="{$classes}" id="{$id}">
          <thead>
            <tr class="heading">
              {foreach from=$head item='th' name='head'}
                <th class="column_{$smarty.foreach.head.iteration}{if $th.sortable} sorting{if $th.sorting}_{$th.sorting}{/if}{/if}"{if $th.sortable} data-sortable-name="sorting[{$th.name}]"{if $th.sorting} data-sortable-value="{$th.sorting}"{/if}{/if}>
                  {$th.title}
                </th>
              {/foreach}
            </tr>
            {if $is_filterable}
              <tr class="filter">
                {foreach from=$head item='th' key='name' name='head'}
                  <td class="column_{$smarty.foreach.head.iteration}">
                    {if $name|array_key_exists:$filters.inputs}{$filters.inputs.$name}{/if}
                  </td>
                {/foreach}
              </tr>
            {/if}
          </thead>

          <tbody>
            {if $data}
              {foreach from=$data item='item'}
                <tr{if $item.classes} class="{$item.classes}"{/if}>
                  {foreach from=$item.row item='td' name='data'}
                    {assign var='icon_class' value=($smarty.foreach.data.iteration > $smarty.foreach.data.total - $action_count)|ternary:' icon':''}

                    <td class="column_{$smarty.foreach.data.iteration}{if $td.classes} {$td.classes}{/if}{$icon_class}"{if $td.data} {$td.data}{/if}>
                      {$td.content}
                    </td>
                  {/foreach}
                </tr>
              {/foreach}
            {else}
              <tr class="no_object">
                <td class="alert alert-error" colspan="{$head|@count}">
                  <span class="heading"><i class="fa fa-warning"></i> {$no_data.heading}</span>
                  <div>{$no_data.message}</div>
                </td>
              </tr>
            {/if}
          </tbody>
        </table>
    {if $is_filterable}
      </form>
    {/if}
  </div>
{/nocache}