<?php
  namespace prosys\core\common\types\html;

  /**
   * Reprezentuje formularove vstupni pole typu text.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class InputText extends Input {
    /**
     * Nastavi vstupni pole typu text.
     * @param string $name
     */
    public function __construct($name) {
      parent::__construct($name);
    }
    
    /**
     * Inicializuje vstupni pole typu text. 
     */
    protected function init() {
      parent::init();

      $this->_attributes['type'] = 'text';
    }
  }
  