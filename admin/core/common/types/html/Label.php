<?php
  namespace prosys\core\common\types\html;

  /**
   * Reprezentuje HTML element Label.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class Label extends Element {
    private $_for;
    private $_isRequired;
    
    /**
     * Vytvori label.
     * 
     * @param string $for
     * @param string $content
     * @param bool $isRequired
     */
    public function __construct($for, $content, $isRequired = FALSE) {
      parent::__construct('label', TRUE);
      
      $this->_for = $for;
      $this->_content = (string)$content;
      $this->_isRequired = $isRequired;
    }

    /**
     * Inicializuje label.
     */
    protected function init() {
      $this->attribute('for', $this->_for);
      $this->addClass('control-label');
      
      if ($this->_isRequired) {
        $this->_content .= (new Span('*'))->addClass('required');
      }
    }
  }
  