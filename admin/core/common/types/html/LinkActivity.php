<?php
  namespace prosys\core\common\types\html;
  
  use prosys\core\common\Settings,
      prosys\core\common\AppException;

  /**
   * Reprezentuje odkaz na aktivitu systemu ProSYS.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class LinkActivity extends LinkProSYS {
    private $_module;
    private $_activity;
    
    /**
     * Inicializuje modul a aktivitu.
     * 
     * @param mixed $content
     * @param string $module
     * @param string $activity
     */
    public function __construct($content, $module, $activity) {
      parent::__construct($content);
      
      if (is_null($module) || is_null($activity)) {
        throw new AppException('Musi byt zadan modul a aktivita, na kterou link vede.');
      }
      
      $this->_module = $module;
      $this->_activity = $activity;
    }
    
    /**
     * Vygeneruje URL odkazu.
     * 
     * @return string
     */
    protected function url() {
      return $this->urlQuery(Settings::ROOT_ADMIN_URL . 'index.php?module=' . $this->_module . '&activity=' . $this->_activity);
    }
  }
  