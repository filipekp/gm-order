<?php
  namespace prosys\core\common\types\html;

  /**
   * Reprezentuje formularove vstupni pole typu hidden.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class InputHidden extends Input {
    /**
     * Nastavi vstupni pole typu hidden.
     * 
     * @param string $name
     * @param string $value
     */
    public function __construct($name, $value) {
      parent::__construct($name);
      $this->_value = $value;
    }
    
    /**
     * Inicializuje vstupni pole typu hidden. 
     */
    protected function init() {
      parent::init();

      $this->_attributes['type'] = 'hidden';
    }
  }
  