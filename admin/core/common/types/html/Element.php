<?php
  namespace prosys\core\common\types\html;
  
  use prosys\core\common\Functions,
      prosys\core\common\Settings,
      prosys\admin\view\View;

  /**
   * Reprezentuje obecny HTML element
   * 
   * @property type $name query
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  abstract class Element {
    /** @var \Smarty */
    protected $_smarty;
    protected $_template;
    
    protected $_paired;
    protected $_tag;
    protected $_content = '';
    
    protected $_defaultClasses;
    protected $_attributes = [
      'id'       => '',
      'class'    => [],
      'title'    => '',
      'tabindex' => ''
    ];
    
    /**
     * Prevede asociativni pole na retezec: atribut1="hodnota" atribut2="hodnota" ...
     * 
     * @param array $attributes
     * @return string
     */
    public static function buildAttributes(array $attributes) {
      $withQuotes = array_map(function($item) {
        return '"' . $item . '"';
      }, array_filter($attributes, function($attribute) {
        return $attribute !== NULL && trim($attribute) !== '';
      }));

      return urldecode(
        http_build_query($withQuotes, NULL, ' ')
      );
    }
    
    /**
     * Inicializuje zakladni data html elementu.
     * 
     * @param string $tag
     * @param bool $paired
     * @param array $defaultClasses
     */
    public function __construct($tag, $paired, $defaultClasses = []) {
      $this->_tag = (string)$tag;
      $this->_paired = (bool)$paired;
      $this->_defaultClasses = $defaultClasses;
      
      $this->_smarty = View::getSmarty();
      $this->_template = 'file:' . Settings::ROOT_PATH . 'admin/core/common/types/html/templates/element.tpl';
    }

    /**
     * Inicializuje element pro vygenerovani.
     */
    abstract protected function init();
    
    /**
     * Vygeneruje element.
     * @return string
     */
    public function generate() {
      $this->addClass($this->_defaultClasses);
      $this->init();

      $attributes = Element::buildAttributes(
          array_combine(
            array_keys($this->_attributes),
            array_map(function($key) {
              return (($key == 'class') ? implode(' ', $this->_attributes[$key]) : $this->_attributes[$key]);
            }, array_keys($this->_attributes))
          )
      );

      $this->_smarty->assign('eltTag', $this->_tag)
                    ->assign('eltIsPaired', $this->_paired)
                    ->assign('eltContent', $this->_content)
                    ->assign('eltAttributes', $attributes);

      return $this->_smarty->fetch($this->_template);
    }

    /**
     * Setter / Getter.
     * 
     * @param string $id nepovinny: neni-li uveden, jedna se o getter, jinak o setter
     * @return \prosys\core\common\types\html\Link | string
     */
    public function id($id = '') {
      if (func_num_args()) {
        $this->_attributes['id'] = $id;
        return $this;
      } else {
        return $this->_attributes['id'];
      }
    }
    
    /**
     * Prida seznam trid k aktualnim tridam.
     * 
     * @param string | array $class
     * @return \prosys\core\common\types\html\Element
     */
    public function addClass($class = []) {
      $classArr = array_filter((is_array($class)) ? $class : explode(' ', $class));
      $this->_attributes['class'] = array_unique(array_merge($this->classHTML(), $classArr));
      
      return $this;
    }
    
    /**
     * Odebere seznam trid z aktualnich trid.
     * 
     * @param string | array $class
     * @return \prosys\core\common\types\html\Element
     */
    public function removeClass($class = []) {
      $classArr = ((is_array($class)) ? $class : explode(' ', $class));
      $this->_attributes['class'] = array_diff($this->_attributes['class'], $classArr);
      
      return $this;
    }

    /**
     * Setter / Getter.
     * 
     * @param string | array $class nepovinny: neni-li uveden, jedna se o getter, jinak o setter
     * @return \prosys\core\common\types\html\Link | string
     */
    public function classHTML($class = []) {
      if (func_num_args()) {
        $this->_attributes['class'] = [];
        return $this->addClass($class);
      } else {
        return $this->_attributes['class'];
      }
    }

    /**
     * Setter / Getter.
     * 
     * @param string $title nepovinny: neni-li uveden, jedna se o getter, jinak o setter
     * @return \prosys\core\common\types\html\Link | string
     */
    public function title($title = '') {
      if (func_num_args()) {
        $this->_attributes['title'] = $title;
        return $this;
      } else {
        return $this->_attributes['title'];
      }
    }

    /**
     * Setter / Getter.
     * 
     * @param int $tabindex nepovinny: neni-li uveden, jedna se o getter, jinak o setter
     * @return \prosys\core\common\types\html\Link | string
     */
    public function tabindex($tabindex = '') {
      if (func_num_args()) {
        $this->_attributes['tabindex'] = $tabindex;
        return $this;
      } else {
        return $this->_attributes['tabindex'];
      }
    }
    
    /**
     * Setter / Getter atributu elementu.
     * 
     * @param string $attribute
     * @param mixed $value hodnota musi mit implementovanou metodu __toString, protoze je z ni nakonec textovy retezec
     * 
     * @return \prosys\core\common\types\html\Link | mixed existuje-li, vraci hodnotu atributu, jinak NULL; je-li jeden atribut, jedna se o getter, jinak o setter
     */
    public function attribute($attribute, $value = '') {
      switch (func_num_args()) {
        case 1:
          return Functions::item($this->_attributes, $attribute);
        case 2:
          $this->_attributes[$attribute] = $value;
          return $this;
          
        default:
          return FALSE;
      }
    }
    
    /**
     * Setter / Getter datoveho atributu elementu.
     * 
     * @param string $data
     * @param mixed $value  hodnota musi mit implementovanou metodu __toString, protoze je z ni nakonec textovy retezec
     * 
     * @return \prosys\core\common\types\html\Link | mixed existuje-li, vraci hodnotu atributu, jinak NULL; je-li jeden atribut, jedna se o getter, jinak o setter
     */
    public function data($data, $value = '') {
      $data = 'data-' . $data;

      switch (func_num_args()) {
        case 1:
          return Functions::item($this->_attributes, $data);
        case 2:
          $this->_attributes[$data] = $value;
          return $this;
          
        default:
          return FALSE;
      }
    }
    
    /**
     * Setter. Nastavi sablonu, ktera zpracovava vystup tabulky => cely smarty string pro funkci fetch.<br />
     * Vychozi: file:{Settings::ROOT_PATH}admin/core/common/types/html/templates/table.tpl
     * 
     * @param type $template
     * @return \prosys\core\common\types\html\Table
     */
    public function setTemplate($template) {
      $this->_template = $template;
      return $this;
    }
    
    /**
     * "Magicka" metoda pro echovani objektu.
     * 
     * @return string
     */
    public function __toString() {
      try {
        return $this->generate();
      } catch (\Exception $e) {
        $error = new \PhpConsole\Dispatcher\Errors(\PC::getConnector(), \PC::getConnector()->getDumper());
        $error->dispatchException($e);

        return '';
      }
    }
  }
  