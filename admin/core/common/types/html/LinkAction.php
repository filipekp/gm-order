<?php
  namespace prosys\core\common\types\html;
  
  use prosys\core\common\Settings;

  /**
   * Reprezentuje odkaz na akci systemu ProSYS.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class LinkAction extends LinkProSYS {
    private $_controller;
    private $_action;
    
    /**
     * Inicializuje controller a akci.
     * 
     * @param mixed $content
     * @param string $controller
     * @param string $action
     */
    public function __construct($content, $controller, $action) {
      parent::__construct($content);
      
      $this->_controller = $controller;
      $this->_action = $action;
    }
    
    /**
     * Vygeneruje URL odkazu.
     * 
     * @return string
     */
    protected function url() {
      return $this->urlQuery(Settings::ROOT_ADMIN_URL . 'index.php?controller=' . $this->_controller . '&action=' . $this->_action);
    }
  }
  