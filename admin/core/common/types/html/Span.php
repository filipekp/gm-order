<?php
  namespace prosys\core\common\types\html;

  /**
   * Reprezentuje HTML span.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class Span extends Element {
    public function __construct($content) {
      parent::__construct('span', TRUE);
      $this->_content = (string)$content;
    }

    protected function init() { }
  }
  