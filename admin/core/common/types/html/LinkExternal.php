<?php
  namespace prosys\core\common\types\html;

  /**
   * Reprezentuje obecny odkaz mimo system ProSYS.
   * 
   * @property type $name query
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class LinkExternal extends Link {
    private $_link;
    
    /**
     * Inicializuje kotvu.
     * 
     * @param mixed $content
     * @param string $link
     */
    public function __construct($content, $link) {
      parent::__construct($content);

      $this->_link = $link;
    }
    
    /**
     * Vygeneruje URL odkazu.
     * 
     * @return string
     */
    protected function url() {
      return $this->_link;
    }
  }
  