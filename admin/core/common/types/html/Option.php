<?php
  namespace prosys\core\common\types\html;

  /**
   * Reprezentuje polozku formularoveho rozbalovaciho seznamu.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class Option extends FormElement {
    protected $_selected;
    
    /**
     * Nastavi element.
     * 
     * @param string $name
     * @param string $value
     */
    public function __construct($name, $value, $selected = FALSE) {
      parent::__construct('option', TRUE, $name);

      $this->_value = $value;
      $this->_selected = $selected;
    }
    
    /**
     * Inicializuje polozku formularoveho rozbalovaciho seznamu.
     */
    protected function init() {
      $this->removeClass('form-control');

      $this->_attributes['value'] = $this->_name;
      $this->_content = $this->_value;
      
      if ($this->_selected) {
        $this->_attributes['selected'] = 'selected';
      }
    }
    
    /**
     * Getter/Setter.
     * 
     * @param bool|NULL $selected neni-li predana zadna hodnota, metoda funguje jako getter, jinak jako setter
     * @return \prosys\core\common\types\html\Option
     */
    public function selected($selected = TRUE) {
      if (func_num_args()) {
        $this->_selected = $selected;
        return $this;
      } else {
        return $this->_selected;
      }
    }
  }
  