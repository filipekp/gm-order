<?php
  namespace prosys\core\common\types\html;

  /**
   * Reprezentuje formularove vstupni pole typu input.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  abstract class Input extends FormElement {
    /**
     * Nastavi vstupni pole formulare.
     * @param type $name
     */
    public function __construct($name) {
      parent::__construct('input', FALSE, $name);
    }

    /**
     * Inicializuje vstupni pole formulare.
     */
    protected function init() {
      parent::init();
      
      if (!is_null($this->_value)) {
        $this->_attributes['value'] = $this->_value;
      }
      
      if ($this->_placeholder) {
        $this->_attributes['placeholder'] = $this->_placeholder;
      }
    }
  }
  