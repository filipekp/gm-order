<?php
  namespace prosys\core\common\types\html;

  /**
   * Reprezentuje formularove vstupni pole typu text.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class InputNumber extends Input {
    private $_min = NULL;
    private $_max = NULL;
    
    /**
     * Nastavi vstupni pole typu text.
     * @param string $name
     */
    public function __construct($name) {
      parent::__construct($name);
    }
    
    /**
     * Inicializuje vstupni pole typu number. 
     */
    protected function init() {
      parent::init();

      $this->_attributes['type'] = 'number';
      if (!is_null($this->_min)) { $this->_attributes['min'] = $this->_min; }
      if (!is_null($this->_max)) { $this->_attributes['max'] = $this->_max; }
    }
    
    /**
     * Getter/Setter.
     * 
     * @param int|NULL $value neni-li predana zadna hodnota, metoda funguje jako getter, jinak jako setter
     * @return \prosys\core\common\types\html\InputNumber
     */
    public function min($value = NULL) {
      if (is_null($value)) {
        return (int)$this->_min;
      }
      
      $this->_min = $value;
      return $this;
    }
    
    /**
     * Getter/Setter.
     * 
     * @param int|NULL $value neni-li predana zadna hodnota, metoda funguje jako getter, jinak jako setter
     * @return \prosys\core\common\types\html\InputNumber
     */
    public function max($value = NULL) {
      if (is_null($value)) {
        return (int)$this->_max;
      }
      
      $this->_max = $value;
      return $this;
    }
  }
  