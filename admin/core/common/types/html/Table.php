<?php
  namespace prosys\core\common\types\html;
  
  use prosys\admin\view\View,
      prosys\core\common\Settings,
      prosys\core\common\AppException,
      prosys\core\common\Functions,
      prosys\core\common\types\html\LinkActivity,
      prosys\core\Translator;

  /**
   * Reprezentuje HTML tabulku.
   * 
   * @property type $name query
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class Table extends Element {
    private $_id;
    private $_columns    = [];
    private $_filters    = [];
    private $_actions    = [];
    private $_data       = [];
    private $_query      = [];
    private $_rowMarkers = [];
    private $_noDataMsg  = [];
    
    /**
     * Nastavi zakladni parametry tabulky.
     * @param string $id
     */
    public function __construct($id = '') {
      parent::__construct('table', TRUE, ['table', 'table-striped', 'table-bordered', 'table-hover']);
      
      $this->_id = $id;
      $this->_template = 'file:' . Settings::ROOT_PATH . 'admin/core/common/types/html/templates/table.tpl';
      
      $this->_noDataMsg = [
        'heading' => Translator::translate(['table', 'no_data', 'heading']),
        'message' => Translator::translate(['table', 'no_data', 'message'])
      ];
    }
    
    /**
     * Nastavi aktualni razeni.
     * 
     * @return \prosys\core\common\types\html\Table
     */
    private function setSorting() {
      foreach ($this->_columns as $name => &$column) {
        $column['sorting'] = Functions::item($this->_query, ['sorting', $name], FALSE);
      }
      
      return $this;
    }
    
    /**
     * Pripravi zahlavi tabulky k prirazeni do smarty.
     * @return array
     */
    private function prepareHead() {
      $query = Functions::modifyArrayByKey($this->_query, ['module', 'activity']);
      $this->setSorting();
      
      $keys = array_keys($this->_columns);
      $values = array_combine(
        $keys,
        array_map(function($name) use ($query) {
          $column = $this->_columns[$name];

          $query['sorting'] = [];
          $query['sorting'][$name] = (($column['sorting'] == 'asc') ? 'desc' : 'asc');

          $link = (new LinkActivity($column['title'], View::ProSYS()->module(), View::ProSYS()->activity()))
                    ->addClass('sorting_link')
                    ->setQuery($query);
          
          return [
            'name' => $name,
            'title' => (($column['sortable']) ? $link : $column['title']),
            'sortable' => $column['sortable'], 
            'sorting' => $column['sorting']
          ];
        }, $keys)
      );

      return array_merge(
        $values,
        (($this->_actions) ? array_fill(0, count($this->_actions), ['name' => '', 'title' => '', 'sortable' => FALSE, 'sorting' => FALSE]) : [])
      );
    }
    
    /**
     * Pripravi filtry tabulky pro vypis.
     * @return array
     */
    private function prepareFilters() {
      $keys = array_keys($this->_filters);
      $inputs = array_map(function($name) {
        return (($value = Functions::item($this->_query, ['filter', $name])) ?
                  $this->_filters[$name]->value($value) : 
                  $this->_filters[$name]);
      }, $keys);
      
      return [
        'inputs' => array_combine($keys, $inputs),
        'hidden' => Functions::generateHiddenInputs(Functions::modifyArrayByKey($this->_query, ['filter']))
      ];
    }
    
    /**
     * Zkontroluje, zda je hodnota callback nebo prima hodnota a zpracuje ji.
     * 
     * @param callable | mixed $value
     * @param Entity | mixed $item
     * 
     * @return mixed
     */
    private function getValue($value, $item) {
      return ((is_callable($value)) ? $value($item) : $value);
    }
    
    /**
     * Pripravi akce aktualniho radku.
     * 
     * @param Entity | mixed $item
     * @return array
     */
    private function prepareActions($item) {
      return array_map(function($action) use($item) {
        // odkaz akce
        $link = $action['link']($item);
        if (!is_a($link, 'prosys\core\common\types\html\Link')) {
          throw new AppException('Akce radku tabulky musí být typu Link.');
        }
        
        // zakladni parametry
        $link->addClass('icon_link')
             ->attribute('title', $this->getValue($action['title'], $item));
        
        // nepovinne parametry:
        // confirm zprava
        if ($action['confirm']) {
          $link->data('confirm-text', $this->getValue($action['confirm'], $item));
        }
        
        // vychozi akce na dvojklik
        if ($this->getValue($action['is_default'], $item)) {
          $link->addClass('default_action');
        }
        
        // dalsi atributy
        foreach ($this->getValue($action['attributes'], $item) as $attribute => $value) {
          $link->attribute($attribute, $this->getValue($value, $item));
        }
        
        // vrati hotovy odkaz
        return ['content' => $link, 'classes' => '', 'data' => ''];
      }, $this->_actions);
    }
    
    /**
     * Zjisti tridy oznaceni pozadovaneho radku.
     * 
     * @param Entity | mixed $item
     * @return string
     */
    private function getRowHtmlClass($item) {
      $classes = [];
      foreach ($this->_rowMarkers as $marker) {
        if ($marker['condition']($item)) {
          $markerClasses = array_map(function($class) use ($item) {
            return $this->getValue($class, $item);
          }, $this->getValue($marker['classes'], $item));
          
          $classes = array_merge($classes, $markerClasses);
        }
      }
      
      return implode(' ', $classes);
    }
    
    /**
     * Pripravi data tabulky k prirazeni do smarty.
     * @return array
     */
    private function prepareData() {
      return array_map(function($item) {
        $row = [];
        foreach ($this->_columns as $column) {
          $cellContent = '';
          if (is_callable($column['value'])) {                                // jedna-li se o callback
            $cellContent = $column['value']($item);
          } else if (preg_match('/^.*\(\)$/', $column['value'])) {            // jedna-li se o metodu entity
            $method = preg_replace('/^(.*)\(\)$/', '$1', $column['value']);
            $cellContent = $item->$method();
          } else {                                                            // jedna-li se o property entity
            $cellContent = $item->{$column['value']};
          }
          
          // html tridy bunky
          $classes = array_map(function($class) use ($item) {
            return $this->getValue($class, $item);
          }, $this->getValue($column['classes'], $item));
          
          // data atributy bunky
          $data = $this->getValue($column['data'], $item);
          
          $dataValues = array_map(function($data) use ($item) {
            return $this->getValue($data, $item);
          }, $data);
          
          $dataKeys = array_keys($data);
          array_walk($dataKeys, function(&$item) {
            $item = 'data-' . $item;
          });
          
          $row[] = ['content' => $cellContent, 'classes' => implode(' ', $classes), 'data' => Element::buildAttributes(array_combine($dataKeys, $dataValues))];
        }

        return ['row' => array_merge($row, $this->prepareActions($item)), 'classes' => $this->getRowHtmlClass($item)];
      }, $this->_data);
    }

    /**
     * Inicializuje HTML tabulku.
     */
    protected function init() {
      $this->_smarty->assign('id', $this->_id)
                    ->assign('classes', implode(' ', $this->_attributes['class']))
                    ->assign('head', $this->prepareHead())
                    ->assign('filters', $this->prepareFilters())
                    ->assign('data', $this->prepareData())
                    ->assign('action_count', count($this->_actions))
                    ->assign('is_filterable', (bool)$this->_filters)
                    ->assign('is_sortable', (bool)array_filter($this->_columns, function($column) { return $column['sortable']; }))
                    ->assign('no_data', $this->_noDataMsg);
    }
    
    /**
     * Prida sloupec tabulky.
     * 
     * @param string $title
     * @param string $name
     * @param string | callable $value
     * @param array | callable $classes kazda trida muze byt string a callback
     * @param array | callable  $attributes data jsou asociativni pole, kde hodnoty mohou byt string a callback
     * 
     * @return \prosys\core\common\types\html\Table
     */
    public function addColumn($title, $name, $value, $classes = [], $data = []) {
      $column = ['title' => $title, 'value' => $value, 'classes' => $classes, 'data' => $data, 'sortable' => FALSE, 'sorting' => FALSE];
      if ($name) {
        $this->_columns[$name] = $column;
      } else {
        $this->_columns[] = $column;
      }
      
      return $this;
    }
    
    /**
     * Nastavi sloupce, podle kterych se da tabulka radit.
     * 
     * @param array $sortable
     * @return \prosys\core\common\types\html\Table
     */
    public function setSortable(array $sortable) {
      foreach ($this->_columns as $name => &$column) {
        $column['sortable'] = in_array($name, $sortable, TRUE);
      }

      return $this;
    }
    
    /**
     * Prida filtr ke sloupci tabulky.
     * 
     * @param string $column
     * @param \prosys\core\common\types\html\FormElement $input
     * 
     * @return \prosys\core\common\types\html\Table
     */
    public function addFilter($column, FormElement $input) {
      if (Functions::item($this->_columns, $column)) {
        $this->_filters[$column] = $input->name("filter[{$input->name()}]");
      }
      
      return $this;
    }
    
    /**
     * Prida akci radku tabulky.<br />
     * Je-li kterykoliv parametr callable, musi callback vracet druhy uvedeny typ.
     * 
     * @param callable $link musi byt callback, ktery vraci link
     * @param callable | string $title
     * @param callable | string $confirm
     * @param callable | array $attributes musi byt asociativni pole a hodnoty mohou byt string, ci callback
     * @param callable | bool $isDefault
     * 
     * @return \prosys\core\common\types\html\Table
     */
    public function addAction(callable $link, $title, $confirm = NULL, $attributes = [], $isDefault = FALSE) {
      if ($isDefault) {
        array_walk($this->_actions, function(&$item) {
          $item['is_default'] = FALSE;
        });
      }
      
      $this->_actions[] = [
        'link' => $link, 
        'title' => $title, 
        'confirm' => $confirm, 
        'attributes' => $attributes, 
        'is_default' => $isDefault || !count($this->_actions)
      ];

      return $this;
    }
    
    /**
     * Oznaci urcite radky tabulky.
     * 
     * @param callable $condition
     * @param array | callable $classes kazda trida muze byt string a callback
     * 
     * @return \prosys\core\common\types\html\Table
     */
    public function markRows(callable $condition, array $classes) {
      $this->_rowMarkers[] = ['condition' => $condition, 'classes' => $classes];
      return $this;
    }
    
    /**
     * Setter.
     * 
     * @param array $data
     * @return \prosys\core\common\types\html\Table
     */
    public function setTableData(array $data) {
      $this->_data = $data;
      return $this;
    }
    
    /**
     * Setter. Nastavi atributy pro filtr, razeni a pripadne dalsi odkazy.
     * 
     * @param array $query
     * @return \prosys\core\common\types\html\Table
     */
    public function setQuery(array $query) {
      $this->_query = $query;
      return $this;
    }
    
    /**
     * Setter.
     * 
     * @param string $heading
     * @param string $message
     */
    public function setNoDataMsg($heading, $message) {
      $this->_noDataMsg = ['heading' => $heading, 'message' => $message];
    }
  }
  