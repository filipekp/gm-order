<?php
  namespace prosys\core\common\types;
  
  use prosys\core\common\AppException,
      prosys\core\common\Functions,
      prosys\core\common\Settings;
  
  /**
   * Reprezentuje tridu obecneho souboru.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class File {
    const BACKUP_CREATE_EXCEPTION_CODE = 1001;
    
    const UPLOAD_KEEP_ALL = 1;
    const UPLOAD_OVERWRITE = 2;
    
    protected $_name;
    protected $_path;
    protected $_originalName;
    
    protected $_info;
    
    protected $_backup = NULL;
    
    /**
     * Inicializuje soubor.
     * 
     * @param string $file
     * @param string $originalName
     * 
     * @throws AppException neexistuje-li soubor 
     */
    public function __construct($file, $originalName = '') {
      $this->_originalName = $originalName;

      $this->_info = pathinfo($file);
      $this->_path = (string)Functions::unsetItem($this->_info, 'dirname') . DIRECTORY_SEPARATOR;
      $this->_name = (string)Functions::unsetItem($this->_info, 'basename');
    }
    
    /**
     * Overi, zda je MIME souboru validni.
     * @return boolean
     */
    public function isMimeAllowed() {
      return TRUE;
    }

    /**
     * Getter.
     * 
     * @param string $parameter
     * @return string | NULL
     * 
     * @throws AppException
     */
    public function info($parameter) {
      $value = Functions::item($this->_info, $parameter);

      if (in_array($parameter, ['mime', 'encoding', 'size']) && is_null($value)) {
        $filepath = $this->getFilepath();
          
        if ($this->exists()) {
          $finfo = new \finfo();
          $this->_info = array_merge(
            $this->_info, [
              'mime' => [
                'type' => $finfo->file($filepath, FILEINFO_MIME_TYPE), 
                'encoding' => $finfo->file($filepath, FILEINFO_MIME_ENCODING)
              ],
              'size' => filesize($this->getFilepath())
            ]
          );
          
          $value = Functions::item($this->_info, $parameter);
        } else {
          throw new AppException('Není možné zjistit požadovanou informaci, protože soubor `' . $filepath . '` neexistuje.');
        }
      }

      return $value;
    }
    
    /**
     * Vytvori zalohu aktualniho souboru.
     * 
     * @return \prosys\core\common\types\File
     */
    public function backup() {
      if ($this->exists()) {
        $this->_backup = tmpfile();
        fwrite($this->_backup, file_get_contents($this->getFilepath()));
      }
      
      return $this;
    }
    
    /**
     * Obnovi soubor ze zalohy.
     * 
     * @return \prosys\core\common\types\File
     * @throws AppException neexistuje-li zaloha, nebo se nepodarilo soubor ze zalohy obnovit
     */
    public function restore() {
      if (is_null($this->_backup)) {
        throw new AppException('Není možné obnovit soubor s neexistující zálohy.');
      } else {
        fseek($this->_backup, 0);
        
        $fstats = fstat($this->_backup);
        if (file_put_contents($this->getFilepath(), fread($this->_backup, $fstats['size']))) {
          throw new AppException('Soubor se nepodařilo obnovit.');
        }
      }
      
      return $this;
    }
    
    /**
     * Uvolni zalohu souboru z pameti.
     * @return \prosys\core\common\types\File
     */
    public function backupFree() {
      if (!is_null($this->_backup)) {
        fclose($this->_backup);
        $this->_backup = NULL;
      }
      
      return $this;
    }
    
    /**
     * Provede upload souboru z globalniho pole $_FILES do vytvorene instance.<br />
     * <i>
     *  Je-li v parametru $flag hodnota self::UPLOAD_OVERWRITE, vytvori zalohu puvodniho souboru, 
     *  je proto zadouci uvolnit pamet metodou backupFree nejpozdeji po ukonceni prace se souborem.
     * </i>
     * 
     * @param string $inputName
     * @param int $flag konstanty tridy File
     * 
     * @return \prosys\core\common\types\File
     * @throws AppException
     */
    public function upload($inputName, $flag = self::UPLOAD_KEEP_ALL) {
      // kontrola hodnoty prepinace $flag
      if (!Functions::isConstantByPrefix($flag, __CLASS__, 'UPLOAD_')) {
        throw new AppException('Parametr $flag musí být jednou z hodnot konstant třídy File.');
      }
      
      // zjisti chyby v souboru
      if (($error = Functions::item($_FILES, [$inputName, 'error'])) != UPLOAD_ERR_OK) {
        switch ($error) {
          case UPLOAD_ERR_NO_FILE:
              throw new AppException('Nebyl odeslán žádný soubor.');
          case UPLOAD_ERR_INI_SIZE:
          case UPLOAD_ERR_FORM_SIZE:
              throw new AppException('Soubor je příliš velký.');
          default:
              throw new AppException('Při nahrávání souboru došlo k neznámé chybě.');
        }
      }

      // kontrola velikosti souboru
      if ($_FILES[$inputName]['size'] > Functions::getMaxUploadFileSize()) {
          throw new AppException('Soubor je příliš velký.');
      }

      // kontrola MIME
      $fileBeforeUpload = new File($_FILES[$inputName]['tmp_name']);
      
      if (!$fileBeforeUpload->isMimeAllowed()) {
        throw new AppException('Nepovolený formát souboru `' . $_FILES[$inputName]['name'] . '`');
      }
      
      // vytvori neexistujici adresar
      if (!is_dir($this->_path)) {
        if (!@mkdir($this->_path, 0775, TRUE)) {
          throw new AppException('Nepodařilo se vytvořit adresář `' . $this->_path . '`.');
        }

        @chmod($this->_path, 0775);
      }

      // presun nahrateho souboru na spravne misto
      $filepath = $this->getFilepath();
      if ($flag == self::UPLOAD_KEEP_ALL) {
        $filepath = Functions::getUniqueFilename($filepath, $fileBeforeUpload->info('extension'));
      } else {
        $this->backup();
      }
      
      if (!move_uploaded_file($_FILES[$inputName]['tmp_name'], $filepath)) {
        throw new AppException('Při přesunu nahraného souboru došlo k neznámé chybě.');
      }

      // zmeni puvodni nazev souboru dle uploadovaneho
      $this->_originalName = $_FILES[$inputName]['name'];

      return $this;
    }
    
    /**
     * Zjisti, zda soubor existuje fyzicky na disku.
     * @return boolean
     */
    public function exists() {
      return file_exists($this->getFilepath());
    }
    
    /**
     * Vrati cestu k souboru.
     * @return string
     */
    public function getFilepath() {
      return $this->_path . $this->_name;
    }
    
    /**
     * Vrati URL k souboru.
     * @return string
     */
    public function getFileURL() {
      return preg_replace('@^' . Settings::ROOT_PATH . '@', Settings::ROOT_URL , $this->getFilepath());
    }
    
    /**
     * Getter.
     * @return string
     */
    public function getName() {
      return $this->_name;
    }

    /**
     * Getter.
     * @return string
     */
    public function getPath() {
      return $this->_path;
    }

    /**
     * Getter.
     * @return string
     */
    public function getOriginalName() {
      return $this->_originalName;
    }
  }
  