<?php
  namespace prosys\core\common\types;
  
  use prosys\core\common\AppException;
  
  /**
   * Reprezentuje tridu obrazku.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class Image extends File {
    const MIME_JPEG = 'image/jpeg';
    const MIME_PNG = 'image/png';
    const MIME_GIF = 'image/gif';
    
    protected static $ALLOWED_MIME = [self::MIME_JPEG, self::MIME_PNG, self::MIME_GIF];
    
    /** @var \Imagick */
    private $_image = NULL;
    
    /**
     * Zjisti, zda je soubor validni obrazek.
     * @return bool
     */
    public function isMimeAllowed() {
      return in_array($this->_info['mime']['type'], self::$ALLOWED_MIME);
    }
    
    /**
     * Inicializuje objekt typu Imagick.
     * @return \prosys\core\common\types\Image
     */
    public function init() {
      if (is_null($this->_image)) {
        if ($this->exists()) {
          $this->_image = new \Imagick(realpath($this->getFilepath()));
        } else {
          throw new AppException('Soubor `' . $this->getFilepath() . '` neni k dispozici.');
        }
      }
      
      return $this;
    }
    
    /**
     * Vrati sirku obrazku.
     * @return int
     */
    public function getWidth() {
      $this->init();
      return $this->_image->getImageWidth();
    }
    
    /**
     * Vrati vysku obrazku.
     * @return int
     */
    public function getHeight() {
      $this->init();
      return $this->_image->getImageHeight();
    }
    
    /**
     * Zmeni velikost obrazku.
     * 
     * @param int $width
     * @param int $height
     * @param boolean $bestfit obrazek se zmensi se zachovanim pomeru tak, aby se "vlezl" do pozadovanych rozmeru - pouze pro zmenseni obrazku
     * @param type $filter algoritmus pro zmenu velikosti - jedna z konstant \Imagick::FILTER_%ALGORITHM%
     * @param type $blur hodnota rozostreni obrazku - hodnota > 1 znamena rozostreni a hodnota < 1 znamena zostreni obrazku
     * 
     * @return \prosys\core\common\types\Image
     */
    public function resize($width, $height, $bestfit = TRUE, $filter = \Imagick::FILTER_CATROM, $blur = 1) {
      $this->init();
      
      if ($width > $this->getWidth() && $height > $this->getHeight()) {
        $bestfit = FALSE;
      }
      
      $this->_image->resizeImage($width, $height, $filter, $blur, $bestfit);
      return $this;
    }
    
    /**
     * Orizne obrazek dle souradnic leveho horniho bodu zadanim sirky a vysky oriznuti.
     * 
     * @param int $leftTopX
     * @param int $leftTopY
     * @param int $width
     * @param int $height
     */
    public function crop($leftTopX, $leftTopY, $width, $height) {
      $this->init();
      $this->_image->cropImage($width, $height, $leftTopX, $leftTopY);
      
      return $this;
    }
    
    /**
     * Ulozi zmeny obrazkku na disk.
     * @return \prosys\core\common\types\Image
     */
    public function store() {
      $this->init();
      $this->_image->writeImage($this->getFilepath());
      
      return $this;
    }
    
    /**
     * Vrati obrazek v binarni podobe zakodovane pomoci algoritmu base64.<br />
     * Urcene pro prime vlozeni do atributu src tagu img.
     * 
     * @return string
     */
    public function getBase64() {
      $this->init();

      return 'data:' . $this->info('mime')['type'] . ';base64,' . base64_encode($this->_image->getImageBlob());
    }
  }
