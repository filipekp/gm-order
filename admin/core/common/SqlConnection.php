<?php
  namespace prosys\core\common;

  /**
   * Reprezentuje SQL pripojeni.
   * 
   * @property-read  string $server
   * @property-read  string $user
   * @property-read  string $password
   * @property-read  string $db
   * @property-read \PDO    $connection
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  abstract class SqlConnection implements \prosys\core\interfaces\IDataHandler
  {
    protected $connection;
    protected $result;

    protected $server;
    protected $user;
    protected $password;
    protected $db;
    protected $prefix;
    
    protected $_last_execution = 0;
    
    abstract protected function connect();

    /**
     * Vytvori SQL pripojeni.
     * 
     * @param string $server
     * @param string $user
     * @param string $password
     * @param string $db
     * @param string $prefix
     * @param bool $newLink
     */
    public function __construct($server, $user = '', $password = '', $db = '', $prefix = '') {
      $this->server = $server;
      $this->user = $user;
      $this->password = $password;
      $this->db = $db;
      $this->prefix = $prefix;
      
      $this->connect();
    }
    
    /**
     * Obnovi pripojeni k databazi.
     * @return \PDO
     */
    public function reconnect() {
      unset($this->connection);
      return $this->connect();
    }
    
    /**
     * Od zavolani teto metody se vse nasledujici neodesle na databazi, dokud neni potvrzeno pomoci metody SqlConnection::commit().<br />
     * Vypne mod autocommitu.
     * 
     * @return bool
     * @throws SqlTransactionException
     */
    public function beginTransaction() {
      if ($this->connection->inTransaction()) {
        throw new SqlTransactionException(
          'Connection is inside of another transaction. It\'s necessary to close previous transaction before begin another one. You should call SqlConnection::commit() method.'
        );
      }

      return $this->connection->beginTransaction();
    }
    
    /**
     * Provede kompletni transakci. V pripade, ze doslo ve kteremkoliv dotazu k chybe, vrati transakci zpet a vyhodi vyjimku.
     * 
     * @return bool
     * @throws SqlTransactionException
     */
    public function commit() {
      if ($this->connection->inTransaction()) {
        try {
          if (!$this->connection->commit()) {
            throw new SqlTransactionException('Committing of current transaction failed.');
          }
        } catch (Exception $ex) {
          $message = (($this->connection->rollBack()) ? 'Transaction was rolled back successfully.' : 'Transaction rollback FAILED.');

          throw new SqlTransactionException(
            array(
              'There was an error in the transaction. ' . $message,
              'Error: ' . $ex->getMessage()
            ), 0, $ex);
        }
      } else {
        throw new SqlTransactionException(
          'Connection is not inside of a transaction state. It\'s necessary to start transaction before commit. You should call SqlConnection::beginTransaction() method.'
        );
      }
      
      return TRUE;
    }

    /**
     * Prepares statement query and throws an error when necessary.
     * 
     * @param string $queryStmt
     * @param array $options
     * 
     * @return \PDOStatement
     * @throws AppException when statement query is wrong
     */
    protected function prepare($queryStmt, array $options = array()) {
      $query = str_replace('_PREFIX_', $this->prefix, $queryStmt);

      try {
        return $this->connection->prepare($query, $options);
      } catch (\PDOException $e) {
        throw new AppException(array('Wrong query statement: ' . $query, $e->getMessage()));
      }
    }

    /**
     * Executes the statement and stores the result statement into the $result property.
     * 
     * @param \PDOStatement $stmt
     * @return \PDOStatement
     * 
     * @throws AppException when execution fails
     */
    protected function execute(\PDOStatement $stmt) {
      try {
        $stmt->execute();
        $this->_last_execution = microtime(TRUE);
        
        $this->result = $stmt;
        return $this->result;
      } catch (\PDOException $e) {
        throw new AppException('Query execution failed: ' . $e->getMessage());
      }
    }
    
    /**
     * Binds prepared params (question mark type) to values.
     * 
     * @param \PDOStatement $stmt
     * @param array $data
     */
    protected function bindParams(\PDOStatement $stmt, array $data) {
      if ($data) {
        foreach ($data as $key => &$value) {
          if ($value == 'NULL') {
            $value = NULL;
            $type = \PDO::PARAM_NULL;
          } else {
            $type = \PDO::PARAM_STR;
          }

          $stmt->bindParam($key + 1, $value, $type);
        }
      }
    }
    
    /**
     * Binds prepared params (question mark type) to values.
     * 
     * @param \PDOStatement $stmt
     * @param array $condition SQL condition for prepared stmt => array('where' => 'col1 = ? AND col2 LIKE ?', 'bindings' => array(1, '%te%'))
     */
    protected function bindWhereParams(\PDOStatement $stmt, array $condition) {
      if ($condition) {
        $this->bindParams($stmt, $condition['bindings']);
      }
    }

    /**
     * Performs SELECT query and stores the result statement into the $result property.
     * 
     * @param string|array $what columns, which should be selected
     * @param string $from table name
     * @param array $condition SQL condition for prepared stmt => array('where' => 'col1 = ? AND col2 LIKE ?', 'bindings' => array(1, '%te%'))
     * @param string $order
     * @param string $groupBy
     * @param string $limit
     * 
     * @return resource|bool
     */
    public function select($what, $from, array $condition = array(), $order = '', $groupBy = '', $limit = '') {
      $where = (($condition) ? ' WHERE ' . $condition['where'] : '');
      $groupBy = (($groupBy) ? ' GROUP BY ' . $groupBy : '');
      $order = (($order) ? ' ORDER BY ' . $order : '');
      
      $query = 'SELECT ' . ((is_string($what)) ? $what : '' . implode(', ', $what) . '') . 
               " FROM _PREFIX_{$from}{$where}{$groupBy}{$order}{$limit}";
      
      $stmt = $this->prepare($query, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_SCROLL));
      $this->bindWhereParams($stmt, $condition);
      
      return $this->execute($stmt);
    }

    /**
     * Fetches result into the array of objects.
     * 
     * @param int $from
     * @param int $to
     * 
     * @return array
     */
    public function fetchObjects($from = -1, $to = -1) {
      $count = $this->rowCount();
    
      $limitBottom = (($from > -1) ? $from : 0);
      $limitTop = (($to > -1) ? min($to, $count) : $count);
      
      if ($limitBottom == 0 && $limitTop == $count) {
        $result = $this->result->fetchAll(\PDO::FETCH_OBJ);
      } else {
        $result = array();
        while (($row = $this->result->fetch(\PDO::FETCH_OBJ, \PDO::FETCH_ORI_ABS, $limitBottom)) && ($limitBottom < $limitTop)) {
          $result[] = $row;
          $limitBottom++;
        }
      }
      
      $this->result = $result;
      return $this->result;
    }

    /**
     * Fetches the first object of the resource.
     * 
     * @return object
     */
    public function fetchObject() {
      $this->result = $this->result->fetch(\PDO::FETCH_OBJ, \PDO::FETCH_ORI_FIRST);
      return $this->result;
    }

    /**
     * Gets the number of rows.
     * 
     * @param \PDOStatement $stmt
     * @return int
     */
    public function rowCount($stmt = NULL) {
      $obj = ((is_null($stmt)) ? $this->result : $stmt);
      if ($obj instanceof \PDOStatement) {
        return $obj->rowCount();
      }

      return 0;
    }

    /**
     * Performs aggregate function.
     * 
     * @param string $aggregateFunction
     * @param string $what
     * @param string $table
     * @param array  $condition SQL condition for prepared stmt => array('where' => 'col1 = ? AND col2 LIKE ?', 'bindings' => array(1, '%te%'))
     * 
     * @return string|NULL
     */
  	public function aggregate($aggregateFunction, $what, $from, array $condition = array()) {
      $what = (($what == '*') ? '*' : "{$what}");
      $this->select($aggregateFunction . '(' . $what . ') AS aggregation', $from, $condition);
      $this->fetchObject();
      
      return (($this->result) ? $this->result->aggregation : NULL);
  	}

    /**
     * Performs MySQL INSERT query.
     * 
     * @param string $into table name
     * @param array $data array('column' => 'value', ...)
     * 
     * @return int inserted id
     */
    public function insert($into, $data) {
      // query
      $query = "INSERT INTO _PREFIX_{$into} (" . implode(', ', array_keys($data)) . ') ' .
               'VALUES (' . implode(', ', array_fill(0, count($data), '?')) . ')';
      
      $stmt = $this->prepare($query);
      $this->bindParams($stmt, array_values($data));
      
      $this->execute($stmt);
      return $this->insertedId();
    }

    /**
     * Performs MySQL DELETE query.
     * 
     * @param string $from
     * @param array $condition SQL condition for prepared stmt => array('where' => 'col1 = ? AND col2 LIKE ?', 'bindings' => array(1, '%te%'))
     * 
     * @return bool
     */
    public function delete($from, array $condition) {
      if ($condition) {
        $stmt = $this->prepare("DELETE FROM _PREFIX_{$from} WHERE {$condition['where']}");
        $this->bindWhereParams($stmt, $condition);
        
        return $this->execute($stmt);
      } else {
        throw new AppException('DELETE query has no condition.');
      }
    }

    /**
     * Performs MySQL UPDATE query.
     * 
     * @param string $table
     * @param array $data array('column' => 'value', ...)
     * @param array $condition SQL condition for prepared stmt => array('where' => 'col1 = ? AND col2 LIKE ?', 'bindings' => array(1, '%te%'))
     * 
     * @return bool
     */
    public function update($table, $data, array $condition) {
      // escape data and prepare to update
      $prepared = $data;
      array_walk($prepared, function(&$value, $column) {
        $value = "{$column} = ?";
      });

      // create prepared statement and bind params of data to set and bind params of where condition -> !WARNING -> the order is essential
      $stmt = $this->prepare("UPDATE _PREFIX_{$table} SET " . implode(', ', $prepared) . " WHERE {$condition['where']}");
      $this->bindParams($stmt, array_merge(array_values($data), $condition['bindings']));
      
      return $this->execute($stmt);
    }
    
    /**
     * Retrieves the ID generated for an AUTO_INCREMENT column by the previous query.
     * 
     * @return int
     */
    public function insertedId() {
      $lastId = (int)$this->connection->lastInsertId();
      return (($lastId === 0) ? TRUE : $lastId);
    }
    
    /**
     * Zavola ulozenou proceduru s parametry
     * 
     * @param string $storedProcedure
     * @param array $params
     */
    public function call($storedProcedure, array $params = []) {
      $query = "CALL {$storedProcedure}('" . implode("', '", $params) . "')";
      $stmt = $this->prepare($query, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_SCROLL));
      
      return $this->execute($stmt);
    }

    /********************************* GETTERS *********************************/

    public function getServer() {
      return $this->server;
    }

    public function getUser() {
      return $this->user;
    }

    public function getPassword() {
      return $this->password;
    }

    public function getDb() {
      return $this->db;
    }
    
    public function getPrefix() {
      return $this->prefix;
    }

    public function getResult() {
      return $this->result;
    }
  }