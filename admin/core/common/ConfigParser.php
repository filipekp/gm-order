<?php
  namespace prosys\core\common;
  
  /**
   * Reprezentuje konektor napojeni na MSSql databazi MoneyS3.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class ConfigParser {
    private $_file;
    private $_config;
    
    /**
     * Jestlize je predan soubor a existuje, je rozparsovan a ulozen do vnitrni promenne.
     * @param string $file
     */
    public function __construct($file = NULL) {
      if ($file) {
        if (file_exists($file)) {
          $this->_file = $file;
          $this->_config = $this->parse();
        } else {
          trigger_error('file "' . $file . '" not exists', E_USER_NOTICE);
        }
      }
    }
    
    /**
     * Rozparsuje INI soubor.
     * @return array
     */
    protected function parseINI() {
      return parse_ini_file($this->_file, TRUE);
    }
    
    /**
     * Rozparsuje konfiguracni soubor.
     * @return array
     */
    private function parse() {
      switch ($this->getConfigType()) {
        case 'ini': return $this->parseINI();
        default:    return NULL;
      }
    }
    
    /**
     * Zjisti typ konfiguracniho souboru.
     * @return string
     */
    private function getConfigType() {
      return 'ini';
    }
    
    /**
     * Getter.
     * @return array
     */
    public function config() {
      return $this->_config;
    }
    
    /**
     * Setter.
     * 
     * @param array $config
     * @return \MoneyS3\ConfigParser
     */
    public function setConfig($config) {
      $this->_config = $config;
      return $this;
    }
  }
