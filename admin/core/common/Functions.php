<?php
  namespace prosys\core\common;
  
  use prosys\core\common\types\html\InputHidden;

  /**
   * Global settings of the application.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class Functions
  {
    /**
     * Handles session messages.
     * 
     * @param string $type
     * @return string
     */
    public static function handleMessages($type = NULL) {
      // handle message of one type
      $handleMessage = function($type) {
        if (array_key_exists($type, $_SESSION) && $_SESSION[$type]) {
          $sessionMsg = $_SESSION[$type];
          if (is_array($sessionMsg)) {
            $message = addslashes(implode('<br />', $sessionMsg));
          } else {
            $message = addslashes($sessionMsg);
          }
          
          unset($_SESSION[$type]);

          $type = filter_var($type, FILTER_SANITIZE_NUMBER_INT);
          return "<script>getStatusMessage({$type}, '{$message}');</script>";
        }
      };
      
      // handle messages
      if (is_null($type)) {
        return $handleMessage(Settings::MESSAGE_INFO) .
               $handleMessage(Settings::MESSAGE_SUCCESS) .
               $handleMessage(Settings::MESSAGE_WARNING) .
               $handleMessage(Settings::MESSAGE_ERROR) .
               $handleMessage(Settings::MESSAGE_EXCEPTION);
      } else {
        return $handleMessage($type);
      }
    }
    
    /**
     * Vrati hlasku pro admin rozhrani.
     * 
     * @param bool $array
     * @param string $type
     * 
     * @return string|array
     */
    public static function handleMessagesAdmin($array = FALSE, $type = NULL, $showingType = 2) {
      // handle message of one type
      $handleMessage = function($type) {
        if (array_key_exists($type, $_SESSION) && $_SESSION[$type]) {
          $sessionMsg = $_SESSION[$type];
                    
          if (is_array($sessionMsg)) {
            $message = '<ul><li>' .
                       implode('</li><li>', $sessionMsg) .
                       '</li></ul>';
          } else {
            $message = '<ul><li>' .
                       $sessionMsg .
                       '</li></ul>';
          }
          
          unset($_SESSION[$type]);
          
          return array($type => $message);
        }
        
        return array();
      };
      
      // handle messages
      if (is_null($type)) {
        $return = 
               array_merge($handleMessage(Settings::MESSAGE_ERROR),
               $handleMessage(Settings::MESSAGE_EXCEPTION),
               $handleMessage(Settings::MESSAGE_WARNING),
               $handleMessage(Settings::MESSAGE_INFO),
               $handleMessage(Settings::MESSAGE_SUCCESS));
      } else {
        $return = $handleMessage($type);
      }
      
      if ($array) {
        return $return;
      } else {
        switch ($showingType) {
          case 1: $showingType = 'getStatusMessage'; break;
          case 2: $showingType = 'getWin8StatusMessage'; break;
          case 3:
          default: $showingType = 'getToasterStatusMessage'; break;
        }
        return "<script>" . $showingType . "('" . addslashes(json_encode($return)) . "');</script>";
      }
    }
    
    /**
     * Converts date into the storage (resp. human friendly) format.
     * 
     * @param string $date PHP date string
     * @param bool $toStorage
     * 
     * @return string
     */
    public static function dateConvert($date, $toStorage = TRUE) {
      $format = (($toStorage) ? 'Y-m-d' : 'd.m.Y' );
      return (($date && !is_null($date)) ? date($format, strtotime($date)) : '');
    }
    
    /**
     * Return the first element of the array.
     * 
     * @param array $param
     * @param bool $getKey
     * 
     * return mixed
     */
    public static function first(array $param, $getKey = FALSE) {
      $value = reset($param);      
      return (($getKey) ? key($param) : (($value) ? $value : NULL));
    }
    
    /**
     * Trims the array.
     * 
     * @param array $array
     */
    public static function trimArray(array $array) {
      array_walk_recursive($array, function(&$item) {
        if (is_string($item)) {
          $item = trim($item);
        }
      });
      
      return $array;
    }
    
    /**
     * Zkontroluje, zda je objekt pozadovaneho typu.
     * 
     * @param object $object
     * @param string $type
     * 
     * @return bool
     */
    public static function isType($object, $type) {
      switch ($type) {
        case 'string':  return is_string($object);
        case 'int':
        case 'integer': return is_int($object);
        case 'float':
        case 'double':  return is_float($object);
        case 'bool':
        case 'boolean': return is_bool($object);

        default:        return is_a($object, $type);
      }
    }
    
    /**
     * Recasts given value to required data type.
     * 
     * @param mixed $object
     * @param string $type
     * 
     * @return mixed
     */
    public static function retype($object, $type) {
      switch (strtolower($type)) {
        case 'datetime':  return ((is_string($object)) ? new \DateTime(date('Y-m-d H:i:s', strtotime($object))) : new \DateTime());
        case 'int':
        case 'integer':   return (int)$object;
        case 'float':
        case 'double':
        case 'real':      return (float)((is_string($object)) ? str_replace(',', '.', $object) : $object);
        case 'bool':
        case 'boolean':   return (bool)$object;
        case 'array':     return (array)$object;
        case 'object':    return (object)$object;
        case 'null':      return (unset)$object;

        default:          return (string)$object;
      }
    }
    
    /**
     * Pretypuje objekt na entitu.
     * 
     * @param object $object
     * @param string $type
     * 
     * @return mixed
     * @throws AppException jestlize dany typ neni podporovan
     */
    public static function retypeToEntity($object, $type) {
      /* @var $dao \prosys\model\DataAccessObject */
      $dao = Agents::getAgent(str_replace('Entity', 'Dao', $type), Agents::TYPE_MODEL);
      $type = Agents::getNamespace(Agents::TYPE_MODEL) . $type;

      // pro prazdny objekt vygeneruje prazdnou instanci entity
      if (!$object) {
        $object = $dao->load();
      }
      
      // je-li treba pretypovat
      if (!is_a($object, $type)) {
        $object = $dao->load(array(Settings::SHOW_DELETED_PREDICATE => TRUE, $type::PRIMARY_KEY() => $object));
        if ($object->isNew()) {
          throw new AppException("Entity '{$type}' failed to be created from the object '{$object}'.");
        }
      }

      return $object;
    }
    
    /**
     * Prevede objekt na textovy retezec.
     * 
     * @param mixed $object
     * @param mixed $caller
     * 
     * @return string
     * @throws AppException jestlize se nepodarilo objekt prevest
     */
    public static function toString($object, $caller = NULL) {
      $objectType = gettype($object);
      $caller = ((is_null($caller)) ? '' : get_class($caller) . ': ');
      
      switch ($objectType) {
        case 'boolean':
          $object = (int)$object;
        case 'integer':
        case 'double':
        case 'string': return (string)$object;
        case 'NULL':   return 'NULL';
        case 'array':  return implode('|', array_map(array($this, 'toString'), $object));

        case 'object':
          if (is_a($object, 'DateTime')) {
            return $object->format('Y-m-d H:i:s');
          } else {
            try {
              $rc = new \ReflectionClass($object);

              if ($rc->hasMethod('__toString')) {
                return (string)$object;
              } else {
                throw new AppException($caller . "The object of type '{$rc->getName()}' has not implemented __toString method.");
              }
            } catch (\ReflectionException $e) { printf('%s', $e->getMessage()); }
          }

        default:
          // entita by misto: get_class($caller) mela zavolat get_class($caller)::classname()
          throw new AppException($caller . "The type '" . $objectType . "' of the object cannot be converted to the string.");
      }
    }
    
    /**
     * Returns a string composed by entire words specified by length.
     * 
     * @param string $string
     * @param int $limit
     * 
     * @return string
     */
    public static function trimEntireWords($string, $limit = 40) {
      // return string when lenght is < then limit
      if (mb_strlen($string, 'UTF-8') < $limit) {
        return $string;
      }

      $regex = '/(.{1, $limit})\b/';
      $matches = array('', '');
      
      preg_match($regex, $string, $matches);
      return $matches[1] . ' ...';
    }
    
    /**
     * Remove or add key to query string.
     * 
     * @param string $query
     * @param array $remove
     * @param array $add
     * 
     * @return string
     */
    public static function modifyHttpQuery($query, $remove = array(), $add = array()) {
      $parsed = array();
      parse_str($query, $parsed);
      
      // remove wanted
      $removed = array_diff_key($parsed, array_flip($remove));
      $added = $removed + $add;

      return http_build_query($added);
    }
    
    /**
     * Remove or add items to array.
     * 
     * @param array $array
     * @param array $remove pole klicu
     * @param array $add asociativni pole
     * 
     * @return array
     */
    public static function modifyArrayByKey(array $array, $remove = array(), $add = array()) {
      $removed = array_diff_key($array, array_flip($remove));
      $added = $removed + $add;

      return $added;
    }
    
    /**
     * Odstrani diakritiku z textu
     * 
     * @param string $text
     * @return string
     */
    public static function removeDiacritics($text) {
      // remove diacritics
      $conversionTable = Array(
        'ä'=>'a', 'Ä'=>'A', 'á'=>'a', 'Á'=>'A', 'à'=>'a', 'À'=>'A', 'ã'=>'a', 'Ã'=>'A', 'â'=>'a', 'Â'=>'A', 'ą'=>'a', 'Ą'=>'A', 'ă'=>'a', 'Ă'=>'A',
        'č'=>'c', 'Č'=>'C', 'ć'=>'c', 'Ć'=>'C', 'ç'=>'c', 'Ç'=>'C',
        'ď'=>'d', 'Ď'=>'D', 'đ'=>'d', 'Đ'=>'D',
        'ě'=>'e', 'Ě'=>'E', 'é'=>'e', 'É'=>'E', 'ë'=>'e', 'Ë'=>'E', 'è'=>'e', 'È'=>'E', 'ê'=>'e', 'Ê'=>'E', 'ę'=>'e', 'Ę'=>'E',
        'í'=>'i', 'Í'=>'I', 'ï'=>'i', 'Ï'=>'I', 'ì'=>'i', 'Ì'=>'I', 'î'=>'i', 'Î'=>'I',
        'ľ'=>'l', 'Ľ'=>'L', 'ĺ'=>'l', 'Ĺ'=>'L', 'ł'=>'l', 'Ł'=>'L',
        'ń'=>'n', 'Ń'=>'N', 'ň'=>'n', 'Ň'=>'N', 'ñ'=>'n', 'Ñ'=>'N',
        'ó'=>'o', 'Ó'=>'O', 'ö'=>'o', 'Ö'=>'O', 'ô'=>'o', 'Ô'=>'O', 'ò'=>'o', 'Ò'=>'O', 'õ'=>'o', 'Õ'=>'O', 'ő'=>'o', 'Ő'=>'O',
        'ř'=>'r', 'Ř'=>'R', 'ŕ'=>'r', 'Ŕ'=>'R',
        'š'=>'s', 'Š'=>'S', 'ś'=>'s', 'Ś'=>'S', 'ş'=>'s', 'Ş'=>'S',
        'ť'=>'t', 'Ť'=>'T', 'ţ'=>'t', 'Ţ'=>'T',
        'ú'=>'u', 'Ú'=>'U', 'ů'=>'u', 'Ů'=>'U', 'ü'=>'u', 'Ü'=>'U', 'ù'=>'u', 'Ù'=>'U', 'ũ'=>'u', 'Ũ'=>'U', 'û'=>'u', 'Û'=>'U', 'ű'=>'u', 'Ű'=>'U',
        'ý'=>'y', 'Ý'=>'Y',
        'ž'=>'z', 'Ž'=>'Z', 'ź'=>'z', 'Ź'=>'Z', 'ż'=>'z', 'Ż'=>'Z'
      );
      
      return strtr($text, $conversionTable);
    }
    
    /**
     * Conversion text to lower charakter, remove diacritics with delimiter.
     * 
     * @param string $text
     * @param string $delimiter
     * @return string
     */
    public static function seoTypeConversion($text, $delimiter = '-') {
      // convert to lower
      $lower = strtolower(self::removeDiacritics($text));

      // replace everything with dashes - excluding upper letters, lower letters, numbers and dashes
      $pattern = '/[^0-9a-zA-Z' . $delimiter . ']/';
      $multipleDelimiters = preg_replace($pattern, $delimiter, $lower);

      // replace multiple dashes to one dash
      $seo = preg_replace('/' . str_replace('.', '\\.', $delimiter) . '+/', $delimiter, $multipleDelimiters);

      // return dash trimmed seo
      return trim($seo, $delimiter);
    }
    
    /**
     * Is date holiday return TRUE else FALSE.
     * 
     * @param type $date
     * @return bolean
     */
    public static function isHoliday($date) {
      $time = strtotime($date);
      
      $isWeekend = date('N', $time) >= 6;
      $isEaster = $time == (easter_date(date('Y', $time)) + strtotime('+1day')); // pricten jeden den aby se pocitalo pondeli velikonocni

      $isHoliday = FALSE;
      $holidays = array(
        array(1,1,'Den obnovy samostatného českého státu'),
        array(1,5,'Svátek práce'),
        array(8,5,'Den vítězství'),
        array(5,7,'Den slovanských věrozvěstů Cyrila a Metoděje'),
        array(6,7,'Den upálení mistra Jana Husa'), 
        array(28,9,'Den české státnosti'),
        array(28,10,'Den vzniku samostatného československého státu'),
        array(17,11,'Den boje za svobodu a demokracii'),
        array(24,12,'Štědrý den'),
        array(25,12,'1. svátek vánoční'),
        array(26,12,'2. svátek vánoční')
      );
      foreach ($holidays as $holiday) {
        if ($holiday[0] . '.' . $holiday[1] . '.' == date('j.n.', $time)) {
          $isHoliday = TRUE;
          break;
        }
      }   
      return ($isHoliday || $isWeekend($time) || $isEaster($time));
    }

    /**
     * Add work day by input.
     * 
     * @param string $date
     * @param int $numOfDays (+1 or -2)
     * @return int
     */
    public static function addBusinessDays($date, $numOfDays) {
      $direction = (($numOfDays < 0) ? '-' : '+');
      $date = date('Y-m-d', strtotime($direction . abs($numOfDays) . 'days', strtotime($date)));

      while (self::isHoliday($date) === TRUE) {
        $date = date('Y-m-d', strtotime($direction . '1days', strtotime($date)));
      }
      return strtotime($date);
    }
    
    /**
     * According to given locale prints out the number in the price format.
     * 
     * @param float $number
     * @param string $locale
     * @param int $decimals
     * 
     * @return string
     */
    public static function priceFormat($number, $locale = 'cs_CZ', $decimals = NULL) {
      if (function_exists('money_format')) {
        setlocale(LC_MONETARY, $locale);

        switch ($locale) {
          case 'cs_CZ':
          default:
            $number = (($decimals) ? round($number, $decimals) : $number);
            return money_format('%i', $number);
        }
      } else {
        switch ($locale) {
          case 'cs_CZ':
          default:
            $decimals = ((is_null($decimals)) ? 2 : $decimals);
            return number_format(round($number, $decimals), $decimals, ',', ' ') . ' Kč';
        }
      }
    }
      
    /**
     * Recalculates numbers into the percents, to create one hundred unit in sum.<br />
     * Preserves associative array keys.
     * 
     * @param array $data
     * @param float $total [optional=100] the number to which should be recalculated (default value is used for percents)
     * @param int $round [optional=FALSE]
     * 
     * @return array
     */
    public static function recalculateByRatio($data, $total = 100, $round = FALSE) {
      $sum = array_sum($data);
      
      if ($sum) {
        $fraction = $total / $sum;
        $returnArray = array_map(function($item) use($fraction, $round) {
          $item = $fraction * $item;
          return (($round === FALSE) ? $item : round($item, $round));
        }, $data);
        
        // adjustment of the output field to the sum of the values ​​was 100
        $sumResult = array_sum($returnArray);
        $difference = abs($total - $sumResult);
        if ($sumResult != $total) {
          if ($sumResult > $total) {
            $maxKey = array_keys($returnArray, max($returnArray));
            $returnArray[$maxKey[0]] = $returnArray[$maxKey[0]] - $difference;
          } else if ($sumResult < $total) {
            $minKey = array_keys($returnArray, min($returnArray));
            $returnArray[$minKey[0]] = $returnArray[$minKey[0]] + $difference;
          }
        }
        
        return $returnArray;
      } else {
        return $data;
      }
    }
    
    /**
     * Převede sekundy do formátu času<br />
     * Pokud je počet sekund menší jak 3600 tak je vrácen fromát i:s jinak H:i:s
     * 
     * @param int $seconds
     * @return string
     */
    public static function secondsToTimeFormat($seconds) {
      return gmdate((($seconds >= 3600) ? 'H \h ' : '') . (($seconds >= 60) ? 'i \m ' : '') . 's \s', $seconds);
    }
    
    /**
     * Vygeneruje náhodný řetězec pro zadanou délku.
     * 
     * @param int $length
     * @return string
     */
    public static function randomString($length = 10) {
      // string pro generovani
      $pool = '0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ';                         
      // zde bude výsledný řetězec uložen
      $resString = '';
      for ($i=0; $i < $length; $i++) {
          $resString .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
      }

      return $resString;
    }
    
    /**
     * Funkce pro skoloňování slov podle čísla.
     * 
     * @param float $number
     * @param type $allOther výraz pro hodnotu vetší než 5 nebo rovno 0
     * @param type $once výraz pro hodnotu rovno 1
     * @param type $oneToFive výraz pro hodnotu vetší než 1 nebo menší než 5
     * @param type $float výraz pro desetinná čísla
     * 
     * @return string '$number word form' => '1 hrnek'
     */
    public static function inflection($number, $allOther, $once, $oneToFive, $float) {
        $string = $number."&nbsp;";
        if ((int)$number != $number) {
          $string .= $float;
        } elseif ($number == 0 OR $number >= 5) {
          $string .= $allOther;
        } elseif ($number == 1) {
          $string .= $once;
        } elseif ($number < 5) {
          $string .= $oneToFive;
        }

        return $string;
    }
    
    /**
     * Vrati nahodnou podmnozinu podmnoziny pole.
     * 
     * @param array $array    kompletni pole
     * @param int $size       velikost pozadovane podmnoziny
     * @param int $offset     parametr pro vytvoreni subpole kompletniho pole, ze ktereho se bude vybirat nahodna podmnozina - pocatecni index
     * @param int $length     parametr pro vytvoreni subpole kompletniho pole, ze ktereho se bude vybirat nahodna podmnozina - pocet prvku od pocatecniho indexu
     * 
     * @return array
     */
    public static function randomArrayAssoc(array $array, $size = NULL, $offset = 0, $length = NULL) {
      $keys = array_slice(array_keys($array), $offset, $length);
      shuffle($keys);
      
      $sample = array_slice($keys, 0, (($size) ? $size : count($keys)));
      return array_combine(
        $sample,
        array_map(function($key) use ($array) {
          return $array[$key];
        }, $sample)
      );
    }
    
    /**
     * Zjisti, zda pozadovany offset existuje v danem objektu.
     * 
     * @param mixed $offset
     * @param mixed $object
     * 
     * @return bool
     */
    private static function offsetExists($offset, $object) {
      return ((is_a($object, '\ArrayAccess')) ? $object->offsetExists($offset) : ((is_array($object)) ? array_key_exists($offset, $object) : FALSE));
    }
    
    /**
     * "Bezpecne" odebere prvek z pole -> zkontroluje existenci.
     * 
     * @param array $array
     * @param string $key
     * 
     * @return mixed
     */
    public static function unsetItem(&$array, $key) {
      if (self::offsetExists($key, $array)) {
        $value = ((is_object($array[$key])) ? clone $array[$key] : $array[$key]);
        unset($array[$key]);
        
        return $value;
      }
      
      return NULL;
    }
    
    /**
     * "Bezpecne" ziska prvek z pole (celou cestu) -> zkontroluje existenci.
     * 
     * @param array|\ArrayAccess $array
     * @param string|array $path
     * @param mixed $default
     * 
     * @return mixed
     */
    public static function item($array, $path, $default = NULL) {
      $current = $array;
      foreach ((array)$path as $key) {
        if (self::offsetExists($key, $current)) {
          $current = $current[$key];
        } else {
          return $default;
        }
      }

      return $current;
    }
  
    /**
     * Ziska parametry name pro inputy.
     * 
     * @param array $array
     * @param array $inputsArr
     * @param string $keyOld
     */
    public static function getNameRecursive($array, &$inputsArr, $keyOld = '') {
      foreach ($array as $key => $value) {
        if (is_array($value)) {
          $newKey = $keyOld . '[' . $key . ']';
          self::getNameRecursive($value, $inputsArr, $newKey);
        } else {
          $inputsArr[$keyOld . '[' . $key . ']'] = $value;
        }
      }
    }

    /**
     * Rekurzivne vygeneruje hidden inputy.
     * 
     * @param array $array
     * @return array
     */
    public static function generateHiddenInputs($array) {
      $inputsArr = array();

      foreach ($array as $key => $value) {
        if (is_array($value)) {
          self::getNameRecursive($value, $inputsArr, $key);
        } else {
          $inputsArr[$key] = $value;
        }
      }

      $getParamsInputs = array();
      foreach ($inputsArr as $key => $value) {
        $getParamsInputs[] = new InputHidden($key, $value);
      }

      return $getParamsInputs;
    }
    
    /**
     * Rekurzivne prohleda prvek a najde pozadovanou hodnotu.
     * 
     * @param mixed $needle
     * @param mixed $haystack
     * @param array $callbacks pole obsahujici callback pro ziskani hodnoty (identita jako vychozi) a callback pro ziskani potomku (klic 'children' jako vychozi)
     * 
     * @return mixed vrati nalezeny prvek, v pripade, ze prvek nenalezne, vrati FALSE
     */
    public static function arraySearchRecursive($needle, $haystack, array $callbacks = ['value' => NULL, 'children' => NULL]) {
      $identity = function($item) { return $item; };
      $children = function($item) { return $item['children']; };
      
      $callbacks['value'] = ((is_null($callbacks['value'])) ? $identity : $callbacks['value']);
      $callbacks['children'] = ((is_null($callbacks['children'])) ? $children : $callbacks['children']);
      
      foreach($haystack as $item) {
        if ($callbacks['value']($item) === $needle) {
          return $item;
        } elseif (($recursive = self::arraySearchRecursive($needle, $callbacks['children']($item), $callbacks)) !== FALSE) {
          return $recursive;
        }
      }
      
      return FALSE;
    }
    
    /**
     * Nahradi promenne v textu za realny text
     * 
     * @param string $string
     * @param array $replacements
     * 
     * @return string
     */
    public static function replaceVars($string, array $replacements = array()) {
      // typy promennych
      $_KEYWORDS = ['TERNARY' => 'ternary', 'VARIABLE' => 'var', 'GLOBAL_VARIABLE' => 'settingsVar'];
    
      $matches = NULL;
      preg_match_all('@(.*)\{(' . implode('|', $_KEYWORDS) . ')=(.*?)\}(.*)@', $string, $matches);
      
      if ($matches) {
        while (preg_match('@(.*?)\{(' . implode('|', $_KEYWORDS) . ')=(.*?)\}(.*)@', $string, $matches)) {
          switch ($matches[2]) {
            case $_KEYWORDS['TERNARY']:
              $ternary = NULL;
              preg_match('@(.*)\?\'(.*)\':\'(.*)\'@', $matches[3], $ternary);

              $replacement = '';
              if (eval('return ' . str_replace(array_keys($replacements), array_map(function($item) { return "'{$item}'"; }, $replacements), $ternary[1]) . ';')) {
                $replacement = str_replace(array_keys($replacements), $replacements, $ternary[2]);
              } else {
                $replacement = str_replace(array_keys($replacements), $replacements, $ternary[3]);
              }

              $stringRes = $matches[1] . $replacement . $matches[4];
            break;
            
            case $_KEYWORDS['GLOBAL_VARIABLE']:
              eval('$valueArr = explode("|", "' . $matches[3] . '");');
              $stringRes = $matches[1] . Settings::getVariable($valueArr[0], $valueArr[1]) . $matches[4];
            break;

            case $_KEYWORDS['VARIABLE']:
              $stringRes = $matches[1] . $replacements[$matches[3]] . $matches[4];
            break;
          }
          
          $string = str_replace($matches[0], $stringRes, $string);
        }
      }
      
      return $string;
    }
    
    /**
     * Funkce pro zjisteni mime-type souboru nebo bufferu
     * 
     * @param mixed $content
     * @param string $type
     * 
     * @return string
     */
    public static function getMimeType($content, $type = 'file') {
      $finfo = finfo_open(FILEINFO_MIME_TYPE);
      switch ($type) {
        case 'buffer':
          $mimeType = finfo_buffer($finfo, $content);
        break;
        
        case 'file':
        default:
          $mimeType = finfo_file($finfo, $content);
        break;
      }
      finfo_close($finfo);
      
      return $mimeType;
    }
    
    /**
     * Odmaskuje retezec podle predaneho cisla a data.
     * 
     * @param string $mask
     * @param ineger $number
     * @param string $date optional
     * 
     * @return string
     */
    public static function unmaskString($mask, $number, $date = '') {
      $time = (($date) ? strtotime($date) : time());
      $maskArray = [];
      preg_match_all('/(\{.*?\})/', $mask, $maskArray);
      
      foreach ($maskArray[1] as $value) {
        $valueReal = preg_replace('/{(.*)}/', '$1', $value);
        
        if (strpos($valueReal, '#') !== FALSE) {
          $valueReal = sprintf('%0' . strlen($valueReal). 'd', $number);
        } else if (strpos($valueReal, 'Y') !== FALSE) {
          switch ($valueReal) {
            case 'YYYY':
              $valueReal = date('Y', $time);
            break;
          
            case 'YYY':
              $valueReal = substr(date('Y', $time), 0, 1) . substr(date('Y', $time), -2);
            break;
          
            case 'Y':
            default:
              $valueReal = date('y', $time);
            break;
          }
        }
        
        $mask = str_replace($value, $valueReal, $mask);
      }
      return $mask;
    }
    
    /**
     * Filtruje pole podle klicu; tzn. stejne jako funkce array_filter, ale parametr $item v callbacku je klicem.
     * 
     * @param array $array
     * @param callable $callback
     * 
     * @return array
     */
    public static function arrayFilterByKey(array $array, $callback) {
      return array_intersect_key($array, 
        array_flip(
          array_filter(array_keys($array), $callback)
        )
      );
    }
    
    /**
     * Zjisti, zda je pozadovana hodnota mezi konstantami s danym prefixem.
     * 
     * @param mixed $value
     * @param string $class
     * @param string $prefix
     * 
     * @return bool
     */
    public static function isConstantByPrefix($value, $class, $prefix) {
      $reflection = new \ReflectionClass($class);
      $constants = $reflection->getConstants();

      if ($prefix) {
        $filtered = Functions::arrayFilterByKey($constants, function($key) use ($prefix) {
          return strpos($key, $prefix) === 0;
        });
      } else {
        $filtered = $constants;
      }
      
      return in_array($value, $filtered);
    }
    
    /**
     * Vrátí zkrácený řetězec končící celým slovem
     * 
     * @param string $string
     * @param int $limit
     * 
     * @return string
     */
    public static function trimWholeWord($string, $limit = 40) {
      //Return strin when lenght is < then limit
      if(strlen($string) < $limit) {return $string;}

      $regex = "/(.{1,$limit})\b/";
      $matches = array('', '');
      
      preg_match($regex, $string, $matches);
      return $matches[1] . ' ...';
    }
    
    /**
     * Funkce pro rekuzivni implode
     * 
     * @param string $glue
     * @param array $array
     * 
     * @return string
     */
    public static function implodeRecursive($glue, array $array) {
      $ret = '';

      foreach ($array as $item) {
        if (is_array($item)) {
          $ret .= self::implodeRecursive($glue, $item) . $glue;
        } else {
          $ret .= $item . $glue;
        }
      }

      $ret = substr($ret, 0, 0-strlen($glue));

      return $ret;
    }
    
    /**
     * Vrati prava lokalniho souboru.
     * 
     * @param string $filepath
     * @return octal
     */
    public static function localPermissions($filepath) {
      return octdec(substr(sprintf('%o', fileperms($filepath)), -4));
    }
    
    /**
     * Rozparsuje velikost s pripadnou jednotkou a vrati hodnotu v bajtech.
     * 
     * @param type $size
     * @return type
     */
    public static function parseIniSize($size) {
      $units = 'bkmgtpezy';   // vsechny mozne jednotky serazene od nejmensi (bajt) k nejvetsimu (yottabajt)
      
      $unit = preg_replace("/[^{$units}]/i", '', $size);
      $number = preg_replace('/[^0-9\.]/', '', $size);
      
      // v pripade, ze je velikost dana s jednotkou, bude nasobit 1024^n, kde n je pozice jednotky v serazenem retezci jednotek
      return $number * (($unit) ? pow(1024, stripos($units, $unit[0])) : 1);
    }
    
    /**
     * Vrati maximalni moznou velikost povolenou serverem pro upload souboru.
     * @return int
     */
    public static function getMaxUploadFileSize() {
      $maxSize = self::parseIniSize(ini_get('post_max_size'));
      $uploadMax = self::parseIniSize(ini_get('upload_max_filesize'));
      
      if ($uploadMax && $uploadMax < $maxSize) {
        $maxSize = $uploadMax;
      }

      return (($maxSize) ? $maxSize : 999999999);
    }
    
    /**
     * Vrati unikatni nazev souboru na dane ceste.<br />
     * Tzn. neexistuje-li soubor, vrati primo $filepath, existuje-li, vrati soubor s podtrzitkem a indexem (dle poctu duplicitnich souboru).
     * 
     * @param string $filepath
     * @param string $extension parametr je jen pro "efektivitu", metoda nemusi zjistovat priponu souboru
     * 
     * @return string
     */
    public static function getUniqueFilename($filepath, $extension = FALSE) {
      $info = (($extension) ? ['extension' => $extension] : pathinfo($filepath));

      $extension = '.' . $info['extension'];
      $base = substr($filepath, 0, -strlen($extension));

      $index = -1;
      do {
        $index++;

        $filepath = $base . (($index) ? '_' . $index : '') . $extension;
        $exists = file_exists($filepath);
      } while ($exists);

      return $filepath;
    }
    
    /**
     * Provede bezpecne vlozeni souboru pres require|require_once.
     * 
     * @param string $file
     * @param bool $once
     */
    public static function requireFile($file, $once = FALSE) {
      $suffix = (($once) ? '_once' : '');
      if (file_exists($file)) {
        call_user_func('require' . $suffix, $file);
      }
    }
    
    /**
     * Pretypuje instanci objektu na jiny objekt.<br />
     * <b>Pozor:</b> prenese pouze to, co je mozne prenest serializaci a deserializaci.
     * 
     * @param mixed $instance
     * @param string $castType
     * 
     * @return mixed
     */
    public static function castToObject($instance, $castType) {
      return unserialize(sprintf(
        'O:%d:"%s"%s',
        strlen($castType),
        $castType,
        strstr(strstr(serialize($instance), '"'), ':')
      ));
    }
  }
