<?php
  namespace prosys\core\common;

  /**
   * Represents identity map, which keeps every agent object.<br />
   * Means e.g.: data access objects, views, SQL connection, ...
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class Agents
  {
    const TYPE_CORE = 'core';
    const TYPE_COMMON = '';
    const TYPE_MODEL = 'model';
    const TYPE_VIEW_FRONTEND = 'fe_view';
    const TYPE_VIEW_ADMIN = 'admin_view';
    const TYPE_CONTROLLER_FRONTEND = 'fe_controller';
    const TYPE_CONTROLLER_ADMIN = 'admin_controller';
    const TYPE_SECURITY = 'security';
    const TYPE_TYPES = 'types';
    const TYPE_STD = 'std_classes';
    
    private static $NS = array(
      self::TYPE_CORE                => '\prosys\core\\',
      self::TYPE_COMMON              => '\prosys\core\common\\',
      self::TYPE_MODEL               => '\prosys\model\\',
      self::TYPE_VIEW_ADMIN          => '\prosys\admin\view\\',
      self::TYPE_CONTROLLER_ADMIN    => '\prosys\admin\controller\\',
      self::TYPE_VIEW_FRONTEND       => '\prosys\web\view\\',
      self::TYPE_CONTROLLER_FRONTEND => '\prosys\web\controller\\',
      self::TYPE_SECURITY            => '\prosys\admin\security\\',
      self::TYPE_TYPES               => '\prosys\core\common\types\\',
      self::TYPE_STD                 => '\\'
    );

    private static $AGENTS = array();
    
    /**
     * Returns namespace of type.
     * 
     * @param string $type Agents::TYPE_{type}
     * @return string
     */
    public static function getNamespace($type) {
      return self::$NS[$type];
    }
    
    /**
     * Vrati agenta.
     * 
     * @param string $agent
     * @param string $type Agents::TYPE_{type}
     * @param array|NULL $params
     * @param string $tag vlastni oznaceni / klic agenta. Pod timto klicem bude mozne agenta vyzvednout
     * 
     * @return mixed
     */
    public static function getAgent($agent, $type = '', $params = NULL, $tag = '') {
      $agent = self::$NS[$type] . $agent;
      $tag = (($tag) ? $tag : $agent);
      if (!array_key_exists($tag, self::$AGENTS)) {
        try {
          $r = new \ReflectionClass($agent);
          
          if (is_null($params)) {

            self::$AGENTS[$tag] = $r->newInstance();
          } else {
            self::$AGENTS[$tag] = $r->newInstanceArgs($params);
          }
        } catch (\ReflectionException $e) {
          \PC::debug($e, 'reflection exception');
          return FALSE;
        }
      }
      
      return self::$AGENTS[$tag];
    }
    
    /**
     * Vrati agenta oznaceneho vlastnim tagem.
     * 
     * @param string $tag
     * @return mixed|NULL
     */
    public static function getAgentByTag($tag) {
      return Functions::item(self::$AGENTS, $tag);
    }
  }
