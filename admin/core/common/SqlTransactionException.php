<?php
  namespace prosys\core\common;

  /**
   * Rozsireni obecne vyjimky pro transakci SQL.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class SqlTransactionException extends AppException
  {
    public function __construct($message, $module = '', $code = 0, \Exception $previous = null) {
      parent::__construct($message, $module, 'transaction', $code, $previous);
    }
  }
