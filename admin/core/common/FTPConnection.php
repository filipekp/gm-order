<?php
  namespace prosys\core\common;
  
  /**
   * Reprezentuje FTP pripojeni.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class FTPConnection {
    protected $connection;

    protected $server;
    protected $user;
    protected $password;
    protected $dir;
    protected $result;
    
    /**
     * Inicializuje FTP server, vytvori spojeni a presune se do pozadovaneho adresare.
     * 
     * @param string $server
     * @param string $user
     * @param string $password
     * @param string $dir cesta k vychozimu adresari oddelena lomitky
     */
    public function __construct($server, $user = '', $password = '', $dir = '') {
      $this->server = $server;
      $this->user = $user;
      $this->password = $password;
      $this->dir = $dir;
      
      $this->connect();
    }
    
    /**
     * Ukonci FTP spojeni.
     */
    public function __destruct() {
      $this->disconnect();
    }

    /**
     * Vytvori FTP spojeni a do property $this->result ulozi jmeno aktualniho adresare.
     * 
     * @return boolean TRUE, je-li aktualni adresar ten, ktery je pozadovan uzivatelem, FALSE jinak.
     * @throws AppException nepodari-li se pripojit k FTP serveru.
     */
    private function connect() {
      $this->connection = ftp_connect($this->server);
      if (!$this->connection || !ftp_login($this->connection, $this->user, $this->password)) {
        throw new AppException('FTP connection to server ' . $this->server . ' failed.');
      }
      
      return $this->changeDir($this->dir);
    }
  
    /**
     * Ukonci FTP spojeni.
     */
    private function disconnect() {
      ftp_close($this->connection);
    }
    
    /**
     * Prejde do pozadovaneho adresare na FTP serveru.
     * 
     * @param string $path cesta k adresari oddelena lomitky
     * @return boolean TRUE, je-li aktualni adresar ten, ktery je pozadovan uzivatelem, FALSE jinak.
     */
    public function changeDir($path) {
      if ($path) {
        $this->result = ftp_pwd($this->connection);

        foreach (explode('/', rtrim($path, '/')) as $dir) {
          if (!ftp_chdir($this->connection, $dir)) {
            // vrati se do puvodniho adresare
            while (ftp_pwd($this->connection) != $this->result && ftp_cdup($this->connection)) {}
            return FALSE;
          }
        }

        $this->result = $dir;
      }

      return TRUE;
    }
    
    /**
     * Vrati seznam souboru v pozadovanem adresari na FTP serveru.
     * 
     * @param string $path cesta k adresari oddelena lomitky
     * @return array|bool seznam souboru nebo FALSE v pripade spatne cesty
     */
    public function listFiles($path = '') {
      if ($this->changeDir($path)) {
        return ftp_nlist($this->connection, $path);
      } else {
        return FALSE;
      }
    }
    
    /**
     * Stahne soubory $files z adresare $remotePath do lokalniho adresare $localTarget.
     * 
     * @param string $localTarget misto na lokalnim disku, kam budou soubory zkopirovany
     * @param array $files pri predani prazdneho pole zkopiruje vsechny soubory
     * @param string $remotePath vzdaleny adresar
     * 
     * @return array|bool seznam stazenych souboru
     * @throws AppException v pripade, ze neexistuje lokalni, ci vzdalena cesta
     * 
     * @todo Stahnout pouze soubory - metoda stahuje i adresare (prazdne).
     */
    public function download($localTarget, array $files = [], $remotePath = '') {
      if (($remote = $this->changeDir($remotePath)) && ($local = is_dir($localTarget))) {
        $copied = [];
        foreach ((($files) ? $files : $this->listFiles()) as $file) {
          if (@ftp_get($this->connection, $localTarget . $file, $file, FTP_BINARY)) {
            $copied[] = $file;
          }
        }
        
        return $copied;
      } else {
        $messages = array();
        if (!$remote) { $messages[] = 'Vzdálená cesta neexistuje: ' . $remotePath;       }
        if (!$local)  { $messages[] = 'Lokální cesta neexistuje: ' . $localTarget; }
        
        throw new AppException($messages);
      }
    }
    
    /**
     * Uploaduje soubory $files z lokalni cesty $localPath do vzdalene cilove slozky $remoteTarget.
     * 
     * @param string $remoteTarget misto na vzdalenem serveru, kam budou soubory zkopirovany
     * @param string $localPath lokalni adresar
     * @param array $filter pri predani prazdneho pole zkopiruje vsechny soubory
     * @param int $maxDepth maximalni hloubka rekurze
     * 
     * @return array|bool seznam uploadovanych souboru
     * @throws AppException
     * 
     * @todo Regularni vyrazy pro vzory souboru, ktere se maji nahrat - parametr $filter.
     */
    public function upload($remoteTarget, $localPath = './', array $filter = [], $maxDepth = -1) {
      $remote = TRUE;

      if (($local = is_dir($localPath)) && ($remote = $this->changeDir($remoteTarget))) {
        $localPath = rtrim($localPath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        
        // ma-li byt adresar zkopirovan rekurzivne
        $directory = new \RecursiveDirectoryIterator($localPath, \RecursiveDirectoryIterator::SKIP_DOTS);

        $iterator = new \RecursiveIteratorIterator($directory, \RecursiveIteratorIterator::SELF_FIRST);
        $iterator->setMaxDepth($maxDepth);

        $copied = [];
        foreach ($iterator as $file) {    /* @var $file \FilesystemIterator */
          $remoteFile = substr($file, strlen($localPath)) . (($file->isDir()) ? '/' : '');
          
          if ($filter && !array_filter($filter, function($pattern) use ($remoteFile) {
            return (bool)preg_match($pattern, $remoteFile);
          })) {
            continue;
          }
          
          $success = FALSE;
          if ($file->isDir()) {
            $success = (bool)@ftp_mkdir($this->connection, $remoteFile);
          } else {
            $success = (bool)@ftp_put($this->connection, $remoteFile, $file, FTP_BINARY);;
          }

          if ($success) {
            @ftp_chmod($this->connection, Functions::localPermissions($file), $remoteFile);
            $copied[] = $remoteFile;
          }
        }
        
        return $copied;
      } else {
        $messages = array();
        if (!$local)  { $messages[] = 'Lokální cesta neexistuje: ' . $localPath; }
        if (!$remote) { $messages[] = 'Vzdálená cesta neexistuje: ' . $remoteTarget; }

        throw new AppException($messages);
      }
    }
    
    /**
     * Smaze pozadovane soubory v aktualnim adresari na FTP serveru.
     * 
     * @param array $files pri predani prazdneho pole smaze vsechny soubory
     * @param string $path
     * 
     * @return array|bool seznam smazanych souboru nebo FALSE v pripade spatne cesty
     */
    public function delete(array $files = array(), $path = '') {
      $deleted = array();
      if ($this->changeDir($path)) {
        foreach ((($files) ? $files : $this->listFiles()) as $file) {
          if (@ftp_delete($this->connection, $file)) {
            $deleted[] = $file;
          }
        }
        
        return $deleted;
      } else {
        return FALSE;
      }
    }
    
    /**
     * Getter
     * @return string
     */
    public function getConnection() {
      return $this->connection;
    }

    /**
     * Getter
     * @return string
     */
    public function getServer() {
      return $this->server;
    }

    /**
     * Getter
     * @return string
     */
    public function getUser() {
      return $this->user;
    }

    /**
     * Getter
     * @return string
     */
    public function getPassword() {
      return $this->password;
    }
    
    /**
     * Getter
     * @return mixed
     */
    public function getResult() {
      return $this->result;
    }
    
    /**
     * Getter
     * @return string
     */
    public function getDir() {
      return $this->dir;
    }
  }
  