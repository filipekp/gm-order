<?php
  namespace prosys\core\common;

  /**
   * Global settings of the application.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class Settings
  {
    // promenna pro tag oznacujici agenta objektu systemu
    const PROSYS = 'ProSYS';
    
    // general
    const ROOT_PATH = SETTINGS_ROOT_PATH;
    const MODULE_PATH = SETTINGS_MODULE_PATH;
    const SYSTEM_STORAGE_PATH = SETTINGS_SYSTEM_STORAGE_PATH;
    
    const SHOW_DELETED_PREDICATE = '$FORCE_SHOWING_DELETED$';
    const BINDING_TYPE_PROPERTY_LABEL = '$BINDING_TYPE_PROPERTY$';
    const CURRENT_LOGIN_ID = 'active_login_id';
    const LAST_URI = 'last_uri';
    const JUST_LOGGED_IN = 'just_logged_in';
    
    const TRANSLATE_VAR = '__TRANSLATE__';
    
    const LOCK_IDLE_TIME = 30;

    const AUTOLOAD_DEBUG = FALSE;
        
    // front-end
    const ROOT_URL = SETTINGS_ROOT_URL;
    const ROOT_FE_URL = SETTINGS_ROOT_FE_URL;
    const FE_RESOURCES = SETTINGS_FE_RESOURCES;
    const FE_VIEW = SETTINGS_FE_VIEW;
    const FE_VIEW_TEMPLATES = SETTINGS_FE_VIEW_TEMPLATES;
    
    // admin
    const ROOT_ADMIN_URL = SETTINGS_ROOT_ADMIN_URL;
    const ADMIN_MODULES = SETTINGS_ADMIN_MODULES;
    const ADMIN_RESOURCES = SETTINGS_ADMIN_RESOURCES;
    const ADMIN_PLUGINS = SETTINGS_ADMIN_PLUGINS;
    const ADMIN_CSS = SETTINGS_ADMIN_CSS;
    const ADMIN_JS = SETTINGS_ADMIN_JS;
    const ADMIN_IMAGES = SETTINGS_ADMIN_IMAGES;
    const ADMIN_ICONS = SETTINGS_ADMIN_ICONS;
    const ADMIN_LANGUAGES_URL = SETTINGS_ADMIN_LANGUAGES_URL;
    const ADMIN_LANGUAGES_PATH = SETTINGS_ADMIN_LANGUAGES_PATH;
    
    // storage
    const STORAGE_GLOBAL_DIR = 'global/';
    const STORAGE_MODULES_DIR = 'modules/';
    
    // smarty
    const SMARTY_DIR = 'smarty/';
    const SMARTY_CACHE = 'cache/';
    const SMARTY_COMPILE = 'templates_c/';
    const SMARTY_PLUGINS = 'plugins/';
    
    // system messages
    const MESSAGE_PREFIX = 'msg_';
    const MESSAGE_INFO = 'msg_201';
    const MESSAGE_WARNING = 'msg_417';
    const MESSAGE_ERROR = 'msg_500';
    const MESSAGE_EXCEPTION = 'msg_400';
    const MESSAGE_SUCCESS = 'msg_200';
    
    // proclient data
    const PROCLIENT_AUTHOR = 'Proclient s.r.o., Aksamitova 1071/1, 779 00 Olomouc, http://www.proclient.cz, e-mail: info@proclient.cz';
    /**
     * Return copyright string.
     * 
     * @param bool $withLink
     * @return string
     */
    public static function getCopyright($withLink = FALSE) {
      $createdYear = 2015;
      $link = 'http://www.proclient.cz';
      
      return (($withLink) ? '<a href="' . $link . '">' : '') . 'Proclient s.r.o.' . (($withLink) ? '</a>' : '') . ' © ' . ((date('Y') > $createdYear) ? $createdYear . ' - ' . date('Y') : $createdYear);
    }
    
    // globalni promenne z databaze
    public static $VARIABLES;

    /**
     * Nacte vsechny promenne systemu z DB.
     */
    private static function loadVariables() {
      // nacteni vsech promennych z databaze
      /* @var $settingDao \prosys\model\SettingDao */
      $settingDao = Agents::getAgent('SettingDao', Agents::TYPE_MODEL);
      
      if (is_null(self::$VARIABLES)) {
        self::$VARIABLES = $settingDao->loadAll();
      }
    }
    
    /**
     * Vrati vsechny promenne systemu z DB. V pripade predani smarty prevede jejich "smarty tvar" do klasickeho retezce.
     * 
     * @param \Smarty &$smarty
     * @return array
     */
    public static function getVariables(\Smarty &$smarty = NULL) {
      self::loadVariables();
      
      if (!is_null($smarty)) {
        array_walk_recursive(self::$VARIABLES, function(&$variable) use (&$smarty) {
          $variable = $smarty->fetch('string:' . $variable);
        });
      }
      
      return self::$VARIABLES;
    }
    
    /**
     * Vrati hodnotu promenne z databaze podle skupiny a nazvu.
     * 
     * @param string $group
     * @param string $name
     * 
     * @return mixed
     */
    public static function getVariable($group, $name) {
      /* @var $settingDao \prosys\model\SettingDao */
      $settingDao = Agents::getAgent('SettingDao', Agents::TYPE_MODEL);
      $variables = self::getVariables();
      $settingDao->setCanBeErased($group, $name);
      
      return ((is_null(($groupArr = Functions::item($variables, $group))) || is_null(($value = Functions::item($groupArr, $name)))) ? NULL : $value);
    }
    
    /**
     * Vrati vsechny konstanty tridy settings
     * @return array
     */
    public static function getConstants() {
      $oClass = new \ReflectionClass(__CLASS__);
      $constants = $oClass->getConstants();
      
      $keys = array_filter(array_keys($constants), function($key) {
        return strpos($key, 'PASSWORD') === FALSE
            && strpos($key, 'LOGIN') === FALSE
            && strpos($key, 'MESSAGE_') === FALSE;
      });
      
      return array_intersect_key($constants, array_flip($keys));
    }
  }
  
  define('SETTINGS_ROOT_PATH', preg_replace('@(.*)admin/core/common@', '$1', __DIR__));
  define('SETTINGS_MODULE_PATH', Settings::ROOT_PATH . 'admin/modules/');
  define('SETTINGS_SYSTEM_STORAGE_PATH', Settings::ROOT_PATH . 'storage/');
  define('SETTINGS_ADMIN_RESOURCES_PATH', SETTINGS_ROOT_PATH . 'admin/resources/');
  define('SETTINGS_ADMIN_LANGUAGES_PATH', SETTINGS_ADMIN_RESOURCES_PATH . 'languages/');
  define('SETTINGS_ROOT_URL', 'http://' . filter_input(INPUT_SERVER, 'HTTP_HOST') . '/');
  define('SETTINGS_ROOT_ADMIN_URL', Settings::ROOT_URL . 'admin/');
  
  define('SETTINGS_ROOT_FE_URL', Settings::ROOT_URL . 'web/');
  define('SETTINGS_FE_RESOURCES', SETTINGS_ROOT_FE_URL . 'resources/');
  define('SETTINGS_FE_VIEW', SETTINGS_ROOT_FE_URL . 'view/');
  define('SETTINGS_FE_VIEW_TEMPLATES', SETTINGS_FE_VIEW . 'templates/');
  
  define('SETTINGS_ADMIN_MODULES', Settings::ROOT_ADMIN_URL . 'modules/');
  define('SETTINGS_ADMIN_RESOURCES', Settings::ROOT_ADMIN_URL . 'resources/');
  define('SETTINGS_ADMIN_PLUGINS', SETTINGS_ADMIN_RESOURCES . 'plugins/');
  define('SETTINGS_ADMIN_CSS', SETTINGS_ADMIN_RESOURCES . 'css/');
  define('SETTINGS_ADMIN_JS', SETTINGS_ADMIN_RESOURCES . 'js/');
  define('SETTINGS_ADMIN_IMAGES', SETTINGS_ADMIN_RESOURCES . 'images/');
  define('SETTINGS_ADMIN_ICONS', SETTINGS_ADMIN_IMAGES . 'icons/');
  define('SETTINGS_ADMIN_LANGUAGES_URL', SETTINGS_ADMIN_RESOURCES . 'languages/');
