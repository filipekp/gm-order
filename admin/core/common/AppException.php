<?php
  namespace prosys\core\common;

  /**
   * Exception extends Exception only because of need to create Exception with the array of messages.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class AppException extends \Exception
  {
    private $_module;
    private $_action;
    
    /**
     * Initializes messages.
     * @param string|array $messages
     */
    public function __construct($message, $module = '', $action = '', $code = 0, \Exception $previous = null) {
      $this->_module = $module;
      $this->_action = $action;
      
      $messageRes = '<b>' . ((is_string($message)) ? $message : implode('<br />', $message)) . '</b>';

      // call parent's constructor
      parent::__construct($messageRes, $code, $previous);
    }
    
    /**
     * Getter.
     * @return string
     */
    public function getModule() {
      return $this->_module;
    }

    /**
     * Getter.
     * @return string
     */
    public function getAction() {
      return $this->_action;
    }
  }
