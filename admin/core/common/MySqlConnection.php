<?php
  namespace prosys\core\common;

  /**
   * Represents MySQL connection.
   * 
   * @property-read  string $server
   * @property-read  string $user
   * @property-read  string $password
   * @property-read  string $db
   * @property-read \PDO    $connection
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class MySqlConnection extends SqlConnection
  {
    protected $_wait_timeout;
    
    /**
     * Vytvori pripojeni k databazy MySQL a nastavi vychozi hodnotu promenne wait_timeout pro obnovovani spojeni.
     * 
     * @param string $server
     * @param string $user
     * @param string $password
     * @param string $db
     * @param string $prefix
     */
    public function __construct($server, $user = '', $password = '', $db = '', $prefix = '') {
      parent::__construct($server, $user, $password, $db, $prefix);

      $this->_wait_timeout = $this->showVariable('wait_timeout');
    }

    /**
     * Vytvori db spojeni s MySQL.
     * 
     * @return \PDO
     * @throws AppException
     */
    protected function connect() {
      try {
        $options = array(
          \PDO::ATTR_PERSISTENT         => TRUE,
          \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
          \PDO::ATTR_EMULATE_PREPARES   => FALSE,
          \PDO::MYSQL_ATTR_FOUND_ROWS   => TRUE,
          \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE'
        );

        $this->connection = new \PDO("mysql:host={$this->server};dbname={$this->db};charset=utf8", $this->user, $this->password, $options);
      } catch (\PDOException $e) {
        throw new AppException('Connection failed: ' . $e->getMessage());
      }
      
      return $this->connection;
    }
    
    /**
     * @inherit
     */
    protected function prepare($queryStmt, array $options = array()) {
      $now = microtime(TRUE);
      if ($this->_last_execution && (($now - $this->_last_execution) >= ($this->_wait_timeout * 0.9))) {
        $this->reconnect();
      }

      return parent::prepare($queryStmt, $options);
    }

    /**
     * Performs SELECT query and stores the result statement into the $result property.
     * 
     * @param string|array $what columns, which should be selected
     * @param string $from table name
     * @param array $condition SQL condition for prepared stmt => array('where' => '`col1` = ? AND `col2` LIKE ?', 'bindings' => array(1, '%te%'))
     * @param string $order
     * @param string $groupBy
     * @param string $limit
     * 
     * @return resource|bool
     */
    public function select($what, $from, array $condition = array(), $order = '', $groupBy = '', $limit = '') {
      $where = (($condition) ? ' WHERE ' . $condition['where'] : '');
      $groupBy = (($groupBy) ? ' GROUP BY ' . $groupBy : '');
      $order = (($order) ? ' ORDER BY ' . $order : '');
      $limit = (($limit) ? ' LIMIT ' . $limit : '');
      
      $query = 'SELECT ' . ((is_string($what)) ? $what : '`' . implode('`, `', $what) . '`') . 
               " FROM `_PREFIX_{$from}`{$where}{$groupBy}{$order}{$limit}";
      
      $stmt = $this->prepare($query, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_SCROLL));
      $this->bindWhereParams($stmt, $condition);
      
      return $this->execute($stmt);
    }
    
    /**
     * Zobrazi seznam tabulek databaze.
     * 
     * @param array $regexpFilters
     * @return array
     */
    public function showTables(array $regexpFilters = []) {
      $column = 'Tables_in_' . $this->db;
      $query = 'SHOW TABLES';

      if ($regexpFilters) {
        $filters = implode('|', $regexpFilters);
        $query .= " WHERE {$column} REGEXP '{$filters}'";
      }

      $this->execute($this->prepare($query, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_SCROLL)));
      $this->fetchObjects();

      return array_map(function($table) use ($column) {
        return $table->$column;
      }, $this->result);
    }
    
    /**
     * Zobrazi query pro vytvoreni pozadovane tabulky.
     * 
     * @param string $table
     * @return string | NULL
     */
    public function showCreateTable($table) {
      $query = "SHOW CREATE TABLE `{$this->prefix}{$table}`";

      try {
        $this->execute($this->prepare($query, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_SCROLL)));
        $this->fetchObject();
      } catch (AppException $e) {
        $this->result = NULL;
      }

      return (($this->result) ? $this->result->{'Create Table'} : NULL);
    }
    
    /**
     * Vrati hodnotu parametru SQL databaze.
     * 
     * @param \PDOStatement $variable
     * @return mixed
     */
    public function showVariable($variable) {
      $query = "SHOW VARIABLES LIKE '$variable'";

      $this->execute($this->prepare($query, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_SCROLL)));
      $this->fetchObject();

      return (($this->result) ? $this->result->Value : NULL);
    }
  }
