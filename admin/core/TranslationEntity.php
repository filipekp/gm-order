<?php
  namespace prosys\model;
  
  use prosys\core\common\Functions,
      prosys\core\ProSYSLayer;

  /**
   * Abstraktni trida pro entity s preklady.
   * 
   * @author Pavel Filípek
   * @copyright (c) 2015, Proclient s.r.o.
   */
  abstract class TranslationEntity extends Entity
  { 
    /**
     * Vrati popisek podle jazyka
     * 
     * @param \prosys\model\LanguageEntity $language
     * @return Entity
     */
    public function getDescription(LanguageEntity $language = NULL) {
      $curLanguage = ((is_null($language)) ? ProSYSLayer::ProSYS()->loggedUser()->language : $language);
      
      return Functions::first((array)array_filter($this->descriptions->getLoadedArrayCopy(), function($description) use ($curLanguage) {
        return $curLanguage->id === $description->language->id;
      }));
    }
  }
