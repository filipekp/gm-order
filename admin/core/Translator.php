<?php
  namespace prosys\core;
  
  use prosys\core\common\Settings,
      prosys\core\ProSYSLayer,
      prosys\model\LanguageEntity,
      prosys\core\common\Functions,
      prosys\core\common\Agents;

  /**
   * Reprezentuje prekladac systemu.
   * 
   * @property-read array $config
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class Translator {
    private static $_GLOBAL_TRANSLATION = NULL;
    private static $_CURRENT_TRANSLATION = [];
    private static $_DEFAULT_LANGUAGE = NULL;
    private static $_DEFAULT_LANGUAGE_TRANSLATION = NULL;
    private static $_DEFAULT_LANGUAGE_CURRENT_TRANSLATION = [];


    /**
     * Nejsou-li predana aktualni data, metoda je zjisti.
     * 
     * @param string $module
     * @param string $activity
     * @param LanguageEntity $language
     * 
     * @return array [module, activity, language] - vhodne pro volani funkce list()
     */
    private static function currentData($module = NULL, $activity = NULL, LanguageEntity $language = NULL) {
      // aktualni modul
      if (is_null($module)) {
        $module = ProSYSLayer::ProSYS()->module();
      }
      
      // aktualni aktivita
      if (is_null($activity)) {
        $activity = ProSYSLayer::ProSYS()->activity();
      }
      
      // aktualni jazyk prekladu
      if (is_null($language)) {
        $language = ProSYSLayer::ProSYS()->language();
      }
      
      return [$module, $activity, $language];
    }
    
    /**
     * Vrati vychozi jazyk systemu
     * 
     * @return LanguageEntity
     */
    private static function getDefaultLanguage() {
      /* @var $languageDao \prosys\model\LanguageDao */
      $languageDao = Agents::getAgent('LanguageDao', Agents::TYPE_MODEL);
      if (is_null(self::$_DEFAULT_LANGUAGE)) {
        self::$_DEFAULT_LANGUAGE = $languageDao->loadDefaultLanguage();
      }
      
      return self::$_DEFAULT_LANGUAGE;
    }
    
    /**
     * Nacte preklady vychoziho jazyka
     * 
     * @param string $languagePath
     */
    private static function loadDefaultTranslation($languagePath, $module, $activity) {
      if (is_null(self::$_DEFAULT_LANGUAGE_TRANSLATION) && file_exists(($file = sprintf($languagePath, self::getDefaultLanguage()->dirPath) . 'global.php'))) {
        require $file;
        self::$_DEFAULT_LANGUAGE_TRANSLATION = ${Settings::TRANSLATE_VAR};
      }
      
      // nacitani globalniho souboru vychoziho jazyka pro aktualni modul
      ${Settings::TRANSLATE_VAR} = [];
      if (file_exists(($file = sprintf($languagePath, self::getDefaultLanguage()->dirPath) . 'modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'global.php'))) {
        require $file;
        self::$_DEFAULT_LANGUAGE_CURRENT_TRANSLATION = ${Settings::TRANSLATE_VAR};
      }
      
      ${Settings::TRANSLATE_VAR} = [];
      if (file_exists(($file = sprintf($languagePath, self::getDefaultLanguage()->dirPath) . 'modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . $activity . '.php'))) {
        require $file;
        self::$_DEFAULT_LANGUAGE_CURRENT_TRANSLATION = array_replace_recursive(self::$_DEFAULT_LANGUAGE_CURRENT_TRANSLATION, ${Settings::TRANSLATE_VAR});
      }
    }

    /**
     * Vlozi soubor s prekladem.
     * 
     * @param string $module
     * @param string $activity
     * @param LanguageEntity $language
     */
    private static function loadTranslation($module, $activity, LanguageEntity $language) {
      $languagePath = Settings::ADMIN_LANGUAGES_PATH . '%s' . DIRECTORY_SEPARATOR;
            
      //nacitani prekladu vychoziho jazyka
      self::loadDefaultTranslation($languagePath, $module, $activity);
      
      // nacitani globalniho souboru aktualniho jazyka
      if (is_null(self::$_GLOBAL_TRANSLATION) && file_exists(($file = sprintf($languagePath, $language->dirPath) . 'global.php'))) {
        require $file;
        self::$_GLOBAL_TRANSLATION = array_replace_recursive((array)self::$_GLOBAL_TRANSLATION, ${Settings::TRANSLATE_VAR});
      } 
      
      // nacitani globalniho souboru aktualniho modulu
      ${Settings::TRANSLATE_VAR} = [];
      $current = [];
      if (file_exists(($file = sprintf($languagePath, $language->dirPath) . 'modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . 'global.php'))) {
        require $file;
        $current = ${Settings::TRANSLATE_VAR};
      }
      
      ${Settings::TRANSLATE_VAR} = [];
      if (file_exists(($file = sprintf($languagePath, $language->dirPath) . 'modules' . DIRECTORY_SEPARATOR . $module . DIRECTORY_SEPARATOR . $activity . '.php'))) {
        require $file;
        $current = array_replace_recursive($current, ${Settings::TRANSLATE_VAR});
      }
      
      self::$_CURRENT_TRANSLATION = array_replace_recursive(
        (array)self::$_DEFAULT_LANGUAGE_TRANSLATION,
        (array)self::$_GLOBAL_TRANSLATION,
        self::$_DEFAULT_LANGUAGE_CURRENT_TRANSLATION,
        $current
      );
    }
    
    /**
     * Prelozi jedno pozadovane slovo.
     * 
     * @param string|array $word
     * @param array $params
     * @param string $module
     * @param string $activity
     * @param LanguageEntity $language
     * 
     * @return string
     */
    public static function translate($word, array $params = [], $module = NULL, $activity = NULL, LanguageEntity $language = NULL) {
      $data = self::currentData($module, $activity, $language);
      self::loadTranslation($data[0], $data[1], $data[2]);
      
      return call_user_func_array('sprintf', array_merge([Functions::item(self::$_CURRENT_TRANSLATION, $word)], $params));
    }
    
    /**
     * Vrati pozadovany preklad.
     * 
     * @param string $module
     * @param string $activity
     * @param LanguageEntity $language
     * 
     * @return array
     */
    public static function getTranslation($module = NULL, $activity = NULL, LanguageEntity $language = NULL) {
      $data = self::currentData($module, $activity, $language);
      self::loadTranslation($data[0], $data[1], $data[2]);
      
      return self::$_CURRENT_TRANSLATION;
    }
  }
