<?php
  namespace prosys\admin\view;
  
  use prosys\model\Entity,
      prosys\core\common\Settings,
      prosys\core\common\AppException,
      prosys\core\common\Agents,
      prosys\core\common\Functions,
      prosys\core\Translator;

  /**
   * Abstract class which should be the parent of every "viewable" classes.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  abstract class BackendView extends View implements \prosys\core\interfaces\IViewable
  {
    /** @var \prosys\model\DataAccessObject */
    protected $_labels;                   // seznam popisku modulu
    protected $_moduleIcon;               // ikona modulu
    protected $_moduleCallbacks = [];     // seznam vsech pouzivanych callbacku
    
    protected $_entityListDefinition = [  // definice seznamu prehledove tabulky entit
      'columns' => [],                        // definice sloupcu prehledove tabulky
      'actions' => []                         // akcni ikony u seznamu entit - klic: akce / aktivita; prvky: link, icon, action_name, title, label, iclass
    ];
    
    public static $_breadcumbs = [];      // aktualni cesta
    
    /**
     * Vrati popisky modulu.
     * 
     * @return array
     */
    public static function getLabels() {
      $calledClassArr = explode('\\', get_called_class());
      $calledClass = array_pop($calledClassArr);
      $newView = Agents::getAgent($calledClass, Agents::TYPE_VIEW_ADMIN);
      
      return $newView->_labels;
    }

    /**
     * Initializes view with prop labels
     * 
     * @param array $labels
     */
    public function __construct($labels = array()) {
      parent::__construct();
      
      // nastaveni slozky se smarty pluginy pro modul
      $currentModuleDir = Settings::MODULE_PATH . $this->_moduleName . '/plugins/';
      if (is_dir($currentModuleDir)) {
        self::$_SMARTY->addPluginsDir($currentModuleDir);
      }
      
      /* @var $moduleActionDao \prosys\model\ModuleActionDao */
      $moduleActionDao = Agents::getAgent('ModuleActionDao', Agents::TYPE_MODEL);
      
      $this->_labels = $labels;
      
      self::$_breadcumbs = [];
      
      /* @var $menuDao \prosys\model\MenuDao */
      $menuDao = Agents::getAgent('MenuDao', Agents::TYPE_MODEL);

      /* @var $menuCurrent prosys\model\MenuEntity */
      $menuCurrent = ((is_null($menuCurrent = $menuDao->loadByModule(self::ProSYS()->module(), self::ProSYS()->activity()))) ? $menuCurrent : $menuCurrent->parent);

      while ($menuCurrent && !$menuCurrent->isNew()) {
        /* @var $menuCurrent \prosys\model\MenuEntity */
        switch ($menuCurrent->type) {
          case 'module_action':
            /* @var $moduleAction \prosys\model\ModuleActionEntity */
            $moduleAction = $moduleActionDao->load($menuCurrent->typeValue);
            $link = '?module=' . $moduleAction->module->module . (($moduleAction->name == 'initial') ? '' : '&activity=' . $moduleAction->name);
          break;
          case 'link':
            $data = (array)json_decode($menuCurrent->typeValue, TRUE);
            $link = $data['href'];
            unset($data['href']);
          break;
          case 'section':
            $link = 'none';
          break;
        }
        
        $icon = json_decode($menuCurrent->icons);
        self::addBreadcumb($menuCurrent->name, $icon, $link);
        
        $menuCurrent = $menuCurrent->parent;
      }
      
      self::$_breadcumbs = array_reverse(self::$_breadcumbs);
    }
    
    /**
     * Generuje drobečkovou navigaci.
     * 
     * @return string
     */
    public static function getBreadcumbs() {
      $breadcumbs = self::$_breadcumbs;
      
      $items = array();
      $countItems = count($breadcumbs);
      for ($i = 0; $i < $countItems; $i++) {
        $icons = '';
        if (array_filter($breadcumbs[$i]['icon'])) {
          if (count($breadcumbs[$i]['icon']) > 1) {
            $icons = '<span class="icons">';
              foreach ($breadcumbs[$i]['icon'] as $icon) {
                $icons .= '<i class="fa fa-' . $icon . '"></i>';
              }
            $icons .= '</span>';
          } else {
            $icons = '<i class="fa fa-' . Functions::first($breadcumbs[$i]['icon']) . '"></i>';
          }
        }
        
        $items[] = $icons .
                   ((array_key_exists($i + 1, $breadcumbs) && $breadcumbs[$i]['href'] != 'none') ? '<a href="' . $breadcumbs[$i]['href'] . '">' . $breadcumbs[$i]['name'] . '</a>' : $breadcumbs[$i]['name']);
      }
      
      if ($items) {
        ob_start();
        ?>
        <div class="page-bar">
        <ul class="page-breadcrumb"><?php
        echo '<li>' . implode('<i class="fa fa-angle-right"></i></li><li>', $items) . '</li>';
        ?>
        </ul>
        </div>
        <?php      
        return ob_get_clean();
      } else {
        return '';
      }
    }
    
    /**
     * Přidá položku do drobečkové navigace.
     * 
     * @param string $name
     * @param string|array $icon default ''
     * @param string $href default '#'
     */
    public static function addBreadcumb($name, $icon = '', $href = '#') {
      self::$_breadcumbs[] = array(
        'name' => $name,
        'icon' => (array)$icon,
        'href' => $href
      );
    }
    
    /**
     * Zobrazi sablonu aktivity.
     * 
     * @param string $template
     * @param string $title
     * @param array $assigned
     * @param bool $templateOnly
     */
    protected function printActivity($template, $title = '', $assigned = [], $templateOnly = FALSE) {
      $assigned = $assigned + ['contentOnly' => FALSE];
      
      // prirazeni dat do sablony
      foreach ($assigned as $varName => $varValue) {
        self::$_SMARTY->assign($varName, $varValue);
      }

      self::$_SMARTY->assign('title', $title);
      self::$_SMARTY->assign('moduleView', $this);
      self::$_SMARTY->assign('moduleName', $this->_moduleName);
      self::$_SMARTY->assign('moduleIcon', $this->_moduleIcon);
      self::$_SMARTY->assign('moduleCallbacks', $this->_moduleCallbacks);
      self::$_SMARTY->assign('labels', $this->_labels);
      self::$_SMARTY->assign('translation', Translator::getTranslation());

      self::$_SMARTY->addTemplateDir([
        Settings::MODULE_PATH . 'admin/templates/include/',
        Settings::MODULE_PATH . $this->_moduleName . '/templates/'
      ]);

      if (!is_null(self::ProSYS()->login())) { 
        self::$_SMARTY->assign('menu_html_structure', \prosys\admin\controller\Controller::getMenu()); 
      }
      
      if ($templateOnly) {
        echo self::$_SMARTY->fetch($template . '.tpl');
      } else {
        echo self::$_SMARTY->fetch('header.tpl');
          echo self::$_SMARTY->fetch($template . '.tpl');
        echo self::$_SMARTY->fetch('footer.tpl');
      }
    }
    
    /**
     * Default behavior of following methods is throwing an exception.
     * @inherit
     */
    public function initial($arg = NULL)                        { throw new AppException('No initial activity is implemented yet.'); }
    public function detail(Entity $entity, $optional = array()) { throw new AppException('No detail activity is implemented yet.');  }
    public function manage(Entity $entity, $optional = array()) { throw new AppException('No manage activity is implemented yet.');  }
    public function table($data, $optional = array())           { throw new AppException('No table activity is implemented yet.');   }
    
    /**
     * Autoload of module resources.
     * 
     * @param string $includeResourceCallback
     * @param string $resourceRoot
     * @param string $titlePlural
     */
    private function includeModuleResource($includeResourceCallback, $resourceRoot, $titlePlural) {
      $reflection = new \ReflectionClass(get_called_class());
      $path = str_replace('\\', '/', dirname($reflection->getFileName()));
      
      $modulePath = array('', '', '');
      preg_match('|.*(modules/)(.*)|', $path, $modulePath);
      
      $resourcePath = $path . '/' . $resourceRoot . '/';
      $resourceUrl = Settings::ROOT_ADMIN_URL . $modulePath[1] . $modulePath[2] . '/' . $resourceRoot . '/';
      if (is_dir($resourcePath)) {
        echo PHP_EOL . '<!-- BEGIN: ' . $titlePlural . ' of module: ' . $modulePath[2] . ' -->' . PHP_EOL;
        foreach (scandir($resourcePath) as $file) {
          if ($file != '.' && $file != '..' && !is_dir($resourcePath . $file)) {
            echo self::$includeResourceCallback($file, $resourceUrl) . PHP_EOL;
          }
        }
        echo '<!-- END: ' . $titlePlural . ' of module: ' . $modulePath[2] . ' -->' . PHP_EOL;
      }
    }
    
    /**
     * Autoload of module javascripts.
     */
    public function includeModuleJS() {
      $this->includeModuleResource('htmlIncludeJS', 'js', 'javascripts');
    }
    
    /**
     * Autoload of module cascading style sheets.
     */
    public function includeModuleCSS() {
      $this->includeModuleResource('htmlIncludeCSS', 'css', 'cascading style sheets');
    }
    
    /**
     * Generate html include for java script file.
     * 
     * @param string $filename
     * @param string $dir
     * @return string
     */
    public static function htmlIncludeJS($filename, $dir = Settings::ADMIN_JS) {
      return '<script src="' . $dir . $filename . '"></script>';
    }
    
    /**
     * Generate html include for cascading style sheets file.
     * 
     * @param string $filename
     * @param string $dir
     * @return string
     */
    public static function htmlIncludeCSS($filename, $dir = Settings::ADMIN_CSS) {
      return '<link type="text/css" rel="stylesheet" href="' . $dir . $filename . '" />';
    }
    
    /**
     * Generates HTML code of list created from given associative object.
     * 
     * @param array $data associative array value => option; option = show value or associative array: array('option' => 'show value', 'data' => array('name' => 'value', ...))
     * @param callback|string $valueGetter if callback, it has to get one parameter and return string
     * @param callback|string $showGetter if callback, it has to get one parameter and return string
     * @param string|array $selectedValues
     * @param string $name
     * @param string $defaultText [optional = '--- vyberte ---']
     * @param string $id [optional = '']
     * @param string $class [optional = '']
     * @param boolean $disabled [optional = FALSE]
     * @param string $htmlProperties [optional = '']
     * 
     * @return string
     */
    public static function makeSelectFromEntityArray(array $data, $valueGetter, $showGetter, $selectedValues, $name, $defaultText = '--- vyberte ---',
                                          $id = '', $class = '', $disabled = FALSE, $htmlProperties = '') {
      
      $values = $data;
      array_walk($values, function(&$item, $key, $getter) {
        $option = ((is_array($item)) ? $item['option'] : $item);
        $item = ((is_callable($getter)) ? $getter($option, $key) : $option->$getter);
      }, $valueGetter);
      
      $options = $data;
      array_walk($options, function(&$item, $key, $getter) {
        if (is_array($item)) {
          $item['option'] = ((is_callable($getter)) ? $getter($item['option'], $key) : $item['option']->$getter);
        } else {
          $item = ((is_callable($getter)) ? $getter($item, $key) : $item->$getter);
        }
      }, $showGetter);
      
      return self::makeSelectFromArray(array_combine($values, $options), $selectedValues, $name, $defaultText, $id, $class, $disabled, $htmlProperties);      
    }
    
    /**
     * Generates HTML code of list created from given associative array.
     * 
     * @param array $data associative array value => option; option = show value or associative array: array('option' => 'show value', 'data' => array('name' => 'value', ...))
     * @param string|array $selectedValues
     * @param string $name
     * @param string $defaultText [optional = '--- vyberte ---']
     * @param string $id [optional = '']
     * @param string $class [optional = '']
     * @param boolean $disabled [optional = FALSE]
     * @param string $htmlProperties [optional = '']
     * 
     * @return string
     */
    public static function makeSelectFromArray(array $data, $selectedValues, $name, $defaultText = '--- vyberte ---',
                                               $id = '', $class = '', $disabled = FALSE, $htmlProperties = '') {
      // selectbox properties
      $id = (($id) ? ' id="' . $id . '"' : '');
      $class = (($class) ? ' class="' . $class . '"' : '');
      $disabled = (($disabled) ? ' disabled="disabled"': ''); 
      
      // options
      $groups = [];
      array_walk($data, function(&$option, $value) use ($selectedValues, &$groups) {
        $show = $option;
        $group = '';
        
        $attributes = array('value="' . $value . '"');
        if (is_array($option)) {
          if (array_key_exists('data', $option)) {
            foreach ($option['data'] as $name => $dataval) {
              $attributes[] = 'data-' . $name . '="' . $dataval . '"';

              if ($name == 'group') {
                $group = $dataval;
                
                if (!Functions::item($groups, $group)) {
                  $groups[$group] = [];
                }
              }
            }
          }
          
          $show = $option['option'];
        }
        
        if ($group) {
          $groups[$group][] = '<option ' . implode(' ', $attributes) . ((((is_array($selectedValues)) ? in_array($value, $selectedValues) : $value === $selectedValues)) ? ' selected="selected"' : '') . '>' . $show . '</option>';
        } else {
          $option = '<option ' . implode(' ', $attributes) . ((((is_array($selectedValues)) ? in_array($value, $selectedValues): $value === $selectedValues)) ? ' selected="selected"' : '') . '>' . $show . '</option>';
        }
      });
      
      if ($groups) {
        array_walk($groups, function(&$item, $key) {
          $item = '<optgroup label="' . $key . '">' . implode(PHP_EOL, $item) . '</optgroup>';
        });        
        $data = array_merge(array('<option></option>'), $groups);
      } else {
        $data = array_merge(array('<option></option>'), $data);
      }
      
      $data = implode(PHP_EOL, $data);
      return <<<SELECT
        <select{$id}{$class} name="{$name}" placeholder="{$defaultText}" {$htmlProperties}{$disabled}>
          {$data}
        </select>
SELECT;
    }
    
    /**
     * Get las number of page
     * 
     * @param int $objCount
     * @param int $objectsPerPage
     * @return type
     */
    public static function getLastPageNumber($objCount, $objectsPerPage) {
      $division = intval($objCount / $objectsPerPage);

      return ((($objCount % $objectsPerPage) === 0) ? (($division) ? $division : 1) : $division + 1);
    }
    
    /**
     * Generating pagination 
     * 
     * @param int $objCount
     * @param array $getParams
     * @param int $objectsPerPage
     * @param int $pagesBeforeActualNumber
     * @param int $pagesAfterActualNumber
     * @param string $htmlClass
     * @param string $htmlId
     */
    public static function generatePaging($objCount, array $getParams, $objectsPerPage = 20, $pagesBeforeActualNumber = 3, $pagesAfterActualNumber = 3,
                                          $htmlClass = 'pagination pagination-sm', $htmlId = '') {
      
      $currentPage = ((array_key_exists('page', $getParams)) ? (int)$getParams['page'] : 1);
      $lastPage = self::getLastPageNumber($objCount, $objectsPerPage);

      $actualPage = min($currentPage, $lastPage);
      if (isset($getParams['page'])) { unset($getParams['page']); }

      $getParamsString = '';
      
      $getParamsString .= http_build_query($getParams);
      $getParamsString .= '&page=';
      
      $getParamsInputs = '';
      $InputsArr = array();

      function getNameRecursive($array, &$InputsArr, $keyOld = '') {
        foreach ($array as $key => $value) {
          if (is_array($value)) {
            $newKey = $keyOld . '[' . $key . ']';
            getNameRecursive($value, $InputsArr, $newKey);
          } else {
            $InputsArr[$keyOld . '[' . $key . ']'] = $value;
          }
        }
      }

      foreach ($getParams as $key => $value) {
        if (is_array($value)) {
          getNameRecursive($value, $InputsArr, $key);
        } else {
          $InputsArr[$key] = $value;
        }
      }

      foreach ($InputsArr as $key => $value) {
        $getParamsInputs .= '<input type="hidden" name="' . $key . '" value="' . $value . '" />';
      }
      
      $html = '<div class="row">';
      $html .= '<div class="col-md-6">' .
                 '<div class="info_of_page">' .
                   '<form method="get">' .
                    $getParamsInputs .
                    'Strana <input class="current_page form-control" type="number" name="page" value="' . $actualPage . '" min="1" max="' . $lastPage . '" /> z ' . $lastPage .
                   '</form>' .
                 '</div>' .
               '</div>';
      
      $html .= '<div class="col-md-6">';
        $html .= '<ul' . (($htmlId) ? ' id="' . $htmlId . '"': '') . ' class="' . $htmlClass . '">';
          // first page link
          $html .= "<li class=\"prev" . (($actualPage > 1) ? '' : ' disabled'). "\"><a " .(($actualPage > 1) ? "href=\"?{$getParamsString}1\"" : ''). ">";
            $html .= "<span class=\"hidden-480\">|<</span>";
          $html .= "</a></li>";

          // previous page link
          $previousPage = $actualPage - 1;
          $html .= "<li class=\"prev" . (($actualPage > 1) ? '' : ' disabled'). "\"><a " .(($actualPage > 1) ? "href=\"?{$getParamsString}{$previousPage}\"" : ''). ">";
            $html .= "<span class=\"hidden-480\"><</span>";
          $html .= "</a></li>";

          // dots before actual page
          $html .= (($actualPage > ($pagesBeforeActualNumber + 1)) ? '<li class="disabled"><span>...</span></li>' : '');

          // navigation before actual page
          $fromBeforeActualPage = ((($actualPage - $pagesBeforeActualNumber) <= 1) ? 1 : $actualPage - $pagesBeforeActualNumber);
          for ($i = $fromBeforeActualPage; $i < $actualPage; $i++) {
            $html .= "<li><a href=\"?{$getParamsString}{$i}\">$i</a></li>";
          }

          // actual page
          $html .= '<li class="active"><span>' . $actualPage . '</span></li>';

          // navigation after actual page
          $toAfterActualPage = ((($actualPage + $pagesBeforeActualNumber) >= $lastPage) ? $lastPage : $actualPage + $pagesBeforeActualNumber);
          for ($i = ($actualPage + 1); $i <= $toAfterActualPage; $i++) {
            $html .= "<li><a href=\"?{$getParamsString}{$i}\">$i</a></li>";
          }

          // dots after actual page
          $html .= (($actualPage < ($lastPage - $pagesBeforeActualNumber)) ? '<li class="disabled"><span>...</span></li>' : '');

          // next page link
          $nextPage = $actualPage + 1;
          $html .= "<li class=\"next" . (($actualPage < $lastPage) ? '' : ' disabled') . "\"><a " . (($actualPage < $lastPage) ? "href=\"?{$getParamsString}{$nextPage}\"" : '') . ">";
            $html .= "<span class=\"hidden-480\">></span>";
          $html .= "</a></li>";

          // last page link
          $html .= "<li class=\"next" . (($actualPage < $lastPage) ? '' : ' disabled') . "\"><a " . (($actualPage < $lastPage) ? "href=\"?{$getParamsString}{$lastPage}\"" : '') . ">";
            $html .= "<span class=\"hidden-480\">>|</span>";
          $html .= "</a></li>";

        $html .= '</ul></div>';
      $html .= '</div>';

      return $html;
    }
    
    /**
     * Vygeneruje HTML pro ikonku.
     * 
     * @param mixed $object
     * @param string $action
     * @param array $iconData
     * <pre>  array(                                                      </pre>
     * <pre>    'icon'          => 'nazev ikony',             povinne     </pre>
     * <pre>    'action_name'   => 'nazev akce',              povinne     </pre>
     * <pre>    'link'          => 'link ikony',              povinne     </pre>
     * <pre>      string|callable($object, $action)                       </pre>
     * <pre>    'title'         => 'titulek odkazu',          nepovinne   </pre>
     * <pre>      string|callable($object, $action)                       </pre>
     * <pre>    'label'         => 'popisek sloupce tabulky', nepovinne   </pre>
     * <pre>    'aditional'     => [                          nepovinne   </pre>
     * <pre>      'nazev attributu' => 'hodnota attributu',               </pre>
     * <pre>      ...                                                     </pre>
     * <pre>    ]                                                         </pre>
     * <pre>  )                                                           </pre>
     * 
     * @return string
     */
    public static function getIcon($object, $action, $iconData) {
      // funkce pro kontrolu povinnych paramteru
      $checkMandatories = function($data, &$exceptions) {
        if (!(bool)Functions::item($data, 'icon')) { $exceptions[] = 'icon'; }
        if (!(bool)Functions::item($data, 'action_name')) { $exceptions[] = 'action_name'; }
        if (!(bool)Functions::item($data, 'link')) { $exceptions[] = 'link'; }
      };
      
      // kontrola povinných klicu v datech
      $exceptions = [];
      $checkMandatories($iconData, $exceptions);
      if ($exceptions) {
        throw new AppException('Funkce getIcon nemá zadány následující povinné parametry: ' . implode(', ', $exceptions) . '.');
      }
      
      extract($iconData);
      
      // overeni zda link neni callable
      if (is_callable($link)) {
        $link = $link($object, $action);
      }
      
      // atribut iclass
      $classes = ['icon_link'];
      if(isset($iclass)) {
        if (is_callable($iclass)) {
          $classes = array_merge($classes, (array)$iclass($object, $action));
        } else {
          $classes = array_merge($classes, (array)$iclass);
        }
      }
      
      // atribut title
      // pokud neni zadan tak se vezme nazev akce
      if(($dataTitle = Functions::item($iconData, 'title'))) {
        if (is_callable($dataTitle)) {
          $title = $dataTitle($object, $action);
        } else {
          $title = $dataTitle;
        }
      } else {
        $title = $action_name;
      }
      
      // pridani dalsich atributu
      $additional = (array)Functions::item($iconData, 'additional');
      array_walk($additional, function(&$item, $key) use ($object, $action) {
        if (is_callable($item)) {
          $item = $key . '="' . $item($object, $action) . '"';
        } else {
          $item = $key . '="' . $item . '"';
        }
      });
      
      return '
        <td class="icon"' . (($label = Functions::item($iconData, 'label')) ? ' data-title="' . $label . '"' : '') . '>
          <a href="' . $link . '" class="' . implode(' ', $classes) . '" title="' . $title . '"' . (($additional) ? ' ' . implode(' ', $additional) : '') . '>
            <i class="icon-' . $icon . '"></i>
          </a>
        </td>';
    }
  }
