<?php
  namespace prosys\core;
  
  use prosys\core\common\Settings,
      prosys\core\common\Agents;

  /**
   * Reprezentuje MVC vrstvu systemu - udrzuje spolecna data.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  abstract class ProSYSLayer {
    /** @var ProSYS */
    private static $ProSYS = NULL;
    
    /**
     * Inicializuje spolecna data vsech vrstev systemu.
     */
    public function __construct() {
      self::initProSYS();
    }
    
    /**
     * Inicializuje objekt ProSYS.
     */
    private static function initProSYS() {
      if (is_null(self::$ProSYS)) {
        self::$ProSYS = Agents::getAgent('ProSYS', Agents::TYPE_CORE, NULL, Settings::PROSYS);
      }
    }
    
    /**
     * Getter.
     * @return ProSYS
     */
    public static function ProSYS() {
      self::initProSYS();
      return self::$ProSYS;
    }
  }
  