<?php
  namespace prosys\admin\controller;
  
  use prosys\core\common\Settings,
      prosys\admin\view\BackendView,
      prosys\core\common\Functions,
      prosys\core\common\Agents,
      prosys\core\ProSYSLayer;

  /**
   * Abstract class from which every controller should inherit.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  abstract class Controller extends \prosys\core\ProSYSLayer implements \prosys\core\interfaces\IController
  {
    /** @var int */
    public $_itemsOnPage = 10;
    
    /** @var int */
    protected $_currentPage = 1;
    
    /** @var array */
    protected $_currentFilter = [];
    
    /** @var array */
    protected $_currentSorting = [];
    
    /** @var string */
    protected $_className;
    
    /** @var string */
    protected $_activity;
    
    /** @var \prosys\admin\view\BackendView */
    protected $_view;
    
    /** @var \prosys\model\DataAccessObject */
    protected $_dao;
    
    /** @var string */
    protected $_infoMessage = '';
    
    /** @var array */
    protected $_get;
    
    /** @var array */
    protected $_post;
    
    /** @var array */
    protected $_exceptions = [];
    
    /** @var array */
    protected $_translations = [];


    /**
     * Initializes module pagination.
     */
    public function __construct() {
      parent::__construct();
      
      // ulozi pocet polozek na strance
      $this->_itemsOnPage = Settings::getVariable('SHOWING', 'ITEMS_PER_PAGE');
      
      // ulozi GET a POST
      $this->_get = Functions::trimArray((array)filter_input_array(INPUT_GET));
      $this->_post = Functions::trimArray((array)filter_input_array(INPUT_POST));
      
      // nastavi preklady pro modul
      $this->_translations = ProSYSLayer::ProSYS()->languageTranslations();
      
      // ulozi nazev tridy
      $reflection = new \ReflectionClass($this);
      $this->_className = join('', array_slice(explode('\\', $reflection->name), -1));
      $this->_activity = filter_input(INPUT_GET, 'activity', FILTER_SANITIZE_STRING, array('options' => array('default' => 'initial')));
      
      $module = filter_input(INPUT_GET, 'module', FILTER_SANITIZE_STRING);
      
      // nastaveni stranky se provede pouze existuje-li modul
      if ($module && ucfirst($module) == ucfirst(str_replace(['Controller'], '', $this->_className))) {
        // set pagination to session if not exists
        if (!array_key_exists('pagination', $_SESSION)) { $_SESSION['pagination'] = array(); }
        if (!array_key_exists($this->_className, $_SESSION['pagination'])) { $_SESSION['pagination'][$this->_className] = array(); }
        if (!array_key_exists($this->_activity, $_SESSION['pagination'][$this->_className])) { $_SESSION['pagination'][$this->_className][$this->_activity] = 1; }

        $loggedUser = self::ProSYS()->loggedUser();
        if (!$loggedUser->isNew() && Functions::item((array)$loggedUser->otherSettings->jsonSerialize(), 'countItemPerPage')) {
          $this->_itemsOnPage = Functions::item((array)$loggedUser->otherSettings->jsonSerialize(), 'countItemPerPage');
        }

        // aktualni stranka
        $page = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);
        
        // nastavi filtr do session kdyz neexistuje
        if (!array_key_exists('filter', $_SESSION)) { $_SESSION['filter'] = array(); }
        if (!array_key_exists($this->_className, $_SESSION['filter'])) { $_SESSION['filter'][$this->_className] = array(); }
        if (!array_key_exists($this->_activity, $_SESSION['filter'][$this->_className])) { $_SESSION['filter'][$this->_className][$this->_activity] = array(); }

        $filter = (array)Functions::item($this->_get, 'filter');
        if ($filter) {
          if ($filter != Functions::item($_SESSION, ['filter', $this->_className, $this->_activity])) {
            $page = 1;
          }
          
          // kdyz je filtr v url tak ho ulozi do session
          $_SESSION['filter'][$this->_className][$this->_activity] = $filter;
        }

        // ulozi filtr do privatni promenne $_currentFilter
        $this->_currentFilter = $_SESSION['filter'][$this->_className][$this->_activity];
        
        // nastaveni razeni
        // nastavi razeni do session pokud neexistuje
        if (!array_key_exists('sorting', $_SESSION)) { $_SESSION['sorting'] = array(); }
        if (!array_key_exists($this->_className, $_SESSION['sorting'])) { $_SESSION['sorting'][$this->_className] = array(); }
        if (!array_key_exists($this->_activity, $_SESSION['sorting'][$this->_className])) { $_SESSION['sorting'][$this->_className][$this->_activity] = array(); }
        
        $sorting = (array)Functions::item($this->_get, 'sorting');
        if ($sorting) {
          if ($sorting != Functions::item($_SESSION, ['sorting', $this->_className, $this->_activity])) {
            $page = 1;
          }
          
          // kdyz je sorting v url tak ho ulozi do session
          $_SESSION['sorting'][$this->_className][$this->_activity] = $sorting;
        }

        // ulozi sorting do privatni promenne $_currentSorting
        $this->_currentSorting = $_SESSION['sorting'][$this->_className][$this->_activity];
        
        // ulozi aktualni stranku
        if ($page) {
          // when page on url then set page to session
          $_SESSION['pagination'][$this->_className][$this->_activity] = (int)$page;
        }

        // store page in property $_currentPage
        $this->_currentPage = $_SESSION['pagination'][$this->_className][$this->_activity];
      }
      
      $this->_get['page'] = $this->_currentPage;
      $this->_get['filter'] = $this->_currentFilter;
      $this->_get['sorting'] = $this->_currentSorting;
    }
    
    /**
     * Vrati strukturu menu
     * 
     * @return \prosys\model\MenuEntity[]
     */
    public static function getMenu() {
      /* @var $menuDao \prosys\model\MenuDao */
      $menuDao = Agents::getAgent('MenuDao', Agents::TYPE_MODEL);
      
      return $menuDao->loadMenuRecursive();
    }
    
    /**
     * Vymaže aktuální filtr.
     */
    public function clearFilter() {
      $module = Functions::unsetItem($this->_get, 'controller');
      $activity = (($activity = Functions::unsetItem($this->_get, 'activity')) ? $activity : 'initial');
      Functions::unsetItem($this->_get, 'action');
      
      // vymaze filtr pro aktivitu ze session
      Functions::unsetItem($_SESSION['filter'][$this->_className], $activity);
      
      // vymaze strankovani pro aktivitu
      Functions::unsetItem($_SESSION['pagination'][$this->_className], $activity);
      
      $this->redirect($module, $activity, http_build_query($this->_get));
    }
    
    /**
     * Vymaže aktuální řazení.
     */
    public function clearSorting() {
      $module = Functions::unsetItem($this->_get, 'controller');
      $activity = (($activity = Functions::unsetItem($this->_get, 'activity')) ? $activity : 'initial');
      Functions::unsetItem($this->_get, 'action');
      
      // vymaze filtr pro aktivitu ze session
      Functions::unsetItem($_SESSION['sorting'][$this->_className], $activity);
      
      // vymaze strankovani pro aktivitu
      Functions::unsetItem($_SESSION['pagination'][$this->_className], $activity);
      
      $this->redirect($module, $activity, http_build_query($this->_get));
    }
    
    /**
     * Redirects admin page.
     * 
     * @param string $module
     * @param string $activity
     * @param string $query
     * @param string $message
     */
    public static function redirect($module = '', $activity = '', $query = '', $message = '', $rootUrl = '') {
      $rootUrl = (($rootUrl) ? $rootUrl : Settings::ROOT_ADMIN_URL);
      
      if ($message) {
        $_SESSION[Settings::MESSAGE_SUCCESS][] = $message;
      }
      
      $module = (($module) ? '?module=' . $module : '');
      $activity = (($activity) ? '&activity=' . $activity : '');
      $query = (($query) ? '&' . $query : '');
      
      header('Location: ' . $rootUrl . $module . $activity . $query);
      exit();
    }

    /**
     * Redirects admin page in this module.
     * 
     * @param string $activity
     * @param string $query
     */
    protected function reload($activity = '', $query = '') {
      $module = lcfirst(preg_replace('/.*\\\\(.+)Controller/', '$1', get_class($this)));
      self::redirect($module, $activity, $query, $this->_infoMessage);
    }
    
    /**
     * Limits requested page between 1 and the last page.
     * 
     * @param int $count
     */
    public function currentPageCorrection($count) {
      $this->_currentPage = min($this->_currentPage, BackendView::getLastPageNumber($count, $this->_itemsOnPage));
      $this->_get['page'] = $this->_currentPage;
      $_SESSION['pagination'][$this->_className][$this->_activity] = $this->_currentPage;
    }
    
    /**
     * Vrati aktulani datum, cas, a datum a cas v JSON formatu
     */
    public function getCurrentDatetimeJson() {
      $dateTime = new \DateTime();
      header('Content-Type: application/json');
      echo json_encode([
        'date' => $dateTime->format('Y-m-d'),
        'time' => $dateTime->format('H:i:s'),
        'date_time' => $dateTime->format('Y-m-d H:i:s')
      ]);
      exit();
    }
    

    /**
     * Funkce pro validování formuláře přes ajax 
     */
    public function validate() {
      $element = Functions::item($this->_post, 'element');
      
      $exceptions = array_merge([], (array)$this->_exceptions);
      $this->checkMandatories($this->_post, $exceptions);
      $messages = Functions::arrayFilterByKey($exceptions, function($key) use ($element) {
        return $key == $element;
      });
      
      $results = [
        'validate' => count($messages) == 0,
        'message' => Functions::implodeRecursive(', ', $messages)
      ];
      
      header('Content-Type: application/json');
      echo json_encode($results);
      exit();
    }
  }
