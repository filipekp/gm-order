<?php
  namespace prosys\model;

  /**
   * Represents the entity of module.
   * 
   * @property string $module primary
   * @property string $name
   * 
   * @property ModuleActionEntity[] $actions binding=1n:>module:>
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class ModuleEntity extends Entity
  {
  }
