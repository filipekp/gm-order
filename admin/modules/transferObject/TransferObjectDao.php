<?php
  namespace prosys\model;
  
  /**
   * Reprezentuje objekt pristupu k definici entity modulu.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class TransferObjectDao extends MyDataAccessObject
  {
    public function __construct() {
      parent::__construct('transfer_objects', TransferObjectEntity::classname());
    }
  }
