<?php
  namespace prosys\model;

  /**
   * Reprezentuje definici entity modulu.
   * 
   * @property int $id
   * @property ModuleEntity $module element=module_id
   * @property string $name
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class TransferObjectEntity extends Entity
  {
  }
