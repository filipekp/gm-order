<?php
  namespace prosys\model;
  
  use prosys\core\mapper\SqlFilter,
      prosys\core\common\Functions,
      prosys\core\common\Agents,
      prosys\model\UserEntity;
  
  /**
   * Represents the menu data access object.
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class ModuleActionDao extends MyDataAccessObject
  {
    /** @var ModuleDao */
    private $_moduleDao;
    
    public function __construct() {
      parent::__construct('module_actions', ModuleActionEntity::classname());
      
      $this->_moduleDao = Agents::getAgent('ModuleDao', Agents::TYPE_MODEL);
    }
    
    /**
     * Nacte modul podle modulu a akce.
     * 
     * @param string $module
     * @param string $action
     * 
     * @return ModuleActionEntity
     */
    public function loadByModuleAndAction($module, $action) {
      return Functions::first($this->loadRecords(
        SqlFilter::create()
          ->compare('module', '=', $module)
          ->andL()->compare('action_name', '=', $action)));
    }
    
    /**
     * Vrati akce, na ktere ma dany uzivatel pravo.<br />
     * Akce jsou seskupeny po modulech, viz return anotace.
     * 
     * @param UserEntity $user
     * @param string $actionType
     * 
     * @return array [['module' => ModuleEntity, 'activities' => ModuleActionEntity[]]
     */
    public function loadByUser(UserEntity $user, $actionType = ModuleActionEntity::TYPE_ACTIVITY) {
      $allowed = [];
      foreach ($this->_moduleDao->loadRecords() as /* @var $module ModuleEntity */ $module) {
        $allowedActions = array_filter($module->actions->getLoadedArrayCopy(), function(/* @var $action ModuleActionEntity */$action) use ($user, $actionType, $module) {
          /* @var $action ModuleActionEntity */
          /* @var $user UserEntity */
          /* @var $module ModuleEntity */
          
          return ($action->type == $actionType) && $user->hasRight($module->module, $action->name);
        });
        
        $allowed[] = ['module' => $module, 'actions' => $allowedActions];
      }
      
      return $allowed;
    }
    
    /**
     * Get children for mudule action.
     * 
     * @param ModuleActionEntity $parent
     * @return type
     */
    private function getChildren($parent) {
      return $this->loadRecords(
        SqlFilter::create()
          ->compare('parent', '=', $parent->id),
        array(
          array('column' => 'module'),
          array('column' => 'action_type'),
          array('column' => 'action_title')
        )
      );
    }
    
    /**
     * Rekurzivne stahuje akce modulu.
     * @param ModuleActionEntity[] $moduleActions
     */
    private function prepareModuleActions($moduleActions) {
      $moduleActionsRes = array();
      foreach ($moduleActions as $moduleAction) {
        $moduleActionsRes[$moduleAction->id]['item'] = $moduleAction;
        $children = $this->getChildren($moduleAction);
        if ($children) {
          $moduleActionsRes[$moduleAction->id]['children'] = $this->prepareModuleActions($children);
        }
      }
      
      return $moduleActionsRes;
    }
    
    /**
     * Vrati celou hierarchii aktivit modulu.
     * 
     * @param ModuleEntity $module
     * @return type
     */
    public function getModuleActions($module) {
      $moduleActions = $this->loadRecords(
        SqlFilter::create()
          ->isEmpty('parent')
          ->andL()->compare('module', '=', $module->module),
        array(
          array('column' => 'module'),
          array('column' => 'action_type'),
          array('column' => 'action_title')
        )
      );
      
      return $this->prepareModuleActions($moduleActions);
    }
    
    /**
     * Vrati moduly se vsemi akcemi.
     * @return array ['module1' => ['name' => 'nazev modulu', 'items' => ['akce1', ...]]], kde akce1 viz metoda prepareModuleActions.
     */
    public function getModulesWithActions() {
      $modules = $this->_moduleDao->loadRecords(NULL, array(['column' => 'name']));
      
      $results = array();
      foreach ($modules as /* @var $module ModuleEntity */ $module) {
        $results[$module->module]['name'] = $module->name;
        $results[$module->module]['items'] = $this->getModuleActions($module);
      }
      
      return $results;
    }
  }
