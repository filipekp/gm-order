<?php
  namespace prosys\model;

  use prosys\core\mapper\SqlFilter,
      prosys\core\common\Functions;
  /**
   * Represents the menu data access object.
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class MenuDao extends MyDataAccessObject
  {
    public function __construct() {
      parent::__construct('menu_items', MenuEntity::classname());
    }
    
    /**
     * Nacte rekurzivne polozky menu.
     * 
     * @param MenuEntity $parent
     * @return MenuEntity[]
     */
    public function loadMenuRecursive($parent = NULL) {
      $filter = SqlFilter::create();
      if (is_null($parent)) {
        $filter->isEmpty('parent');
      } else {
        $filter->compare('parent', '=', $parent->id);
      }
      
      $menus = $this->loadRecords(
        $filter,
        [
          ['column' => 'parent',    'direction' => 'asc'],
          ['column' => 'sequence',  'direction' => 'asc']
        ]
      );
      
      foreach ($menus as $key => &$menu) {
        /* @var $menu MenuEntity */
        $children = $this->loadMenuRecursive($menu);
        if ($children) {
          $visibles = array_filter($children, function($child) {
            /* @var $child MenuEntity */
            return $child->displayAlways > -1;
          });
          $menu->visibleMenu = count($visibles);
          $menu->children = $children;
        }
        
        // inicializace entity menu
        $menu->init(self::ProSYS());
        
        if (!$menu->render) {
          unset($menus[$key]);
        }
      }
      
      return $menus;
    }
    
    /**
     * Vrátí menu entity podle zadaného modulu pro aktivitu initial.
     * 
     * @param string $module
     * @param string $activity
     * 
     * @return MenuEntity
     */
    public function loadByModule($module, $activity = 'initial') {
      return Functions::first($this->loadRecords(
        SqlFilter::create()
          ->inFilter('type_value',
            SqlFilter::create()
              ->filter('id', 'module_actions', 
                SqlFilter::create()
                  ->compare('module', '=', $module)
                  ->andL()->compare('action_name', '=', $activity)))
      ));
    }
  }