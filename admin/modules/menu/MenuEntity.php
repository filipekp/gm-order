<?php
  namespace prosys\model;
  use prosys\core\common\Agents,
      prosys\core\common\Functions;

  /**
   * Represents the entity of menu.
   * 
   * @property int $id
   * @property string $name
   * @property string $title
   * @property string $type
   * @property string $typeValue element=type_value
   * @property string $icons
   * @property MenuEntity $parent element=parent
   * @property int $sequence
   * @property int $displayAlways element=display_always
   * 
   * @property MenuEntity[] $children noelement
   * @property bool $active noelement
   * @property string $link noelement
   * @property bool $render noelement
   * @property int $visibleMenu noelement
   * @property array $iconsHtml noelement
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class MenuEntity extends Entity
  {
    private $_children = array();
    private $_active = FALSE;
    private $_link;
    private $_render = TRUE;
    private $_additionalAttributes = [];
    private $_visibleMenu = 0;
    private $_iconsHtml = [];
    
    /**
     * Inicializace dalších properties entity menu
     * 
     * @return MenuEntity
     */
    public function init(\prosys\core\ProSYS $ProSYS) {
      /* @var $moduleActionDao \prosys\model\ModuleActionDao */
      $moduleActionDao = Agents::getAgent('ModuleActionDao', Agents::TYPE_MODEL);
    
      switch ($this->type) {
        case 'module_action':
          /* @var $moduleAction \prosys\model\ModuleActionEntity */
          $moduleAction = $moduleActionDao->load($this->typeValue);
          $this->_active = $moduleAction->module->module == $ProSYS->module() && $moduleAction->name == $ProSYS->activity();
          
          switch ($moduleAction->type) {
            case 1:
              $link = '?module=' . $moduleAction->module->module . (($moduleAction->name == 'initial') ? '' : '&activity=' . $moduleAction->name);
            break;
            case 2:
              $link = '?controller=' . $moduleAction->module->module . '&action=' . $moduleAction->name;
            break;
            default:
              $link = '';
            break;
          }
          
          if (!$ProSYS->loggedUser()->hasRight($moduleAction->module->module, $moduleAction->name)) { $this->_render = FALSE; }
        break;
        case 'link':
          $data = json_decode($this->typeValue, TRUE);
          $link = Functions::unsetItem($data, 'href');
          $this->_active = (filter_input(INPUT_SERVER, 'REQUEST_URI') == $link);
          foreach ($data as $attributte => $value) {
            $this->_additionalAttributes[] = $attributte . '="' . $value . '"';
          }
        break;
        case 'section':
          $data = (array)json_decode($this->typeValue, TRUE);
          $link = '#';
          foreach ($data as $attributte => $value) {
            $this->_additionalAttributes[] = $attributte . '="' . $value . '"';
          }
          
          if ($this->_visibleMenu < 1) { $this->_render = FALSE; }
        break;
      }
      
      $this->_link = $link;
      
      $this->_iconsHtml = (array)json_decode($this->icons);
      if ($this->_iconsHtml) {
        array_walk($this->_iconsHtml, function(&$icon) {
          $icon = '<i class="fa fa-' . $icon . '"></i>';
        });
      }
      
      return $this;
    }
    
    /* getters */
    public function getActive() {
      return $this->_active;
    }

    public function getLink() {
      return $this->_link;
    }
    
    public function getChildren() {
      return $this->_children;
    }
    
    public function getRender() {
      return $this->_render;
    }
    
    public function getAditionalAttributes() {
      return $this->_additionalAttributes;
    }
    
    public function getVisibleMenu() {
      return $this->_visibleMenu;
    }
    
    public function getIconsHtml() {
      return $this->_iconsHtml;
    }

        
    /* setters */
    public function setActive($active) {
      $this->_active = $active;
    }

    public function setLink($link) {
      $this->_link = $link;
    }

    public function setChildren($children) {
      $this->_children = $children;
      return $this;
    }
    
    public function setRender($render) {
      $this->_render = $render;
    }
    
    public function setAditionalAttributes($aditionalAttributes) {
      $this->_additionalAttributes = $aditionalAttributes;
    }
    
    public function setVisibleMenu($visibleMenu) {
      $this->_visibleMenu = $visibleMenu;
    }
    
    public function setIconsHtml($icons_html) {
      $this->_iconsHtml = $icons_html;
    }
  }
