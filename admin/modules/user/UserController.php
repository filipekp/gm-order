<?php
  namespace prosys\admin\controller;
  
  use prosys\core\common\Agents,
      prosys\core\common\AppException,
      prosys\core\mapper\SqlFilter,
      prosys\core\common\Functions,
      prosys\core\common\Settings,
      prosys\core\Translator,
      prosys\model\UserEntity,
    
      prosys\core\common\types\JSON,
      prosys\core\common\types\Image;

  /**
   * Processes the users requests.
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   * 
   * @property \prosys\admin\view\UserView $_view PROTECTED property, this annotation is only because of Netbeans Autocompletion
   * @property \prosys\model\UserDao $_dao PROTECTED property, this annotation is only because of Netbeans Autocompletion
   */
  class UserController extends Controller
  {
    /** @var LoginController */
    private $_loginController;
    
    /**
     * Initializes view and dao.
     */
    public function __construct() {
      parent::__construct();
      
      $this->_dao = Agents::getAgent('UserDao', Agents::TYPE_MODEL);
      $this->_view = Agents::getAgent('UserView', Agents::TYPE_VIEW_ADMIN);
      $this->_loginController = Agents::getAgent('LoginController', Agents::TYPE_CONTROLLER_ADMIN);
    }
    
    /**
     * Check required entries.
     * 
     * @param array $post
     * @param array $exceptions
     */
    protected function checkMandatories(array &$post, array &$exceptions) {      
      if (!Functions::item($post, 'first_name')) { $exceptions['first_name'] = 'Musíte zadat jméno.'; }
      if (!Functions::item($post, 'last_name')) { $exceptions['last_name'] = 'Musíte zadat příjmení.'; }
      if (!Functions::item($post, 'groups')) { $exceptions['groups'] = 'Musíte vybrat minimálně jednu uživatelskou skupinu.'; }
      if (!Functions::item($post, 'email')) { $exceptions['email'] = 'Musíte zadat email.'; }
      if (!Functions::item($post, 'language')) { $exceptions['language'] = 'Musíte vybrat jazyk.'; }
    }
    
    /**
     * Save user into the database.
     * 
     * @throws AppException
     */
    public function save($reload = TRUE) {
      $post = Functions::trimArray(filter_input_array(INPUT_POST));
      
      if (array_key_exists('delete', $post)) {
        header('Location: ' . Settings::ROOT_ADMIN_URL . '?controller=user&action=delete&id=' . $post['id']);
        exit();
      }
      
      // check form validity
      $exceptions = array();
      $this->checkMandatories($post, $exceptions);
      
      if ($exceptions) {
        throw new AppException($exceptions);
      } else {        
        $post['phone'] = str_replace(['(', ')', ' '], '', $post['phone']);

        $user = $this->_dao->load($post); /* @var $user UserEntity */
        $user->theme = 'default';

        if ($user->isNew()) {
          $user->otherSettings = [
            'countItemPerPage' => Settings::getVariable('SHOWING', 'ITEMS_PER_PAGE'),
            'lock' => [
              'idleTime' => Settings::LOCK_IDLE_TIME
            ],
            'messages_settings' => [
              'horizontal'  => 'right',
              'vertical'    => 'top',
              'hideTime'    => 5
            ]
          ];
        }
        
        try {
          $this->_dao->beginTransaction();
            if ($this->_dao->store($user)) {
              $_SESSION[Settings::MESSAGE_SUCCESS][] = "Uživatel &bdquo;{$user->getFullName()}&ldquo; byl úspěšně uložen.";

              if (array_key_exists('groups', $post) && $post['groups']) {
                /* @var $groupDao \prosys\model\GroupDao */
                $groupDao = Agents::getAgent('GroupDao', Agents::TYPE_MODEL);
                foreach ($post['groups'] as $group) {
                  /* @var $group \prosys\model\GroupEntity */
                  $group = $groupDao->load($group);
                  $userGroup = $this->_dao->getUserGroupItemByUserAndGroup($user, $group);
                  if (!$userGroup) {
                    $userGroup = (object)array(
                      'id_group'  => $group->id,
                      'user_id'   => $user->id
                    );
                    $this->_dao->saveUserGroup($userGroup);
                  }
                }

                $this->_dao->deleteOtherUserGroups($user, $post['groups']);
              }
              if (Functions::item($post, 'create_login')) {
                $post['user'] = $user->id;
                $this->_loginController->save(FALSE, $post);
              }
            } else {
              throw new AppException("Uživatele &bdquo;{$user->getFullName()}&ldquo; se nepodařilo uložit.");
            }
          $this->_dao->commit();
        } catch (AppException $e) {
          $_SESSION[Settings::MESSAGE_SUCCESS] = [];
          if ($e->getPrevious()) {
            $_SESSION[Settings::MESSAGE_EXCEPTION][] = $e->getPrevious()->getMessage();
          } else {
            $_SESSION[Settings::MESSAGE_EXCEPTION][] = 'Při ukládání došlo k následující chybě: ' . $e->getMessage();
          }
        }
        
        if ($reload) {
          if (array_key_exists('apply', $post)) {
            switch ($post['back_to']) {
              case 'changeProfile':
                $this->reload($post['back_to']);
              break;
              default:
                $this->reload($post['back_to'], 'id=' . $user->id);
              break;
            }
          } else {
            $this->reload();
          }
        }
      }
    }
    
    /**
     * Ulozi heslo z upravy profilu uzivatele.
     */
    public function saveProfile() {
      try {
        $login = $this->_loginController->savePassword();
        if (($phone = Functions::item($this->_post, 'phone'))) { $this->_post['phone'] = str_replace(['(', ')', ' '], '', $phone); }
        $user = $this->_dao->load(array_merge(['id' => $login->user->id], $this->_post));
        
        if ($this->_dao->store($user)) {
          $_SESSION[Settings::MESSAGE_SUCCESS][] = 'Váš profil byl uložen.';
        } else {
          throw new AppException('Váš profil se nepodařilo uložit.');
        }
      } catch (AppException $exception) {
        $_SESSION[Settings::MESSAGE_EXCEPTION][] = $exception->getMessage();
      }
      
      $this->redirect('user', 'changeProfile', $this->_post['back_to']);
    }
    
    /**
     * Ulozi avatara k uzivateli.
     * @throws AppException
     */
    public function saveAvatar() {
      /* @var $fileDao \prosys\model\FileDao */
      /* @var $fileDescriptionDao \prosys\model\Desc_FileDao */
      /* @var $fileBindingStorageDao \prosys\model\FileBindingStorageDao */
      /* @var $languageDao \prosys\model\LanguageDao */
      $fileDao = Agents::getAgent('FileDao', Agents::TYPE_MODEL);
      $fileDescriptionDao = Agents::getAgent('Desc_FileDao', Agents::TYPE_MODEL);
      $fileBindingStorageDao = Agents::getAgent('FileBindingStorageDao', Agents::TYPE_MODEL);
      $languageDao = Agents::getAgent('LanguageDao', Agents::TYPE_MODEL);

      $createdNewAvatar = FALSE;
      $avatar = self::ProSYS()->loggedUser()->avatar;

      try {
        $this->_dao->beginTransaction();

          // nema-li uzivatel zadneho avatara, vytvori noveho
          if ($avatar->isNew()) {
            // vytvori "prazdny" avatar -> ziska id
            $avatar->fileBindingStorage = $fileBindingStorageDao->load(2);
            $fileDao->store($avatar);

            // pripravi prazdny avatar pro dalsi praci
            $avatar->setIsNew(FALSE);
            $avatar->mime = Image::MIME_JPEG;     // zaridi vytvoreni obrazku ve file entity
            
            $createdNewAvatar = TRUE;
            
            // vytvori popisek noveho avataru
            $avatarDescription = $fileDescriptionDao->load();     /* @var $avatarDescription \prosys\model\Desc_FileEntity */
            $avatarDescription->file = $avatar;
            $avatarDescription->language = $languageDao->loadDefaultLanguage();
            $avatarDescription->name = self::ProSYS()->loggedUser()->getIdentification();
            $avatarDescription->description = Translator::translate(['avatar', 'descriptions', 'description'], [$avatarDescription->name]);
            $fileDescriptionDao->store($avatarDescription);
          }

          // uploaduje avatara
          $avatar->file->upload('avatar', Image::UPLOAD_OVERWRITE);

          // zmeni velikost a rozmery avataru
          $resizedWidth = Functions::item($this->_post, 'imgW');
          $selectedX = Functions::item($this->_post, 'selectedX');
          $selectedY = Functions::item($this->_post, 'selectedY');
          $selectedSize = Functions::item($this->_post, 'selectedSize');

          if (!$selectedSize || is_null($selectedX) || is_null($selectedY)) {
            $_SESSION[Settings::MESSAGE_EXCEPTION][] = 'Obrázek k účtu se nepodařilo uložit kvůli chybějícím údajům o rozměrech.';
          }

          // prepocita vyber z velikosti nahledu v prohlizeci do realne velikosti obrazku
          $ratio = $avatar->file->getWidth() / $resizedWidth;
          $x = $ratio * $selectedX;
          $y = $ratio * $selectedY;
          $size = $ratio * $selectedSize;

          $avatar->file->crop($x, $y, $size, $size);
          $avatar->file->resize(AvatarEntity::AVATAR_WIDTH, AvatarEntity::AVATAR_HEIGHT);
          $avatar->file->store();

          // ulozeni novych dat k avatarovi
          $avatar->extension = $avatar->file->info('extension');
          $avatar->mime = $avatar->file->info('mime');
          $avatar->originalName = $avatar->file->getOriginalName();
          $avatar->size = $avatar->file->info('size');
          
          if ($createdNewAvatar) {
            $avatar->modification = new \DateTime();
          }

          $fileDao->store($avatar);

          // ulozi avatar k uzivateli
          self::ProSYS()->loggedUser()->setWasChanged(TRUE);    // nastavi stav uzivatele, protoze probehly zmeny v jeho avataru (na ty on nevidi)
          $this->_dao->store(self::ProSYS()->loggedUser());

        $this->_dao->commit();
      } catch (AppException $e) {
          $_SESSION[Settings::MESSAGE_SUCCESS] = [];
          if ($e->getPrevious()) {
            $_SESSION[Settings::MESSAGE_EXCEPTION][] = $e->getPrevious()->getMessage();
          } else {
            $_SESSION[Settings::MESSAGE_EXCEPTION][] = 'Při ukládání došlo k následující chybě: ' . $e->getMessage();
          }
          
          if (!$createdNewAvatar) {
            try {
              $avatar->file->restore();
            } catch (AppException $ex) {
              $_SESSION[Settings::MESSAGE_EXCEPTION][] = $ex->getMessage();
            }
          }
      }

      // proslo-li vse v poradku, uvolni pamet od zalohy puvodniho souboru
      $avatar->file->backupFree();
      
      header('Content-Type: application/json');
      echo new JSON([
        'response'  => [
          'status'  => 200,
          'message' => Functions::handleMessagesAdmin(TRUE)
        ],
        'data'      => ['image' => $avatar->file->getBase64()]
      ]);
      
      exit();
    }
    
    /**
     * Delete user from the database.
     * 
     * @throws AppException
     */
    public function delete() {
      $id = (string)filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
      $user = $this->_dao->load($id);
      
      if ($user->isNew()) {
        throw new AppException("Uživatele nebylo možné smazat. Uživatel s přihlašovacím jménem &bdquo;{$id}&ldquo; neexistuje.");
      } else {
        if ($this->_dao->delete($user)) {
          $this->_infoMessage = "Uživatel &bdquo;{$user->getFullName()}&ldquo; byl úspěšně smazán.";
          $this->reload();
        } else {
          throw new AppException("Uživatele &bdquo;{$user->getFullName()}&ldquo; se nepodařilo smazat.");
        }
      }
    }
    
    /**
     * Vycisti session od prihlaseneho uzivatele.
     */
    public static function cleanSession() {
      foreach ($_SESSION as $key => $value) {
        Functions::unsetItem($_SESSION, $key);
      }
    }
    
    /**
     * Prihlasi uzovatele do systemu.
     * 
     * @param \prosys\model\UserEntity $user
     * @return bool
     */
    public function loginUser($user) {
      $_SESSION['logged_user_id'] = $user->id;
      $_SESSION['last_access'] = $user->lastAccess;
      
      $user->setLoggedHistory(FALSE);
      $user->lastAccess = new \DateTime();
      
      return (bool)$this->_dao->store($user);
    }
    
    /**
     * Odstrani vsechny informace o uzivateli ze session
     */
    public function logout() {
      if (array_key_exists('logged_user_id', $_SESSION)) {
        self::cleanSession();
      }
    }

    /**
     * @inherit
     */
    public function response($activity) {
      $templateOnly = (bool)Functions::item($this->_get, 'template_only');
      
      /* @var $groupDao \prosys\model\GroupDao */
      $groupDao = Agents::getAgent('GroupDao', Agents::TYPE_MODEL);
      $groups = $groupDao->loadRecords(NULL, array(array('column' => 'is_admin', 'direction' => 'DESC'), array('column' => 'name')));
      
      /* @var $languageDao \prosys\model\LanguageDao */
      $languageDao = Agents::getAgent('LanguageDao', Agents::TYPE_MODEL);
      $languageEntities = $languageDao->loadLanguages(NULL, [['column' => 'name']]);
      $languages =  array_combine(array_map(function($language /* @var $language \prosys\model\LanguageEntity */) {
                      /* @var $language \prosys\model\LanguageEntity */
                      return $language->id;
                    }, $languageEntities), array_map(function($language) {
                      /* @var $language \prosys\model\LanguageEntity */
                      return ['option' => $language->name, 'data' => ['img_path' => $language->flagImage()]];
                    }, $languageEntities));
      
      /* @var $moduleActionDao \prosys\model\ModuleActionDao */
      $moduleActionDao = Agents::getAgent('ModuleActionDao', Agents::TYPE_MODEL);

      switch ($activity) {
        case 'changeProfile':
          $allowedActivities = [];
          foreach ($moduleActionDao->loadByUser(self::ProSYS()->loggedUser()) as $moduleActivities) {
            /* @var $moduleActivities['module'] \prosys\model\ModuleEntity */
            
            foreach ($moduleActivities['actions'] as /* @var $activity \prosys\model\ModuleActionEntity */$activity) {
              $allowedActivities[$activity->id] = ['option' => $activity->title, 'data' => ['group' => $moduleActivities['module']->name]];
            }
          }
          
          $optional = array(
            'allowed_pages'     => $allowedActivities,
            'themes'            => UserEntity::$_THEMES,
            'languages'         => $languages,
            'defaultLanguage' => Functions::first(array_filter($languageEntities, function($language) {
              /* @var $language \prosys\model\LanguageEntity */ 
              return $language->isDefault;
            })),
            'template_only'     => $templateOnly
          );
          
          $this->_view->manageProfile(self::ProSYS()->loggedUser(), $optional);
        break;
        
        case 'manage':
          $data = (($this->_post) ? $this->_post : $this->_get);
          
          /* @var $user prosys\model\UserEntity */
          $user = $this->_dao->load($data);
          $userGroups = Functions::item($this->_post, 'groups', $user->groups->keys());

          $optional = array(
            'post'          => $this->_post,
            'get'           => $this->_get,
            'groups'        => $groups,
            'userGroups'    => $userGroups,
            'template_only' => $templateOnly,
            'UserEntity'    => [
              '_SEX'      => UserEntity::$_SEX,
              'SEX_WOMAN' => UserEntity::SEX_WOMAN,
              'SEX_MAN'   => UserEntity::SEX_MAN
            ],
            'languages'     => $languages,
            'defaultLanguage' => Functions::first(array_filter($languageEntities, function($language) {
              /* @var $language \prosys\model\LanguageEntity */ 
              return $language->isDefault;
            }))
          );

          $this->_view->manage($user, $optional);
        break;
      
        case 'initial':
        case 'table':
        default:
          // filtr
          $filter = SqlFilter::create()->identity();
          
          if (($filterName = Functions::item($this->_currentFilter, 'name'))) {
            $filter->andL(
              SqlFilter::create()->contains('CONCAT(last_name, " ", first_name)', $filterName)
            );
          }
          
          // vychozi razeni
          if (!$this->_currentSorting) {
            $this->_currentSorting = ['name' => 'asc'];
          }
          
          // uprava razeni do spravneho formatu
          $sorting = array_map(function($column){
            switch ($column) {
              case 'name':
                $sortBy = 'CONCAT(last_name, " ", first_name)';
              break;
              
              default: $sortBy = $column;
            }
            
            return ['column' => $sortBy, 'direction' => $this->_currentSorting[$column]];
          }, array_keys($this->_currentSorting));
          
          // strankovani
          $count = $this->_dao->count($filter);   // output variable

          // korekce aktualni strany
          $this->currentPageCorrection($count);
          $limitFrom = ($this->_currentPage - 1) * $this->_itemsOnPage;

          $users = $this->_dao->loadAllUsers($filter, $sorting, [$limitFrom, $this->_itemsOnPage]);

          $optional = array(
            'get'               => $this->_get,
            'query'             => array_merge($this->_get, ['sorting' => $this->_currentSorting]),
            'count'             => $count,
            'items_on_page'     => $this->_itemsOnPage,
            'template_only'     => $templateOnly
          );
          
          $this->_view->table($users, $optional);
        break;
      }
    }
    
    /**
     * inherit
     */
    public function validate() {
      $this->_loginController->checkMandatories($this->_post, $this->_exceptions);
      
      parent::validate();
    }
  }

