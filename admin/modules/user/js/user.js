var User = {
  selectors: {
    loginCheckbox: '#create_login',
    loginForm: 'div.login_data'
  },
  avatar: {
    selection: {
      defaultSizeFraction: 1
    }
  }
};

function toggleLoginForm(speed) {
  if ($(User.selectors.loginCheckbox).prop('checked')) {
    $(User.selectors.loginForm).show('slide', {}, speed);
  } else {
    $(User.selectors.loginForm).hide('slide', {}, speed);
  }
}

$(document).ready(function() {  
  toggleLoginForm(0);
  $('body').on('switchChange.bootstrapSwitch', User.selectors.loginCheckbox, function() {
    toggleLoginForm(400);
  });
  
  $('form.user_information').submit(function() {
    $(this).find('input[name="back_to"]').val(document.location.hash);
  });
  
  // validace formulare
  $('#user_manage_form').customValidator({
    rules: {
      'groups[]': {
        required: true,
        minlength: 1
      }
    },
    messages: {
      'groups[]': {
        required: 'Musíte vybrat alespoň jednu skupinu',
        minlength: $.validator.format('Prosím vyberte alespoň {0} skupinu.')
      }
    }
  }).enablePHPValidate('user', 'validate');
  
  // oriznuti obrazku avataru
  $('body').on('change.bs.fileinput', '.profile-userpic [data-provides]', function() {
    var $provider = $(this);
    
    $('#selectedX').remove();
    $('#selectedY').remove();
    $('#selectedSize').remove();
    $('#imgW').remove();
    
    $provider.prepend($('<input id="selectedX" type="hidden" name="selectedX" />'))
             .prepend($('<input id="selectedY" type="hidden" name="selectedY" />'))
             .prepend($('<input id="selectedSize" type="hidden" name="selectedSize" />'))
             .prepend($('<input id="imgW" type="hidden" name="imgW" />'));
    
    $.fancybox({
      href        : '#cropper',
      maxWidth    : '90%',
      maxHeight   : '90%',
      scrolling   : 'no',
      closeClick  : false,
      
      helpers     : {
        overlay: { closeClick: false }
      },

      tpl         : $.extend({}, Lightbox.$tpl, {
        closeBtn : '<button class="btn btn-circle green" type="button" style="float: right; margin-top: 10px;">Výběr uložit jako obrázek účtu</button><br style="clear: both;" />'
      }),
      
      beforeShow     : function() {
        $('.fancybox-wrap').css('margin-top', '-5000px');
      },
      
      afterShow   : function() {
        setTimeout(function() {
          var $img = $('#cropper img'),
              $overlay = $('.fancybox-overlay'),
              $wrap = $('.fancybox-wrap'),
              $inner = $('.fancybox-inner');
        
          // zjistit sirku a vysku bilych okraju!!
          var screenW = $overlay.actual('width') * 0.9,                     // rozmery obrazovky (resp. polopruhledne vrstvy fancyboxu)
              screenH = $overlay.actual('height') * 0.9,
              marginW = $wrap.actual('width') - $inner.actual('width'),     // rozmery "bilych" okraju fancyboxu
              marginH = $wrap.actual('height') - $inner.actual('height'),
              imgW = $img.actual('width'),                                  // realne rozmery obrazku
              imgH = $img.actual('height'),
              ratio = imgW / imgH,                                          // pomer stran obrazku
              boundaryW = screenW - marginW,                                // nove rozmery obrazku, resp. obdelnik, do ktereho se musi obrazek prizpusobit
              boundaryH = screenH - marginH,
              imgNewW = boundaryW,                                          // vychozi rozmery noveho obrazku
              imgNewH = imgNewW / ratio;

          if (imgNewH > boundaryH) {
            imgNewH = boundaryH;
            imgNewW = imgNewH * ratio;
          }
          
          $('#imgW').val(imgNewW);

          $img.width(imgNewW);
          $.fancybox.update();

          $wrap.animate({
            marginTop: 0
          }, 500);

          var selectionSize = imgNewW,
              x = y = 0;
              
          if (ratio !== 1) {
            selectionSize = Math.min(imgNewW * User.avatar.selection.defaultSizeFraction, imgNewH * User.avatar.selection.defaultSizeFraction);
            x = (imgNewW - selectionSize) / 2;
            y = (imgNewH - selectionSize) / 2;
          }
          
          $img.Jcrop({
            aspectRatio: 1,
            allowSelect: false,
            setSelect: [x, y, x + selectionSize, y + selectionSize],
            minSize: [192, 192],
            bgColor: 'white',
            bgOpacity: 0.3,

            onSelect: function(c) {
              $('#selectedX').val(c.x);
              $('#selectedY').val(c.y);
              $('#selectedSize').val(c.w);
            },
            
            onRelease: function() {
              this.setSelect([0, 0, 192, 192]);
            }
          });
        }, 100);
      },
      
      beforeClose  : function() {
        if (parseInt($('#selectedSize').val()) === 0) {
          return false;
        }
        
        // zapne animaci nacitani
        var $toBlock = $provider.find('.thumbnail');
        Metronic.blockUI({
          target: $toBlock,
          animate: true,
          bounceColorClass: 'white',
          overlayColor: '#000000',
          overlayOpacity: 0.5
        });

        var $form = new FormData($('form.profile-userpic')[0]);
        $.ajax('index.php', {
          method: 'POST',
          contentType: false,
          processData: false,
          dataType: 'json',
          data: $form
        }).done(function(response) {
          $toBlock.find('img').attr('src', response.data.image);
          $('.page-header .top-menu .dropdown-user img.img-circle').attr('src', response.data.image);
        }).always(function($result) {
          $provider.find('input[type="file"]').val('');
          getToasterStatusMessage(JSON.stringify($result.response.message));
          
          Metronic.unblockUI($toBlock);
        });
      }
    });
  });
});
