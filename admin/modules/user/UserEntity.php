<?php
  namespace prosys\model;
  use prosys\core\common\Agents;

  /**
   * Represents the entity of backend user.
   * 
   * @property int $id
   * @property string $firstName element=first_name
   * @property string $lastName element=last_name
   * @property string $phone
   * @property string $email
   * @property string $salutation
   * @property int $sex
   * @property string $theme
   * @property FileEntity $avatar
   * @property LanguageEntity $language element=language_id
   * @property \prosys\core\common\types\JSON $otherSettings element=other_settings
   * @property \DateTime $lastAccess element=last_access
   * @property boolean $deleted delete_flag
   * 
   * binding
   * @property GroupEntity[] $groups binding=mn:user_group:id>user_id:id>id_group:name>asc
   * @property LoginEntity[] $logins binding=1n:id>user_id:login>asc
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class UserEntity extends Entity
  {
    const DEFAULT_AVATAR = 3;
    
    const AVATAR_WIDTH = 200;
    const AVATAR_HEIGHT = 200;

    public function __construct() {
      $this->loggedHistory = TRUE;
      
      parent::__construct();
    }
    
    public static $_THEMES = array(
      'default'   => 'Výchozí',
      'darkblue'  => 'Tmavě modrá',
      'blue'      => 'Modrá',
      'grey'      => 'Šedá',
      'light'     => 'Bílá',
      'light2'    => 'Bílá 2'
    );
    
    const SEX_WOMAN = 0;
    const SEX_MAN = 1;

    public static $_SEX = array(
      self::SEX_WOMAN => 'žena',
      self::SEX_MAN   => 'muž'
    );

    /**
     * Returns full name of the user.
     * @return string
     */
    public function getFullName() {
      return $this->lastName . ' ' . $this->firstName;
    }
    
    /**
     * Returns identification of the user (name and e-mail).
     * @return string
     */
    public function getIdentification() {
      return $this->getFullName();
    }
    
    /**
     * Returns contact of the user (e-mail and phone).
     * @return type
     */
    public function getContact($separator = ', ') {
      $contact = array();
      if ($this->email) { $contact[] = $this->email; }
      if ($this->phone) { $contact[] = $this->phone; }
      
      return implode($separator, $contact);
    }
    
    /**
     * Overi, zda ma osoba prava na dotazovany modul a akci.
     * 
     * @param string $module
     * @param string $action
     * 
     * @return boolean
     */
    public function hasRight($module, $action) {
      /* @var $securityEngine \prosys\admin\security\SecurityEngine */
      $securityEngine = Agents::getAgent('SecurityEngine', Agents::TYPE_SECURITY);
      
      $pageName = '';
      return $securityEngine->authorizeAction($module, $action, '', $pageName) || 
             $securityEngine->authorizeActivity($module, $action, '', $pageName) ||
             $securityEngine->authorizeOtherRights($module, $action, '', $pageName);
    }
    
    /**
     * Vrati TRUE|FALSE podle toho zda uzivatel ma prirazeny login ci nikoli.
     * 
     * @return boolean
     */
    public function hasLogin() {
      return (bool)count($this->logins);
    }
    
    /**
     * Vrati avatara pro uživatele
     * 
     * @return AvatarEntity
     */
    public function getCurrentAvatar() {
      if ($this->avatar->isNew()) {
        return Agents::getAgent('FileDao', Agents::TYPE_MODEL)->load(self::DEFAULT_AVATAR);
      } else {
        return $this->avatar;
      }
    }
    
    /**
     * Vrati vsechny pouzitelne avatary pro uzivatele.
     * 
     * @return AvatarEntity[]
     */
    public function getAvailableAvatars() {
      /* @var $avatarDao AvatarDao */
      $avatarDao = Agents::getAgent('AvatarDao', Agents::TYPE_MODEL);
      
      return $avatarDao->getAvatars($this);
    }
    
    /**
     * Vrati skupiny, do kterych uzivatel patri.<br />
     * Pokud je predan parametr $what tak vrati pole retezcu s vyslednou property predanou v parametru.
     * 
     * @param string $what nazev property z group entity
     * @return array|GroupEntity[]
     */
    public function getGroupsProjection($what) {
      return array_map(function($group) use ($what) {
        return $group->$what;
      }, $this->groups->getLoadedArrayCopy());
    }
  }
