<?php
  namespace prosys\model;
  
  use prosys\core\mapper\SqlFilter,
      prosys\core\mapper\MySqlMapper,
      prosys\core\common\Agents,
      prosys\core\common\Functions;

  /**
   * Represents the user data access object.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class UserDao extends MyDataAccessObject
  {
    /** @var MySqlMapper */
    private $_userGroupMapper;
    
    public function __construct() {
      // create data mapper
      $DB = self::ProSYS()->config['SQL'];
      $mySqlHandler = Agents::getAgent('MySqlConnection', Agents::TYPE_COMMON, [
        $DB['server'],
        $DB['user'],
        $DB['password'],
        $DB['database'],
        $DB['prefix']
      ]);

      // set user_group mapper
      $this->_userGroupMapper = new MySqlMapper($mySqlHandler, 'user_group', '');
      
      parent::__construct('users', UserEntity::classname());
    }
    
    /**
     * Get user group by user.
     * 
     * @param UserEntity $user
     * @param GroupEntity $group
     * 
     * @return object
     */
    public function getUserGroupItemByUserAndGroup($user, $group) {
      return Functions::first($this->_userGroupMapper->findRecords(
        SqlFilter::create()
          ->compare('user_id', '=', $user->id)
          ->andL()->compare('id_group', '=', $group->id)));
    }
    
    /**
     * Uloží do tabulky user_group skupinu pro uživatele.
     * 
     * @param object $userGroup
     * @return bool
     */
    public function saveUserGroup($userGroup) {
      return $this->_userGroupMapper->insert(array('user_id' => $userGroup->user_id, 'id_group' => $userGroup->id_group));      
    }
    
    /**
     * Vymaže všechny ostatní skupiny z tabulky user_group pro daného uživatele.
     * 
     * @param UserEntity $user
     * @param array $groups
     * 
     * @return bool
     */
    public function deleteOtherUserGroups($user, $groups) {
      return $this->_userGroupMapper->deleteRecords(SqlFilter::create()
        ->compare('user_id', '=', $user->id)
        ->andL()->notInArray('id_group', $groups));      
    }
    
    /**
     * Nacte vsechny uzivatele.
     */
    public function loadAllUsers($filter = NULL, $order = [['column' => 'CONCAT(last_name, " ", first_name)', 'direction' => 'asc']], $limit = []) {
      return $this->loadRecords($filter, $order, $limit);
    }
  }
