<?php
  namespace prosys\admin\view;
  
  use prosys\model\Entity,
      prosys\core\common\types\html\Icon,
      prosys\core\common\types\html\Span,
      prosys\core\common\types\html\LinkActivity,
      prosys\core\common\types\html\LinkAction,
      prosys\core\common\types\html\Button,
      prosys\core\common\types\html\Theme;

  /**
   * Represents the admin user module view.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class UserView extends BackendView
  {
    /**
     * Initializes the label of every user property.
     */
    public function __construct()
    {
      $this->_moduleIcon = 'user';
      
      $labels = array(
        'first_name'        => 'Jméno',
        'last_name'         => 'Příjmení',
        'phone'             => 'Telefon',
        'email'             => 'E-mail',
        'contact'           => 'Kontakt',
        'groups'            => 'Uživatelské skupiny',
        'type'              => 'Typ uživatele',
        'sex'               => 'Pohlaví',
        'salutation'        => 'Oslovení',
        'default_page'      => 'Výchozí stránka',
        'theme'             => 'Barevné schéma',
        'countItemPerPage'  => 'Počet položek na stránku',
        'idleTime'          => 'Uzamčení obrazovky po (sec.)',
        'language'          => 'Jazyk',
        'messages'          => [
          'horizontal' => [
            'title'   => 'Horizontální zarovnání',
            'values'  => [
              'left'  => 'vlevo',
              'right' => 'vpravo',
              'center' => 'uprostřed',
              'full-width' => 'celá šířka'
            ]
          ],
          'vertical' => [
            'title'   => 'Vertikální zarovnání',
            'values'  => [
              'top'     => 'nahoře',
              'bottom'  => 'dole'
            ]
          ],
          'hideTime'  => 'Doba zobrazení hlášky (sec.)'
        ],
        
        'autosave'      => [
          'enable'  => 'Stav',
          'time'  => 'Čas autoukládání'
        ],
        
        'login'             => LoginView::getLabels(),
        'create_login'      => 'Přihlašovací údaje'
      );
      
      $this->_moduleCallbacks = [
        'table' => [
          'columns' => [
            'avatar' => [
              'value' => function($user) {
                return '<img src="' . $user->getCurrentAvatar()->file->getFileURL() . '" alt="' . $user->getIdentification() . '" />';
              }
            ],
            'login_title' => [
              'value' => function($user) {
                if ($user->hasLogin()) {
                  $logins = array_map(function($login) {
                    return (new LinkActivity($login->login, 'login', 'manage'))->setQuery(['id' => $login->id]);
                  }, $user->logins->getLoadedArrayCopy());
                  
                  $icon = (new Icon('check-circle-o'))->addClass(['status_icon', 'green']);
                  $value = $icon . implode(', ', $logins);
                } else {
                  $icon = (new Icon('times-circle-o'))->addClass(['status_icon']);
                  $value = (new Span($icon . 'nemá přihlašovací údaje'))->addClass(['unimportant', 'unimportant-block']);
                }
                
                return $value;
              }
            ],
            'groups' => [
              'value' => function($user) {
                return (($user->groups) ? 
                          implode(', ', $user->getGroupsProjection('name')) : 
                          (new Span('nemá uživatelské skupiny'))->addClass(['unimportant', 'unimportant-block']));
              }
            ]
          ],
              
          'actions' => [
            'manage' => [
              'action' => function($user) {
                $link = (new LinkActivity(new Icon('pencil'), $this->_moduleName, 'manage'))->setQuery(['id' => $user->id]); 
                if (!self::ProSYS()->loggedUser()->hasRight($this->_moduleName, 'manage')) {
                  $link->addClass('disabled');
                }
              
                return $link;
              },
              'title' => function($user) {
                return 'Upravit ' . $user->getIdentification();
              }
            ],

            'delete' => [
              'action' => function($user) {
                $link = (new LinkAction(new Icon('trash'), $this->_moduleName, 'delete'))->setQuery(['id' => $user->id]); 
                
                $loggedUser = self::ProSYS()->loggedUser();
                if ($user->id == $loggedUser->id || $user->hasLogin() || !$loggedUser->hasRight($this->_moduleName, 'delete')) {
                  $link->addClass('disabled');
                }
              
                return $link;
              },
              'title' => function($user) {
                return 'Smazat ' . $user->getIdentification();
              },
              'confirm' => function($user) {
                return 'Opravdu chcete osobu `' . $user->getIdentification() . '` smazat?';
              }
            ]
          ]
        ]
      ];

      parent::__construct($labels);
    }
    
    /**
     * Prints out manage form to managing the user profile.
     * 
     * @param \prosys\model\UserEntity $user
     * @param array $optional associative array with optional data
     */
    public function manageProfile(Entity $user, $optional = array()) {
      /* @var $user \prosys\model\UserEntity */
      $assign = $optional + array('user' => $user);
      
      $heading = 'Úprava profilu &bdquo;' . $user->getIdentification() . '&ldquo;';      
      
      $templateOnly = ((array_key_exists('template_only', $optional)) ? $optional['template_only'] : FALSE);

      $this->printActivity('ManageProfile', $heading, $assign, $templateOnly);
    }
    
    /**
     * Prints out manage form to managing the user.
     * 
     * @param \prosys\model\UserEntity $user
     * @param array $optional associative array with optional data
     */
    public function manage(Entity $user, $optional = array()) {
      /* @var $user \prosys\model\UserEntity */
      $assign = $optional + array('user' => $user);
      
      if ($user->isNew()) {
        $heading = 'Nový uživatel';
        $assign['delete'] = '';
        
      } else {
        $heading = 'Úprava uživatele &bdquo;' . $user->getIdentification() . '&ldquo;';
        
        $loggedUser = self::ProSYS()->loggedUser();
        if ($loggedUser->hasRight($this->_moduleName, 'delete') && $user->id != $loggedUser->id && !$user->hasLogin()) {
          $assign['delete'] = (new Button((new Icon('trash')) . 'Smazat', Theme::get(Theme::RED), Button::TYPE_LINK))
                                ->setLink((new LinkAction('', $this->_moduleName, 'delete'))->setQuery(['id' => $user->id]))
                                ->addClass(['red', 'delete'])
                                ->title('Smazat')
                                ->attribute('onclick', 'return confirm(\'Opravdu chcete uživatele smazat?\');');
        } else {
          $assign['delete'] = '';
        }
      }
      
      $templateOnly = ((array_key_exists('template_only', $optional)) ? $optional['template_only'] : FALSE);

      $this->printActivity('Manage', $heading, $assign, $templateOnly);
    }
    
    /**
     * Prints out the table of users.
     * 
     * @param \prosys\model\UserEntity[] $data
     * @param array $optional
     */
    public function table($data = array(), $optional = array()) {
      $assign = $optional + array(
        'data' => $data,
        'totalcount' => $optional['count'],
        'pagination' => BackendView::generatePaging($optional['count'], $optional['get'], $optional['items_on_page'])
      );
      
      $templateOnly = ((array_key_exists('template_only', $optional)) ? $optional['template_only'] : FALSE);

      $this->printActivity('Table', 'Seznam uživatelů', $assign, $templateOnly);
    }
  }
