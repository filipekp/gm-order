{nocache}{handle_admin_messages}{/nocache}
<div class="row">
  <div class="col-md-12">
    <h3 class="page-title">{nocache}{$title}{/nocache}</h3>
    {nocache}
      {breadcrumb_add title=$title icons=[$moduleIcon,($user->isNew())|ternary:'plus':'pencil'|cat:' aditional'] link='?module='|cat:$moduleName|cat:'&activity=manage'|cat:(($user->isNew())|ternary:'':('&id='|cat:$user->id))}
      {breadcrumbs_get}
    {/nocache}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet box red">
      <div class="portlet-title">
        <div class="caption">
          {link_external assign='link' text='Zpět na seznam uživatelů' href=$ProSYS.SETTINGS.ROOT_ADMIN_URL|cat:'?module='|cat:$moduleName}
          {button text='Zpět na seznam uživatelů' icon=$moduleIcon|icon type='a' link=$link theme='grey' classes='btn-xs' title='Zpět na seznam uživatelů'}
        </div>
        <div class="tools"></div>
      </div>
      <div class="portlet-body form">
        <form id="user_manage_form" action="" class="form-horizontal" method="post">
          <input type="hidden" name="controller" value="{$moduleName}" />
          <input type="hidden" name="action" value="save" />
          <input type="hidden" name="back_to" value="{nocache}{$get.activity}{/nocache}" />
          {nocache}
          {if !$user->isNew()}
            <input type="hidden" name="id" value="{$user->id}" />
          {/if}
          {/nocache}
          
          <div class="form-body clearfix">
            <div class="col-md-6">
              <h3 class="form-section">Informace o uživateli</h3>
              <div class="form-group">
                <label for="first_name" class="control-label col-md-2">{$labels.first_name}: *</label>
                <div class="col-md-8">
                  <input class="form-control" id="first_name" type="text" name="first_name" maxlength="64" value="{nocache}{$user->firstName}{/nocache}" data-required="1" placeholder="{$labels.first_name}" autofocus />
                </div>
              </div>

              <div class="form-group">
                <label for="last_name" class="control-label col-md-2">{$labels.last_name}: *</label>
                <div class="col-md-8">
                  <input class="form-control col-md-8" id="last_name" type="text" name="last_name" maxlength="64" value="{nocache}{$user->lastName}{/nocache}" placeholder="{$labels.last_name}" />
                </div>
              </div>

              <div class="form-group">
                <label for="salutation" class="control-label col-md-2">{$labels.salutation}:</label>
                <div class="col-md-8">
                  <input class="form-control col-md-8" id="salutation" type="text" name="salutation" maxlength="64" value="{nocache}{$user->salutation}{/nocache}" placeholder="{$labels.salutation}" />
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-2">{$labels.sex}: *</label>
                <div class="col-md-8">
                  {nocache}
                  <div class="radio-list">
                  <label for="sex_{$UserEntity['SEX_MAN']}" class="radio-inline">
                    <input id="sex_{$UserEntity['SEX_MAN']}" type="radio" name="sex" class="form-control" value="{$UserEntity['SEX_MAN']}"{if $user->sex == $UserEntity['SEX_MAN']} checked="checked"{/if} />
                    {$UserEntity['_SEX'][$UserEntity['SEX_MAN']]}
                  </label>

                  <label for="sex_{$UserEntity['SEX_WOMAN']}" class="radio-inline">
                    <input id="sex_{$UserEntity['SEX_WOMAN']}" type="radio" name="sex" class="form-control" value="{$UserEntity['SEX_WOMAN']}"{if $user->sex == $UserEntity['SEX_WOMAN']} checked="checked"{/if} />
                    {$UserEntity['_SEX'][$UserEntity['SEX_WOMAN']]}
                  </label>
                  </div>
                  {/nocache}
                </div>
              </div>
                
              <div class="form-group">
                <label for="language_id" class="control-label col-md-2">{$labels['language']}: *</label>
                <div class="col-md-8">
                  {nocache}
                    {make_select_from_array 
                      data=$languages
                      selectedValues=($user->isNew())|ternary:$defaultLanguage->id:$user->language->id 
                      name='language' 
                      defaultText='--- vyberte '|cat:$labels['language']|cat:' ---' 
                      id='language' 
                      classes=['form-control']}
                  {/nocache}
                </div>
              </div>

              <div class="form-group">
                <label for="phone" class="control-label col-md-2">{$labels.phone}:</label>
                <div class="col-md-8">
                  <input class="form-control col-md-8 phonemask" id="phone" type="text" name="phone" value="{nocache}{$user->phone}{/nocache}" placeholder="(+420) 587 458 789" />
                </div>
              </div>

              <div class="form-group">
                <label for="email" class="control-label col-md-2">{$labels.email}: *</label>
                <div class="col-md-8">
                  <input class="form-control col-md-8 emailmask" id="email" type="text" name="email" value="{nocache}{$user->email}{/nocache}" placeholder="{$labels.email}" />
                </div>
              </div>

              <div class="form-group">
                <label class="control-label col-md-2">{$labels.groups}: *</label>
                <div class="col-md-8">
                  <div class="checkbox-list">
                  {nocache}
                    {$groupsRes = []}
                    {foreach from=$groups item='group'}
                      {if $ProSYS.CURRENT_LOGIN->user->hasRight($moduleName, 'change_group')}
                        <label class="checkbox-inline">
                          <input type="checkbox"
                           class="form-control"
                           name="groups[]" 
                           id="group_{$group->id}"  
                           value="{$group->id}"
                           {if in_array($group->id, $userGroups)}checked="checked"{/if} />
                           {$group->name}
                        </label>
                      {else}
                        <input type="hidden" name="groups[]" value="{$group->id}" />
                        {append var='groupsRes' value=$group->name}
                      {/if}
                    {/foreach}
                    {if $groupsRes}{', '|implode:$groupsRes}{/if}
                  {/nocache}
                  </div>
                </div>
              </div>

              <div class="form-group">  
                {nocache}
                  {if $user->isNew()}
                    <label for="create_login" class="control-label col-md-2">{$labels.create_login}:</label>
                    <div class="col-md-8">
                      <input type="hidden" name="create_login" value="0" />
                      <input id="create_login" type="checkbox" class="custom-bootstrap-switch" name="create_login" value="1"
                             data-on-text="{$translation|item:['text','yes']}" data-off-text="{$translation|item:['text','no']}" 
                             data-size="medium" data-off-color="danger" data-on-color="success"{if $post|item:'create_login'} checked="checked"{/if} />
                    </div>
                  {/if}
                {/nocache}
              </div>
            </div>

            <div class="col-md-6">
              {if $user->isNew()}
              <div class="login_data">
                <h3 class="form-section">Přihlašovací údaje uživatele</h3>
                <div class="form-group">
                  <label for="user_login" class="control-label col-md-2">{$labels.login.login}: *</label>
                  <div class="col-md-8">
                    <input class="form-control col-md-8" id="user_login" type="text" name="login" maxlength="32" value="{nocache}{$post|item:'login'}{/nocache}" placeholder="{$labels.login.login}" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="password" class="control-label col-md-2">{$labels.login.password}: *</label>
                  <div class="col-md-8">
                    <input class="form-control col-md-8" id="password" type="password" name="password" placeholder="{$labels.login.password}" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="repassword" class="control-label col-md-2">{$labels.login.repassword}: *</label>
                  <div class="col-md-8">
                    <input class="form-control col-md-8" id="repassword" type="password" name="repassword" placeholder="{$labels.login.repassword}" />
                  </div>
                </div>  
              </div>
              {/if}
            </div>
            <div class="clearfix"></div>

            <div class="mandatory">* Povinné položky</div>
          </div>

          <div class="form-actions">
            <div class="row">
              <div class="col-md-12">
                {nocache}
                  {if $ProSYS.CURRENT_LOGIN->user->hasRight($moduleName, 'initial')}
                    {button text='Uložit' icon='check'|icon theme='green' purpose='submit' title='Uložit a vrátit se na přehled'}
                  {/if}
                  {button text='Použít' icon='upload'|icon theme='blue' purpose='submit' name='apply' title='Uložit a změny'}
                  {$delete}

                  {link_external text='Zrušit' href=$ProSYS.SETTINGS.ROOT_ADMIN_URL|cat:'?module='|cat:$moduleName assign='link'}
                  {button text='Zrušit' icon='ban'|icon theme='yellow' title='Zrušit a vrátit se na přehled' classes='cancel' type='a' link=$link}
                {/nocache}
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>