{nocache}{handle_admin_messages}{/nocache}
<div class="row">
  <div class="col-md-12">
    <h3 class="page-title">{nocache}{$title}{/nocache}</h3>
    {nocache}
      {breadcrumb_add title=$title icons=[$moduleIcon,($user->isNew())|ternary:'plus':'pencil'|cat:' aditional'] link='?module='|cat:$moduleName|cat:'&activity=manage'|cat:(($user->isNew())|ternary:'':('&id='|cat:$user->id))}
      {breadcrumbs_get}
    {/nocache}
  </div>
</div>
<div class="row margin-top-20">
  <div class="col-md-12">
    <div class="profile-sidebar">
      <div class="portlet light profile-sidebar-portlet">
        <form class="profile-userpic" action="index.php" method="post" enctype="multipart/form-data">
          {input_hidden name="controller" value=$moduleName}
          {input_hidden name="action" value="saveAvatar"}
          
          <div class="fileinput fileinput-new" data-provides="fileinput">
            <div>
              <span class="btn-file"><input type="file" name="avatar" accept=".jpg" /></span>
            </div>
            <div class="thumbnail" data-trigger="fileinput">
              {nocache}<img src="{$user->getCurrentAvatar()->file->getFileURL()}" class="img-responsive" alt="obázek profilu uživatele {$user->getIdentification()}" />{/nocache}
            </div>
            <div id="cropper" class="fileinput-preview crop"></div>
          </div>
        </form>
        <div class="profile-usertitle">
          <div class="profile-usertitle-name">
            {nocache}{$user->getFullName()}{/nocache}
          </div>
          <div class="profile-usertitle-job">
            {nocache}
              {$userGroups = $user->getGroupsProjection('name')}
              {($userGroups)|ternary:(', '|implode:$userGroups):''}
            {/nocache}
          </div>
        </div>
      </div>
    </div>
    <div class="profile-content">
      <div class="row">
        <div class="col-md-12">
          <div class="portlet light">
            <div class="portlet-title tabbable-line">
              <div class="caption caption-md">
                <i class="icon-globe theme-font hide"></i>
                <span class="caption-subject font-blue-madison bold uppercase">Profil účtu</span>
              </div>
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#tab_1_1" data-toggle="tab" aria-expanded="true">{'cog'|icon} Uživatelské informace</a>
                </li>
                <li class="">
                  <a href="#tab_1_2" data-toggle="tab" aria-expanded="false">{'key'|icon} Změna hesla</a>
                </li>
                <li class="">
                  <a href="#tab_1_3" data-toggle="tab" aria-expanded="false">{'eye'|icon} Přizpůsobení</a>
                </li>
              </ul>
            </div>
            <div class="portlet-body">
              <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                  <form action="index.php" method="post" class="user_information col-md-8 col-lg-6" enctype="multipart/form-data">
                    <input type="hidden" name="controller" value="user" />
                    <input type="hidden" name="action" value="saveProfile" />
                    <input type="hidden" name="back_to" value="" />
                    <input type="hidden" name="login_id" value="{nocache}{$ProSYS.CURRENT_LOGIN->id}{/nocache}" />
                    
                    <div class="form-group">
                      <label class="control-label" for="first_name">{$labels.first_name}: </label>
                      <input id="first_name" class="form-control" name="first_name" type="text" value="{nocache}{$user->firstName}{/nocache}" placeholder="{$labels.first_name}" />
                    </div>
                    <div class="form-group">
                      <label class="control-label" for="last_name">{$labels.last_name}: </label>
                      <input id="last_name" class="form-control" name="last_name" type="text" value="{nocache}{$user->lastName}{/nocache}" placeholder="{$labels.last_name}" />
                    </div>
                    <div class="form-group">
                      <label class="control-label" for="phone">{$labels.phone}: </label>
                      <input id="phone" class="form-control phonemask" name="phone" type="text" value="{nocache}{$user->phone}{/nocache}" placeholder="{$labels.phone}" />
                    </div>
                    <div class="form-group">
                      <label class="control-label" for="email">{$labels.email}: </label>
                      <input id="email" class="form-control emailmask" name="email" type="text" value="{nocache}{$user->email}{/nocache}" placeholder="{$labels.email}" />
                    </div>
                    <div class="form-group">
                      <label class="control-label" for="email">{$labels.language}: </label>
                      {nocache}
                        {make_select_from_array 
                          data=$languages
                          selectedValues=($user->isNew())|ternary:$defaultLanguage->id:$user->language->id 
                          name='language' 
                          defaultText='--- vyberte '|cat:$labels['language']|cat:' ---' 
                          id='language' 
                          classes=['form-control']}
                      {/nocache}
                    </div>
                    <div class="form-group">
                      <label class="control-label" for="userGroup">{$labels.groups}: </label>
                      <div class="form-control-static bold">
                        {nocache}
                        {($userGroups)|ternary:(', '|implode:$userGroups):''}
                        {/nocache}
                      </div>
                    </div>
                    
                    <div class="margiv-top-10">
                      {nocache}
                        {button text='Použít' icon='check'|icon theme='green' purpose='submit'}
                        {button text='Zrušit' icon='ban'|icon theme='yellow' purpose='reset'}
                      {/nocache}
                    </div>
                  </form>
                </div>
                
                <div class="tab-pane" id="tab_1_2">
                  <form action="index.php" method="post" class="user_information col-md-8 col-lg-6">
                    <input type="hidden" name="controller" value="user" />
                    <input type="hidden" name="action" value="saveProfile" />
                    <input type="hidden" name="back_to" value="" />
                    <input type="hidden" name="login_id" value="{nocache}{$ProSYS.CURRENT_LOGIN->id}{/nocache}" />
                    
                    <div class="form-group">
                      <label class="control-label" for="old_password">{$labels.login.old_password}</label>
                      <input id="old_password" type="password" name="old_password" class="form-control" placeholder="{$labels.login.old_password}" />
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label" for="new_password">{$labels.login.new_password}</label>
                      <input id="new_password" type="password" name="password" class="form-control" placeholder="{$labels.login.new_password}" />
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label" for="repassword">{$labels.login.repassword}</label>
                      <input id="repassword" type="password" name="repassword" class="form-control" placeholder="{$labels.login.repassword}" />
                    </div>
                    
                    <div class="margiv-top-10">
                      {nocache}
                        {button text='Změnit heslo' icon='check'|icon theme='green' purpose='submit'}
                        {button text='Zrušit' icon='ban'|icon theme='yellow' purpose='reset'}
                      {/nocache}
                    </div>
                  </form>
                </div>
                
                <div class="tab-pane" id="tab_1_3">
                  <form action="index.php" method="post" class="user_information form-horizontal col-md-8 col-lg-6">
                    <input type="hidden" name="controller" value="user" />
                    <input type="hidden" name="action" value="saveProfile" />
                    <input type="hidden" name="back_to" value="" />
                    <input type="hidden" name="login_id" value="{nocache}{$ProSYS.CURRENT_LOGIN->id}{/nocache}" />
                    
                    <div class="form-group">
                      <label class="control-label col-md-3" for="theme">{$labels.default_page}: </label>
                      <div class="col-md-9">
                        {nocache}
                          {make_select_from_array
                            data=$allowed_pages
                            selectedValues=[$user->otherSettings|item:'default_page']
                            name='otherSettings[default_page]'
                            defaultText='--- vyberte `'|cat:$labels.default_page|cat:'` ---'
                            id='default_page'
                            classes=['form-control']}
                        {/nocache}
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-md-3" for="theme">{$labels.theme}: </label>
                      <div class="col-md-9">
                        {nocache}
                          {make_select_from_array
                            data=$themes
                            selectedValues=$user->theme
                            name='theme' 
                            defaultText='--- vyberte `'|cat:$labels.theme|cat:'` ---'
                            id='theme'
                            classes=['form-control']}
                        {/nocache}
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-md-3" for="countItemPerPage">{$labels.countItemPerPage}: </label>
                      <div class="col-md-9">
                        <input id="countItemPerPage" type="number" name="otherSettings[countItemPerPage]" 
                               value="{nocache}{$ProSYS.CURRENT_LOGIN->user->otherSettings|item:['countItemPerPage']|ternary:($ProSYS.CURRENT_LOGIN->user->otherSettings|item:['countItemPerPage']):$ProSYS.VARIABLES.SHOWING.ITEMS_PER_PAGE}{/nocache}" 
                               class="form-control col-md-9" placeholder="{$labels.countItemPerPage}" />
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-md-3" for="idleTime">{$labels.idleTime}: </label>
                      <div class="col-md-9">
                        <input id="idleTime" type="number" name="otherSettings[lock][idleTime]" 
                               value="{nocache}{$ProSYS.CURRENT_LOGIN->user->otherSettings|item:['lock', 'idleTime']|ternary:($ProSYS.CURRENT_LOGIN->user->otherSettings|item:['lock', 'idleTime']):$ProSYS.SETTINGS.LOCK_IDLE_TIME}{/nocache}" 
                               class="form-control col-md-9" placeholder="{$labels.idleTime}" />
                      </div>
                    </div>
                    
                    <h3 class="form-section">Nastavení hlášek systému</h3>
                    <div class="form-group">
                      <label class="control-label col-md-3" for="messagesSettings_horizontal">{$labels.messages.horizontal.title}: </label>
                      <div class="col-md-9">
                        {nocache}
                        {make_select_from_array
                          data=$labels.messages.horizontal.values
                          selectedValues=$user->otherSettings|item:['messages_settings', 'horizontal']
                          name='otherSettings[messages_settings][horizontal]' 
                          defaultText='--- vyberte `'|cat:$labels.messages.horizontal.title|cat:'` ---'
                          id='messagesSettings_horizontal'
                          classes=['form-control', 'col-md-9']}
                        {/nocache}
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="control-label col-md-3" for="messagesSettings_vertical">{$labels.messages.vertical.title}: </label>
                      <div class="col-md-9">
                        {nocache}
                        {make_select_from_array
                          data=$labels.messages.vertical.values
                          selectedValues=$user->otherSettings|item:['messages_settings', 'vertical']
                          name='otherSettings[messages_settings][vertical]' 
                          defaultText='--- vyberte `'|cat:$labels.messages.vertical.title|cat:'` ---'
                          id='messagesSettings_vertical'
                          classes=['form-control', 'col-md-9']}
                        {/nocache}
                      </div>
                    </div>
                      
                    <div class="form-group">
                      <label class="control-label col-md-3" for="messagesSettings_hideTime">{$labels.messages.hideTime}: </label>
                      <div class="col-md-9">
                        <input id="messagesSettings_hideTime" type="number" name="otherSettings[messages_settings][hideTime]" 
                               value="{nocache}{$user->otherSettings|item:['messages_settings', 'hideTime']}{/nocache}" 
                               class="form-control col-md-9" placeholder="{$labels.messages.hideTime}" min="2" max="15" />
                      </div>
                    </div>
                      
                    <h3 class="form-section">Automatické ukládání</h3>
                    <div class="form-group">
                      <label class="control-label col-md-3" for="autosave_enable">{$labels.autosave.enable}: </label>
                      <div class="col-md-9">
                        <input type="hidden" name="otherSettings[autosave][enable]" value="0"/>
                        <input class="basic-bootstrap-switch" 
                               id="autosave_enable" 
                               type="checkbox" 
                               name="otherSettings[autosave][enable]" 
                               value="1"
                               {nocache}{if $ProSYS.GLOBAL->loggedUser()->otherSettings|item:['autosave', 'enable']} checked="checked"{/if}{/nocache}/>
                      </div>
                    </div>
                      
                    <div class="form-group">
                      <label class="control-label col-md-3" for="autosave_time">{$labels.autosave.time}: </label>
                      <div class="col-md-9">
                        <input id="autosave_time" type="number" name="otherSettings[autosave][time]" 
                               value="{nocache}{$user->otherSettings|item:['autosave', 'time']:5}{/nocache}" 
                               class="form-control col-md-9" placeholder="{$labels.autosave.time}" min="2" max="15" />
                      </div>
                    </div>
                    
                    <div class="margiv-top-10">
                      {nocache}
                        {button text='Použít' icon='check'|icon theme='green' purpose='submit'}
                        {button text='Zrušit' icon='ban'|icon theme='yellow' purpose='reset'}
                      {/nocache}
                    </div>
                  </form>
                </div>
                <!-- END PRIVACY SETTINGS TAB -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END PROFILE CONTENT -->
  </div>
</div>