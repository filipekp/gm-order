{nocache}{handle_admin_messages}{/nocache}
<div class="row">
  <div class="col-md-12">
    <h3 class="page-title">{nocache}{$title}{/nocache}</h3>
    {nocache}
      {breadcrumb_add title=$title icons=[$moduleIcon] link='?module='|cat:$moduleName}
      {breadcrumbs_get}
    {/nocache}
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="portlet box green">
      <div class="portlet-title">
        <div class="caption">
          {link_external assign='link' text='Přidat uživatele' href=$ProSYS.SETTINGS.ROOT_ADMIN_URL|cat:'?module='|cat:$moduleName|cat:'&activity=manage'}
          {button text='Přidat uživatele' icon='plus'|icon type='a' link=$link theme='grey' classes='btn-xs' title='Přidat uživatele'}
        </div>
        <div class="tools">
          <small>celkem záznamů: {nocache}{$count}{/nocache}</small>
        </div>
      </div>
      <div class="portlet-body no-more-tables">
        {nocache}
          {input_text name="name" placeholder=$labels.first_name assign="filterName"}
          
          {table  id='users'
                  columns=[
                    ['', '', $moduleCallbacks.table.columns.avatar.value, ['avatar']],
                    [$labels.first_name, 'name', 'getFullName()'],
                    [$labels.login.title, '', $moduleCallbacks.table.columns.login_title.value],
                    [$labels.groups, '', $moduleCallbacks.table.columns.groups.value],
                    [$labels.contact, '', 'getContact()']
                  ]
                  sortable=['name']
                  filters=[['name', $filterName]]
                  actions=[
                    [
                      $moduleCallbacks.table.actions.manage.action,
                      $moduleCallbacks.table.actions.manage.title
                    ],
                    [
                      $moduleCallbacks.table.actions.delete.action,
                      $moduleCallbacks.table.actions.delete.title,
                      $moduleCallbacks.table.actions.delete.confirm
                    ]
                  ]
                  classes=['manage-table']
                  data=$data
                  query=$query
          }
          {if $count > $items_on_page}{$pagination}{/if}
        {/nocache}
      </div>
    </div>
  </div>
</div>