<?php
  namespace prosys\admin\controller;
  
  use prosys\core\common\Agents,
      prosys\core\common\AppException,
      prosys\model\LoginEntity,
      prosys\core\common\Functions,
      prosys\core\common\Settings,
      prosys\core\mapper\SqlFilter,
      prosys\core\Translator;

  /**
   * Processes the users requests.
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   * 
   * @property \prosys\admin\view\LoginView $_view PROTECTED property, this annotation is only because of Netbeans Autocompletion
   * @property \prosys\model\LoginDao $_dao PROTECTED property, this annotation is only because of Netbeans Autocompletion
   */
  class LoginController extends Controller
  { 
    /**
     * Initializes view and dao.
     */
    public function __construct() {
      parent::__construct();
      
      $this->_dao = Agents::getAgent('LoginDao', Agents::TYPE_MODEL);
      $this->_view = Agents::getAgent('LoginView', Agents::TYPE_VIEW_ADMIN);
    }
    
    /**
     * Check login uniqueness.
     * 
     * @param array $post
     * @param array $exceptions
     */
    private function checkLoginUniqueness(array $post, array &$exceptions) {
      if ($this->_dao->loadByLogin(Functions::item($post, 'login'))) {
        $exceptions['login'] = 'Přihlašovací jméno již existuje, zvolte prosím jiné.';
      }
    }
    
    /**
     * Check if password and repassword are equal.
     * 
     * @param array $post
     * @param array $exceptions
     */
    private function checkPasswordsEquality(array $post, array &$exceptions) {
      if (($password = Functions::item($post, 'password'))) {
        if ($password !== Functions::item($post, 'repassword')) {
          $exceptions['repassword'] = 'Heslo a ověření hesla musí být stejné.';
        }
      }
    }
    
    /**
     * Check password length.
     * 
     * @param array $post
     * @param array $exceptions
     */
    private function checkPasswordLength(array $post, array &$exceptions) {
      if (array_key_exists('password', $post)) {
        $length = mb_strlen($post['password'], 'UTF-8');
        
        $min = Settings::getVariable('PASSWORD', 'MIN_PASSWORD_LENGTH');
        $max = Settings::getVariable('PASSWORD', 'MAX_PASSWORD_LENGTH');
        if ($length < $min || $length > $max) {
          $exceptions['password'] = 'Heslo musí být dlouhé ' . $min . ' - ' . ' ' . Functions::inflection($max, 'znaků', 'znak', 'znaky', 'znaku') . '.';
        }
      }
    }
    
    /**
     * Check required entries.
     * 
     * @param array $post
     * @param array $exceptions
     */
    public function checkMandatories(array &$post, array &$exceptions) {
      $login = $this->_dao->load($post);
      if ($login->isNew()) {
        if (!Functions::item($post, 'login')) { $exceptions['login'] = 'Musíte zadat přihlašovací jméno.'; }
        $this->checkLoginUniqueness($post, $exceptions);
        
        if (!Functions::item($post, 'password')) { $exceptions['password'] = 'Musíte zadat heslo.'; }
        $this->checkPasswordsEquality($post, $exceptions);
        $this->checkPasswordLength($post, $exceptions);
      } else {
        if (Functions::item($post, 'password')) {
          if (strlen($post['password']) > 0) {
            $this->checkPasswordsEquality($post, $exceptions);
            $this->checkPasswordLength($post, $exceptions);
          } else {
            Functions::unsetItem($post, 'password');
          }
        }
      }
      
      Functions::unsetItem($post, 'repassword');
      
      if (!Functions::item($post, 'user')) { $exceptions['user'] = 'Musíte vybrat uživatele k loginu.'; }
    }
    
    /**
     * Ulozi prihlasovaci udaj do DB.
     * 
     * @param bool $reload
     * @param array $data
     * @throws AppException
     */
    public function save($reload = TRUE, $data = array()) {
      $post = (($data) ? $data : $this->_post);
      
      if (array_key_exists('delete', $post)) {
        header('Location: ' . Settings::ROOT_ADMIN_URL . '?controller=login&action=delete&id=' . $post['id']);
        exit();
      }
      
      // check form validity
      $exceptions = array();
      $this->checkMandatories($post, $exceptions);
      
      if ($exceptions) {
        throw new AppException($exceptions);
      } else {
        // cypher password
        if (($newPass = trim(Functions::item($post, 'password')))) {
          $post['password'] = $this->_dao->cypher($newPass);
        } else {
          Functions::unsetItem($post, 'password');
        }
        
        $login = $this->_dao->load($post);
        if ($this->_dao->store($login)) {
          $_SESSION[Settings::MESSAGE_SUCCESS][] = "Přihlašovací údaje pro login &bdquo;{$login->login}&ldquo; byly úspěšně uloženy.";
          
          if ($reload) {
            if (array_key_exists('apply', $post)) {
              $this->reload('manage', 'id=' . $login->id);
            } else {
              $this->reload();
            }
          }
        } else {
          throw new AppException("Přihlašovací údaje pro login &bdquo;{$login->login}&ldquo; se nepodařilo uložit.");
        }
      }
    }
    
    /**
     * Ulozi heslo z úpravy profilu.
     * 
     * @throws AppException
     * @return \prosys\model\LoginEntity
     */
    public function savePassword() {
      $post = $this->_post;
      $change = TRUE;
      
      // check form validity
      $exceptions = [];
      
      $loginOld = $this->_dao->load($post['login_id']);
      
      if (array_key_exists('password', $post) || array_key_exists('repassword', $post) || array_key_exists('old_password', $post)) {
        if (!$post['password'] && !$post['repassword'] && !$post['old_password']) {
          $change = FALSE;
        }
        
        if ($post['password'] == $post['repassword']) {
          if (array_key_exists('old_password', $post) && $this->_dao->cypher($post['old_password']) != $loginOld->password) {
            $exceptions[] = 'Původní heslo nebylo správně zadáno.';
          }
        } else {
          $exceptions[] = 'Nové heslo a kontrola nového hesla se musí shodovat.';
        }
      } else {
        $change = FALSE;
        $login = $loginOld;
      }
      
      if ($change) {
        if ($exceptions) {
          $_SESSION[Settings::MESSAGE_EXCEPTION] = $exceptions;
          throw new AppException('');
        } else {
          // cypher password
          if (array_key_exists('password', $post)) {          
            $post['password'] = $this->_dao->cypher($post['password']);
          }
          
          $post['id'] = Functions::unsetItem($post, 'login_id');

          $login = $this->_dao->load($post);
          if ($this->_dao->store($login)) {
            $_SESSION[Settings::MESSAGE_SUCCESS][] = 'Váše heslo bylo změněno.';
          } else {
            throw new AppException("Váše heslo se nepodařilo uložit.");
          }
        }
      }
      
      return $login;
    }
    
    /**
     * Delete user from the database.
     * 
     * @throws AppException
     */
    public function delete() {
      $id = (string)filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
      $login = $this->_dao->load($id);
      
      if ($login->isNew()) {
        throw new AppException("Přihlašovací údaje nebylo možné smazat.");
      } else {
        if ($this->_dao->delete($login)) {
          $this->_infoMessage = "Přihlašovací údaje s loginem &bdquo;{$login->login}&ldquo; byly úspěšně smazány.";
          $this->reload();
        } else {
          throw new AppException("Přihlašovací údaje pro login &bdquo;{$login->login}&ldquo; se nepodařilo smazat.");
        }
      }
    }
    
    /**
     * Log user in.
     */
    public function login() {
      // ulozeni posledni url
      $lastUri = $_SESSION[Settings::LAST_URI];
      $lastUriArr = [];

      parse_str($lastUri, $lastUriArr);
      
      UserController::cleanSession();

      /* @var $login \prosys\model\LoginEntity */
      $login = $this->_dao->doLogin(filter_input(INPUT_POST, 'login'),
                                   $this->_dao->cypher(filter_input(INPUT_POST, 'password')));
      
      if ($login) {
        if ($login->user) {
          // pridani do session aktualni login id
          $_SESSION[Settings::CURRENT_LOGIN_ID] = $login->id;
          self::ProSYS()->storeLogin($login);
          
          /* @var $userController UserController */
          $userController = Agents::getAgent('UserController', Agents::TYPE_CONTROLLER_ADMIN);
          $userController->loginUser($login->user);
          
          // vlozi do session informaci o nalogovani
          $_SESSION[Settings::JUST_LOGGED_IN] = TRUE;
        } else {
          Controller::redirect('', '', '', Translator::translate(['messages', 'not_allowed_login'], [$login->login]), Settings::ROOT_ADMIN_URL);
        }
        
        $module = Functions::unsetItem($lastUriArr, 'module');
        $activity = Functions::unsetItem($lastUriArr, 'activity');

        Controller::redirect($module, $activity, http_build_query($lastUriArr), Translator::translate(['messages', 'login'], [$login->getIdentification()]), Settings::ROOT_ADMIN_URL);
      } else {
        throw new AppException(Translator::translate(['messages', 'incorrect_credentials']));
      }
    }
    
    /**
     * Log user out.
     */
    public function logout() {
      /* @var $userController UserController */
      $userController = Agents::getAgent('UserController', Agents::TYPE_CONTROLLER_ADMIN);
      $userController->logout();
        
      $root = ((/*$user->isFrontendUser()*/ FALSE) ? Settings::ROOT_URL : Settings::ROOT_ADMIN_URL);
      Controller::redirect('', '', '', Translator::translate(['messages', 'logout']), $root);
    }
    
    /**
     * Zamce aktualne prihlaseny ucet
     */
    public function lockScreen() {
      $_SESSION[LoginEntity::LOCK_SCREEN] = TRUE;
      
      header('Content-Type: application/json');
      echo json_encode(['locked' => $_SESSION[LoginEntity::LOCK_SCREEN]]);
      exit();
    }
    
    /**
     * Funkce pro udržování sezení
     */
    public function All_PollingInterval() {
      header('Content-Type: application/json');
      echo ((Functions::item($_SESSION, LoginEntity::LOCK_SCREEN, FALSE)) ? 'OK' : 'NO_LOCK');
      exit();
    }
    
    /**
     * Odemcce aktualne prihlaseny ucet
     */
    public function unlockScreen() {
      $this->_post;
      
      $login = $this->_dao->doLogin(Functions::item($this->_post, 'login'),
                                   $this->_dao->cypher(Functions::item($this->_post, 'password')));
      
      $_SESSION[LoginEntity::LOCK_SCREEN] = is_null($login);
      
      $results = [
        'login' => !$_SESSION[LoginEntity::LOCK_SCREEN]
      ];
      
      header('Content-Type: application/json');
      echo json_encode($results);
      exit();
    }

    /**
     * @inherit
     */
    public function response($activity) {
      /* @var $userDao \prosys\model\UserDao */
      $userDao = Agents::getAgent('UserDao', Agents::TYPE_MODEL);
      
      switch ($activity) {
        case 'manage':
          $data = (($this->_post) ? $this->_post : $this->_get);
          $login = $this->_dao->load($data);
          
          $users = $userDao->loadAllUsers();
          
          $optional = array(
            'users' => $users,
            'get'   => $this->_get
          );
          $this->_view->manage($login, $optional);
        break;
      
        case 'initial':
        case 'table':
        default:
          // filtr
          $filter = SqlFilter::create()->identity();
          
          if (($filterLogin = Functions::item($this->_currentFilter, 'login'))) {
            $filter->andL(
              SqlFilter::create()->contains('login', $filterLogin)
            );
          }
          
          // vychozi razeni
          if (!$this->_currentSorting) {
            $this->_currentSorting = ['login' => 'asc'];
          }
          
          // uprava razeni do spravneho formatu
          $sorting = array_map(function($column){
            switch ($column) {
              default: $sortBy = $column;
            }
            
            return ['column' => $sortBy, 'direction' => $this->_currentSorting[$column]];
          }, array_keys($this->_currentSorting));
          
          
          // pagination
          $count = $this->_dao->count($filter);   // output variable
          // correction of current page
          $this->currentPageCorrection($count);
          $limitFrom = ($this->_currentPage - 1) * $this->_itemsOnPage;

          $logins = $this->_dao->loadRecords($filter, $sorting, [$limitFrom, $this->_itemsOnPage]);

          $optional = array(
            'get'               => $this->_get,
            'query'             => array_merge($this->_get, ['sorting' => $this->_currentSorting]),
            'count'             => $count,
            'items_on_page'     => $this->_itemsOnPage
          );
          
          $this->_view->table($logins, $optional);
        break;
      }
    }
  }

