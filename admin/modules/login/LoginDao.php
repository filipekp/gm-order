<?php
  namespace prosys\model;
  
  use prosys\core\mapper\SqlFilter,
      prosys\core\common\Functions;

  /**
   * Represents the user data access object.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class LoginDao extends MyDataAccessObject
  {
    public function __construct() {      
      parent::__construct('logins', LoginEntity::classname());
    }
    
    /**
     * Stahne nesmazaneho uzivatele podle zadaneho prihlasovaciho jmena.
     * 
     * @param string $login
     * @return UserEntity
     */
    public function loadByLogin($login) {
      /* @var $login LoginEntity */
      $login = Functions::first(
        $this->loadRecords(
          SqlFilter::create()->compare('login', '=', $login)
        )
      );

      return (($login) ? $login->user : NULL);
    }

    /**
     * Cyphers the password (generally the string).
     * 
     * @param string $string
     * @return string
     */
    public function cypher($string) {
      return sha1($string);
    }

    /**
     * Tries to log into the system. In the case of successfull login returns logged player.
     * 
     * @param string $login
     * @param string $password
     * 
     * @return \prosys\model\LoginEntity
     */
    public function doLogin($login, $password) {
      /* @var $logged \prosys\model\LoginEntity */
      $logged = Functions::first(
        $this->loadRecords(
          SqlFilter::create()->compare('login', '=', $login)
                             ->andL()
                             ->compare('password', '=', $password)
        )
      );
      
      return (($logged) ? $logged : NULL);
    }
  }