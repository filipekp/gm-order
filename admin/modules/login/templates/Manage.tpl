{nocache}{handle_admin_messages}{/nocache}
<div class="row">
  <div class="col-md-12">
    <h3 class="page-title">{$title}</h3>
    {nocache}
      {breadcrumb_add title=$title icons=[$moduleIcon,($login->isNew())|ternary:'plus':'pencil'|cat:' aditional'] link='?module='|cat:$moduleName|cat:'&activity=manage'|cat:(($login->isNew())|ternary:'':('&id='|cat:$login->id))}
      {breadcrumbs_get}
    {/nocache}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet box red">
      <div class="portlet-title">
        <div class="caption">
          {link_external assign='link' text='Zpět na seznam loginů' href=$ProSYS.SETTINGS.ROOT_ADMIN_URL|cat:'?module='|cat:$moduleName}
          {button text='Zpět na seznam loginů' icon=$moduleIcon|icon type='a' link=$link theme='grey' classes='btn-xs' title='Zpět na seznam loginů'}
        </div>
        <div class="tools"></div>
      </div>
      <div class="portlet-body form">
        <form id="login_manage_form" action="" class="form-horizontal" method="post">
          <input type="hidden" name="controller" value="{$moduleName}" />
          <input type="hidden" name="action" value="save" />
          <input type="hidden" name="back_to" value="{nocache}{$get.activity}{/nocache}" />
          {nocache}
          {if !$login->isNew()}
          <input type="hidden" name="id" value="{$login->id}" />
          {/if}
          {/nocache}

          <div class="form-body col-md-6">
            <div class="form-group">
              <label for="user_login" class="control-label col-md-3">{$labels['login']}: *</label>
              <div class="col-md-9">
                {nocache}
                {if $login->isNew()}
                <input class="form-control" id="user_login" type="text" name="login" maxlength="32" value="{$login->login}" placeholder="{$labels['login']}" autofocus />
                {else}
                <div class="form-control-static"><b>{$login->login}</b></div>
                {/if}
                {/nocache}
              </div>
            </div>

            <div class="form-group">
              <label for="password" class="control-label col-md-3">{$labels['password']}: {nocache}{($login->isNew())|ternary:'*':'**'}{/nocache}</label>
              <div class="col-md-9">
                <input class="form-control" id="password" type="password" name="password" placeholder="{$labels['password']}" />
              </div>
            </div>

            <div class="form-group">
              <label for="repassword" class="control-label col-md-3">{$labels['repassword']}: {nocache}{($login->isNew())|ternary:'*':'**'}{/nocache}</label>
              <div class="col-md-9">
                <input class="form-control" id="repassword" type="password" name="repassword" data-equalto="#password" placeholder="{$labels['repassword']}" />
              </div>
            </div>

            <div class="form-group">
              <label for="user_id" class="control-label col-md-3">{$labels['user']['title']}: *</label>
              <div class="col-md-9">
                {nocache}
                {make_select_from_entity_array 
                  data=$users 
                  valueGetter='id'
                  showGetter=$moduleCallbacks.select.userSelect.showGetter 
                  selectedValues=($login->user->isNew())|ternary:'0':$login->user->id 
                  name='user' 
                  defaultText='--- vyberte '|cat:$labels['user']['first_name']|cat:' ---' 
                  id='user' 
                  classes=['form-control']}
                {/nocache}
              </div>
            </div>

            <div class="mandatory">* Povinné položky{nocache}{if !$login->isNew()}<br />** Pokud nechcete měnit heslo nechte prázdné{/if}{/nocache}</div>
          </div>
          <div class="clearfix"></div>

          <div class="form-actions">
            <div class="row">
              <div class="col-md-12">
                {nocache}
                  {if $ProSYS.CURRENT_LOGIN->user->hasRight($moduleName, 'initial')}
                    {button text='Uložit' icon='check'|icon theme='green' purpose='submit' title='Uložit a vrátit se na přehled'}
                  {/if}
                  {button text='Použít' icon='upload'|icon theme='blue' purpose='submit' name='apply' title='Uložit a změny'}
                  {$delete}

                  {link_external text='Zrušit' href=$ProSYS.SETTINGS.ROOT_ADMIN_URL|cat:'?module='|cat:$moduleName assign='link'}
                  {button text='Zrušit' icon='ban'|icon theme='yellow' title='Zrušit a vrátit se na přehled' classes='cancel' type='a' link=$link}
                {/nocache}
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>