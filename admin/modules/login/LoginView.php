<?php
  namespace prosys\admin\view;
  use prosys\model\Entity,
      prosys\core\common\types\html\Icon,
      prosys\core\common\types\html\LinkActivity,
      prosys\core\common\types\html\LinkAction,
      prosys\core\common\types\html\Button,
      prosys\core\common\types\html\Theme;

  /**
   * Represents the admin user module view.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class LoginView extends BackendView
  {
    /**
     * Initializes the label of every user property.
     */
    public function __construct()
    {
      $this->_moduleName = 'login';
      $this->_moduleIcon = 'key';
      
      $labels = array(
        'title'             => 'Přihlašovací údaje',
        'login'             => 'Přihlašovací jméno',
        'old_password'      => 'Staré heslo',
        'new_password'      => 'Nové heslo',
        'password'          => 'Heslo',
        'repassword'        => 'Ověření hesla',
        'user'              => ['title' => 'Osoba', 'first_name' => 'Jméno']
      );
      
      $this->_moduleCallbacks = [
        'table'  => [
          'columns' => [
            'user_title' => [
              'value' => function($login) {
                return (($login->user) ? $login->user->getIdentification() : 'nemá osobu');
              }
            ]
          ],
              
          'actions' => [
            'manage' => [
              'action' => function($login) {
                $link = (new LinkActivity(new Icon('pencil'), $this->_moduleName, 'manage'))->setQuery(['id' => $login->id]); 
                if (!self::ProSYS()->loggedUser()->hasRight($this->_moduleName, 'manage')) {
                  $link->addClass('disabled');
                }
              
                return $link;
              },
              'title' => function($login) {
                return 'Upravit ' . $login->getIdentification();
              }
            ],

            'delete' => [
              'action' => function($login) {
                $link = (new LinkAction(new Icon('trash'), $this->_moduleName, 'delete'))->setQuery(['id' => $login->id]); 
                
                if (!self::ProSYS()->loggedUser()->hasRight($this->_moduleName, 'delete')) {
                  $link->addClass('disabled');
                }
              
                return $link;
              },
              'title' => function($login) {
                return 'Smazat login ' . $login->getIdentification();
              },
              'confirm' => function($login) {
                return 'Opravdu chcete login `' . $login->getIdentification() . '` smazat?';
              }
            ]
          ]
        ],
              
        'select' => [
          'userSelect' => [
            'showGetter' => function($user) {
              /* @var $user \prosys\model\UserEntity */
              return $user->getIdentification();
            }
          ]
        ]
      ];

      parent::__construct($labels);
    }
    
    /**
     * Zobrazi sablonu pro editaci loginu
     * 
     * @param \prosys\model\LoginEntity $login
     * @param array $optional associative array with optional data
     */
    public function manage(Entity $login, $optional = array()) {
      /* @var $login \prosys\model\LoginEntity */
      $assign = $optional + array('login' => $login);
      
      if ($login->isNew()) {
        $heading = 'Nový přihlašovací údaj';
        $assign['delete'] = '';        
      } else {
        $heading = 'Úprava loginu &bdquo;' . $login->login . '&ldquo;';
        if (self::ProSYS()->loggedUser()->hasRight($this->_moduleName, 'delete')) {
          $assign['delete'] = (new Button((new Icon('trash')) . 'Smazat', Theme::get(Theme::RED), Button::TYPE_LINK))
                                ->setLink((new LinkAction('', $this->_moduleName, 'delete'))->setQuery(['id' => $login->id]))
                                ->addClass(['red', 'delete'])
                                ->title('Smazat')
                                ->attribute('onclick', 'return confirm(\'Opravdu chcete login smazat?\');');
        } else {
          $assign['delete'] = '';
        }
      }
      
      $templateOnly = ((array_key_exists('template_only', $optional)) ? $optional['template_only'] : FALSE);

      $this->printActivity('Manage', $heading, $assign, $templateOnly);
    }
    
    /**
     * Prints out the table of users.
     * 
     * @param \prosys\model\LoginEntity[] $data
     * @param array $optional
     */
    public function table($data = array(), $optional = array()) {
      $assign = $optional + array(
        'data' => $data,
        'totalcount' => $optional['count'],
        'pagination' => BackendView::generatePaging($optional['count'], $optional['get'], $optional['items_on_page'])
      );
      
      $templateOnly = ((array_key_exists('template_only', $optional)) ? $optional['template_only'] : FALSE);

      $this->printActivity('Table', 'Seznam přihlašovacích údajů', $assign, $templateOnly);
    }
  }
