<?php
  namespace prosys\model;

  /**
   * Entita pro login.
   * 
   * @property int $id
   * @property string $login
   * @property string $password
   * @property UserEntity $user element=user_id
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class LoginEntity extends Entity
  {
    const LOCK_SCREEN = 'lock_screen';
    
    public function __construct() {
      $this->loggedHistory = TRUE;
      
      parent::__construct();
    }
    
    /**
     * Returns identification of current user's login.
     * @return string
     */
    public function getIdentification() {
      return $this->login . ' (' . $this->user->getIdentification() . ')';
    }
  }
