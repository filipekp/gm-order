<?php
  namespace prosys\model;

  /**
   * Represents the entity of group.
   * 
   * @property int $id
   * @property string $name
   * 
   * @property GroupRightEntity[] $rights binding=1n:>id_group:>
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class GroupEntity extends Entity
  {
    public function __construct() {
      $this->loggedHistory = TRUE;
      
      parent::__construct();
    }
    
    public function getIdentification() {
      return $this->name;
    }
  }
