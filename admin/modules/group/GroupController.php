<?php
  namespace prosys\admin\controller;
  
  use prosys\core\common\Agents,
      prosys\core\common\AppException,
      prosys\core\common\Functions,
      prosys\core\common\Settings,
      prosys\core\mapper\SqlFilter;

  /**
   * Processes the users requests.
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   * 
   * @property \prosys\admin\view\GroupView $_view PROTECTED property, this annotation is only because of Netbeans Autocompletion
   * @property \prosys\model\GroupDao $_dao PROTECTED property, this annotation is only because of Netbeans Autocompletion
   */
  class GroupController extends Controller
  {
    /**
     * Initializes view and dao.
     */
    public function __construct() {
      parent::__construct();
      
      $this->_dao = Agents::getAgent('GroupDao', Agents::TYPE_MODEL);
      $this->_view = Agents::getAgent('GroupView', Agents::TYPE_VIEW_ADMIN);
    }
    
    /**
     * Zkontroluje povinna pole.
     * 
     * @param array $post
     * @param array $exceptions
     */
    protected function checkMandatories(array $post, array &$exceptions) {
      if (!$post['name']) { $exceptions[] = 'Musíte zadat jméno skupiny.'; }
    }
    
    /**
     * Ulozi informace skupiny z formulare.
     * 
     * @throws AppException
     */
    public function save($reload = TRUE) {
      if (array_key_exists('delete', $this->_post)) {
        header('Location: ' . Settings::ROOT_ADMIN_URL . '?controller=group&action=delete&id=' . $this->_post['id']);
        exit();
      }

      // check form validity
      $exceptions = array();
      $this->checkMandatories($this->_post, $exceptions);

      if ($exceptions) {
        throw new AppException($exceptions);
      } else {        
        $group = $this->_dao->load($this->_post);
        if ($this->_dao->store($group)) {
          $this->_dao->assignGroupGroupRights($group, (array)Functions::item($this->_post, 'group_rights'));

          $this->_infoMessage = "Skupina &bdquo;{$group->name}&ldquo; byla úspěšně uložena.";

          if ($reload) {
            if (array_key_exists('apply', $this->_post)) {
              $this->reload('manage', 'id=' . $group->id);
            } else {
              $this->reload();
            }
          }
        } else {
          throw new AppException("Skupinu &bdquo;{$group->name}&ldquo; se nepodařilo uložit.");
        }
      }
    }
    
    /**
     * Delete group from the database.
     * 
     * @throws AppException
     */
    public function delete() {
      $groupId = Functions::item($this->_get, 'id', NULL);
      $group = $this->_dao->load($groupId);
      
      if ($group->isNew()) {
        throw new AppException("Skupinu nebylo možné smazat, protože neexistuje id &bdquo;#{$group->id}&ldquo;.");
      } else {
        if ($this->_dao->delete($group)) {
          $this->_infoMessage = "Skupina &bdquo;{$group->name}&ldquo; byla úspěšně smazána.";
          $this->reload();
        } else {
          throw new AppException("Skupinu &bdquo;{$group->name}&ldquo; se nepodařilo smazat.");
        }
      }
    }

    /**
     * @inherit
     */
    public function response($activity) {
      switch ($activity) {
        case 'manage':          
          $id = Functions::item($this->_get, 'id', NULL);
          $group = $this->_dao->load($id);

          $groupRights = array_combine(
            array_map(function($item) {
              /* @var $item \prosys\model\GroupRightEntity */
              return $item->action->id;
            }, $group->rights->getLoadedArrayCopy()),
            array_map(function($item) {
              /* @var $item \prosys\model\GroupRightEntity */
              return $item->isUncheckable;
            }, $group->rights->getLoadedArrayCopy())
          );
          
          /* @var $moduleActionDao \prosys\model\ModuleActionDao */
          $moduleActionDao = Agents::getAgent('ModuleActionDao', Agents::TYPE_MODEL);
          $modulesWithAction = $moduleActionDao->getModulesWithActions();
          
          $optional = array(
            'modulesWithAction' => $modulesWithAction,
            'groupRights'       => $groupRights
          );
          $this->_view->manage($group, $optional);
        break;

        case 'initial':
        case 'table':
        default:
          // filtr
          $filter = SqlFilter::create()->identity();
          
          if (($filterLogin = Functions::item($this->_currentFilter, 'name'))) {
            $filter->andL(
              SqlFilter::create()->contains('name', $filterLogin)
            );
          }
          
          // vychozi razeni
          if (!$this->_currentSorting) {
            $this->_currentSorting = ['name' => 'asc'];
          }
          
          // uprava razeni do spravneho formatu
          $sorting = array_map(function($column){
            switch ($column) {
              default: $sortBy = $column;
            }
            
            return ['column' => $sortBy, 'direction' => $this->_currentSorting[$column]];
          }, array_keys($this->_currentSorting));
          
          // pagination
          $count = $this->_dao->count($filter);   // output variable
          // correction of current page
          $this->currentPageCorrection($count);
          $limitFrom = ($this->_currentPage - 1) * $this->_itemsOnPage;

          $groups = $this->_dao->loadRecords($filter, $sorting, [$limitFrom, $this->_itemsOnPage]);

          $optional = array(
            'get'               => $this->_get,
            'query'             => array_merge($this->_get, ['sorting' => $this->_currentSorting]),
            'count'             => $count,
            'items_on_page'     => $this->_itemsOnPage
          );
          
          $this->_view->table($groups, $optional);
        break;
      }
    }
  }

