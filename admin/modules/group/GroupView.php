<?php
  namespace prosys\admin\view;
  use prosys\model\Entity,
      prosys\model\ModuleActionEntity,
      prosys\core\common\types\html\Icon,
      prosys\core\common\types\html\LinkActivity,
      prosys\core\common\types\html\LinkAction,
      prosys\core\common\types\html\Button,
      prosys\core\common\types\html\Theme;

  /**
   * Represents the admin group module view.
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class GroupView extends BackendView
  {
    /**
     * Initializes the label of every group property.
     */
    public function __construct()
    {
      $this->_moduleName = 'group';
      $this->_moduleIcon = 'group';
      
      $labels = array(
        'id'      => 'ID',
        'name'    => 'Jméno skupiny',
        'rights'  => 'Práva skupiny'
      );
      
      $this->_moduleCallbacks = [
        'table'  => [
          'actions' => [
            'manage' => [
              'action' => function($group) {
                $link = (new LinkActivity(new Icon('pencil'), $this->_moduleName, 'manage'))->setQuery(['id' => $group->id]); 
                if (!self::ProSYS()->loggedUser()->hasRight($this->_moduleName, 'manage')) {
                  $link->addClass('disabled');
                }
              
                return $link;
              },
              'title' => function($group) {
                return 'Upravit skupinu ' . $group->getIdentification();
              }
            ],

            'delete' => [
              'action' => function($group) {
                $link = (new LinkAction(new Icon('trash'), $this->_moduleName, 'delete'))->setQuery(['id' => $group->id]); 
                
                if (!self::ProSYS()->loggedUser()->hasRight($this->_moduleName, 'delete')) {
                  $link->addClass('disabled');
                }
              
                return $link;
              },
              'title' => function($group) {
                return 'Smazat skupinu ' . $group->getIdentification();
              },
              'confirm' => function($group) {
                return 'Opravdu chcete skupinu `' . $group->getIdentification() . '` smazat?';
              }
            ]
          ]
        ]
      ];

      parent::__construct($labels);
    }
    
    /**
     * Vytvori rekurzivne checkboxy pro nastaveni prav skupiny
     * 
     * @param array $data
     * @param array $groupRights
     * 
     * @return string
     */
    private static function generateGroupRights($data, $groupRights) {
      $html = '';
      foreach ($data as $moduleActionData) {
        /* @var $moduleAction ModuleActionEntity */
        $moduleAction = $moduleActionData['item'];

        $disabled = array_key_exists($moduleAction->id, $groupRights) && !$groupRights[$moduleAction->id];
        ob_start(); 
        ?>
        <li>
          <?php echo (($disabled) ? '<input type="hidden" name="group_rights[]" value="' . $moduleAction->id . '" />' : ''); ?>
          <label for="group_right_<?php echo $moduleAction->id; ?>">
            <input type="checkbox"
                 name="group_rights[]" 
                 id="group_right_<?php echo $moduleAction->id; ?>"
                 data-id="<?php echo $moduleAction->id; ?>"
                 data-parent="<?php echo (($moduleAction->parent) ? $moduleAction->parent->id : '0'); ?>"
                 value="<?php echo $moduleAction->id; ?>"
                 <?php echo (($disabled) ? '' : ' data-group="' . $moduleAction->module->module . '"'); ?>
                 <?php echo ((array_key_exists($moduleAction->id, $groupRights)) ? ' checked="checked"' : ''); ?>
                 <?php echo (($disabled) ? ' disabled="disabled"' : ''); ?>/>
            <?php echo $moduleAction->title; ?> 
            <span class="activity_tag" style="background-color: <?php echo ModuleActionEntity::$TYPES[$moduleAction->type]['bgColor']; ?>; color: <?php echo ModuleActionEntity::$TYPES[$moduleAction->type]['color']; ?>;">
              <?php echo ModuleActionEntity::$TYPES[$moduleAction->type]['name']; ?>
            </span>
          </label>
        <?php
        if (array_key_exists('children', $moduleActionData)) {
          echo '<ul>';
          echo self::generateGroupRights($moduleActionData['children'], $groupRights);
          echo '</ul>';
        }
        ?>
        </li>
        <?php
        $html .= ob_get_clean();
      }
      
      return $html;
    }
    
    /**
     * Vytvori skupiny prav dle modulu
     * 
     * @param \prosys\model\ModuleActionEntity[] $data
     * @return string
     */
    public static function getHtmlGroupRights($data) {
      $html = '';
      foreach ($data['modulesWithAction'] as $moduleId => $module) {
        $html .= '<div class="module_name check_all" data-group-id="' . $moduleId . '">' . $module['name'] . '</div>';
        $html .= '<ul>';
          $html .= self::generateGroupRights($module['items'], $data['groupRights']);
        $html .= '</ul>';
      }
      
      return $html;
    }
    
    /**
     * Prints out manage form to managing the group.
     * 
     * @param \prosys\model\GroupEntity $group
     * @param array $optional associative array with optional data
     */
    public function manage(Entity $group, $optional = array()) {
      /* @var $group \prosys\model\GroupEntity */
      $assign = $optional + [
        'group' => $group,
        'rightCheckboxes' => self::getHtmlGroupRights([
          'modulesWithAction' => $optional['modulesWithAction'],
          'groupRights' => $optional['groupRights']
        ])
      ];
      
      if ($group->isNew()) {
        $heading = 'Nová skupina';
        $assign['delete'] = '';
        
      } else {
        $heading = 'Úprava skupiny &bdquo;' . $group->name . '&ldquo;';
        if (self::ProSYS()->loggedUser()->hasRight($this->_moduleName, 'delete')) {
          $assign['delete'] = (new Button((new Icon('trash')) . 'Smazat', Theme::get(Theme::RED), Button::TYPE_LINK))
                                ->setLink((new LinkAction('', $this->_moduleName, 'delete'))->setQuery(['id' => $group->id]))
                                ->addClass(['red', 'delete'])
                                ->title('Smazat')
                                ->attribute('onclick', 'return confirm(\'Opravdu chcete skupinu smazat?\');');
        }
      }
      
      $templateOnly = ((array_key_exists('template_only', $optional)) ? $optional['template_only'] : FALSE);

      $this->printActivity('Manage', $heading, $assign, $templateOnly);
    }
    
    /**
     * Prints out the table of groups.
     * 
     * @param \prosys\model\GroupEntity[] $data
     * @param array $optional
     */
    public function table($data = array(), $optional = array()) {
      $assign = $optional + array(
        'data' => $data,
        'totalcount' => $optional['count'],
        'pagination' => BackendView::generatePaging($optional['count'], $optional['get'], $optional['items_on_page'])
      );
      
      $templateOnly = ((array_key_exists('template_only', $optional)) ? $optional['template_only'] : FALSE);

      $this->printActivity('Table', 'Přehled skupin', $assign, $templateOnly);
    }
  }
