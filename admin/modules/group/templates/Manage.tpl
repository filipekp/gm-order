{nocache}{handle_admin_messages}{/nocache}
<div class="row">
  <div class="col-md-12">
    <h3 class="page-title">{nocache}{$title}{/nocache}</h3>
    {nocache}
      {breadcrumb_add title=$title icons=[$moduleIcon,($group->isNew())|ternary:'plus':'pencil'|cat:' aditional'] link='?module='|cat:$moduleName|cat:'&activity=manage'|cat:(($group->isNew())|ternary:'':('&id='|cat:$group->id))}
      {breadcrumbs_get}
    {/nocache}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet box red">
      <div class="portlet-title">
        <div class="caption">
          {link_external assign='link' text='Zpět na seznam skupin' href=$ProSYS.SETTINGS.ROOT_ADMIN_URL|cat:'?module='|cat:$moduleName}
          {button text='Zpět na seznam skupin' icon=$moduleIcon|icon type='a' link=$link theme='grey' classes='btn-xs' title='Zpět na seznam skupin'}
        </div>
        <div class="tools"></div>
      </div>
      <div class="portlet-body form rights">
        <form action="" class="form-horizontal" method="post">
          <input type="hidden" name="controller" value="{$moduleName}" />
          <input type="hidden" name="action" value="save" />
          {nocache}
            {if !$group->isNew()}
            <input type="hidden" name="id" value="{$group->id}" />
            {/if}
          {/nocache}
          
          <div class="form-body clearfix">
            <div class="form-group">
              <label for="name" class="control-label col-md-2">{$labels['name']}: *</label>
              <div class="col-md-4">
                <input class="form-control" id="name" type="text" name="name" value="{nocache}{$group->name}{/nocache}" placeholder="{$labels['name']}" />
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">{$labels['rights']}: *</label>
              <div class="col-md-8">
                {nocache}
                {$rightCheckboxes}
                {/nocache}
              </div>
            </div>

            <div class="mandatory">* Povinné položky</div>
          </div>

          <div class="form-actions">
            <div class="row">
              <div class="col-md-12">
                {nocache}
                  {if $ProSYS.CURRENT_LOGIN->user->hasRight($moduleName, 'initial')}
                    {button text='Uložit' icon='check'|icon theme='green' purpose='submit' title='Uložit a vrátit se na přehled'}
                  {/if}
                  {button text='Použít' icon='upload'|icon theme='blue' purpose='submit' name='apply' title='Uložit a změny'}
                  {$delete}

                  {link_external text='Zrušit' href=$ProSYS.SETTINGS.ROOT_ADMIN_URL|cat:'?module='|cat:$moduleName assign='link'}
                  {button text='Zrušit' icon='ban'|icon theme='yellow' title='Zrušit a vrátit se na přehled' classes='cancel' type='a' link=$link}
                {/nocache}
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>        
</div>