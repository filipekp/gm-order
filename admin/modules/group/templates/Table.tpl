{nocache}{handle_admin_messages}{/nocache}
<div class="row">
  <div class="col-md-12">
    <h3 class="page-title">{nocache}{$title}{/nocache}</h3>
    {nocache}
      {breadcrumb_add title=$title icons=[$moduleIcon] link='?module='|cat:$moduleName}
      {breadcrumbs_get}
    {/nocache}
  </div>
</div>
  
<div class="row">
  <div class="col-md-12">
    <div class="portlet box green">
      <div class="portlet-title">
        <div class="caption">
          {link_external assign='link' text='Přidat skupinu' href=$ProSYS.SETTINGS.ROOT_ADMIN_URL|cat:'?module='|cat:$moduleName|cat:'&activity=manage'}
          {button text='Přidat skupinu' icon='plus'|icon type='a' link=$link theme='grey' classes='btn-xs' title='Přidat skupinu'}
        </div>
        <div class="tools">
          <small>celkem záznamů: {nocache}{$count}{/nocache}</small>
        </div>
      </div>
      <div class="portlet-body  no-more-tables">
        {nocache}          
          {table  id='logins'
                  columns=[
                    [$labels.id, 'id', 'id'],
                    [$labels.name, 'name', 'name']
                  ]
                  sortable=['name']
                  actions=[
                    [
                      $moduleCallbacks.table.actions.manage.action,
                      $moduleCallbacks.table.actions.manage.title
                    ],
                    [
                      $moduleCallbacks.table.actions.delete.action,
                      $moduleCallbacks.table.actions.delete.title,
                      $moduleCallbacks.table.actions.delete.confirm
                    ]
                  ]
                  classes=['manage-table']
                  data=$data
                  query=$query
          }
          {if $count > $items_on_page}{$pagination}{/if}
        {/nocache}
      </div>
    </div>
  </div>
</div>