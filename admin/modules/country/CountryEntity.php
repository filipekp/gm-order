<?php
  namespace prosys\model;

  /**
   * Reprezentuje entitu zeme.
   * 
   * @property int $id
   * @property string $name
   * @property string $isoCode2 element=iso_code_2
   * @property string $isoCode3 element=iso_code_3
   * @property string $addressFormat element=address_format
   * @property bool $isZipcodeRequired element=zipcode_required
   * @property bool $disabled
   * 
   * @property CurrencyEntity[] $currencies binding=mn:country_currencies:>:>:>
   * 
   * @author Jan Svěží
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class CountryEntity extends Entity
  {
  }
