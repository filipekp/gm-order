<?php
  namespace prosys\model;
  
  /**
   * Reprezentuje DAO zeme.
   * 
   * @author Jan Svěží
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class CountryDao extends MyDataAccessObject
  {
    public function __construct() {
      parent::__construct('countries', CountryEntity::classname());
    }
  }
