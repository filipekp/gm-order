<?php
  namespace prosys\model;
  
  use prosys\core\common\AppException,
      prosys\core\common\Agents,
      prosys\core\common\Settings,
      prosys\core\common\types\File,
      prosys\core\common\types\Image;

  /**
   * Reprezentuje entitu souboru.
   * 
   * @property int $id
   * @property FileBindingStorageEntity $fileBindingStorage element=file_binding_storage_id
   * @property string $extension
   * @property string $mime
   * @property int $size
   * @property \DateTime $modification
   * @property string $originalName element=original_name
   * @property int $downloads
   * @property int $sortOrder element=sort_order
   * 
   * @property Desc_FileEntity[] $descriptions binding=1n:>:language_id>asc
   * 
   * @property \prosys\core\common\types\File $file noelement
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class FileEntity extends Entity
  {
    private $_file = NULL;
    
    /**
     * Getter.
     * 
     * @return type
     * @throws AppException jestlize file neni nacteny
     */
    public function getFile() {
      if (is_null($this->_file)) {
        // dle mime rozhodne o jaky typ souboru se jedna a vytvori jej
        $this->_file = new Image($this->getFilepath(), $this->originalName);
      }
      
      return $this->_file;
    }
    
    /**
     * Vrati jmeno ulozeneho souboru.
     * @return string
     */
    protected function getFilename() {
      return $this->id . '.' . $this->extension;
    }
    
    /**
     * Vrati cestu k adresari ulozeneho souboru.
     * @return string
     */
    protected function getPath() {
      /* @var $dao \prosys\model\FileDao */
      $dao = Agents::getAgent('FileDao', Agents::TYPE_MODEL);
      return Settings::ROOT_PATH . Settings::getVariable('GLOBAL', 'STORAGE_PATH') . 
             (($this->fileBindingStorage->isNew()) ?
                Settings::STORAGE_GLOBAL_DIR : 
                Settings::STORAGE_MODULES_DIR . $this->fileBindingStorage->transferObject->module->module . '/' . $this->fileBindingStorage->transferObject->name . '/' . 
                  $dao->getBinder($this) . '/' . $this->fileBindingStorage->name . '/');
    }
    
    /**
     * Vrati cestu k ulozenemu souboru.
     * @return string
     */
    protected function getFilepath() {
      return $this->getPath() . $this->getFilename();
    }
  }
