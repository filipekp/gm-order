<?php
  namespace prosys\model;
  
  /**
   * Reprezentuje DAO popisku souboru.
   * 
   * @author Jan Svěží
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class Desc_FileDao extends MyDataAccessObject
  {
    public function __construct() {
      parent::__construct('file_descriptions', Desc_FileEntity::classname());
    }
  }
