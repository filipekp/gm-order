<?php
  namespace prosys\model;
  
  use prosys\core\mapper\MySqlMapper,
      prosys\core\common\Agents;

  /**
   * Reprezentuje DAO k entite souboru.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class FileDao extends MyDataAccessObject
  {    
    public function __construct() {      
      parent::__construct('files', FileEntity::classname());
    }
    
    /**
     * Vrati objekt, na ktereho je navazany dany obrazek.
     * 
     * @param \prosys\model\FileEntity $file
     * @return mixed
     */
    public function getBinder(FileEntity $file) {
      if ($file->isNew()) {
        return NULL;
      }
      
      // mapper pro vazebni tabulku
      $mapper = new MySqlMapper(Agents::getAgentByTag('MySQL'), $file->fileBindingStorage->name, $file->fileBindingStorage->fileKey);
      
      // zjisti sloupec primarniho klice dane entity
      $reflection = new \ReflectionClass(Agents::getNamespace(Agents::TYPE_MODEL) . $file->fileBindingStorage->transferObject->name . 'Entity');
      $primary = $reflection->getMethod('PRIMARY_KEY')->invoke(NULL, TRUE);
      
      return $mapper->find($file->id)->$primary;
    }
  }
