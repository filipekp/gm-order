<?php
  namespace prosys\model;

  /**
   * Reprezentuje entitu popisu souboru.
   * 
   * @property int $id
   * @property FileEntity $file element=file_id
   * @property LanguageEntity $language element=language_id
   * @property string $name
   * @property string $description
   * 
   * @author Jan Svěží
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class Desc_FileEntity extends Entity
  {
    
  }
