<?php
  namespace prosys\model;

  /**
   * Represents the entity of avatar.
   * 
   * @property int $id
   * @property string $modul
   * @property string $rowPrimary element=row_primary
   * @property string $old element=row_old
   * @property string $new element=row_new
   * @property \prosys\core\common\types\JSON $changedProperties element=changed_properties
   * @property string $user
   * @property \DateTime $date
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class HistoryEntity extends Entity
  {
  }