<?php
  namespace prosys\model;
  
  use prosys\core\common\Agents,
      prosys\core\common\Settings,
      prosys\core\common\types\JSON,
      prosys\core\common\Functions;

  /**
   * Representuje dao pro historii zmen.
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class HistoryDao extends MyDataAccessObject
  {
    const HISTORY_ARRAY = 'history_array';
    
    public function __construct() {
      parent::__construct('history_changes', HistoryEntity::classname());
    }
    
    /**
     * Funkce udrzuje v poli puvodni entitu.
     * 
     * @param \prosys\model\Entity $oldEntity
     */
    public static function saveChange(Entity $oldEntity) {
      if (!array_key_exists(self::HISTORY_ARRAY, $_SESSION)) {
        $_SESSION[self::HISTORY_ARRAY] = [];
      }
      
      /* @var $loginDao LoginDao */
      $loginDao = Agents::getAgent('LoginDao', Agents::TYPE_MODEL);
      
      /* @var $currentLogin LoginEntity */
      $currentLogin = $loginDao->load($_SESSION[Settings::CURRENT_LOGIN_ID]);
      
      $idEntity = $oldEntity->{$oldEntity->PRIMARY_KEY(TRUE)};
      $moduleName = lcfirst(str_replace('Entity', '', $oldEntity->classname(FALSE)));
      
      if (!array_key_exists($moduleName, $_SESSION[self::HISTORY_ARRAY])) {
        $_SESSION[self::HISTORY_ARRAY][$moduleName] = [
          'entities'    => []
        ];
      }
      
      if (!array_key_exists($idEntity, $_SESSION[self::HISTORY_ARRAY][$moduleName]['entities'])) {
        $history = [
          'rowPrimary'  => $idEntity,
          'old'         => (($oldEntity->isNew()) ? json_encode(array('isNew' => TRUE), JSON_PRETTY_PRINT) : $oldEntity->serializeEntity()),
          'modul'       => $moduleName,
          'user'        => $currentLogin->getIdentification()
        ];
        
        $_SESSION[self::HISTORY_ARRAY][$moduleName]['entities'][$idEntity] = $history;
      }
    }
    
    /**
     * Funkce projde pole zmen a ulozi vsechny zaznamy do tybulky historie.
     */
    public function storeAllChanges() {
      if (array_key_exists(self::HISTORY_ARRAY, $_SESSION)) {
        foreach ($_SESSION[self::HISTORY_ARRAY] as $moduleName => $moduleData) {
          $entityDao = Agents::getAgent(ucfirst($moduleName) . 'Dao', Agents::TYPE_MODEL);

          foreach ($moduleData['entities'] as $entityId => $historyData) {
            /* @var $history HistoryEntity */
            $history = $this->load($historyData);
              
            $changeProperties = array();
            /* @var $entity Entity */
            $entity = $entityDao->load($entityId);
            if ($entity->isNew()) {
              $serializeEntity = new JSON(array('isDelete' => TRUE));
            } else {
              $serializeEntity = $entity->serializeEntity();
            }
            
            $arrayNewEntity = (array)json_decode($serializeEntity, TRUE);
            $arrayOldEntity = (array)json_decode($history->old, TRUE);
            if (!(bool)Functions::item($arrayOldEntity, 'isNew') && !(bool)Functions::item($arrayNewEntity, 'isDelete')) {
              foreach ($arrayOldEntity as $property => $value) {
                if (Functions::item($arrayNewEntity, $property) !== $value) {
                  $changeProperties[] = $property;
                }
              }
            }
            
            $history->new = $serializeEntity;
            $history->changedProperties = $changeProperties;
            $history->date = new \DateTime();
            
            if ($changeProperties || (bool)Functions::item($arrayOldEntity, 'isNew') || (bool)Functions::item($arrayNewEntity, 'isDelete')) {
              $this->store($history);
            }
          }
        }
        
        // vymazani pole historie v session
        unset($_SESSION[self::HISTORY_ARRAY]);
      }
    }
  }
