{nocache}{handle_admin_messages}{/nocache}
<div class="row">
  <div class="col-md-12">
    <h3 class="page-title">{$title}</h3>
    {nocache}
      {breadcrumb_add title=$title icons=[$moduleIcon,($setting->isNew())|ternary:'plus':'pencil'|cat:' aditional'] link='?module='|cat:$moduleName|cat:'&activity=manage'|cat:(($setting->isNew())|ternary:'':('&id='|cat:$setting->id))}
      {breadcrumbs_get}
    {/nocache}
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="portlet box red">
      <div class="portlet-title">
        <div class="caption">
          {link_external assign='link' text='Zpět na seznam proměnných' href=$ProSYS.SETTINGS.ROOT_ADMIN_URL|cat:'?module='|cat:$moduleName}
          {button text='Zpět na seznam proměnných' icon=$moduleIcon|icon type='a' link=$link theme='grey' classes='btn-xs' title='Zpět na seznam proměnných'}
        </div>
        <div class="tools"></div>
      </div>
      <div class="portlet-body form">
        <form id="manage-setting" action="" class="form-horizontal" method="post">
          <input type="hidden" name="controller" value="{$moduleName}" />
          <input type="hidden" name="action" value="save" />
          <input type="hidden" name="back_to" value="manage" />
          {nocache}
          {if !$setting->isNew()}
          <input type="hidden" name="id" value="{$setting->id}" />
          {/if}
          {/nocache}
          
          <div class="form-body clearfix">
            <div class="col-md-6">
              <h3 class="form-section">Nastavení proměnné</h3>
              <div class="form-group">
                <label for="variable_type" class="control-label col-md-3">{$labels['variable_type']}: *</label>
                <div class="col-md-9">
                  {nocache}
                  {if $setting->isNew() || $ProSYS.CURRENT_LOGIN->user->hasRight($moduleName, 'change_type')}
                  {make_select_from_entity_array 
                    data=$variableTypes 
                    valueGetter=$moduleCallbacks.select.variableTypes.valueGetter
                    showGetter=$moduleCallbacks.select.variableTypes.showGetter
                    selectedValues=$setting->variableType 
                    name='variable_type' 
                    defaultText='--- vyberte '|cat:$labels['variable_type']|cat:' ---' 
                    id='id_variable_type' 
                    classes=['form-control', 'input-medium']}
                  {else}
                  <input type="hidden" name="variable_type" value="{$setting->variableType}" />
                  <div class="form-control-static bold">{$SettingEntity.variableTypes[$setting->variableType]['option']['title']}</div>
                  {/if}
                  {/nocache}
                </div>
              </div>

              <div class="form-group">
                <label for="group" class="control-label col-md-3">{$labels['group']}: *</label>
                <div class="col-md-9">
                  {nocache}
                  {if $setting->isNew()}
                  <input class="form-control" id="group" type="text" name="group" value="{$setting->group}" placeholder="{$labels['group']}" />
                  {else}
                  <input type="hidden" name="group" value="{$setting->group}" />
                  <div class="form-control-static bold">{$setting->group}</div>
                  {/if}
                  {/nocache}
                </div>
              </div>

              <div class="form-group">
                <label for="name" class="control-label col-md-3">{$labels['name']}: *</label>
                <div class="col-md-9">
                  {nocache}
                  {if $setting->isNew()}
                  <input class="form-control" id="name" type="text" name="name" value="{$setting->name}" placeholder="{$labels['name']}" />
                  {else}
                  <input type="hidden" name="name" value="{$setting->name}" />
                  <div class="form-control-static bold">{$setting->name}</div>
                  {/if}
                  {/nocache}
                </div>
              </div>

              <div class="form-group">
                <label for="value_type" class="control-label col-md-3">{$labels['value_type']}: *</label>
                <div class="col-md-9">
                  {nocache}
                  {if $setting->isNew()}
                  <input class="form-control" id="value_type" type="text" name="value_type" value="string" placeholder="{$labels['value_type']}" />
                  {else}
                  <input type="hidden" name="value_type" value="{$setting->type}" />
                  <div class="form-control-static bold">{$setting->type}</div>
                  {/if}
                  {/nocache}
                </div>
              </div>

              <div class="form-group">
                <label for="value_id" class="control-label col-md-3">{$labels['value']}: *</label>
                <div class="col-md-9 value_wrapper">
                  {nocache}
                    {$settingValue = ($setting->type == 'boolean')|ternary:(($setting->value)|ternary:' checked="checked"':''):$setting->value|escape:'html'}
                    {eval var=$SettingEntity.types[(($setting->isNew())|ternary:'string':$setting->type)]['element']}
                  {/nocache}
                </div>
              </div>
            </div>
                
            <div class="col-md-6">
              <h3 class="form-section">Popis proměnné</h3>
              <div class="form-group">
                {nocache}
                {if $setting->isNew()}
                  <textarea class="form-control tinymce" id="help" name="help">{$setting->help}</textarea>
                {else}
                  <div class="form-control-static help">{if $setting->help}{eval var=$setting->help}{else}<i>Není žádný popis.</i>{/if}</div>
                {/if}
                {/nocache}
              </div>
            </div>
              
            <div class="clearfix"></div>
            <div class="mandatory">* Povinné položky</div>
          </div>

          <div class="form-actions">
            <div class="row">
              <div class="col-md-12">
                {nocache}
                  {if $ProSYS.CURRENT_LOGIN->user->hasRight($moduleName, 'initial')}
                    {button text='Uložit' icon='check'|icon theme='green' purpose='submit' title='Uložit a vrátit se na přehled'}
                  {/if}
                  {button text='Použít' icon='upload'|icon theme='blue' purpose='submit' name='apply' title='Uložit a změny'}
                  {$delete}

                  {link_external text='Zrušit' href=$ProSYS.SETTINGS.ROOT_ADMIN_URL|cat:'?module='|cat:$moduleName assign='link'}
                  {button text='Zrušit' icon='ban'|icon theme='yellow' title='Zrušit a vrátit se na přehled' classes='cancel' type='a' link=$link}
                {/nocache}
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>