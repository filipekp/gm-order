{nocache}{handle_admin_messages}{/nocache}
<div class="row">
  <div class="col-md-12">
    <h3 class="page-title">{nocache}{$title}{/nocache}</h3>
    {nocache}
      {breadcrumb_add title=$title icons=[$moduleIcon] link='?module='|cat:$moduleName}
      {breadcrumbs_get}
    {/nocache}
  </div>
</div>
  
<div class="row">
  <div class="col-md-12">
    <div class="portlet box green">
      <div class="portlet-title">
        <div class="caption">
          {link_external assign='link' text='Přidat proměnnou' href=$ProSYS.SETTINGS.ROOT_ADMIN_URL|cat:'?module='|cat:$moduleName|cat:'&activity=manage'}
          {button text='Přidat proměnnou' icon='plus'|icon type='a' link=$link theme='grey' classes='btn-xs' title='Přidat proměnnou'}
        </div>
        <div class="tools">
          <small>celkem záznamů: {nocache}{$count}{/nocache}</small>
          <small>{link_external icon='question-circle'|icon href='#module_help' data=['toggle' => 'modal'] title='nápověda k modulu'}</small>
        </div>
      </div>
        
      <div id="module_help" class="modal fade" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
              <h4 class="modal-title">Nápověda modulu proměnných</h4>
            </div>
            
            <div class="modal-body">
              Pro použití jakékoli proměnné použijte následující kód:
              <pre>Settings::getVariable('nazev_skupiny', 'nazev_promenne');</pre>
            </div>
            
            <div class="modal-footer">
              <button type="button" data-dismiss="modal" class="btn">Zavřít</button>
            </div>
          </div>
        </div>
      </div>
        
      <div class="portlet-body  no-more-tables">
        {nocache}
          {input_text name="group_tag" placeholder=$labels.group assign="filterGroup"}
          {input_text name="name" placeholder=$labels.name assign="filterName"}
          {input_text name="value_type" placeholder=$labels.value_type assign="filterValueType"}
          
          {table  id='settings'
                  columns=[
                    [$labels.group, 'group_tag', 'group'],
                    [$labels.name, 'name', 'name'],
                    [$labels.value_type, 'value_type', 'type'],
                    [$labels.value, 'value', $moduleCallbacks.table.columns.value.value]
                  ]
                  sortable=['group_tag', 'name', 'value_type']
                  filters=[
                    ['group_tag', $filterGroup],
                    ['name', $filterName],
                    ['value_type', $filterValueType]
                  ]
                  actions=[
                    [
                      $moduleCallbacks.table.actions.manage.action,
                      $moduleCallbacks.table.actions.manage.title
                    ],
                    [
                      $moduleCallbacks.table.actions.delete.action,
                      $moduleCallbacks.table.actions.delete.title,
                      $moduleCallbacks.table.actions.delete.confirm
                    ]
                  ]
                  classes=['manage-table']
                  data=$data
                  query=$query
          }
          {if $count > $items_on_page}{$pagination}{/if}
        {/nocache}
      </div>
    </div>
  </div>
</div>