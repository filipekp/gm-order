<?php
  namespace prosys\admin\view;
  use prosys\model\Entity,
      prosys\core\common\types\html\Icon,
      prosys\core\common\types\html\LinkActivity,
      prosys\core\common\types\html\LinkAction,
      prosys\core\common\Functions,
      prosys\core\common\types\html\Button,
      prosys\core\common\types\html\Theme;

  /**
   * Represents the setting module view.
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class SettingView extends BackendView
  {
    /**
     * Initializes the label of every user property.
     */
    public function __construct()
    {
      // nastavuje ikonu modulu
      $this->_moduleIcon = 'cogs';
      
      $labels = array(
        'id'              => 'ID',
        'variable_type'   => 'Typ proměnné',
        'group'           => 'Skupina',
        'name'            => 'Jméno proměnné',
        'value_type'      => 'Datový typ hodnoty',
        'value'           => 'Hodnota proměnné'
      );
      
      /* @var $setting \prosys\model\SettingEntity */
      $this->_moduleCallbacks = [
        'table'  => [
          'columns' => [
            'value' => [
              'value' => function ($setting) {
                /* @var $setting \prosys\model\SettingEntity */
                $iconDesc = NULL;
                if ($setting->help) {
                  $iconDesc = (new Icon('question-circle'))->title('Popis promenné ' . $setting->getIdentification())
                                                           ->addClass(['fright', 'popovers']);
                  
                  $smarty = self::$_SMARTY;
                  $smarty->assign('setting', $setting);
                  
                  $data = [
                    'container' => 'body',
                    'html' => TRUE,
                    'content' => htmlentities($smarty->fetch('string: ' . $setting->help)),
                    'placement' => 'left',
                    'original-title' => '',
                    'trigger' => 'hover'
                  ];
                  foreach ($data as $name => $value) {
                    $iconDesc->data($name, $value);
                  }
                }
                
                return htmlentities(Functions::trimEntireWords($setting->value, 120)) . 
                       (($iconDesc) ? ' ' . $iconDesc : '');
              }
            ]
          ],
          'actions' => [
            'manage' => [
              'action' => function($setting) {
                $link = (new LinkActivity(new Icon('pencil'), $this->_moduleName, 'manage'))->setQuery(['id' => $setting->id]); 
                if (!self::ProSYS()->loggedUser()->hasRight($this->_moduleName, 'manage') || !$setting->canBeEditCurrentUser(self::ProSYS()->loggedUser())) {
                  $link->addClass('disabled');
                }
              
                return $link;
              },
              'title' => function($setting) {
                return 'Upravit ' . $setting->getIdentification();
              }
            ],

            'delete' => [
              'action' => function($setting) {
                $link = (new LinkAction(new Icon('trash'), $this->_moduleName, 'delete'))->setQuery(['id' => $setting->id]); 
                
                if (!self::ProSYS()->loggedUser()->hasRight($this->_moduleName, 'delete') || !$setting->canBeErased) {
                  $link->addClass('disabled');
                }
              
                return $link;
              },
              'title' => function($setting) {
                return 'Smazat proměnnou ' . $setting->getIdentification();
              },
              'confirm' => function($setting) {
                return 'Opravdu chcete proměnnou `' . $setting->getIdentification() . '` smazat?';
              }
            ]
          ]
        ],
              
        'select' => [
          'variableTypes' => [
            'valueGetter' => function($item, $key) { return $key; },
            'showGetter' => function($item, $key) { return $item['title']; }
          ]
        ]
      ];
          
      parent::__construct($labels);
    }
    
    /**
     * Prints out manage form to managing the user.
     * 
     * @param \prosys\model\SettingEntity $setting
     * @param array $optional associative array with optional data
     */
    public function manage(Entity $setting, $optional = array()) {
      /* @var $setting \prosys\model\SettingEntity */
      $assign = $optional + array('setting' => $setting);
      
      if ($setting->isNew()) {
        $heading = 'Nová proměnná';
        $assign['delete'] = '';
        
      } else {
        $heading = 'Úprava proměnné &bdquo;' . $setting->name . '&ldquo;';
        if (self::ProSYS()->loggedUser()->hasRight($this->_moduleName, 'delete') && $setting->canBeErased) {
          $assign['delete'] = (new Button((new Icon('trash')) . 'Smazat', Theme::get(Theme::RED), Button::TYPE_LINK))
                                ->setLink((new LinkAction('', $this->_moduleName, 'delete'))->setQuery(['id' => $setting->id]))
                                ->addClass(['red', 'delete'])
                                ->title('Smazat')
                                ->attribute('onclick', 'return confirm(\'Opravdu chcete proměnnou smazat?\');');
        } else {
          $assign['delete'] = '';
        }
      }
      
      $templateOnly = ((array_key_exists('template_only', $optional)) ? $optional['template_only'] : FALSE);

      $this->printActivity('Manage', $heading, $assign, $templateOnly);
    }
    
    /**
     * Prints out the table of users.
     * 
     * @param \prosys\model\SettingEntity[] $data
     * @param array $optional
     */
    public function table($data = array(), $optional = array()) {
      $assign = $optional + array(
        'data' => $data,
        'pagination' => BackendView::generatePaging($optional['count'], $optional['get'], $optional['items_on_page'])
      );
      
      $templateOnly = ((array_key_exists('template_only', $optional)) ? $optional['template_only'] : FALSE);

      $this->printActivity('Table', 'Přehled proměnných', $assign, $templateOnly);
    }
  }
