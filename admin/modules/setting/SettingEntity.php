<?php
  namespace prosys\model;

  /**
   * Represents the entity of setting.
   * 
   * @property int $id
   * @property string $variableType element=variable_type
   * @property string $group element=group_tag
   * @property string $name
   * @property string $type element=value_type
   * @property string $value
   * @property int $canBeErased element=can_be_erased
   * @property string $help
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class SettingEntity extends Entity
  { 
    public static $_variableTypes = array(
      'system'  => [
        'option'  => [
          'title' => 'Systémová proměnná'
        ]
      ],
      'global'  => [
        'option'  => [
          'title' => 'Globální proměnná'
        ]
      ],
      'user'  => [
        'option'  => [
          'title' => 'Uživatelská proměnná'
        ]
      ]
    );
    
    public static $_types = array(
      'datetime'      => [
        'desc'      => 'datum a čas',
        'element'   => '
          <div class="input-append date form_datetime">
            <input id="value" size="16" type="text" name="value" value="{$settingValue}" readonly class="form-control" />
						<span class="add-on"><i class="icon-calendar"></i></span>
          </div>'
      ],
      'integer'      => [
        'desc'      => 'celé číslo',
        'element'   => '<input class="form-control" id="value" type="number" name="value" value="{$settingValue}" placeholder="Hodnota proměnné" />'
      ],
      'float'      => [
        'desc'      => 'desetinné číslo',
        'element'   => '<input class="form-control" id="value" type="text" name="value" value="{$settingValue}" placeholder="Hodnota proměnné" />'
      ],
      'boolean'      => [
        'desc'      => 'hodnota ano/ne',
        'element'   => '<input type="hidden" name="value" value="0"/><input class="basic-bootstrap-switch" id="value" type="checkbox" name="value" value="1"{$settingValue}/>'
      ],
      'string'      => [
        'desc'      => 'řetězec',
        'element'   => '<input class="form-control" type="text" id="value" name="value" value="{$settingValue}"/>'
      ],
      'text'      => [
        'desc'      => 'dlouhý řetězec',
        'element'   => '<textarea class="form-control tinymce" id="value" name="value">{$settingValue}</textarea>'
      ]
    );
    
    public function __construct() {
      $this->loggedHistory = TRUE;
      
      parent::__construct();
    }
    
    /**
     * Funkce pro kontrolu práv na editaci proměnné pro aktuálně přihlášeného uživatele.
     * 
     * @param UserEntity $loggedUser
     * @return boolean
     */
    public function canBeEditCurrentUser(UserEntity $loggedUser) {
      $allowed = [];
      foreach (self::$_variableTypes as $type => $typeData) {
        if ($loggedUser->hasRight(lcfirst(str_replace('Entity', '', $this->classname(FALSE))), 'type_' . $type . '_manage')) {
          $allowed[$type] = TRUE;
        }
      }
      
      return $this->isNew() || in_array($this->variableType, array_keys($allowed));
    }
    
    /**
     * Vrátí identifikaci proměnné.
     * @return string
     */
    public function getIdentification() {
      return $this->group . ' | ' . $this->name;
    }
  }
