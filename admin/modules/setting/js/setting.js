function loadTypes() {
  var $typeInput = $('input#value_type');
  
  autocompleteSetting($typeInput, {
    source: function(request, response) { 
      tinyMCE.triggerSave();
      var value = $('div.value_wrapper #value').val();
      
      var data = {
        controller: 'Setting',
        action: 'loadTypes',
        user_string: request.term,
        value: value
      };
      $.post('index.php', data, function(data) {
        response(data);
      }, 'json').fail(function(error) { debug.log(error); });
    },
    select: function(event, $ui) {
      $('#value').rules('remove', 'checkValidate');
      
      $('div.value_wrapper').html($ui.item.element);
      
      if ($ui.item.label === 'boolean') {
        FormComponents.handleBootstrapSwitch();
      } else if ($ui.item.label === 'text') {
        pluginTinymce.reInit('#value');
      } else if ($ui.item.label === 'datetime') {
        FormComponents.handlejQueryUIDatePickers()
        $('#value').colorSetter($('#value').closest('.portlet'));
      }
      
      $('#value').rules('add', { checkValidate: true });
      $('#value').valid();
      $('#value').closest('form').valid();
    }
  });
}

function loadGroups() {
  var $groupInput = $('input#group');
  
  autocompleteSetting($groupInput, {
    source: function(request, response) {      
      var data = {
        controller: 'Setting',
        action: 'loadGroups',
        user_string: request.term
      };
      $.get('index.php', data, function(data) {
        response(data);
      }, 'json').fail(function(error) { debug.log(error); });
    }
  });
}

$(document).ready(function() {
  loadTypes();
  loadGroups();
  
  $('form#manage-setting').customValidator().enablePHPValidate('setting', 'validate');
});