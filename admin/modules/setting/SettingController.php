<?php
  namespace prosys\admin\controller;
  
  use prosys\core\common\Agents,
      prosys\core\common\AppException,
      prosys\core\mapper\SqlFilter,
      prosys\core\common\Functions,
      prosys\core\common\Settings,
      prosys\model\SettingEntity;

  /**
   * Processes the setting requests.
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   * 
   * @property \prosys\admin\view\SettingView $_view PROTECTED property, this annotation is only because of Netbeans Autocompletion
   * @property \prosys\model\SettingDao $_dao PROTECTED property, this annotation is only because of Netbeans Autocompletion
   */
  class SettingController extends Controller
  {
    /**
     * Initializes view and dao.
     */
    public function __construct() {
      parent::__construct();
      
      $this->_dao = Agents::getAgent('SettingDao', Agents::TYPE_MODEL);
      $this->_view = Agents::getAgent('SettingView', Agents::TYPE_VIEW_ADMIN);
    }
    
    /**
     * Kontrola povinných polí.
     * 
     * @param array $data
     * @param array $exceptions
     */
    protected function checkMandatories(array $data, array &$exceptions) {
      if (!Functions::item($data, 'name')) { $exceptions['name'] = 'Nebyl zadán název proměnné. Prosím vyplňte jej.'; }
      if (!Functions::item($data, 'group')) { $exceptions['group'] = 'Nebyla zadána skupina proměnné. Prosím zadejte ji.'; }
      if (!Functions::item($data, 'variable_type')) { $exceptions['variable_type'] = 'Nebyl zadán typ proměnné. Prosím zadejte jej.'; }
      if (!Functions::item($data, 'value_type')) { $exceptions['value_type'] = 'Nebyl zadán typ hodnoty. Prosím zadejte jej.'; }
      if (!Functions::item($data, 'value') && strlen(Functions::item($data, 'value')) <= 0) { $exceptions['value'] = 'Nebyla zadána hodnota proměnné. Prosím zadejte ji.'; }
    }
    
    /**
     * Ulozi promennou do databaze.
     * 
     * @throws AppException
     */
    public function save($reload = TRUE) {
      if (array_key_exists('delete', $this->_post)) {
        header('Location: ' . Settings::ROOT_ADMIN_URL . '?controller=setting&action=delete&id=' . $this->_post['id']);
        exit();
      }
      
      // check form validity
      $exceptions = array();
      $this->checkMandatories($this->_post, $exceptions);
      
      if ($exceptions) {
        throw new AppException($exceptions);
      } else {
        
        $setting = $this->_dao->load($this->_post);
        $setting->canBeErased = (($setting->isNew()) ? 1 : $setting->canBeErased);
        if ($this->_dao->store($setting)) {
          $_SESSION[Settings::MESSAGE_SUCCESS] = "Proměnná &bdquo;{$setting->name}&ldquo; byla úspěšně uložena.";          
          
          
          if ($reload) {
            if (array_key_exists('apply', $this->_post)) {
              switch ($this->_post['back_to']) {
                default:
                  $this->reload($this->_post['back_to'], 'id=' . $setting->id);
                break;
              }
            } else {
              $this->reload();
            }
          }
        } else {
          throw new AppException("Proměnnou &bdquo;{$setting->name}&ldquo; se nepodařilo uložit.");
        }
      }
    }
    
    /**
     * Vymaze promennou z databaze.
     * 
     * @throws AppException
     */
    public function delete() {
      $id = (string)filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
      $setting = $this->_dao->load($id);
      
      if ($setting->isNew()) {
        throw new AppException("Proměnnou nebylo možné smazat. Proměnná s id &bdquo;{$id}&ldquo; neexistuje.");
      } else {
        if ($this->_dao->delete($setting)) {
          $this->_infoMessage = "Proměnná &bdquo;{$setting->name}&ldquo; byla úspěšně smazána.";
          $this->reload();
        } else {
          throw new AppException("Proměnnou &bdquo;{$setting->name}&ldquo; se nepodařilo smazat.");
        }
      }
    }

    /**
     * @inherit
     */
    public function response($activity) {
      switch ($activity) {        
        case 'manage':
          $id = ((array_key_exists('id', $this->_get)) ? $this->_get['id'] : NULL);
          $setting = $this->_dao->load((($this->_post) ? $this->_post : $id));
          /* @var $setting SettingEntity */
          
          if (!$setting->canBeEditCurrentUser(self::ProSYS()->loggedUser())) {
            $_SESSION[Settings::MESSAGE_EXCEPTION][] = 'Nemáte právo editovat proměnnou typu: ' . SettingEntity::$_variableTypes[$setting->variableType]['option']['title'];
            $this->reload();
          }
          
          $variableTypes = [];
          foreach (SettingEntity::$_variableTypes as $type => $typeData) {
            if (self::ProSYS()->loggedUser()->hasRight(lcfirst(str_replace('Controller', '', $this->_className)), 'type_' . $type . '_manage')) {
              $variableTypes[$type] = $typeData;
            }
          }
          
          $optional = array(
            'variableTypes' => $variableTypes,
            'SettingEntity' => [
              'variableTypes' => SettingEntity::$_variableTypes,
              'types'         => SettingEntity::$_types
            ]
          );
          $this->_view->manage($setting, $optional);
        break;
      
        case 'initial':
        case 'table':
        default:          
          // filtr
          $filter = SqlFilter::create()->identity();
          
          if (($filterGroup = Functions::item($this->_currentFilter, 'group_tag'))) {
            $filter->andL(
              SqlFilter::create()->contains('group_tag', $filterGroup)
            );
          }
          
          if (($filterName = Functions::item($this->_currentFilter, 'name'))) {
            $filter->andL(
              SqlFilter::create()->contains('name', $filterName)
            );
          }
          
          if (($filterValueType = Functions::item($this->_currentFilter, 'value_type'))) {
            $filter->andL(
              SqlFilter::create()->contains('value_type', $filterValueType)
            );
          }
          
          // zjisti povolene typy proměnných a přidá k filtru
          $allowed = [];
          foreach (SettingEntity::$_variableTypes as $type => $typeData) {
            if (self::ProSYS()->loggedUser()->hasRight(lcfirst(str_replace('Controller', '', $this->_className)), 'type_' . $type)) {
              $allowed[] = $type;
            }
          }
          
          if ($allowed) {
            $filter->andL()->inArray('variable_type', $allowed);
          }
          
          // vychozi razeni
          if (!$this->_currentSorting) {
            $this->_currentSorting = ['group_tag' => 'asc', 'name' => 'asc'];
          }
          
          // uprava razeni do spravneho formatu
          $sorting = array_map(function($column){
            switch ($column) {
              default: $sortBy = $column;
            }
            
            return ['column' => $sortBy, 'direction' => $this->_currentSorting[$column]];
          }, array_keys($this->_currentSorting));
          
          // pagination
          $count = $this->_dao->count($filter);
          // correction of current page
          $this->currentPageCorrection($count);
          
          $limitFrom = ($this->_currentPage - 1) * $this->_itemsOnPage;
          $settings = $this->_dao->loadRecords($filter, $sorting,[$limitFrom, $this->_itemsOnPage]);

          $optional = array(
            'get'               => $this->_get,
            'query'             => array_merge($this->_get, ['sorting' => $this->_currentSorting]),
            'count'             => $count,
            'items_on_page'     => $this->_itemsOnPage,
            'template_only'     => Functions::item($this->_get, 'template_only', FALSE)
          );
          
          $this->_view->table($settings, $optional);
        break;
      }
    }
    
    /**
     * Nacita a vraci pomoci ajaxu typy hodnot.
     */
    public function loadTypes() {
      $string = (string)Functions::item($this->_post, 'user_string');
      $value = (string)Functions::item($this->_post, 'value');
      $smarty = $this->_view->getSmarty();
      
      $types = SettingEntity::$_types;
      array_walk($types, function(&$item, $key) {
        $item['key'] = $key;
      });
      
      $results = array_filter($types, function($item) use ($string) {
        return mb_strpos($item['desc'], $string) !== FALSE || strpos($item['key'], $string) !== FALSE;
      });
      
      $smarty->assign('settingValue', $value);
      array_walk($results, function(&$item, $key) use ($smarty) {
        $item = array(
          'label'   => $key,
          'desc'    => $item['desc'],
          'element' => $smarty->fetch('string:' . $item['element'])
        );
      });
      
      header('Content-Type: application/json');
      echo json_encode($results);
      exit();
    }
    
     /**
     * Nacita a vraci pomoci ajaxu skupiny.
     */
    public function loadGroups() {
      $string = filter_input(INPUT_GET, 'user_string');
      $results = $this->_dao->distinct('group_tag', SqlFilter::create()->contains('group_tag', $string));
      
      array_walk($results, function(&$item) {
        $item = array(
          'label' => $item->group_tag,
          'desc'  => ''
        );
      });
      
      header('Content-Type: application/json');
      echo json_encode($results);
      exit();
    }
  }

