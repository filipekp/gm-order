<?php
  namespace prosys\model;
  
  use prosys\core\mapper\SqlFilter,
      prosys\core\common\Functions;

  /**
   * Represents the setting data access object.
   * 
   * @author Pavel Filípek
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class SettingDao extends MyDataAccessObject
  {    
    public function __construct() {      
      parent::__construct('settings', SettingEntity::classname());
    }
    
    /**
     * Nacte hodnotu promenne podle skupiny a nazvu promenne
     * 
     * @param string $group
     * @param string $name
     * 
     * @return SettingEntity
     */
    public function loadByGroupAndName($group, $name) {
      $filter = SqlFilter::create()
        ->compare('group_tag', '=', $group)
        ->andL()->compare('name', '=', $name);
      
      /* @var $setting SettingEntity */
      $setting = Functions::first($this->loadRecords($filter));
      if ($setting) {
        $setting->value = Functions::retype($setting->value, $setting->type);
        return $setting;
      } else {
        return NULL;
      }
    }
    
    /**
     * Nacte hodnoty promennych podle skupiny
     * 
     * @param string $group
     * 
     * @return mixed
     */
    public function loadByGroup($group) {
      $filter = SqlFilter::create()
        ->compare('group_tag', '=', $group);
      
      /* @var $settings SettingEntity[] */
      $settings = $this->loadRecords($filter);
      
      $results = array();
      if ($settings) {
        array_walk($settings, function(&$item) use (&$results) {          
          $results[$item->name] = Functions::retype($item->value, $item->type);
        });
        
        return $results;
      } else {
        return NULL;
      }
    }
    
    /**
     * Nacte hodnoty promennych
     * 
     * @return mixed
     */
    public function loadAll() {      
      /* @var $settings SettingEntity[] */
      $settings = $this->loadRecords();
      
      $results = array();
      if ($settings) {
        array_walk($settings, function(&$item) use (&$results) {
          if (!array_key_exists($item->group, $results)) { $results[$item->group] = array(); }
          $results[$item->group][$item->name] = Functions::retype($item->value, $item->type);
        });
        
        return $results;
      } else {
        return [];
      }
    }
    
    /**
     * Nastavi ochranu promenne proti vymazani.
     * 
     * @param string $group
     * @param string $name
     */
    public function setCanBeErased($group, $name) {
      $setting = $this->loadByGroupAndName($group, $name);
      if ($setting && $setting->canBeErased) {
        $setting->canBeErased = 0;
        $this->store($setting);
      }
    }
  }
