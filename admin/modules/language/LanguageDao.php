<?php
  namespace prosys\model;
  use prosys\core\mapper\SqlFilter,
      prosys\core\common\Functions;
  /**
   * Reprezentuje DAO pro jazyk
   * 
   * @author Pavel Filípek
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class LanguageDao extends MyDataAccessObject
  {
    public function __construct() {
      parent::__construct('languages',  LanguageEntity::classname());
    }
    
    /**
     * Nacte vychozi jazyk z databaze
     * 
     * @return LanguageEntity
     */
    public function loadDefaultLanguage() {
      return Functions::first($this->loadRecords(
        SqlFilter::create()
          ->compare('is_default', '=', '1')));
    }
    
    /**
     * Nacte jazyky z databaze
     * 
     * @param SqlFilter $filter
     * @param array $order
     * @param array $limit
     */
    public function loadLanguages(SqlFilter $filter = NULL, array $order = [['column' => 'is_default', 'direction' => 'desc'], ['column' => 'sort_order']], array $limit = []) {
      return $this->loadRecords($filter, $order, $limit);
    }
    
    /**
     * Nacte vsechny aktivni jazyky
     */
    public function loadActive(array $order = [['column' => 'sort_order']]) {
      return $this->loadLanguages(SqlFilter::create()
        ->compare('disabled', '=', '1'), $order);
    }
  }
