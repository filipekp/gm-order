<?php
  namespace prosys\model;
  use prosys\core\common\Settings;

  /**
   * Reprezentuje entitu jazyka.
   * 
   * @property int $id
   * @property string $code
   * @property string $locale
   * @property string $name
   * @property string $image
   * @property string $dirPath element=dir_path
   * @property int $isDefault element=is_default
   * @property int $sortOrder
   * @property int $disabled
   * 
   * @author Pavel Filípek
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class LanguageEntity extends Entity
  {
    /**
     * Vrati identifikaci jazyka
     * 
     * @return string
     */
    public function getIdentification() {
      return $this->name;
    }
    
    /**
     * Vrati cestu k obrazku jazyka.
     * 
     * @return string|NULL
     */
    public function flagImage() {
      $path = 'admin/resources/languages/' . $this->dirPath . '/resources/';
      
      $image = NULL;
      if (is_file(Settings::ROOT_PATH . $path . $this->image)) {
        $image = $this->image;
      } else if (is_file(Settings::ROOT_PATH . $path . 'icon.png')) {
        $image = 'icon.png';
      }
      
      return (($image) ? Settings::ROOT_URL . $path . $image : $image);
    }
  }
