<?php
  namespace prosys\admin\controller;
  
  use prosys\core\common\Agents,
      prosys\core\common\AppException,
      prosys\core\common\Functions,
      prosys\core\common\Settings,
      prosys\model\DevelopmentEntity,
      prosys\core\common\types\html\Button,
      prosys\core\common\types\html\Icon,
      prosys\core\common\types\html\LinkAction,
      prosys\core\common\types\html\Theme;

  /**
   * Zpracovava pozadavky vyvojove stranky.
   * 
   * @author Jan Svěží
   * @copyright (c) 2015, Proclient s.r.o.
   * 
   * @property \prosys\admin\view\DevelopmentView $_view PROTECTED property, this annotation is only because of Netbeans Autocompletion
   * @property \prosys\model\DevelopmentDao $_dao PROTECTED property, this annotation is only because of Netbeans Autocompletion
   */
  class DevelopmentController extends Controller
  {
    /**
     * Initializes view and dao.
     */
    public function __construct() {
      parent::__construct();
      
      $this->_dao = Agents::getAgent('DevelopmentDao', Agents::TYPE_MODEL);
      $this->_view = Agents::getAgent('DevelopmentView', Agents::TYPE_VIEW_ADMIN);
    }
    
    /**
     * Check required entries.
     * 
     * @param array $exceptions
     */
    protected function checkMandatories(array &$exceptions) {      
      if (!Functions::item($this->_post, 'source')) { $exceptions[] = 'Musíte zadat zdrojový kód.'; }
      if (!Functions::item($this->_post, 'name')) { $exceptions[] = 'Musíte zadat název.'; }
    }
    
    /**
     * Ulozi priklad zdrojoveho kodu do databaze.
     * 
     * @throws AppException
     */
    public function save() {
      if (array_key_exists('execute', $this->_post)) {
        $this->response('initial');
        exit();
      }
      
      $sendByAjax = (bool)Functions::item($this->_post, 'send_by_ajax', FALSE);
      $source = $this->_dao->load($this->_post);   /* @var $source \prosys\model\DevelopmentEntity */
      $status = 200;
      
      // check form validity
      $exceptions = array();
      $this->checkMandatories($exceptions);
      
      if ($exceptions) {
        if ($sendByAjax) {
          $_SESSION[Settings::MESSAGE_EXCEPTION] = $exceptions;
          $status = 400;
        } else {
          throw new AppException($exceptions);
        }
      } else {
        try {
          if ($this->_dao->store($source)) {
            $_SESSION[Settings::MESSAGE_SUCCESS][] = "Zdrojový kód &bdquo;{$source->name}&ldquo; byl úspěšně uložen.";
          } else {
            throw new AppException("Zdrojový kód &bdquo;{$source->name}&ldquo; se nepodařilo uložit.");
          }
        } catch (AppException $e) {
          $status = 400;
          if ($e->getPrevious()) {
            $_SESSION[Settings::MESSAGE_SUCCESS] = array();
            $_SESSION[Settings::MESSAGE_EXCEPTION][] = $e->getPrevious()->getMessage();
          } else {
            $_SESSION[Settings::MESSAGE_EXCEPTION][] = 'Při ukládání došlo k následující chybě: ' . $e->getMessage();
          }
        }
      }
      
      if ($sendByAjax) {
        $data = [];
        if ($status == 200) {
          $sources = $this->_dao->loadByType($source->sourceMode);
          array_walk($sources, function(&$item) {
            $item = [
              'id' => $item->id,
              'text' => $item->name
            ];
          });
          
          $data = [
            'id'      => $source->id,
            'name'    => $source->name,
            'source'  => $source->source,
            'description' => $source->description,
            'url'  => Settings::ROOT_ADMIN_URL . '?module=development&activity=initial&id=' . $source->id,
            'sources' => $sources
          ];
        }

        $output = [
          'response'  => [
            'status'  => $status,
            'message' => Functions::handleMessagesAdmin(TRUE)
          ],
          'data'      => array_merge([], $data),
          'delete_button' => (string)(new Button((new Icon('trash')) . 'Smazat', Theme::get(Theme::RED), Button::TYPE_LINK))->setLink((new LinkAction('', 'development', 'delete'))
                                                                                                                            ->setQuery(['id' => $source->id]))
                                                                                                                            ->data('confirm-text', 'Opravdu chcete smazat zdrojový kód ' . $source->name)
        ];

        header('Content-Type: application/json');
        echo json_encode($output);
        exit();
      } else {
        $this->reload('initial', 'id=' . $source->id);
      }
    }
    
    /**
     * Smaze zdrojovy kod z databaze.
     * 
     * @throws AppException
     */
    public function delete() {
      $id = (string)filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
      $source = $this->_dao->load($id);
      /* @var $source DevelopmentEntity */
      
      if ($source->isNew()) {
        throw new AppException("Zdrojový kód nebylo možno smazat, protože id: &bdquo;{$id}&ldquo; neexistuje.");
      } else {
        if ($this->_dao->delete($source)) {
          $_SESSION[Settings::MESSAGE_SUCCESS][] = "Zdrojový kód &bdquo;{$source->name}&ldquo; byl úspěšně smazán.";
          $this->reload();
        } else {
          throw new AppException("Zdrojový kód &bdquo;{$source->name}&ldquo; se nepodařilo smazat.");
        }
      }
    }

    /**
     * @inherit
     */
    public function response($activity) {
      $templateOnly = (bool)Functions::item($this->_get, 'template_only');
      $development = $this->_dao->load((($this->_post) ? $this->_post : Functions::item($this->_get, 'id', 0)));
      /* @var $development DevelopmentEntity */
      
      switch ($activity) {
        case 'showEvaluated':
          $optional = [
            'execute'     => Functions::item($this->_post, 'execute', FALSE),
            'contentOnly' => TRUE
          ];
          
          $this->_view->showEvaluated($development, $optional);
        break;
        
        case 'initial':
        case 'manage':
        default:
          $optional = array(
            'source_modes'  => array_map(function($mode) {
              return ['option' => $mode];
            }, DevelopmentEntity::$SOURCE_MODES),
            'sources'       => $this->_dao->loadByType((($development->sourceMode) ? $development->sourceMode : Functions::first(DevelopmentEntity::$SOURCE_MODES, TRUE))),
            'source'        => $development,
            'template_only' => $templateOnly
          );

          $this->_view->manage($development, $optional);
        break;
      }
    }
    
    /**
     * Nacte ukazku zdrojoveho kodu z databaze
     */
    public function loadSourceAjax() {
      $id = Functions::item($this->_post, 'id', '');
      $code = 200;
      $message = 'Kód byl načten.';
      
      try {
        $source = $this->_dao->load($id); /* @var $source \prosys\model\DevelopmentEntity */
      } catch (\prosys\core\common\AppException $e) {
        $code = 500;
        $message = $e->getMessage();
      }
      
      $result = [
        'response'  => [
          'code'    => $code,
          'message' => $message
        ],
        'data'      => [
          'id'      => $source->id,
          'name'    => $source->name,
          'source'  => $source->source,
          'description' => $source->description
        ],
        'url'  => Settings::ROOT_ADMIN_URL . '?module=development&activity=initial&id=' . $source->id,
        'delete_button' => (string)(new Button((new Icon('trash')) . 'Smazat', Theme::get(Theme::RED), Button::TYPE_LINK))->setLink((new LinkAction('', 'development', 'delete'))
                                                                                                                          ->setQuery(['id' => $source->id]))
                                                                                                                          ->data('confirm-text', 'Opravdu chcete smazat zdrojový kód ' . $source->name)
      ];
      
      header('Content-Type: application/json');
      echo json_encode($result);
      exit();
    }
    
    /**
     * Nacte ukazku zdrojoveho kodu z databaze podle typu
     */
    public function loadSourcesByTypeAjax() {
      $type = Functions::item($this->_post, 'type', '');
      $code = 200;
      $message = 'Kódy byly načteny.';
      
      try {
        $sources = $this->_dao->loadByType($type); /* @var $sources \prosys\model\DevelopmentEntity[] */
        array_walk($sources, function(&$item) {
          $item = [
            'id'    => $item->id,
            'text'  => $item->name
          ];
        });
      } catch (\prosys\core\common\AppException $e) {
        $code = 500;
        $message = $e->getMessage();
      }
      
      $result = [
        'response'  => [
          'code'    => $code,
          'message' => $message
        ],
        'data'      => [
          'sources'  => $sources
        ]
      ];
      
      header('Content-Type: application/json');
      echo json_encode($result);
      exit();
    }
  }