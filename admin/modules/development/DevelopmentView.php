<?php
  namespace prosys\admin\view;
  
  use prosys\model\Entity,
      prosys\core\common\types\html\Button,
      prosys\core\common\types\html\Icon,
      prosys\core\common\types\html\LinkAction,
      prosys\core\common\types\html\Theme;

  /**
   * Represents the admin user module view.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class DevelopmentView extends BackendView
  {
    /**
     * Initializes the label of every user property.
     */
    public function __construct()
    {
      $this->_moduleIcon = 'user';
      
      $labels = array(
        'name'        => 'Název',
        'source'      => 'Zdrojový kód',
        'description' => 'Popis',
        'source_mode' => 'Mód zdrojového kódu'
      );
      
      $this->_moduleCallbacks = [
        'select' => [
          'development_example' => [
            'showGetter' => function($item, $key) {
              return [
                'option' => $item->name,
                'data' => [
                  'item-id'     => $item->id,
                  'action-icon' => (new Icon('trash-o'))
                ]
              ];
            }
          ]
        ]
      ];

      parent::__construct($labels);
      
      self::$_SMARTY->php_handling = \Smarty::PHP_ALLOW;
    }
    
    /**
     * Zobrazi testovaci stranku vyvoje.
     * 
     * @param \prosys\model\DevelopmentEntity $development
     * @param array $optional associative array with optional data
     */
    public function manage(Entity $development, $optional = array()) {
      /* @var $development \prosys\model\DevelopmentEntity */
      $assign = $optional + array('development' => $development);
      $templateOnly = ((array_key_exists('template_only', $optional)) ? $optional['template_only'] : FALSE);
      
      $loggedUser = self::ProSYS()->loggedUser();
      $assign['delete'] = '';
      if (!$development->isNew()) {
        if ($loggedUser->hasRight($this->_moduleName, 'delete')) {
          $assign['delete'] = (string)(new Button((new Icon('trash')) . 'Smazat', Theme::get(Theme::RED), Button::TYPE_LINK))->setLink((new LinkAction('', $this->_moduleName, 'delete'))
                                                                                                                             ->setQuery(['id' => $development->id]))
                                                                                                                             ->data('confirm-text', 'Opravdu chcete smazat zdrojový kód ' . $development->name);
        }
      }
      
      self::$_SMARTY->setCaching(FALSE);
      $this->printActivity('Development', 'Testovací stránka vývoje', $assign, $templateOnly);
    }
    
    /**
     * Zobrazi pouze prelozeny kod z vyvojove stranky.
     * 
     * @param \prosys\model\DevelopmentEntity $development
     * @param array $optional associative array with optional data
     */
    public function showEvaluated(Entity $development, $optional = array()) {
      /* @var $development \prosys\model\DevelopmentEntity */
      $assign = $optional + array('development' => $development);
      $templateOnly = ((array_key_exists('template_only', $optional)) ? $optional['template_only'] : FALSE);
            
      self::$_SMARTY->setCaching(FALSE);
      $this->printActivity('Evaluated', 'Výsledek', $assign, $templateOnly);
    }
  }
