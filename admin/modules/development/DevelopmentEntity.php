<?php
  namespace prosys\model;

  /**
   * Reprezentuje entitu prikladu testovaciho kodu.
   * 
   * @property int $id
   * @property string $name
   * @property string $source
   * @property string $sourceMode element=source_mode
   * @property string $description
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class DevelopmentEntity extends Entity
  {
    public static $SOURCE_MODES = [
      'text/x-php'    => 'PHP',
      'smarty'        => 'Smarty',
      'php'           => 'PHP s HTML, CSS a JS',
      'htmlmixed'     => 'HTML s CSS a JS',
      'css'           => 'CSS',
      'javascript'    => 'JS',
      'xml'           => 'XML',
      'clike'         => 'Kód typu C'
    ];
  }
