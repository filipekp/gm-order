<?php
  namespace prosys\model;
  
  use prosys\core\mapper\SqlFilter;
  
  /**
   * Reprezentuje objekt pristupu k datum prikladu testovaciho vyvoje.
   * 
   * @author Jan Svěží <svezi@proclient.cz>
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class DevelopmentDao extends MyDataAccessObject
  {
    public function __construct() {
      parent::__construct('development_examples', DevelopmentEntity::classname());
    }
    
    /**
     * Načte ukazky zdrojovych kodu z databaze
     * 
     * @param string $type
     * @return DevelopmentEntity[]
     */
    public function loadByType($type) {
      $filter = SqlFilter::create()->compare('source_mode', '=', $type);
      
      return $this->loadRecords($filter, [['column' => 'name']]);
    }
  }
