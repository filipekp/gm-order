{handle_admin_messages}
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12">
      <h3 class="page-title">{$title}</h3>
      {breadcrumb_add title='Domů' icons=['home'] link=$ProSYS.SETTINGS.ROOT_ADMIN_URL}
      {breadcrumb_add title=$title icons=['html5']}
      {breadcrumbs_get}
    </div>
  </div>

  <div class="row-fluid">
    <div class="span12">
      <div class="portlet box red">
        <div class="portlet-title">
          <div class="caption">Vývoj</div>
        </div>

        <div class="portlet-body form">
          <div class="clearfix"></div>
          {nocache}
            {if $development->source && 'execute'|array_key_exists:$smarty.post}
              <div class="evaluated_code">{eval_code source=$development}</div>
            {/if}
          {/nocache}

          <form class="form-horizontal development" method="post">
            <input type="hidden" name="controller" value="development" />
            <input type="hidden" name="action" value="save" />
            
            <div class="form-body clearfix">
              <div class="col-md-4">
                <label class="control-label col-md-5" for="source_mode">{$labels['source_mode']}: </label>
                <div class="col-md-7">
                  {nocache}
                    {make_select_from_array
                      data=$source_modes
                      selectedValues=$development->sourceMode|ternary:$development->sourceMode:'text/x-php'
                      name='source_mode'
                      defaultText='-- vyberte mód kódu --'
                      id='source_mode'
                      classes=['form-control']}
                  {/nocache}
                </div>
              </div>
              
              <div class="col-md-3"></div>
              
              <div class="col-md-5">
                <label class="control-label col-md-4" for="source_mode">{$labels['source']}: </label>
                <div class="col-md-8" id="source_code_examples">
                  {nocache}
                    {make_select_from_entity_array 
                      data=$sources 
                      valueGetter='id'
                      showGetter='name'
                      selectedValues=$development->id|ternary:$development->id:0
                      name='id'
                      defaultText='-- vyberte zdrojový kód --' 
                      id='development_example' 
                      classes=['form-control']}
                  {/nocache}
                </div>
              </div>
              
              <div class="clearfix"></div>
              
              <div class="codeEditorWrapper col-md-12">
                <textarea id="code_editor" class="codeEditor" name="source">{nocache}{$development->source|escape:'html'}{/nocache}</textarea>
              </div>
            </div>

            <div class="form-actions">
              <div class="row">
                <div class="col-md-6">
                  <input type="text" class="form-control input-inline input-medium" name="name" placeholder="Název" value="{nocache}{$development->name|escape:'html'}{/nocache}" />&nbsp;&nbsp;
                  {button text='Uložit kód' icon='floppy-o'|icon theme='blue' purpose='submit' name="save" title="zkratka: Ctrl+S"}
                  <span class="delete_action">
                    {$delete}
                  </span>
                    <input id="autosave_checkbox" type="checkbox" class="custom-bootstrap-switch" value="1" data-width="150"
                           data-on-text="Autosave ON" data-off-text="Autosave OFF" data-size="mini" data-off-color="danger"
                           data-on-color="success"
                           {nocache}{if $ProSYS.GLOBAL->loggedUser()->otherSettings->jsonSerialize()|item:['autosave', 'enable']} checked="checked"{/if}{/nocache}/>
                </div>
                <div class="col-md-6 tright">
                  {button text='Zobrazit výsledek' icon='gears'|icon theme='green' purpose='submit' name='execute_fancy'}
                  {button text='Vykonat kód' icon='gears'|icon theme='green' purpose='submit' name='execute'}
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
