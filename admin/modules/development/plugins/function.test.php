<?php
  /**
   * Funkce pro testovani
   * 
   * @param array $params
   * @param Smarty $smarty
   *
   * @example {test someParam=""}
   */
  function smarty_function_test($params, &$smarty) {
    var_dump($params);
  }
