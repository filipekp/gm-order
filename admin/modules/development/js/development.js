var CodeMirrorPlugin = function() {
  var lastMode = null;
  
  /**
   * Funkce pro změnu modu codemirror editoru
   * 
   * @param {string} mode
   * @param {object} editor
   */
  function changeMode(mode, editor) {
    mode = ((mode) ? mode : lastMode);
    lastMode = mode;
    
    editor.setOption('mode', mode);
  }

  function InitCodeEditor() {
    CodeMirror.modeURL = '../mode/%N/%N.js';
    
    var $codeEditor = $('#code_editor');
    
    var editor = null;
    editor = CodeMirror.fromTextArea($codeEditor.get(0), {
      lineNumbers: true,
      matchBrackets: true,
      extraKeys: { 'Ctrl-Space': 'autocomplete' },
      indentUnit: 2,
      indentWithTabs: true,
      tabSize: 2
    });

    $codeEditor.data(CodeMirrorPlugin.dataMark, editor);

    var $sourceMode = $('#source_mode');
    changeMode($sourceMode.val(), editor);
    $('body').on('change', '#source_mode', function() {
      changeMode($sourceMode.val(), editor);
    });
  }
  
  
  return {
    init: function() {
      InitCodeEditor();
    },
    'dataMark': 'code-mirror-instance'
  };
}();

/**
 * Plugin na obsluhu nacitani zdrojového kódu do formuláře
 */
var SourceServicePlugin = function() {
  var notLoad = false;
  
  /**
   * Nacitani zdrojoveho kodu z DB
   * 
   * @param {string} type
   */
  function loadSourcesByType(type) {
    // zapne animaci nacitani
    Metronic.blockUI({
      target: $(SourceServicePlugin.selectors.source_mode).closest('.portlet-body'),
      animate: true,
      overlayColor: 'rgba(0, 0, 0, 0.75)'
    });

    var $data = {
      async: true,
      method: 'POST',
      dataType: 'json',
      data: {
        controller: 'development',
        action:     'loadSourcesByTypeAjax',
        type:       type
      }
    };

    $.ajax('index.php', $data).done(function($result) {
      // prenacte vyberovy selectbox development example
      $(SourceServicePlugin.selectors.development_example).html('');
      $('<option />').appendTo($(SourceServicePlugin.selectors.development_example));
      $.each($result.data.sources, function(idx, item) {
        var $option = $('<option />').attr('value', item.id).text(item.text);
        if (item.id === $result.data.id) {
          $option.attr('selected', 'selected');
        }
        $option.appendTo($(SourceServicePlugin.selectors.development_example));
      });
      
      $(SourceServicePlugin.selectors.development_example).select2('val', '');

      // zrusi animaci nacitani
      Metronic.unblockUI($(SourceServicePlugin.selectors.development_example).closest('.portlet-body'));
    });
      
      
    // smaze nazev kodu v poli nazev
    $(SourceServicePlugin.selectors.name).val('').trigger('change');

    // zrusi tlacitko pro smazani
    $(SourceServicePlugin.selectors.delet_action_wrapper).html('');
    
    $(SourceServicePlugin.selectors.source).data(CodeMirrorPlugin.dataMark).setCursor({line: 0 , ch: 0 });

    // zmeni url adresu
    window.history.pushState({"pageTitle":document.title}, "", location.current_path + '?module=development');
  }
  
  /**
   * Nacitani zdrojoveho kodu z DB
   * 
   * @param {int} id
   */
  function loadData(id) {
    if (id) {
      // zapne animaci nacitani
      Metronic.blockUI({
        target: $(SourceServicePlugin.selectors.development_example).closest('.portlet-body'),
        animate: true,
        overlayColor: 'rgba(0, 0, 0, 0.75)'
      });
                
      var $data = {
        async: true,
        method: 'POST',
        dataType: 'json',
        data: {
          controller: 'development',
          action:     'loadSourceAjax',
          id:         id
        }
      };

      $.ajax('index.php', $data).done(function($result) {
        // vyplni nazev kodu do pole nazev
        $(SourceServicePlugin.selectors.name).val($result.data.name).trigger('change');

        // vyplni zdrojovy kod do CodeMirror
        var $source = $(SourceServicePlugin.selectors.source);
        $source.val($result.data.source).trigger('change');      
        $source.data(CodeMirrorPlugin.dataMark).setValue($result.data.source);
        
        // zmeni url adresu
        window.history.pushState({"pageTitle":document.title}, "", $result.url);
        
        $(SourceServicePlugin.selectors.source).data(CodeMirrorPlugin.dataMark).setCursor({line: 0 , ch: 0 });
        
        // prida tlacitko pro smazani
        $(SourceServicePlugin.selectors.delet_action_wrapper).html($result.delete_button);
        
        // zrusi animaci nacitani
        Metronic.unblockUI($(SourceServicePlugin.selectors.development_example).closest('.portlet-body'));
      });
    } else {      
      // smaze nazev kodu v poli nazev
      $(SourceServicePlugin.selectors.name).val('').trigger('change');

      // smaze zdrojovy kod z CodeMirror
      var $source = $(SourceServicePlugin.selectors.source);
      $source.val('').trigger('change');      
      $source.data(CodeMirrorPlugin.dataMark).setValue('');
      
      // zrusi tlacitko pro smazani
      $(SourceServicePlugin.selectors.delet_action_wrapper).html('');
      
      // zmeni url adresu
      window.history.pushState({"pageTitle":document.title}, "", location.current_path + '?module=development');
    }
  }
  
  /**
   * Ulozi data formulare
   * 
   * @param {jQuery} $data
   */
  function saveData($data, $form) {
    var editorCursor = $(SourceServicePlugin.selectors.source).data(CodeMirrorPlugin.dataMark).getCursor();
    var editorScroll = $(SourceServicePlugin.selectors.source).data(CodeMirrorPlugin.dataMark).getScrollInfo();

    $.post('index.php', $.extend({}, {send_by_ajax: true}, $data), function($result) {
      if ($result.response.status === 200) {

        // prenacte vyberovy selectbox
        $(SourceServicePlugin.selectors.development_example).html('');
        $('<option />').appendTo($(SourceServicePlugin.selectors.development_example));
        $.each($result.data.sources, function(idx, item) {
          var $option = $('<option />').attr('value', item.id).text(item.text);
          if (item.id === $result.data.id) {
            $option.attr('selected', 'selected');
          }
          $option.appendTo($(SourceServicePlugin.selectors.development_example));
        });

        notLoad = true;
          $(SourceServicePlugin.selectors.development_example).trigger('change');
        notLoad = false;

        // prida tlacitko pro smazani
        $(SourceServicePlugin.selectors.delet_action_wrapper).html($result.delete_button);

        // zmeni url adresu
        window.history.pushState({"pageTitle":document.title}, "", $result.data.url);
      }
    }).always(function($result) {
      getToasterStatusMessage(JSON.stringify($result.response.message));

      $(SourceServicePlugin.selectors.source).data(CodeMirrorPlugin.dataMark).scrollTo({x: editorScroll.left, y: editorScroll.top});
      $(SourceServicePlugin.selectors.source).data(CodeMirrorPlugin.dataMark).setCursor({line: editorCursor.line , ch : editorCursor.ch });

      Metronic.unblockUI($form.closest('.portlet-body'));
    });
  }
  
  /**
   * 
   * @param {type} $data
   * @param {type} $form
   * @returns {undefined}
   */
  function evalCode($data, $form) {
    var $myData = {
      id: $data.id,
      name: $data.name,
      source: $data.source,
      execute: true,
    };
    
    $.post('index.php?module=' + $data.controller + '&activity=showEvaluated', $.extend({}, $myData), function($result) {
      $.fancybox({
        type        : 'iframe',
        href        : 'data:text/html;charset=utf-8;base64,' + Base64.encode($result),
        closeClick	: true,
        minWidth    : '90%',
        minHeight   : '90%',
        tpl         : Lightbox.$tpl
      });
    }).always(function($result) {
      Metronic.unblockUI($form.closest('.portlet-body'));
    });
  }
  
  /**
   * 
   * @param {type} $button
   * @returns {undefined}
   */
  function decider($button) {
    var $form = $(SourceServicePlugin.selectors.form);
    
    Metronic.blockUI({
      target: $form.closest('.portlet-body'),
      animate: true,
      overlayColor: 'rgba(0, 0, 0, 0.75)'
    });
    
    $(SourceServicePlugin.selectors.source).data(CodeMirrorPlugin.dataMark).save();
    var $data = $form.serializeObject();
    $form.trigger('form-autosave-stopTimer');
    
    if ($button.attr('name') === 'save') {
      saveData($data, $form);
    } else if ($button.attr('name') === 'execute_fancy') {
      evalCode($data, $form);
    }
  }
  
  return {
    init: function() {
      $('body').on('change', SourceServicePlugin.selectors.development_example, function() {
        if (!notLoad) {
          loadData($(this).select2('val'));
        }
      });
      
      $('body').on('change', SourceServicePlugin.selectors.source_mode, function() {
        loadSourcesByType($(this).select2('val'));
      });
      
      $(SourceServicePlugin.selectors.form).on('click', SourceServicePlugin.selectors.button_save + ', ' + SourceServicePlugin.selectors.button_execute, function(e) {
        e.preventDefault();
        decider($(this));
      });
      
      $(SourceServicePlugin.selectors.form).formAutosave({
        storeAfter: (((typeof __GLOBALS.user.autosave !== 'undefined') && 
                      (typeof __GLOBALS.user.autosave.time !== 'undefined') && 
                      __GLOBALS.user.autosave.time) ? 
                        __GLOBALS.user.autosave.time : 
                        5
                    ),
        onAutosave: function($form) {
          if ($(SourceServicePlugin.selectors.autosave_checkbox).bootstrapSwitch('state') && $(SourceServicePlugin.selectors.development_example).val() > 0) {
            $('[name="save"]').trigger('click');
          }
        }
      });
    },
    
    selectors: {
      'form':                 'form.development',
      'development_example':  '#development_example',
      'source_mode':          '#source_mode',
      'source':               '[name="source"]',
      'name':                 '[name="name"]',
      'id':                   '[name="id"]',
      'button_save':          'button[name="save"]',
      'button_execute':       'button[name="execute_fancy"]',
      'delet_action_wrapper': 'span.delete_action',
      'autosave_checkbox':    '#autosave_checkbox'
    }
  };
}();

$(document).ready(function() {
  CodeMirrorPlugin.init();
  SourceServicePlugin.init();
  
  // naváže odeslání uložení formuláře po stisknutí CTRL+S
  $.Shortcuts.add({
    type: 'down',
    mask: 'Ctrl+S',
    enableInInput: true,
    handler: function() {
      $('[name="save"]').trigger('click');
    }
  }).add({
    type: 'down',
    mask: 'Ctrl+Enter,F5',
    enableInInput: true,
    handler: function() {
      $('[name="execute"]').trigger('click');
    }
  }).start();
});

