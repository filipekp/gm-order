<?php
  namespace prosys\model;

  /**
   * Reprezentuje entitu meny.
   * 
   * @property int $id
   * @property string $name
   * @property string $code
   * 
   * @author Jan Svěží
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class CurrencyEntity extends Entity
  {
  }
