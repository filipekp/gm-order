<?php
  namespace prosys\model;
  
  /**
   * Reprezentuje DAO meny.
   * 
   * @author Jan Svěží
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class CurrencyDao extends MyDataAccessObject
  {
    public function __construct() {
      parent::__construct('currencies', CurrencyEntity::classname());
    }
  }
