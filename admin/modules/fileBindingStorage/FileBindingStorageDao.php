<?php
  namespace prosys\model;
  
  /**
   * Reprezentuje DAO vazebnich `skladist` (tabulek) se soubory.
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class FileBindingStorageDao extends MyDataAccessObject
  {
    public function __construct() {
      parent::__construct('file_binding_storages', FileBindingStorageEntity::classname());
    }
  }
