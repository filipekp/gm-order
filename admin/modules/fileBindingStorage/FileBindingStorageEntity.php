<?php
  namespace prosys\model;

  /**
   * Reprezentuje entitu vazebniho `skladiste` (tabulky) se soubory.
   * 
   * @property int $id
   * @property TransferObjectEntity $transferObject element=transfer_object_id
   * @property string $name
   * @property string $foreignKey element=foreign_key
   * @property string $fileKey element=file_key
   * 
   * @author Jan Svěží
   * @copyright (c) 2014, Proclient s.r.o.
   */
  class FileBindingStorageEntity extends Entity
  {
  }
