<?php
  namespace prosys\admin\controller;
  
  use prosys\core\common\Agents,
      prosys\core\common\Settings,
      prosys\core\common\Functions,
      prosys\core\common\types\html\Icon;

  /**
   * Processes main requests.
   * 
   * @author Pavel Filípek
   * @copyright (c) 2015, Proclient s.r.o.
   * 
   * @property \prosys\view\admin\AdminView $_view PROTECTED property, this annotation is only because of Netbeans Autocompletion
   */
  class AdminController extends Controller
  {
    /**
     * Initializes view.
     */
    public function __construct() {
      parent::__construct();
      
      $this->_view = Agents::getAgent('AdminView', Agents::TYPE_VIEW_ADMIN);
    }

    protected function checkMandatories(array $array, array &$exceptions) {
      throw new \prosys\core\common\AppException('Check mandatories not implemented yet.');
    }
    
    /**
     * Přidá notifikační zprávu.
     * 
     * @param string $html
     */
    public static function addNotification($html, Icon $icon, \DateTime $dateTime = NULL) {
      if (is_null($dateTime)) {
        $dateTime = new \DateTime();
      }
      
      $_SESSION['notification_messages'][] = [
        'html'  => $html,
        'icon'  => (string)$icon,
        'time'  => $dateTime->format('Y-m-d H:i:s')
      ];
    }
    
    /**
     * Vymaže všechny notifikační zprávy
     */
    public static function deleteNotifications() {
      unset($_SESSION['notification_messages']);
    }
    
    /**
     * @inherit
     */
    public function response($activity){
      switch ($activity) {
        case 'login':
          $this->_view->login();
        break;
        
        case 'phpinfo':
          ob_start();
            phpinfo();
          $pinfo = ob_get_clean();

          $pinfo = preg_replace('%^.*<body>(.*)</body>.*$%ms', '$1', $pinfo);
          $pinfo = preg_replace('%width=".*?"%ms', '', $pinfo);
          $pinfo = preg_replace('%<th colspan="2">(.*?)</th>%ms', '<th>$1</th><th>&nbsp;</th>', $pinfo);
          
          $this->_view->phpInfo(['pinfo' => $pinfo]);
        break;
      
        case 'e401':
        case 'e405':
          $get = filter_input_array(INPUT_GET);
          $requested = ((array_key_exists('requested', $get)) ? $get['requested'] : array('module' => '', 'action' => ''));
          /* @var $moduleActionDao \prosys\model\ModuleActionDao */
          $moduleActionDao = Agents::getAgent('ModuleActionDao', Agents::TYPE_MODEL);
          /* @var $moduleAction \prosys\model\ModuleActionEntity */
          $moduleAction = $moduleActionDao->loadByModuleAndAction($requested['module'], $requested['action']);
          
          if ($moduleAction && !$moduleAction->isNew()) {
            $message = 'Nemáte práva k ';
            $message.= (($moduleAction->type == 1) ? 'zobrazení stránky: ' : 'provedení akce: ') . '<b>&bdquo;' . $moduleAction->title . '&ldquo;</b>';
            $message.= ' modulu: <b>&bdquo;' . $moduleAction->module->name . '&ldquo;</b>.';
            
            $_SESSION[Settings::MESSAGE_EXCEPTION] = $message;
          }
          
          $this->_view->errorPage($activity);
        break;

        default:
          $this->_view->initial();
        break;
      }
    }
}
