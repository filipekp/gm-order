<?php
  namespace prosys\admin\view;
  use prosys\core\common\Settings;

  /**
   * Reprezentuje View pro hlavní administrační sekci.
   * 
   * @author Pavel Filípek
   * @copyright (c) 2015, Proclient s.r.o.
   */
  class AdminView extends BackendView
  {
    /**
     * Show dashboard.
     */
    public function initial($arg = NULL) {
      $this->printActivity('Home', 'Hlavní strana');
    }
    
    /**
     * Show login form.
     */
    public function login() {
      $this->printActivity('Login', 'Přihlášení do administrace', [], TRUE);
    }
    
    /**
     * Show error page.
     */
    public function errorPage($code) {
      switch ($code) {
        case 400:
          $title = 'Pozor vyjímka';
        break;
        case 401:
          $title = 'Přístup odmítnut';
        break;
        case 405:
          $title = 'Akce není povolena';
        break;
        default:
          $title = $code . ' - Chybová stránka';
        break;
      }
      
      $text = ((array_key_exists(Settings::MESSAGE_EXCEPTION, $_SESSION)) ? $_SESSION[Settings::MESSAGE_EXCEPTION] : 'Vyskatla se blíže nespecifikovaná chyba s kódem: ' . filter_var($code, FILTER_SANITIZE_NUMBER_INT));
      unset($_SESSION[Settings::MESSAGE_EXCEPTION]);
      
      $this->printActivity('Error', $title, ['code' => strtoupper($code), 'text' => $text]);
    }
    
    /**
     * Show phpinfo.
     */
    public function phpInfo($data) {
      self::$_SMARTY->setCaching(FALSE);
      $this->printActivity('PhpInfo', 'Informace o serveru', $data);
    }
  }
