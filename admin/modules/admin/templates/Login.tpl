<!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <title>{if $title == $ProSYS.VARIABLES.SHOWING.WEB_NAME}{$ProSYS.VARIABLES.SHOWING.WEB_NAME}{else}{$title} | {$ProSYS.VARIABLES.SHOWING.WEB_NAME}{/if}</title>
    <meta name="author" content="{$ProSYS.SETTINGS.PROCLIENT_AUTHOR}" />
    <link type="image/x-icon" rel="shortcut icon" href="{$ProSYS.SETTINGS.ADMIN_IMAGES}favicon.ico" />
    
    {include file='stylesLogin.tpl'}
    {include file='javascriptsLogin.tpl'}
  </head>
	<body class="login">
    <div class="logo">
      {$ProSYS.VARIABLES.SHOWING.WEB_TITLE}
    </div>
    <div class="menu-toggler sidebar-toggler"></div>
    <div class="content">
      <form class="login-form" action="" method="post">
        <input type="hidden" name="controller" value="login" />
        <input type="hidden" name="action" value="login" />
        
        <h3 class="form-title">{$translation|item:['labels','login_form','title']}</h3>
        <div id="messages_container"></div>
        {nocache}{handle_admin_messages showingType=1}{/nocache}
        
        <div class="form-group">
          <label class="control-label visible-ie8 visible-ie9">{nocache}{$translation|item:['labels','login_form','user_name']}{/nocache}</label>
          <div class="input-icon">
            <i class="fa fa-user"></i>
            {nocache}<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="{$translation|item:['labels','login_form','user_name']}" name="login" autofocus="autofocus"/>{/nocache}
          </div>
        </div>
        <div class="form-group">
          <label class="control-label visible-ie8 visible-ie9">{nocache}{$translation|item:['labels','login_form','password']}{/nocache}</label>
          <div class="input-icon">
            <i class="fa fa-lock"></i>
            {nocache}<input class="form-control placeholder-no-fix notTogglePassword" type="password" autocomplete="off" placeholder="{$translation|item:['labels','login_form','password']}" name="password"/>{/nocache}
          </div>
        </div>
        <div class="form-actions clearfix">
          <button type="submit" class="btn green-haze pull-right">
            {$translation|item:['labels','login_form','login_btn']} <i class="m-icon-swapright m-icon-white"></i>
          </button>
        </div>
      </form>
    </div>
    <div class="copyright">
       {get_copyright withLink=TRUE}
    </div>
    <script>
      jQuery(document).ready(function() {     
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Login.init();
      });
    </script>
	</body>
</html>
