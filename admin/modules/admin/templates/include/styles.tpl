  {assign var="plugins" value=$ProSYS.SETTINGS.ADMIN_PLUGINS}
  
  {include_resource type="css" name="" path="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"}
  {include_resource type="css" name="font-awesome.min.css" path=$plugins|cat:"font-awesome/css/"}
  {include_resource type="css" name="simple-line-icons.min.css" path=$plugins|cat:"simple-line-icons/"}
  {include_resource type="css" name="jquery-ui.css"}
  {include_resource type="css" name="jquery-ui-metro.css"}
  {include_resource type="css" name="bootstrap.min.css" path=$plugins|cat:"bootstrap/css/"}
  {include_resource type="css" name="uniform.default.css" path=$plugins|cat:"uniform/css/"}
  {include_resource type="css" name="bootstrap-switch.min.css" path=$plugins|cat:"bootstrap-switch/css/"}
  {include_resource type="css" name="codemirror.css" path=$plugins|cat:"codemirror/lib/"}
  {include_resource type="css" name="show-hint.css" path=$plugins|cat:"codemirror/addon/hint/"}
  {include_resource type="css" name="select2.css" path=$plugins|cat:"select2/"}
  {include_resource type="css" name="bootstrap-fileinput.css" path=$plugins|cat:"bootstrap-fileinput/"}  
  {include_resource type="css" name="jquery.notific8.min.css" path=$plugins|cat:"jquery-notific8/"}
  {include_resource type="css" name="toastr.css" path=$plugins|cat:"bootstrap-toastr/"}
  {include_resource type="css" name="pace-theme-minimal.css" path=$plugins|cat:"pace/themes/"}
  {include_resource type="css" name="jquery.fancybox.css" path=$plugins|cat:"fancybox/source/"}
  {include_resource type="css" name="jquery.Jcrop.min.css" path=$plugins|cat:"jcrop/css/"}
  {include_resource type="css" name="image-crop.css"}
  
  {include_resource type="css" name="lock.css"}
  {include_resource type="css" name="profile.css"}
  {include_resource type="css" name="components-rounded.css"}
  {include_resource type="css" name="plugins.css"} 
  {include_resource type="css" name="layout.css"}
  {nocache}{include_resource type="css" name=($ProSYS.CURRENT_LOGIN->user->theme|cat:".css") path=$ProSYS.SETTINGS.ADMIN_CSS|cat:"themes/"}{/nocache}
  {include_resource type="css" name="custom.css"}
  
  {include_resource type="css" name="modules.css"}
  
  {nocache}{include_module_resource type="css"}{/nocache}