<ul class="page-sidebar-menu" data-slide-speed="200">
  <li class="sidebar-toggler-wrapper">
    <div class="sidebar-toggler"></div>
  </li>
  <li>&nbsp;</li>
  {function menu level=1}
    {assign var='classActive' value='active'}
  
    {foreach $menus as $menu}
      {assign var='classes' value=NULL}
      {append var='classes' value='level'|cat:$level}
      {if $menu->active}
        {append var='classes' value=$classActive}
      {/if}
      {if $menu->displayAlways === -1}
        {append var='classes' value='hide-item'}
      {/if}
      
      <li{if $classes} class="{' '|implode:$classes}"{/if} data-id="{$menu->id}" data-parent="{($menu->parent)|ternary:$menu->parent->id:0}">
        <a href="{$menu->link}"{' '|implode:$menu->aditionalAttributes}>
          <span class="icons">{''|implode:$menu->iconsHtml}</span> <span class="title">{$menu->name}</span>{if $menu->visibleMenu > 0} <span class="arrow"></span>{/if}
        </a>
        {if $menu->children|@count > 0}
          <ul{if $menu->visibleMenu > 0} class="sub-menu"{/if}>
            {menu menus=$menu->children level=$level+1}
          </ul>
        {/if}
      </li>
    {/foreach}
  {/function}
  
  {nocache}
    {menu menus=$menu_html_structure}
  {/nocache}
</ul>