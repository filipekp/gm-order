  {assign var="plugins" value=$ProSYS.SETTINGS.ADMIN_PLUGINS}
  {assign var="js" value=$ProSYS.SETTINGS.ADMIN_JS}
  
  {* nacteni prekladu do javascriptu *}
  {include_resource type="js"
                    name=''
                    path=$ProSYS.SETTINGS.ROOT_ADMIN_URL|cat:"resources/languages/translations.php"}
  
  <!--[if lt IE 9]>
  {include_resource type="js" name="respond.min.js" path=$plugins}
  {include_resource type="js" name="excanvas.min.js" path=$plugins}
  <![endif]-->

  {include_resource type="js" name="jquery.min.js" path=$plugins}
  {include_resource type="js" name="jquery-migrate.min.js" path=$plugins}
  {include_resource type="js" name="jquery-ui.js" path=$plugins|cat:'jquery-ui/'}
  {include_resource type="js" name="jquery-ui-timepicker-addon.js" path=$plugins}
  {include_resource type="js" name="select2.js" path=$plugins|cat:'select2/'}
  {include_resource type="js" name="bootstrap.min.js" path=$plugins|cat:'bootstrap/js/'}
  {include_resource type="js" name="bootstrap-hover-dropdown.min.js" path=$plugins|cat:'bootstrap-hover-dropdown/'}
  {include_resource type="js" name="jquery.slimscroll.min.js" path=$plugins|cat:'jquery-slimscroll/'}
  {include_resource type="js" name="jquery.blockui.min.js" path=$plugins}
  {include_resource type="js" name="jquery.cokie.min.js" path=$plugins}
  {include_resource type="js" name="jquery.uniform.min.js" path=$plugins|cat:'uniform/'}
  {include_resource type="js" name="bootstrap-switch.min.js" path=$plugins|cat:'bootstrap-switch/js/'}
  {include_resource type="js" name="jquery.flot.min.js" path=$plugins|cat:'flot/'}
  {include_resource type="js" name="jquery.flot.resize.min.js" path=$plugins|cat:'flot/'}
  {include_resource type="js" name="jquery.flot.categories.min.js" path=$plugins|cat:'flot/'}
  {include_resource type="js" name="jquery.pulsate.min.js" path=$plugins}
  {include_resource type="js" name="jquery.validate.min.js" path=$plugins|cat:"jquery-validation/js/"}
  {include_resource type="js" name="jquery.fancybox.js" path=$plugins|cat:"fancybox/"}
  {include_resource type="js" name="jquery.inputmask.js" path=$plugins|cat:"inputmask-multi/"}
  {include_resource type="js" name="jquery.bind-first.js" path=$plugins|cat:"inputmask-multi/"}
  {include_resource type="js" name="jquery.inputmask-multi.js" path=$plugins|cat:"inputmask-multi/"}
  {include_resource type="js" name="jquery.inputmask.extensions.js" path=$plugins|cat:"inputmask-multi/"}
  {include_resource type="js" name="jquery.inputmask.phone.extensions.js" path=$plugins|cat:"inputmask-multi/"}
  {include_resource type="js" name="bootbox.js" path=$plugins|cat:"bootbox/"}
  
  {include_resource type="js" name="color-setter.js"}
  {include_resource type="js" name="multi-sortable.js"}
  {include_resource type="js" name="form-autosave.js"}
  {include_resource type="js" name="metronic.js"}
  {include_resource type="js" name="layout.js"}
  {include_resource type="js" name="quick-sidebar.js"}
  {include_resource type="js" name="index.js"}
  {include_resource type="js" name="form-components.js"}
  {include_resource type="js" name="login.js"}
  {include_resource type="js" name="tinymce.min.js" path=$plugins|cat:"tinymce/"}
  
  {include_resource type="js" name="custom.js"}
  
  {nocache}
  <script>
    location.current_path = '{$ProSYS.SETTINGS.ROOT_ADMIN_URL}';
  </script>
  {/nocache}
  
  {nocache}{include_module_resource type="js"}{/nocache}

