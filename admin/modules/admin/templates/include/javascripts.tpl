  {assign var="plugins" value=$ProSYS.SETTINGS.ADMIN_PLUGINS}
  {assign var="js" value=$ProSYS.SETTINGS.ADMIN_JS}
  
  {* spusteni loaderu *}
  {include_resource type="js" name="pace.min.js" path=$plugins|cat:'pace/'}
  
  {* nacteni prekladu do javascriptu *}
  {include_resource type="js"
                    name=''
                    path=$ProSYS.SETTINGS.ROOT_ADMIN_URL|cat:"resources/languages/translations.php?module="|cat:$ProSYS.GLOBAL->module()|cat:"&activity="|cat:$ProSYS.GLOBAL->activity()|cat:"&language="|cat:$ProSYS.GLOBAL->language()->id}
  
  <!--[if lt IE 9]>
  {include_resource type="js" name="respond.min.js" path=$plugins}
  {include_resource type="js" name="excanvas.min.js" path=$plugins}
  <![endif]-->

  {include_resource type="js" name="jquery.min.js" path=$plugins}
  {include_resource type="js" name="jquery-migrate.min.js" path=$plugins}
  {include_resource type="js" name="jquery-ui.js" path=$plugins|cat:'jquery-ui/'}
  {include_resource type="js" name="jquery-ui-timepicker-addon.js" path=$plugins}
  {include_resource type="js" name="select2.js" path=$plugins|cat:'select2/'}
  {include_resource type="js" name="bootstrap.js" path=$plugins|cat:'bootstrap/js/'}
  {include_resource type="js" name="bootstrap-hover-dropdown.min.js" path=$plugins|cat:'bootstrap-hover-dropdown/'}
  {include_resource type="js" name="jquery.slimscroll.min.js" path=$plugins|cat:'jquery-slimscroll/'}
  {include_resource type="js" name="jquery.blockui.min.js" path=$plugins}
  {include_resource type="js" name="jquery.cokie.min.js" path=$plugins}
  {include_resource type="js" name="jquery.uniform.min.js" path=$plugins|cat:'uniform/'}
  {include_resource type="js" name="bootstrap-switch.min.js" path=$plugins|cat:'bootstrap-switch/js/'}
  {include_resource type="js" name="jquery.flot.min.js" path=$plugins|cat:'flot/'}
  {include_resource type="js" name="jquery.flot.resize.min.js" path=$plugins|cat:'flot/'}
  {include_resource type="js" name="jquery.flot.categories.min.js" path=$plugins|cat:'flot/'}
  {include_resource type="js" name="jquery.pulsate.min.js" path=$plugins}
  {include_resource type="js" name="jquery.validate.js" path=$plugins|cat:"jquery-validation/js/"}
  {include_resource type="js" name="additional-methods.min.js" path=$plugins|cat:"jquery-validation/js/"}
  {include_resource type="js" name="messages_cs.js" path=$plugins|cat:"jquery-validation/js/localization/"}
  {include_resource type="js" name="jquery.fancybox.js" path=$plugins|cat:"fancybox/"}
  {include_resource type="js" name="jquery.inputmask.js" path=$plugins|cat:"inputmask-multi/"}
  {include_resource type="js" name="jquery.bind-first.js" path=$plugins|cat:"inputmask-multi/"}
  {include_resource type="js" name="bootstrap-fileinput.js" path=$plugins|cat:"bootstrap-fileinput/"}
  {include_resource type="js" name="jquery.inputmask-multi.js" path=$plugins|cat:"inputmask-multi/"}
  {include_resource type="js" name="jquery.inputmask.extensions.js" path=$plugins|cat:"inputmask-multi/"}
  {include_resource type="js" name="jquery.inputmask.phone.extensions.js" path=$plugins|cat:"inputmask-multi/"}
  {include_resource type="js" name="jquery-parser-url.js" path=$plugins}
  {include_resource type="js" name="jquery.idletimeout.js" path=$plugins|cat:"jquery-idle-timeout/"}
  {include_resource type="js" name="jquery.idletimer.js" path=$plugins|cat:"jquery-idle-timeout/"}
  {include_resource type="js" name="jquery.notific8.js" path=$plugins|cat:"jquery-notific8/"}
  {include_resource type="js" name="toastr.js" path=$plugins|cat:"bootstrap-toastr/"}
  {include_resource type="js" name="bootbox.js" path=$plugins|cat:"bootbox/"}
  {include_resource type="js" name="jquery.bootstrap.wizard.min.js" path=$plugins|cat:"bootstrap-wizard/"}
  {include_resource type="js" name="serialize-object.js" path=$plugins}
  {include_resource type="js" name="jquery.actual.js" path=$plugins}
  {include_resource type="js" name="jquery.formAjax.js" path=$plugins}
  {include_resource type="js" name="jquery.sprintf.js" path=$plugins}
  {include_resource type="js" name="jquery.Jcrop.min.js" path=$plugins|cat:"jcrop/js/"}
  {* code mirror plugin *}
  {include_resource type="js" name="codemirror.js" path=$plugins|cat:"codemirror/lib/"}
  {include_resource type="js" name="loadmode.js" path=$plugins|cat:"codemirror/addon/mode/"}
  {include_resource type="js" name="show-hint.js" path=$plugins|cat:"codemirror/addon/hint/"}
  {include_resource type="js" name="anyword-hint.js" path=$plugins|cat:"codemirror/addon/hint/"}
  {include_resource type="js" name="matchbrackets.js" path=$plugins|cat:"codemirror/addon/edit/"}
  {include_resource type="js" name="htmlmixed.js" path=$plugins|cat:"codemirror/mode/htmlmixed/"}
  {include_resource type="js" name="htmlembedded.js" path=$plugins|cat:"codemirror/mode/htmlembedded/"}
  {include_resource type="js" name="smarty.js" path=$plugins|cat:"codemirror/mode/smarty/"}
  {include_resource type="js" name="smartymixed.js" path=$plugins|cat:"codemirror/mode/smartymixed/"}
  {include_resource type="js" name="xml.js" path=$plugins|cat:"codemirror/mode/xml/"}
  {include_resource type="js" name="javascript.js" path=$plugins|cat:"codemirror/mode/javascript/"}
  {include_resource type="js" name="css.js" path=$plugins|cat:"codemirror/mode/css/"}
  {include_resource type="js" name="clike.js" path=$plugins|cat:"codemirror/mode/clike/"}
  {include_resource type="js" name="php.js" path=$plugins|cat:"codemirror/mode/php/"}
  
  {include_resource type="js" name="metronic.js"}
  {include_resource type="js" name="layout.js"}
  {include_resource type="js" name="quick-sidebar.js"}
  {include_resource type="js" name="index.js"}
  {include_resource type="js" name="form-components.js"}
  {include_resource type="js" name="login.js"}
  
  {include_resource type="js" name="color-setter.js"}
  {include_resource type="js" name="validator.js"}
  {include_resource type="js" name="multi-sortable.js"}
  {include_resource type="js" name="form-autosave.js"}
  {include_resource type="js" name="shortcutKeys.js" path=$plugins}
  {include_resource type="js" name="tinymce.min.js" path=$plugins|cat:"tinymce/"}
  {include_resource type="js" name="lock.js"}
  {include_resource type="js" name="custom.js"}
  
  {nocache}
  <script>
    location.current_path = '{$ProSYS.SETTINGS.ROOT_ADMIN_URL}';
    {literal}
    var __GLOBALS = {user: {/literal}{$ProSYS.CURRENT_LOGIN->user->otherSettings}{literal}};
    {/literal}
  </script>
  {/nocache}
  
  {nocache}{include_module_resource type="js"}{/nocache}

