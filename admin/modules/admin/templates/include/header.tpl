<!DOCTYPE html>
<html lang="cs">
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="author" content="{$ProSYS.SETTINGS.PROCLIENT_AUTHOR}" />
    <title>{nocache}{if $title == $ProSYS.VARIABLES.SHOWING.WEB_NAME}{$ProSYS.VARIABLES.SHOWING.WEB_NAME}{else}{$title} | {$ProSYS.VARIABLES.SHOWING.WEB_NAME}{/if}{/nocache}</title>
    <link type="image/x-icon" rel="shortcut icon" href="{$ProSYS.SETTINGS.ADMIN_IMAGES}favicon.ico" />
    {include file='styles.tpl'}
    {include file='javascripts.tpl'}
  </head>
  <body class="page-quick-sidebar-over-content page-style-square page-footer-fixed page-sidebar-fixed{nocache}{if $contentOnly} content_only{else}  page-header-fixed{/if}{/nocache}">
    
  {include file="lock.tpl"}
  
  <div class="all_body{nocache}{if $smarty.session|item:'lock_screen'} blur{/if}{/nocache}">
    <div class="page-header navbar navbar-fixed-top"{nocache}{if $contentOnly} style="display: none;"{/if}{/nocache}>
      <div class="page-header-inner">
        <div class="page-logo">
          <a class="brand" href="{$ProSYS.SETTINGS.ROOT_ADMIN_URL}">
            {nocache}{$ProSYS.VARIABLES.SHOWING.WEB_TITLE}{/nocache}
          </a>
          <div class="menu-toggler sidebar-toggler hide"></div>
        </div>
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>

        <div class="top-menu">
          <ul class="nav navbar-nav pull-right">
              {* obsluha notifikačních zpráv *}
              {nocache}
              {get_notification_messages assign="notification_messages"}
              {if $notification_messages}
              <li class="dropdown dropdown-extended dropdown-notification" id="header_task_bar">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
                  {'bell'|icon:1}
                  <span class="badge">{$notification_messages|@count}</span>
                </a>
                <ul class="dropdown-menu extended tasks">
                  <li>
                    <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                      {foreach from=$notification_messages item='message'}
                        <li>
                          <a href="javascript:;">
                            <span class="time">{$message.time|diff_times_to_string}</span>
                            <span class="details">
                              <span class="label label-sm label-icon label-success">
                                {$message.icon}
                              </span>
                              {$message.html}
                            </span>
                          </a>
                        </li>
                      {/foreach}
                    </ul>
                  </li>
                </ul>
              </li>
              {/if}
              {/nocache}

              <li class="dropdown dropdown-user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                  {nocache}<img class="img-circle" alt="obrázek profilu uživatele {$ProSYS.CURRENT_LOGIN->user->getIdentification()}" src="{$ProSYS.CURRENT_LOGIN->user->getCurrentAvatar()->file->getFileURL()}" />{/nocache}
                <span class="username">{nocache}{$ProSYS.CURRENT_LOGIN->user->getFullName()} ({$ProSYS.CURRENT_LOGIN->login}){/nocache}</span>
                {'angle-down'|icon}
                </a>
                <ul class="dropdown-menu dropdown-menu-default">
                  <li><a href="{$ProSYS.SETTINGS.ROOT_ADMIN_URL}?module=user&activity=changeProfile"><i class="icon-user"></i> Upravit profil</a></li>
                  <li class="divider"></li>
                  <li><a class="lock" href="#"><i class="icon-lock"></i> Zamknout</a></li>
                  <li><a href="{$ProSYS.SETTINGS.ROOT_ADMIN_URL}?controller=login&action=logout"><i class="icon-key"></i> Odhlásit</a></li>
                </ul>
              </li>
              <li class="current_time">
                <a href="#">
                  <i class="fa fa-clock-o"></i>
                  <span id="top_current_time">{nocache}{current_time}{/nocache}</span>
                </a>
                <div id="top_current_time_calendar" class="black"></div>
              </li>
            </ul>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="page-container">
      {nocache}{if !$contentOnly}
      <div class="page-sidebar-wrapper">
        <div class="page-sidebar navbar-collapse collapse">
          {include file='menu.tpl'}
        </div>
      </div>
      {/if}{/nocache}
      <div class="page-content-wrapper" {nocache}{if $contentOnly}style="margin-left: 0px; min-height: 150px;"{/if}{/nocache}>
        <div class="page-content{nocache}{if $contentOnly} none_margin{/if}{/nocache}">
          <div id="messages_container"></div>

