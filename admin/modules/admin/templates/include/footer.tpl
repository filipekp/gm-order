          </div>
        </div>
      </div>

      <div class="page-footer" {if $contentOnly} style="display: none;"{/if}>
        <div class="page-footer-inner">
          Webdesign &amp; code {get_copyright withLink=TRUE} <span class="smarty_logo" title="v. {$smarty.version}">used Smarty templates</span>
        </div>
        <div class="footer-tools">
          <span class="scroll-to-top">
            <i class="icon-arrow-up"></i>
          </span>
        </div>
      </div>
    </div>
	</body>
</html>