  {assign var="plugins" value=$ProSYS.SETTINGS.ADMIN_PLUGINS}
  {assign var="css" value=$ProSYS.SETTINGS.ADMIN_CSS}
  
  {include_resource type="css" name="" path="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"}
  {include_resource type="css" name="font-awesome.min.css" path=$plugins|cat:"font-awesome/css/"}
  {include_resource type="css" name="simple-line-icons.min.css" path=$plugins|cat:"simple-line-icons/"}
  {include_resource type="css" name="bootstrap.min.css" path=$plugins|cat:"bootstrap/css/"}
  {include_resource type="css" name="uniform.default.css" path=$plugins|cat:"uniform/css/"}
  
  {include_resource type="css" name="select2.css" path=$plugins|cat:"select2/"}
  {include_resource type="css" name="login.css"}
  
  {include_resource type="css" name="components-rounded.css"}
  {include_resource type="css" name="plugins.css"} 
  {include_resource type="css" name="layout.css"}
  {include_resource type="css" name="default.css" path=$css|cat:"themes/"}
  {include_resource type="css" name="custom.css"}
  
  {include_resource type="css" name="modules.css"}
  
  {nocache}{include_module_resource type="css"}{/nocache}