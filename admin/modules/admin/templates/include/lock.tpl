{* modal okno pro upozornění neaktivity *}
<div class="modal fade" id="idle-timeout-dialog" data-backdrop="static">
  <div class="modal-dialog modal-small">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Byli jste dlouho neaktivní.</h4>
      </div>
      <div class="modal-body">
        <p><i class="fa fa-warning"></i> Vaše sezení bude uzamčeno za <span id="idle-timeout-counter"></span> sekund.</p>
        <p>Chcete pokračovat v sezení?</p>
      </div>
      <div class="modal-footer">
        <button id="idle-timeout-dialog-logout" type="button" class="btn btn-default">Uzamknout hned</button>
        <button id="idle-timeout-dialog-keepalive" type="button" class="btn btn-primary" data-dismiss="modal">Pokračovat v práci</button>
      </div>
    </div>
  </div>
</div>

<div class="page-lock-wrapper"{nocache}{if $smarty.session|item:'lock_screen'} style="display: block;"{/if}{/nocache}>
  <div class="page-lock">
    <div class="page-logo">
      {nocache}{$ProSYS.VARIABLES.SHOWING.WEB_TITLE}{/nocache}
    </div>
    <div class="page-body">
      <div class="lock-head">
         Zamčeno
      </div>
      <div class="lock-body">
        <div class="pull-left lock-avatar-block">
          <img src="{nocache}{$ProSYS.CURRENT_LOGIN->user->getCurrentAvatar()->file->getFileURL()}{/nocache}" class="lock-avatar" alt="{nocache}obrázek profilu uživatele {$ProSYS.CURRENT_LOGIN->user->getIdentification()}{/nocache}">
        </div>
        <form class="lock-form pull-left" action="" method="post">
          <h4>{nocache}{$ProSYS.CURRENT_LOGIN->user->getFullName()} ({$ProSYS.CURRENT_LOGIN->login}){/nocache}</h4>
          <input type="hidden" name="controller" value="login" />
          <input type="hidden" name="action" value="unlockScreen" />
          <input type="hidden" name="login" value="{nocache}{$ProSYS.CURRENT_LOGIN->login}{/nocache}" />
          
          <div class="form-group">
            <input id="lock_password" class="form-control placeholder-no-fix notTogglePassword" type="password" autocomplete="off" placeholder="heslo" name="password" />
          </div>
          <div class="form-actions">
            <button type="submit" class="btn btn-success uppercase">Odemknout</button>
          </div>
        </form>
      </div>
      <div class="lock-bottom">
        Webdesign &amp; code {get_copyright withLink=TRUE}
      </div>
    </div>
  </div>
</div>