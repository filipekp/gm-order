{nocache}{handle_admin_messages}{/nocache}
<div class="row">
  <div class="col-md-12">
    <h3 class="page-title">{$title}</h3>
    {breadcrumb_add title=$title icons=['info-circle']}
    {breadcrumbs_get}
  </div>
</div>
<div class="row phpinfo">
  <div class="col-md-12">
    <style>
      table {
        table-layout: fixed;
        width: 100%;
      }

      td.e {
        width: 25% !important;
        font-weight: bold;
      }

      td.v {
        word-wrap: break-word;
      }

      th {
        text-align: left;
        background-color: #EFEFEF;
      }

      th:first-child {
        width: 25% !important;
      }
    </style>
    {nocache}{$pinfo}{/nocache}
  </div>
</div>