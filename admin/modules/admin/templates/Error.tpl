<h3 class="page-title">{$title|@ucfirst}</h3>
{breadcrumb_add title='Domů' icons=['home'] link=$ProSYS.SETTINGS.ROOT_ADMIN_URL}
{breadcrumb_add title={$title|@ucfirst} icons=['warning']}
{breadcrumbs_get}

<div class="row">
  <div class="col-md-12 page-404">
    <div class="number">
      {nocache}{$code}{/nocache}
    </div>
    <div class="details">
      <h3>{nocache}{$text}{/nocache}</h3>
    </div>
  </div>
</div>
        