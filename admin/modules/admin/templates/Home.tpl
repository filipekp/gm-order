{nocache}{handle_admin_messages}{/nocache}
<h3 class="page-title">{$title}</h3>
{breadcrumb_add title=$title icons=['home']}
{breadcrumbs_get}

  <div class="row" id="sortable_portlets">
    <div class="col-md-4 column sortable">
      <div class="portlet portlet-sortable light bg-inverse">
        <div class="portlet-title">
          <div class="caption">
            {'picture-o'|icon}
            <span class="caption-subject bold uppercase">Informace o přihlášení</span>
          </div>
          <div class="tools"></div>
        </div>
        <div class="portlet-body">
          <h4>Uživatel</h4>
          <p>
            Přihlášený uživatel: <strong>{$ProSYS.CURRENT_LOGIN->user->getFullName()}</strong><br />
            Datum a čas přihlášení: <strong>{$ProSYS.CURRENT_LOGIN->user->lastAccess->format($ProSYS.VARIABLES.SHOWING.DATETIME_FORMAT)}</strong><br />
            <br />
          </p>
          
          <h4>Server</h4>
          <p>
            Vaše IP adresa: <strong>{$smarty.server.REMOTE_ADDR}</strong><br />
            Aktuální čas: <strong id="current_datetime">{nocache}{current_date} - {current_time}{/nocache}</strong>
          </p>
        </div>
      </div>
      
      <!-- empty sortable porlet required for each columns! -->
      <div class="portlet portlet-sortable-empty"></div>
    </div>
    
    <div class="col-md-4 column sortable">
      <!-- empty sortable porlet required for each columns! -->
      <div class="portlet portlet-sortable-empty"></div>
    </div>
    
    <div class="col-md-4 column sortable">
      <!-- empty sortable porlet required for each columns! -->
      <div class="portlet portlet-sortable-empty"></div>
    </div>
  </div>
