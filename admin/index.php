<?php
  /* STARTS SESSION */
  session_start();

  /* SET MB_ENCODING  */
  mb_internal_encoding('UTF-8');

  /* INCLUDES PHP CONSOLE MASTER */
  require_once 'resources/libs/PhpConsole/__autoload.php';
  
  $phpConsoleConnector = PhpConsole\Connector::getInstance();
//  $phpConsoleConnector->setPassword('pro' . date('Y') . 'client');
  $phpConsoleHelper = PhpConsole\Helper::register(); // register global PC class, use: \PC::debug($whateverTheSameAsIn_var_dump);
  $phpConsoleHandler = PC::getHandler();
  $phpConsoleHandler->start();
  
  
  /* AUTOLOAD */
  require_once '__load__.php';

  /* USING NAMESPACE */
  use prosys\core\ProSYSLayer,
      prosys\core\common\Agents,
      prosys\core\common\AppException,
      prosys\core\common\Settings,
      prosys\admin\controller\Controller,
      prosys\core\common\Functions;

  /* @var $mailer prosys\core\common\Mailer */
  $mailer = Agents::getAgent('Mailer', Agents::TYPE_COMMON);
  $emails = explode('|', Settings::getVariable('EMAILS', 'DEVELOPER_MAILS'));
  
  // action login should be the first
  if ((string)filter_input(INPUT_POST, 'controller', FILTER_SANITIZE_STRING) == 'login' &&
      (string)filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'login') {
    /* @var $controller \prosys\admin\controller\LoginController */
    $controller = Agents::getAgent('LoginController', Agents::TYPE_CONTROLLER_ADMIN);
    
    try {
      $controller->login();
    } catch (AppException $exception) {
      $_SESSION[Settings::MESSAGE_EXCEPTION] = $exception->getMessage();
    }
  }
  
  // check if the user is logged in
  /* @var $loginDao \prosys\model\LoginDao */
  $loginDao = Agents::getAgent('LoginDao', Agents::TYPE_MODEL);
  if (array_key_exists('active_login_id', $_SESSION)) {
    ProSYSLayer::ProSYS()->storeLogin($loginDao->load($_SESSION['active_login_id']));
  } else {
    ProSYSLayer::ProSYS()->storeLogin($loginDao->load());
  }
  
  if (ProSYSLayer::ProSYS()->login()->isNew()) {
    $controller = Agents::getAgent('AdminController', Agents::TYPE_CONTROLLER_ADMIN);
    $controller->response('login');
    $_SESSION[Settings::LAST_URI] = filter_input(INPUT_SERVER, 'QUERY_STRING');
  } else {
    /* @var $securityEngine \prosys\admin\security\SecurityEngine */
    $securityEngine = Agents::getAgent('SecurityEngine', Agents::TYPE_SECURITY);
    
    /* CONTROLS "ACTIVE" REQUESTS (FORMS, ...) */
    $controllerPost = filter_input(INPUT_POST, 'controller');
    $controllerGet = filter_input(INPUT_GET, 'controller');

    if ($controllerPost || $controllerGet) {
      $repository = (($controllerPost) ? INPUT_POST : INPUT_GET);
      $controller = (($controllerPost) ? $controllerPost : $controllerGet);
      
      $action = filter_input($repository, 'action');
      
      if ($action) {
        $actionName = '';
        if ($securityEngine->authorizeAction($controller, $action, filter_input_array($repository), $actionName)) {
          $controllerName = $controller;
          $controller = Agents::getAgent(ucfirst($controller . 'Controller'), Agents::TYPE_CONTROLLER_ADMIN);

          try {
            $controller->$action();
          } catch (AppException $exception) {
            $exMsg = $exception->getMessage();
            $_SESSION[Settings::MESSAGE_EXCEPTION] = $exMsg;
      
            // zasílání informací o chybách
            if ($exMsg && Settings::getVariable('EMAILS', 'INFO_ABOUT_APPEXCEPTION')) {
              $mailer->sendMail(
                'Chyba na serveru: ' . Settings::getVariable('SHOWING', 'WEB_NAME'),
                'Na serveru ' . Settings::getVariable('SHOWING', 'WEB_NAME') . ' nastala následující vyjímka: ' . $exception,
                [Settings::getVariable('EMAILS', 'MAIL_SENDER') => Settings::getVariable('SHOWING', 'WEB_NAME')],
                $emails
              );
            }
    
            if (!filter_input(INPUT_GET, 'module', FILTER_SANITIZE_STRING)) {
              $query = filter_input_array($repository);
              Functions::unsetItem($query, 'action');
              Functions::unsetItem($query, 'controller');
              
              Controller::redirect(
                $controllerName,
                filter_input(INPUT_GET, 'activity', FILTER_SANITIZE_STRING, array('options' => array('default' => 'initial'))),
                http_build_query($query)
              );
            }
          }
        } else {
          $_SESSION[Settings::MESSAGE_EXCEPTION] = 'Nemáte práva k provedení akce <b>"' . $actionName . '"<b/>';
          
          // reload to page 405 - access denied
          Controller::redirect('admin', 'e405', '&requested[module]=' . $controller . '&requested[action]=' . $action);
        }
      }
    }

    /* SHOWS ADMIN PAGE */
    $activity = ProSYSLayer::ProSYS()->activity();
    $module = ProSYSLayer::ProSYS()->module();

    // presmeruje na vychozi stranku
    if (Functions::item($_SESSION, Settings::JUST_LOGGED_IN) && 
        is_null(filter_input(INPUT_GET, 'module')) && 
        is_null(filter_input(INPUT_GET, 'activity')) && 
        ($defaultPage = Functions::item(ProSYSLayer::ProSYS()->loggedUser()->otherSettings, 'default_page'))) {
      /* @var $moduleActionDao \prosys\model\ModuleActionDao */
      $moduleActionDao = Agents::getAgent('ModuleActionDao', Agents::TYPE_MODEL);
      $activity = $moduleActionDao->load((int)$defaultPage); /* @var $activity \prosys\model\ModuleActionEntity */
      
      if (!$activity->isNew()) {
        Controller::redirect($activity->module->module, $activity->name);
      }
    }
    
    Functions::unsetItem($_SESSION, Settings::JUST_LOGGED_IN);
    
    // zobrazi nactenou stranku
    $pageName = '';
    if ($securityEngine->authorizeActivity($module, $activity, $_GET, $pageName)) {
      try {
        $controller = Agents::getAgent(ucfirst($module) . 'Controller', Agents::TYPE_CONTROLLER_ADMIN);        
        $controller->response($activity);
        
        // ukladani entit do historie
        /* @var $historyDao \prosys\model\HistoryDao */
        $historyDao = Agents::getAgent('HistoryDao', Agents::TYPE_MODEL);
        $historyDao->storeAllChanges();
      } catch (AppException $exception) {
        $exMsg = $exception->getMessage();
        $_SESSION[Settings::MESSAGE_EXCEPTION] = $exMsg;
        
        // zasílání informací o chybách
        if ($exMsg && Settings::getVariable('EMAILS', 'INFO_ABOUT_APPEXCEPTION')) {
          $mailer->sendMail(
            'Chyba na serveru: ' . Settings::getVariable('SHOWING', 'WEB_NAME'),
            'Na serveru ' . Settings::getVariable('SHOWING', 'WEB_NAME') . ' nastala následující vyjímka: ' . $exception,
            [Settings::getVariable('EMAILS', 'MAIL_SENDER') => Settings::getVariable('SHOWING', 'WEB_NAME')],
            $emails
          );
        }
      }
      
    } else {
      $_SESSION[Settings::MESSAGE_EXCEPTION] = 'Nemáte práva k zobrazení stránky: <b>' . $pageName . '</b> modulu: ' . $module;
      // reload to page 401 - access denied
      Controller::redirect('admin', 'e401',  '&requested[module]=' . $module . '&requested[action]=' . $activity);
    }
  }
