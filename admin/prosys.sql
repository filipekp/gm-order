-- --------------------------------------------------------
-- Hostitel:                     mysql.prohosting.cz
-- Verze serveru:                5.5.40-0+wheezy1 - (Debian)
-- OS serveru:                   debian-linux-gnu
-- HeidiSQL Verze:               9.1.0.4882
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportování struktury pro tabulka db21675_dysporta.avatars
CREATE TABLE IF NOT EXISTS `avatars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_czech_ci NOT NULL COMMENT 'nazev avatara',
  `file_name` varchar(64) COLLATE utf8_czech_ci NOT NULL COMMENT 'nazev hlavniho obrazku avatara',
  `user_type` int(11) DEFAULT NULL COMMENT 'NULL = vsichni, -1 = backend_users, -2 = frontend_users, #NO > 0 = user_id (kvuli filtrovani pro studenty, aby se jim nenabizeli i avatari z admina)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='tabulka s hlavnimi avatary';

-- Exportování dat pro tabulku db21675_dysporta.avatars: ~3 rows (přibližně)
/*!40000 ALTER TABLE `avatars` DISABLE KEYS */;
INSERT INTO `avatars` (`id`, `name`, `file_name`, `user_type`) VALUES
	(1, 'Výchozí', 'main.png', -1);
/*!40000 ALTER TABLE `avatars` ENABLE KEYS */;


-- Exportování struktury pro tabulka db21675_dysporta.groups
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `is_admin` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- Exportování dat pro tabulku db21675_dysporta.groups: ~4 rows (přibližně)
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` (`id`, `name`, `is_admin`) VALUES
	(1, 'Administrators', 1);
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;


-- Exportování struktury pro tabulka db21675_dysporta.menu_items
CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `title` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `type` enum('link','module_action','section') COLLATE utf8_czech_ci NOT NULL,
  `type_value` text COLLATE utf8_czech_ci NOT NULL,
  `icons` text COLLATE utf8_czech_ci COMMENT 'nazvy ikon podle http://fortawesome.github.io/Font-Awesome/icons/. Např: pro ikonu fa-home napište pouze home do json pole tzn: ["home"]',
  `parent` int(11) DEFAULT NULL,
  `sequence` int(11) NOT NULL DEFAULT '1',
  `display_always` varchar(45) COLLATE utf8_czech_ci DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `parent_fk` FOREIGN KEY (`parent`) REFERENCES `menu_items` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- Exportování dat pro tabulku db21675_dysporta.menu_items: ~30 rows (přibližně)
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` (`id`, `name`, `title`, `type`, `type_value`, `icons`, `parent`, `sequence`, `display_always`) VALUES
	(1, 'Domů', '', 'link', '{"href":""}', '["home"]', NULL, 1, '1'),
  (2, 'Konfigurace', 'Konfigurace', 'section', '', '["cogs"]', NULL, 30, '0'),
	(3, 'PHP Info', 'informace o serveru', 'module_action', '10', '["info-sign"]', 2, 20, '0'),
  (4, 'Testovací stránka - vývoj', 'Testovací stránka - vývoj', 'module_action', '48', '["html5"]', NULL, 100, '0'),
  (5, 'Globální proměnné', 'Globální proměnné', 'section', '', '["cogs"]', 2, 1, '0'),
	(6, 'Přehled proměnných', 'Přehled proměnných', 'module_action', '49', '["cogs"]', 5, 1, '0'),
	(7, 'Přidat proměnnou', 'Přidat proměnnou', 'module_action', '51', '["cogs","plus aditional"]', 5, 5, '0'),
	(8, 'Správa uživatelů', 'správa uživatelů', 'section', '', '["group"]', NULL, 10, '0'),
	(9, 'Uživatelé', 'Uživatelé', 'section', '', '["user"]', 8, 1, '0'),
  (10, 'Přehled uživatelů', 'Přehled uživatelů', 'module_action', '2', '["user"]', 9, 1, '0'),
  (11, 'Přidat uživatele', 'Přidat uživatele', 'module_action', '3', '["user","plus aditional"]', 10, 2, '-1'),
	(12, 'Skupiny uživatelů', 'Skupiny uživatelů', 'section', '', '["group"]', 8, 2, '0'),
  (13, 'Přehled skupin', 'Přehled skupin', 'module_action', '6', '["group"]', 12, 2, '0'),
	(14, 'Přidat skupinu', 'Přidat skupinu', 'module_action', '7', '["group","plus aditional"]', 13, 2, '-1');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;


-- Exportování struktury pro tabulka db21675_dysporta.modules
CREATE TABLE IF NOT EXISTS `modules` (
  `module` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `name` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- Exportování dat pro tabulku db21675_dysporta.modules: ~10 rows (přibližně)
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` (`module`, `name`) VALUES
	('admin', 'Hlavní administrace'),
	('group', 'Skupiny'),
	('setting', 'Globální proměnné'),
	('studentResult', 'Statistiky'),
	('studentTest', 'Studentské testy'),
	('test', 'Správa testů'),
	('topic', 'Správa témat'),
	('user', 'Správa uživatelů'),
	('userGroup', 'Správa skupin studentů');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;


-- Exportování struktury pro tabulka db21675_dysporta.module_actions
CREATE TABLE IF NOT EXISTS `module_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `action_name` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `action_type` int(11) NOT NULL,
  `action_title` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `parent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `module_2` (`module`,`action_name`,`action_type`),
  KEY `module` (`module`),
  KEY `parent` (`parent`),
  CONSTRAINT `module_actions_ibfk_1` FOREIGN KEY (`module`) REFERENCES `modules` (`module`),
  CONSTRAINT `module_actions_ibfk_2` FOREIGN KEY (`parent`) REFERENCES `module_actions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- Exportování dat pro tabulku db21675_dysporta.module_actions: ~41 rows (přibližně)
/*!40000 ALTER TABLE `module_actions` DISABLE KEYS */;
INSERT INTO `module_actions` (`id`, `module`, `action_name`, `action_type`, `action_title`, `parent`) VALUES
	(2, 'user', 'initial', 1, 'přehled uživatelů', NULL),
	(4, 'user', 'delete', 2, 'smazání uživatele', NULL),
	(5, 'user', 'save', 2, 'uložení uživatele', NULL),
  (3, 'user', 'manage', 1, 'správa uživatele', 5),
  (11, 'user', 'change_group', 3, 'změna skupiny uživatele', 5),
	(6, 'group', 'initial', 1, 'přehled skupin', NULL),
	(7, 'group', 'manage', 1, 'správa skupiny', 8),
	(8, 'group', 'save', 2, 'uložení skupiny', NULL),
	(9, 'group', 'delete', 2, 'smazaní skupiny', NULL),
	(10, 'admin', 'phpinfo', 1, 'informace o serveru', NULL),
	(12, 'admin', 'development', 1, 'testovací stránka určená pro vývoj', NULL),
	(13, 'setting', 'initial', 1, 'přehled proměnných', NULL),
	(14, 'setting', 'save', 2, 'uložení proměnné', 13),
	(15, 'setting', 'manage', 1, 'úprava proměnné', 14),
	(16, 'setting', 'delete', 2, 'smazání proměnné', 13);
/*!40000 ALTER TABLE `module_actions` ENABLE KEYS */;

-- Exportování struktury pro tabulka db21675_dysporta.group_rights
CREATE TABLE IF NOT EXISTS `group_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_group` int(11) NOT NULL,
  `module_action` int(11) NOT NULL,
  `allowed_queries` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `is_uncheckable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_group` (`id_group`,`module_action`),
  KEY `module_action` (`module_action`),
  CONSTRAINT `group_rights_ibfk_1` FOREIGN KEY (`id_group`) REFERENCES `groups` (`id`),
  CONSTRAINT `group_rights_ibfk_2` FOREIGN KEY (`module_action`) REFERENCES `module_actions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- Exportování dat pro tabulku db21675_dysporta.group_rights: ~63 rows (přibližně)
/*!40000 ALTER TABLE `group_rights` DISABLE KEYS */;
INSERT INTO `group_rights` (`id`, `id_group`, `module_action`, `allowed_queries`, `is_uncheckable`) VALUES
	(1, 1, 2, '', 0),
	(2, 1, 3, '', 0),
	(3, 1, 4, '', 0),
	(4, 1, 5, '', 0),
	(5, 1, 7, '', 0),
	(6, 1, 8, '', 0),
	(7, 1, 9, '', 0);
/*!40000 ALTER TABLE `group_rights` ENABLE KEYS */;

-- Exportování struktury pro tabulka db21675_dysporta.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_tag` varchar(50) NOT NULL DEFAULT '0' COMMENT 'nazev skupiny parametru',
  `name` varchar(50) NOT NULL DEFAULT '0' COMMENT 'nazev parametru',
  `value_type` varchar(20) NOT NULL DEFAULT 'text' COMMENT 'urcuje typ hodnoty',
  `value` text COMMENT 'hodnota parametru',
  `can_be_erased` int(1) NOT NULL DEFAULT '1' COMMENT 'urcuje zda muze byt promenna smazana ci nikoli',
  `help` text COMMENT 'popisek promenne',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_name` (`group_tag`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tabulka pro ukladani obecnych nastaveni pro ruzne moduly, server, apod ...';

-- Exportování dat pro tabulku db21675_dysporta.settings: ~0 rows (přibližně)
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;


-- Exportování struktury pro tabulka db21675_dysporta.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(32) COLLATE utf8_czech_ci DEFAULT '',
  `user_password` varchar(40) COLLATE utf8_czech_ci DEFAULT '',
  `email` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `first_name` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `last_name` varchar(64) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(32) COLLATE utf8_czech_ci DEFAULT NULL,
  `type` varchar(15) COLLATE utf8_czech_ci DEFAULT NULL,
  `sex` int(11) NOT NULL DEFAULT '1',
  `salutation` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL COMMENT 'oslovení',
  `theme` varchar(45) COLLATE utf8_czech_ci NOT NULL DEFAULT 'default',
  `avatar` int(11) NOT NULL DEFAULT '1',
  `count_item_per_page` int(11) DEFAULT NULL,
  `last_access` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- Exportování dat pro tabulku db21675_dysporta.users: ~322 rows (přibližně)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `login`, `user_password`, `email`, `first_name`, `last_name`, `phone`, `type`, `sex`, `salutation`, `theme`, `avatar`, `count_item_per_page`, `last_access`, `deleted`) VALUES
	(1, 'admin', '4f405b8ed552d83fa014e50eac577df611b1e006', 'info@proclient.cz', 'ProSYS', 'Administrator', '(+420) 775 780 484', 'admin', 1, NULL, 'default', 1, 34, '2014-12-18 12:23:25', 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
